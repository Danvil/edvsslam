#include "MapImage.hpp"
#include "impl/lodepng.h"
#include <stdexcept>
#include <iostream>
#include <fstream>

namespace Edvs
{

void SaveImageFloat(const std::string& fn, const Eigen::MatrixXf& map)
{
	std::ofstream ofs(fn, std::ios::binary);
	uint32_t w = map.rows();
	uint32_t h = map.cols();
	ofs.write(reinterpret_cast<const char*>(&w), sizeof(w));
	ofs.write(reinterpret_cast<const char*>(&h), sizeof(h));
	float v;
	for(int y=0; y<h; y++) {
		for(int x=0; x<w; x++) {
			v = map(x,y);
			ofs.write(reinterpret_cast<const char*>(&v), sizeof(v));
		}
	}
}

Eigen::MatrixXf LoadImageFloat(const std::string& fn)
{
	std::ifstream ifs(fn, std::ios::binary);
	uint32_t w, h;
	ifs.read(reinterpret_cast<char*>(&w), sizeof(w));
	ifs.read(reinterpret_cast<char*>(&h), sizeof(h));
	Eigen::MatrixXf map(w, h);
	float v;
	for(int y=0; y<h; y++) {
		for(int x=0; x<w; x++) {
			ifs.read(reinterpret_cast<char*>(&v), sizeof(v));
			map(x,y) = v;
		}	
	}
	return map;
}

void SaveImageUchar4(const std::string& fn, const Eigen::MatrixXf& map)
{
	std::vector<unsigned char> image(4*map.cols()*map.rows());

	const float max = 1.0;
	const float min = 0.0;
	const float span = 1.0;

	const unsigned int cols = map.cols();
	const unsigned int rows = map.rows();
	auto it = image.begin();
	for(unsigned int y=0; y<cols; y++) {
		for(unsigned int x=0; x<rows; x++) {
			const float v = map(x,y);
			//unsigned char g = std::max(0, std::min(255, static_cast<int>(255.0f * v)));
			const unsigned char g = std::max(0, std::min(255, static_cast<int>(255.0f * ((v + min) / span))));
			*(it++) = g;
			*(it++) = g;
			*(it++) = g;
			*(it++) = 255;
		}
	}
	unsigned error = lodepng::encode(fn, image, map.rows(), map.cols());
	if(error) {
		std::cout << "encode error " << error << ": " << lodepng_error_text(error) << std::endl;
		throw std::runtime_error("Could not save image");
	}
}

Eigen::MatrixXf LoadImageUchar4(const std::string& fn)
{
	// load image
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;
	//decode
	unsigned error = lodepng::decode(image, width, height, fn);
	//if there's an error, display it
	if(error) {
		std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
		throw std::runtime_error("Could not load image");
	}
	auto it = image.begin();
	Eigen::MatrixXf img(width, height);
	for(unsigned int y=0; y<height; y++) {
		for(unsigned int x=0; x<width; x++) {
			img(x, y) = static_cast<float>(*it) / 255.0f;
			it += 4;
		}
	}	
	return img;
}

}