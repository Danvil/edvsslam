/*
 * Ray.hpp
 *
 *  Created on: Jun 28, 2012
 *      Author: david
 */

#ifndef EDVS_TOOLS_RAY_HPP_
#define EDVS_TOOLS_RAY_HPP_

#include <Eigen/Dense>

namespace Edvs
{
	template<typename K>
	struct Ray
	{
		typedef Eigen::Matrix<K,3,1> Vec3;
		Vec3 a;
		Vec3 u;
	};

}

#endif
