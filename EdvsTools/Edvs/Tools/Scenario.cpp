/*
 * Scenario.cpp
 *
 *  Created on: Jul 17, 2012
 *      Author: david
 */

#include "Scenario.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>

namespace Edvs {
namespace Scenario {

std::ostream& operator<<(std::ostream& os, const Calibration& c)
{
	std::cout << "Calibration = { Frame = " << std::endl
			<< c.frame << std::endl
			<< "LensDelta = (" << c.lens_dx << "," << c.lens_dy << ")}";
	return os;
}

Calibration LoadCalibration(const std::string& fn)
{
	Eigen::Matrix<float,3,4> frame;
	double lens_dx, lens_dy;
	std::ifstream ifs_frame(fn);
	ifs_frame >> frame(0,0) >> frame(0,1) >> frame(0,2) >> frame(0,3);
	ifs_frame >> frame(1,0) >> frame(1,1) >> frame(1,2) >> frame(1,3);
	ifs_frame >> frame(2,0) >> frame(2,1) >> frame(2,2) >> frame(2,3);
	ifs_frame >> lens_dx >> lens_dy;
	return Calibration{frame, lens_dx, lens_dy};
}

unsigned int TransformPath(const std::string& fn_input, const std::string& fn_output, const Calibration& calibration, double time_offset)
{
	// load, convert and save
	std::ifstream ifs(fn_input);
	std::ofstream ofs(fn_output);
	unsigned int n = 0;
	while(!ifs.eof()) {
		double time_mus;
		ifs >> time_mus;
		Eigen::Matrix4f mat = Eigen::Matrix4f::Identity();
		for(unsigned int i=0; i<3; i++) { // do not read last row
			for(unsigned int j=0; j<4; j++) {
				ifs >> mat(i,j);
			}
		}
		// scale position from millimeters to meters
		mat(0,3) *= 0.001f;
		mat(1,3) *= 0.001f;
		mat(2,3) *= 0.001f;
		// transform point
		if(!ifs.eof()) {
			Eigen::Vector3f c = calibration(mat);
			double time = time_mus/1000000.0 + time_offset;
//			std::cout << time << " || " << c.transpose() << std::endl;
			ofs << time << "\t" << c.x() << "\t" << c.y() << "\t0" << std::endl;
			n++;
		}
	}
	return n;
}

void ScenarioSettings::load(const std::string& fn_config)
{
	boost::property_tree::ptree pt;
	boost::property_tree::read_xml(fn_config, pt);
	time_offset_s = pt.get<double>("scenario.time_offset_s");
	fn_map = pt.get<std::string>("scenario.map");
	fn_calib = pt.get<std::string>("scenario.calibration");
	fn_retina = pt.get<std::string>("scenario.retina");
	std::string start = pt.get<std::string>("scenario.start", "");
	if(start.empty()) {
		start_x = 0;
		start_y = 0;
		start_rot = 0;
	}
	else {
		std::vector<std::string> start_tokens;
		boost::split(start_tokens, start, boost::is_any_of(" ,\t"));
		start_x = boost::lexical_cast<double>(start_tokens[0]);
		start_y = boost::lexical_cast<double>(start_tokens[1]);
		start_rot = boost::lexical_cast<double>(start_tokens[2]);
	}
}

}}
