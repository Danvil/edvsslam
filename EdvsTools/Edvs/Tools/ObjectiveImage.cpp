/*
 * ObjectiveImage.cpp
 *
 *  Created on: Jun 28, 2012
 *      Author: david
 */

#include "ObjectiveImage.hpp"
#include "ScoreToColor.hpp"
#include "impl/lodepng.h"
#include <boost/threadpool.hpp>

namespace Edvs
{

void SaveImage(const std::string& filename, const std::vector<uint32_t>& image, unsigned int width, unsigned int height)
{
	unsigned error = lodepng::encode(filename, reinterpret_cast<const unsigned char*>(image.data()), width, height);
	//if there's an error, display it
	if(error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
}

template<typename Functor>
void CreateAndSave(const std::string& filename, const Roi& roi, Functor f)
{
	unsigned int num_u = roi.getNumU();
	unsigned int num_v = roi.getNumV();
	// precompute values for u
	std::vector<float> val_u(num_u);
	for(unsigned int j=0; j<num_u; j++) {
		val_u[j] = roi.getU(j);
	}
	// fill image
	boost::threadpool::pool pool(12);
	std::vector<uint32_t> image(num_u*num_v);
	for(unsigned int i=0; i<num_v; i++) {
		float val_v = roi.getV(i);
		auto it_image = image.begin() + i * num_u;
		auto itu_begin = val_u.begin();
		auto itu_end = val_u.end();
		pool.schedule([it_image,f,itu_begin,itu_end,val_v]() {
			auto iti = it_image;
			for(auto itu=itu_begin; itu!=itu_end; ++itu, ++iti) {
				*iti = Score2Color(f(*itu, val_v));
			}
		});
	}
	pool.wait();
	SaveImage(filename, image, num_u, num_v);
}

void CreateAndSaveXY(const std::string& filename, const std::vector<Event>& events, const ObjectiveFunctionF& fnc, const Roi& roi, float z, float rot)
{
	float rot_sin = std::sin(rot);
	float rot_cos = std::cos(rot);
	CreateAndSave(filename, roi, [&fnc,z,rot_sin,rot_cos,&events](float u, float v) {
		return fnc.meanOfScores(Eigen::Vector3f{u, v, z}, rot_sin, rot_cos, events);
	});
}

}
