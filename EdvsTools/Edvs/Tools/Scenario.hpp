/*
 * Scenario.hpp
 *
 *  Created on: Jul 17, 2012
 *      Author: david
 */

#ifndef EDVS_SCENARIO_HPP_
#define EDVS_SCENARIO_HPP_

#include <Eigen/Dense>
#include <ostream>
#include <string>

namespace Edvs
{
	namespace Scenario
	{
		struct Calibration
		{
			Eigen::Matrix<float,3,4> frame;

			double lens_dx, lens_dy;

			Eigen::Vector3f operator()(const Eigen::Matrix4f& mat) const {
				return frame * mat * Eigen::Vector4f(lens_dx, lens_dy, 0, 1);
			}
		};

		std::ostream& operator<<(std::ostream& os, const Calibration& c);

		Calibration LoadCalibration(const std::string& fn);

		unsigned int TransformPath(const std::string& fn_input, const std::string& fn_output, const Calibration& calibration, double time_offset);

		struct ScenarioSettings
		{
			double time_offset_s;
			std::string fn_calib;
			std::string fn_map;
			std::string fn_retina;
			double start_x, start_y, start_rot;

			void load(const std::string& filename);
		};

	}
}

#endif
