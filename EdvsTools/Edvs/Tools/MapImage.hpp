#ifndef MAPIMAGE_HPP_
#define MAPIMAGE_HPP_

#include <Eigen/Dense>
#include <string>

namespace Edvs {

void SaveImageFloat(const std::string& fn, const Eigen::MatrixXf& map);

Eigen::MatrixXf LoadImageFloat(const std::string& fn);

void SaveImageUchar4(const std::string& fn, const Eigen::MatrixXf& map);

Eigen::MatrixXf LoadImageUchar4(const std::string& fn);

}
#endif
