/*
 * MapSettings.cpp
 *
 *  Created on: Jul 10, 2012
 *      Author: david
 */

#include "MapSettings.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

void Edvs::MapSettings::load(const std::string& fn_config)
{
	boost::property_tree::ptree pt;
	boost::property_tree::read_xml(fn_config, pt);
	fn_raw = pt.get<std::string>("map.raw");
	fn_dist = pt.get<std::string>("map.distmap");
	height = pt.get<double>("map.height");
	pixel_size = pt.get<double>("map.pixel_size");
}

void Edvs::MapSettings::save(const std::string& fn_config)
{
	boost::property_tree::ptree pt;
	pt.put("map.raw", fn_raw);
	pt.put("map.distmap", fn_dist);
	pt.put("map.height", height);
	pt.put("map.pixel_size", pixel_size);
	boost::property_tree::write_xml(fn_config, pt);
}
