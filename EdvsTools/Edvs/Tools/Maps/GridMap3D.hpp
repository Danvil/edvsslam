#ifndef INCLUDED_EDVS_TOOLS_MAP_GRIDMAP3D_HPP
#define INCLUDED_EDVS_TOOLS_MAP_GRIDMAP3D_HPP

#include "Map.hpp"
#include <octomap/octomap.h>
#include <octomap/OcTree.h>

using namespace octomap;

namespace Edvs
{

struct GridMap3D
:public Map
{
public:
	GridMap3D();
	
	std::string getInfoString() const;
	
	float evaluate(const PixelViewCone& pvc);

	void store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event);

	void resample(const ZAlignedPose<double>& state) {}

	void reduce(const Eigen::Vector3f& pos, float radius);
public:
	OcTree tree_;


};
	
}

#endif
