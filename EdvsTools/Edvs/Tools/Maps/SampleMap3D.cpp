#include "SampleMap3D.hpp"
#include <boost/format.hpp>
#include <algorithm>
#include <cmath>
#include <iostream>

namespace Edvs {

SampleMap3D::SampleMap3D()
{

}

SampleMap3D::~SampleMap3D()
{

}

std::string SampleMap3D::getInfoString() const
{
	return (boost::format("3D Sample Map (#=%d)") % getSampleStore().numSamples()).str();
}

float SampleMap3D::evaluate(const PixelViewCone& pvc)
{
	std::vector<sample_t> samples = createViewConeSamples(pvc);
	float score_total = 0.0f;
	for(const sample_t& x : samples) {
		std::vector<sample_t> candidates = samples_.getInRange(x);
		float score = 0.0f;
		for(const sample_t& c : candidates) {
			score += Overlap(c, x) * c.weight;
		}
		score_total += score * x.weight;
	}
	return std::min(1.0f, 0.1f * score_total);
}

void SampleMap3D::store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event)
{
	for(const PixelViewCone& pvc : v_pvc) {
		std::vector<sample_t> samples = createViewConeSamples(pvc);
		samples_.add(samples);
	}
}

std::vector<SampleMap3D::sample_t> SampleMap3D::createViewConeSamples(const PixelViewCone& pvc) const
{
	constexpr float RANGE_MIN = 0.5f;
	constexpr float RANGE_MAX = 3.0f;
	const float tan_alpha = std::tan(0.5f*pvc.opening_angle);
	assert(tan_alpha < 1.0f);
	std::vector<sample_t> samples;
	samples.reserve(8);
	float d = RANGE_MIN;
	while(d < RANGE_MAX) {
		// compute radius of next sample
		float r = d * tan_alpha / (1.0f - tan_alpha);
		// add sample
		samples.push_back(sample_t{
			pvc.ray_start + (d+r)*pvc.ray_dir, r, pvc.weight
		});
		// update d
		d += 2.0f*r;
	}
	return samples;
}

}
