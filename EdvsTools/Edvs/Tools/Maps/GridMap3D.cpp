
#include "GridMap3D.hpp"
#include <boost/format.hpp>

namespace Edvs {

GridMap3D::GridMap3D()
: tree_(0.05) //0.01
{}
	
std::string GridMap3D::getInfoString() const
{
	return ("3D Gridmap Info, not implemented");
}

float GridMap3D::evaluate(const PixelViewCone& pvc)
{
	typedef Eigen::Vector3f vec3_t;

	float length = 3.0f; //max ray length
	vec3_t pvc_endPoint = pvc.ray_start + length * pvc.ray_dir;

	point3d startPoint(pvc.ray_start[0], pvc.ray_start[1], pvc.ray_start[2]);
	point3d endPoint(pvc_endPoint[0], pvc_endPoint[1], pvc_endPoint[2]);
	std::vector<point3d> results;
	tree_.computeRay(startPoint, endPoint, results);
	float maxVal = 0.0;
	for(unsigned int i=0; i< results.size(); i++)
		{
			OcTreeNode* resultingNode = tree_.search(results[i]);
			if(resultingNode)
			{
			if(resultingNode->getOccupancy() > maxVal)
				{maxVal = resultingNode->getOccupancy();}
			}
		}
	return maxVal;
}

void GridMap3D::store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event)
{
	typedef Eigen::Vector3f vec3_t;
	float minLength = 2.4f; //2.2 //max ray length
	float maxLength = 2.7f; //3.5 //max ray length
	for(unsigned int v=0; v<v_pvc.size(); v++)
	{
		//point3d startPoint(v_pvc[v].ray_start[0], v_pvc[v].ray_start[1], v_pvc[v].ray_start[2]);
		//tree_.updateNode(startPoint, true);

		vec3_t pvc_clippedEndPoint = v_pvc[v].ray_start + maxLength * v_pvc[v].ray_dir;
		vec3_t pvc_clippedStartPoint = v_pvc[v].ray_start + minLength * v_pvc[v].ray_dir;

		point3d startPoint(pvc_clippedStartPoint[0], pvc_clippedStartPoint[1], pvc_clippedStartPoint[2]);
		point3d endPoint(pvc_clippedEndPoint[0], pvc_clippedEndPoint[1], pvc_clippedEndPoint[2]);
		std::vector<point3d> results;

		tree_.computeRay(startPoint, endPoint, results);

		for(unsigned int i=0; i< results.size(); i++)
		{
			//std::cout<<" x "<<results[i].x()<<" y "<<results[i].y()<<" z "<<results[i].z()<<std::endl;
			OcTreeNode* resultingNode = tree_.search(results[i]);
			if(resultingNode)
			{
				tree_.updateNode(results[i], float(resultingNode->getOccupancy()+0.1));
			}
			else
			{
				tree_.updateNode(results[i], 0.1f);	
			}
		}
		//draw trajectory into 3D map
		point3d trajectoryPoint(v_pvc[v].ray_start[0], v_pvc[v].ray_start[1], v_pvc[v].ray_start[2]);
		tree_.updateNode(trajectoryPoint, float(1.0));
	}
}

void GridMap3D::reduce(const Eigen::Vector3f& pos, float radius)
{
}

}