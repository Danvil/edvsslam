/*
 * GridMap.cpp
 *
 *  Created on: Mar 9, 2012
 *      Author: user
 */

#include "GridMap.hpp"
#include "../MapSettings.hpp"
#include "../MapImage.hpp"
//#include <Edvs/Tracking/Tools/Random.hpp>
#include <QtGui/QPainter>
#include <QtGui/QImage>
#include <boost/filesystem.hpp>
#include <boost/random.hpp>
#include <boost/format.hpp>
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <fstream>

namespace Edvs {

constexpr bool USE_KERNEL = true;
constexpr float PIXEL_SIGMA = 0.5f;
constexpr float JITTER_COMPENSATION = 0.15f;
constexpr unsigned int LOWRES_MAP_RATIO = 2;
constexpr float RETINA_SIZE_FACTOR = 1.00f;
constexpr float MOTION_CONSTANT = 100.0f;

std::string GridMap::getInfoString() const
{
	return (boost::format("2D GridMap %dx%d") % getMapProbability().cols() % getMapProbability().rows()).str();
}

void GridMap::createKernel()
{
	if(!sensor_model_) {
		return;
	}
	// compute kernel size on map h*2*tan(fov/128/2) / map_pixel_size
	const float px_focal_length = sensor_model_->getPixelFocalLength();
	const float MAX_PIXEL_SIZE = map_height_ / px_focal_length / pixel_size_;
	const float KERNEL_SIGMA = 0.5f * MAX_PIXEL_SIZE / PIXEL_SIGMA;
	const float kernel_scale = 1.0f / (2.0f * 3.1415f * KERNEL_SIGMA * KERNEL_SIGMA);
	// create kernel
	const int KERNEL_R = static_cast<int>(std::ceil(2.0f * KERNEL_SIGMA));
	event_map_kernel_ = Eigen::MatrixXf::Zero(2*KERNEL_R+1, 2*KERNEL_R+1);
	float total = 0.0f;
	for(int y=-KERNEL_R; y<=KERNEL_R; y++) {
		for(int x=-KERNEL_R; x<=KERNEL_R; x++) {
			const float maha = static_cast<float>(x*x + y*y) / (KERNEL_SIGMA * KERNEL_SIGMA);
			float w = kernel_scale * std::exp(-0.5f*maha);
			event_map_kernel_(x+KERNEL_R, y+KERNEL_R) = w;
			total += w;
		}
	}
	// std::cout << "kernel: "
	// 		<< "ps=" << MAX_PIXEL_SIZE << ", "
	// 		<< "r=" << KERNEL_R << ", "
	// 		<< "dim=" << event_map_kernel_.rows() << "x" << event_map_kernel_.cols() << ", "
	// 		<< "total=" << total << std::endl;
	event_map_kernel_ /= total;
}

inline float visibility_protected_div(float num_occ, float num_vis) {
	return (num_vis == 0.0f) ? 0.0f : (num_occ / num_vis);
}

void GridMap::store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event)
{
	const unsigned int h = map_probability_.cols();
	const unsigned int w = map_probability_.rows();

	// compute occurrence map update
	{
		const unsigned int KERNEL_R = (event_map_kernel_.rows() - 1)/2;
		// write events into map
		for(const PixelViewCone& pvc : v_pvc) {
			// compute event position on map
			Eigen::Matrix<int,2,1> q = getMapPoint(pvc.ray_start, pvc.ray_dir);
			int x = q[0];
			int y = h - q[1] - 1;
			if(USE_KERNEL) {
				// write kernel
				int x1 = x - KERNEL_R;
				int x2 = x + KERNEL_R;
				int y1 = y - KERNEL_R;
				int y2 = y + KERNEL_R;
				if(x1 >= 0 && x2 < w && y1 >= 0 && y2 < h) {
					occurrence_map_.block(x1, y1, event_map_kernel_.rows(), event_map_kernel_.cols()) += pvc.weight*event_map_kernel_;
				}
			}
			else {
				// write only center
				if(x >= 0 && x < w && y >= 0 && y < h) {
					occurrence_map_(x, y) += pvc.weight;
				}
			}
		}
	}

}

void GridMap::resample(const ZAlignedPose<double>& state)
{
	createKernel(); // FIXME do not call every resample ...

	// check all points in the visibility map
	const int rows = distance_map_.rows();
	const int cols = distance_map_.cols();

	const float px_focal_length = sensor_model_->getPixelFocalLength();
	const float retina_half_size = RETINA_SIZE_FACTOR * 64.0f * map_height_ / px_focal_length / pixel_size_;
	const int retina_visibility_outer_radius = static_cast<unsigned int>(std::sqrt(2.0f) * retina_half_size / static_cast<float>(LOWRES_MAP_RATIO) + 0.5f);
	const int retina_visibility_inner_radius = static_cast<unsigned int>(retina_half_size / static_cast<float>(LOWRES_MAP_RATIO) + 0.5f);
	const int retina_visibility_inner_radius_2 = retina_visibility_inner_radius * retina_visibility_inner_radius;

	// compute visible area bounding box
	PixelViewCone pvc = sensor_model_->Transform(
			state.pos.cast<float>(), std::sin(state.rot), std::cos(state.rot),
			sensor_model_->createPixelViewCone(64,64)
		);
	const Eigen::Vector3f& pos = pvc.ray_start;
	const Eigen::Vector3f& dir = pvc.ray_dir;
	const Eigen::Matrix<int,2,1> center = getMapPoint(pos, dir);
	const int mx = center[0] / LOWRES_MAP_RATIO;
	const int my = cols - 1 - center[1] / LOWRES_MAP_RATIO;
	const int visibility_box_x1_ = std::max<int>(0, std::min<int>(rows-1, mx - retina_visibility_outer_radius));
	const int visibility_box_x2_ = std::max<int>(0, std::min<int>(rows-1, mx + retina_visibility_outer_radius));
	const int visibility_box_y1_ = std::max<int>(0, std::min<int>(cols-1, my - retina_visibility_outer_radius));
	const int visibility_box_y2_ = std::max<int>(0, std::min<int>(cols-1, my + retina_visibility_outer_radius));

	visibility_01_map_.fill(0.0f);

	const Eigen::Vector3f state_pos = state.pos.cast<float>();
	const float state_rot_sin = std::sin(state.rot);
	const float state_rot_cos = std::cos(state.rot);
	const Eigen::Vector3f state_previous_pos = state_previous_.pos.cast<float>();
	const float state_previous_rot_sin = std::sin(state_previous_.rot);
	const float state_previous_rot_cos = std::cos(state_previous_.rot);

	for(int y=visibility_box_y1_; y<=visibility_box_y2_; y++) {
		const float yf = ((static_cast<float>(cols - 1 - y) + 0.5f) * static_cast<float>(LOWRES_MAP_RATIO) - cy_) * pixel_size_;
		Eigen::Vector3f pw(0.0f, yf, map_height_);
		for(int x=visibility_box_x1_; x<=visibility_box_x2_; x++) {
			const float xf = ((static_cast<float>(x) + 0.5f) * static_cast<float>(LOWRES_MAP_RATIO) - cx_) * pixel_size_;
			pw[0] = xf;
			// get map location
			// convert to camera coordinates
			const Eigen::Vector3f pc = sensor_model_->TransformWorldToCamera(pw,
				state_pos, state_rot_sin, state_rot_cos);
			// project onto retina
			const Eigen::Vector2f pr = sensor_model_->projectCameraOnRetina(pc);
			// check visibility
			bool is_visible = sensor_model_->isVisible(pr);
			// if visible update distance map
			if(is_visible) {
				// convert to camera coordinates using previous state
				const Eigen::Vector3f pc_previous = sensor_model_->TransformWorldToCamera(pw,
					state_previous_pos, state_previous_rot_sin, state_previous_rot_cos);
				// project onto retina
				const Eigen::Vector2f pr_previous = sensor_model_->projectCameraOnRetina(pc_previous);
				const float dist = (pr - pr_previous).norm();
				distance_map_(x,y) += dist;
			}	

			visibility_01_map_(x,y) = (is_visible ? 1.0f : 0.0f);
		}
	}
	state_previous_ = state;

	// scale normalization value due to different resolution
	const float dist_scl = 1.0f / static_cast<float>(LOWRES_MAP_RATIO * LOWRES_MAP_RATIO);
	// update likelihood only in visible area
	const int x1 = visibility_box_x1_*LOWRES_MAP_RATIO;
	const int x2 = visibility_box_x2_*LOWRES_MAP_RATIO;
	const int y1 = visibility_box_y1_*LOWRES_MAP_RATIO;
	const int y2 = visibility_box_y2_*LOWRES_MAP_RATIO;
	for(int y=y1; y<=y2; y++) {
		int ylow = y / LOWRES_MAP_RATIO;
		for(int x=x1; x<=x2; x++) {
			int xlow = x / LOWRES_MAP_RATIO;
			const float v_occurrence = occurrence_map_(x, y);
			const float v_num_visible = JITTER_COMPENSATION * dist_scl * distance_map_(xlow, ylow);
			map_probability_(x,y) = visibility_protected_div(v_occurrence, v_num_visible);
		}
	}

	// // normalize distance map because we do not know how to compensate jitter
	// {
	// 	const float zmean = distance_map_.sum() / static_cast<float>(distance_map_.rows()*distance_map_.cols());
	// 	float cmin = 1000.0f;
	// 	float cmax = 0.0f;
	// 	// float cmin = map_probability_.minCoeff();
	// 	// float cmax = map_probability_.maxCoeff();
	// 	for(int y=0; y<rows; y++) {
	// 		int ylow = y / LOWRES_MAP_RATIO;
	// 		for(int x=0; x<cols; x++) {
	// 			int xlow = x / LOWRES_MAP_RATIO;
	// 			float vz = distance_map_(xlow, ylow);
	// 			if(vz > zmean) {
	// 				float vm = map_probability_(x, y);
	// 				cmin = std::min(cmin, vm);
	// 				cmax = std::max(cmax, vm);
	// 			}
	// 		}
	// 	}
	// 	if(cmin < cmax) {
	// 		map_probability_ *= (1.0f / (cmax - cmin));
	// 	}
	// }

	// sum up map probability over visible area
	float likelihood_total = 0.0f;
	for(int y=y1; y<=y2; y++) {
		int ylow = y / LOWRES_MAP_RATIO;
		for(int x=x1; x<=x2; x++) {
			int xlow = x / LOWRES_MAP_RATIO;
			if(visibility_01_map_(xlow, ylow) > 0.0f) {
				likelihood_total += map_probability_(x,y);
			}
		}
	}
//	std::cout << current_visible_likelihood_ << " - " << ll_num << std::endl;
	current_visible_likelihood_ = likelihood_total / MOTION_CONSTANT; // FIXME constant ???
//	std::cout << "total(O) = " << occurrence_map_.sum() << std::endl;

}

float GridMap::estimateCurrentVisibleLikelihood()
{
	return current_visible_likelihood_;
}

void GridMap::reduce(const Eigen::Vector3f& pos, float radius)
{
	const int R = static_cast<int>(radius / pixel_size_);
	const int px = static_cast<int>(pos.x() / pixel_size_ + cx_);
	const int py = static_cast<int>(pos.y() / pixel_size_ + cy_);

	for(int y=0; y<map_probability_.cols(); y++) {
		for(int x=0; x<map_probability_.rows(); x++) {
			if(!( x > (px - R) && x < (px + R) && y > (py - R) && y < (py + R))) {
 				map_probability_(x, map_probability_.cols() - 1 - y) = 0.0f;
			}
		}		
	}

}

void GridMap::setMaps(const Eigen::MatrixXf& occurence)
{
	map_probability_ = occurence;
	current_visible_likelihood_ = 0.0f;

	const int rows = map_probability_.rows();
	const int cols = map_probability_.cols();

	cx_ = static_cast<coord_t>(rows) / 2.0;
	cy_ = static_cast<coord_t>(cols) / 2.0;
	
	occurrence_map_ = Eigen::MatrixXf::Zero(rows, cols);

	visibility_01_map_ = Eigen::MatrixXf::Zero(rows / LOWRES_MAP_RATIO, cols/LOWRES_MAP_RATIO);

	distance_map_ = Eigen::MatrixXf::Zero(rows/LOWRES_MAP_RATIO, cols/LOWRES_MAP_RATIO);
}

void GridMap::setRawMap()
{
	Eigen::MatrixXf raw = LoadImageUchar4(raw_map_filename_);
	setMaps(raw);
}

GridMap GridMap::clone() const
{
	GridMap x = *this;
	x.map_probability_ = this->map_probability_;
	return x;
}

void GridMap::computeOccurenceMap(float pixel_focal_length, float scale)
{
	// load raw image
	Eigen::MatrixXf raw = LoadImageUchar4(raw_map_filename_);

	std::cout << "Computing probability from raw map..." << std::endl;
	std::cout << "pixel_size_: " << pixel_size_ << " => " << pixel_size_/scale << "" << std::endl;
	
	const int orows = static_cast<int>(raw.rows());
	const int ocols = static_cast<int>(raw.cols());

	const int nrows = static_cast<int>(static_cast<float>(orows) * scale + 0.5f);
	const int ncols = static_cast<int>(static_cast<float>(ocols) * scale + 0.5f);

	const float sigma = map_height_ / pixel_focal_length / pixel_size_;
	const float exp_arg_scl = 0.5f / sigma / sigma;
	const int radius = static_cast<int>(3.0f * sigma);

	std::cout << "resolution: (" << orows << "x" << ocols << ") => (" << nrows << "x" << ncols << ")" << std::endl;
	std::cout << "sigma: " << sigma << std::endl;
	std::cout << "radius: " << radius << std::endl;

	Eigen::MatrixXf soft_occurrence_map = Eigen::MatrixXf(nrows, ncols);

	boost::progress_display progress(ncols);
	// for each pixel
	for(int y=0; y<ncols; y++, ++progress) {
		const int i0 = static_cast<int>(static_cast<float>(y) / scale + 0.5f);
		const int i_min = std::max(i0-radius, 0) - i0;
		const int i_max = std::min(i0+radius, ocols-1) - i0;
		for(int x=0; x<nrows; x++) {
			const int j0 = static_cast<int>(static_cast<float>(x) / scale + 0.5f);
			const int j_min = std::max(j0-radius, 0) - j0;
			const int j_max = std::min(j0+radius, orows-1) - j0;
			// search an box of size 2*R
			// for distance the nearest border pixel
			// border pixels are those with value >= 0.5
			int d_min_2 = 2*radius*radius;
			for(int i=i_min; i<=i_max; i++) {
				for(int j=j_min; j<=j_max; j++) {
					if(raw(j0+j, i0+i) >= 0.5f) {
						d_min_2 = std::min(d_min_2, i*i + j*j);
					}
				}
			}
			// compute probability and store in map
			soft_occurrence_map(x, y) = 0.02f + 0.98f*std::exp(-exp_arg_scl*static_cast<float>(d_min_2));
		}
	}

	pixel_size_ /= scale;
	setMaps(soft_occurrence_map);

	std::cout << "Finished." << std::endl;
}

boost::shared_ptr<GridMap> GridMap::CreateGridMap()
{
	const unsigned int cLineThickness = 4;
	const unsigned int cCellSize = 128;
	const unsigned int cCellCount = 8;
	const unsigned int cSize = cCellCount * cCellSize;

	Eigen::MatrixXf map(cSize, cSize);

	for(unsigned int k=0; k<cCellCount; k++) {
		unsigned int y = k * cCellSize;
		for(unsigned int x=0; x<cSize; x++) {
			for(unsigned int t=0; t<cLineThickness; t++) {
				if(x>=0 && x<map.rows() && y+t>=0 && y+t<map.cols()) {
					map(x, y+t) = 1.0f;
				}
				if(y+t>=0 && y+t<map.rows() && x>=0 && x<map.cols()) {
					map(y+t, x) = 1.0f;
				}
			}
		}
	}

	boost::shared_ptr<GridMap> m(new GridMap());
	m->setMaps(map);
	m->setPixelSize(0.01f);
	return m;
}

boost::shared_ptr<GridMap> GridMap::CreateRandomMap(unsigned int size, const RandomMapParameters& params, int seed)
{
	QImage img(size, size, QImage::Format_ARGB32);
	img.fill(qRgb(0,0,0));

	QPainter painter(&img);
//	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(qRgb(255,255,255));

	boost::mt19937 engine(seed);
	boost::variate_generator<boost::mt19937&,boost::uniform_smallint<>> die(engine, boost::uniform_smallint<>(0, size));
	boost::variate_generator<boost::mt19937&,boost::uniform_smallint<>> dier(engine, boost::uniform_smallint<>(10, 100));

	for(unsigned int k=0; k<params.cnt_lines; k++) {
		painter.drawLine(die(), die(), die(), die());
	}

	for(unsigned int k=0; k<params.cnt_circles; k++) {
		painter.drawEllipse(QPoint(die(),die()), dier(), dier());
	}

	Eigen::MatrixXf map(size, size);
	for(unsigned int y=0; y<size; y++) {
		for(unsigned int x=0; x<size; x++) {
			map(x,y) = qRed(img.pixel(x,y));
		}
	}

	boost::shared_ptr<GridMap> m(new GridMap());
	m->setMaps(map);
	m->setPixelSize(0.01f);
	return m;
}

boost::shared_ptr<GridMap> GridMap::LoadImage(const std::string& fn_img)
{
	boost::shared_ptr<GridMap> m(new GridMap());
	m->setMaps(LoadImageUchar4(fn_img));
	m->setPixelSize(0.01f);
	return m;
}

void GridMap::SaveImage(const std::string& fn_img, const boost::shared_ptr<GridMap>& map)
{
	SaveImageUchar4(fn_img, map->getMapProbability());
}

boost::shared_ptr<GridMap> GridMap::LoadConfig(const std::string& fn_config)
{
	boost::shared_ptr<GridMap> map(new GridMap());
	MapSettings settings;
	settings.load(fn_config);
	map->setMapHeight(settings.height);
	map->setPixelSize(settings.pixel_size);
	map->raw_map_filename_ = boost::filesystem::path(fn_config).parent_path().string() + "/" + settings.fn_raw;
//	std::cout << "LoadConfig() size: " << map->getMapProbability().cols() << " " << map->getMapProbability().rows() << std::endl;
	return map;
}

void GridMap::SaveConfig(const std::string& fn_config, const boost::shared_ptr<GridMap>& map)
{
	MapSettings settings;
	settings.fn_raw = "raw.png";
	settings.fn_dist = "occurence.bin";
	settings.height = map->map_height_;
	settings.pixel_size = map->pixel_size_;
	settings.save(fn_config);
	std::string fn_raw_abs = boost::filesystem::path(fn_config).parent_path().string() + "/" + settings.fn_raw;
	std::string fn_dist_abs = boost::filesystem::path(fn_config).parent_path().string() + "/" + settings.fn_dist;
	SaveImageFloat(fn_dist_abs, map->getMapProbability());
}

//boost::shared_ptr<GridMap> GridMap::CreateMapFromImage(const QImage& img)
//{
//	boost::shared_ptr<GridMap> map(new GridMap());
//	map->qt_img_ = img;
//	map->map_ = Image1r::FromQImage(map->qt_img_);
//	map->cx_ = static_cast<Real>(map->map_.width()) / 2;
//	map->cy_ = static_cast<Real>(map->map_.height()) / 2;
//	return map;
//}

}
