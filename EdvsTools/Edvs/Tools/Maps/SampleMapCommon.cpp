#include "SampleMapCommon.hpp"
#include <cmath>

namespace Edvs {

namespace sample_map_detail
{
	float Overlap(const Sample<2>& a, const Sample<2>& b)
	{
		// samples are 2D normal distributions
		// multiply them and integrate
		// this formula gives the closed form solution
		const float SCALE = 2.0f * 3.1415f;
		const float d2 = (a.position - b.position).squaredNorm();
		const float q = a.sigma * a.sigma + b.sigma * b.sigma;
		float result = std::exp(-0.5f * d2/q) / (SCALE * q);
//		std::cout << "d=" << std::sqrt(d2) << ", s=" << a.sigma << "/" << b.sigma << " -> " << result << std::endl;
		return result;
	}

	float Overlap(const Sample<3>& a, const Sample<3>& b)
	{
		// samples are 3D normal distributions
		// multiply them and integrate
		// this formula gives the closed form solution
		const float SCALE = 2.0f * std::sqrt(2.0f) * std::pow(3.1415f, 1.5f);
		const float p = (a.position - b.position).squaredNorm();
		const float q = a.sigma * a.sigma + b.sigma * b.sigma;
		return std::exp(-0.5f * p/q) / (SCALE * std::pow(q, 1.5f));
	}

}

}
