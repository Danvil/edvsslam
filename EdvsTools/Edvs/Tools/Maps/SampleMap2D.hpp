#ifndef INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAP2D_HPP
#define INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAP2D_HPP

#include "Map.hpp"
#include "SampleMapCommon.hpp"
#include <vector>

namespace Edvs {

class SampleMap2D
: public Map
{
public:
	typedef sample_map_detail::Sample<2> sample_t;
	typedef sample_map_detail::SampleStore<2> sample_store_t;

	SampleMap2D();
	~SampleMap2D();

	std::string getInfoString() const;

	/**
	 * @param sigma radius of samples in world coordinates
	 * @param scale converts from pixel to world coordinates
	 */
	void createFromRawMap(const Eigen::MatrixXf& raw, float sigma, float scale);

	float evaluate(const PixelViewCone& pvc);

	void store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event);

	void resample(const ZAlignedPose<double>& state) {}

	void reduce(const Eigen::Vector3f& pos, float radius) {}

	const sample_store_t& getSampleStore() const {
		return samples_;
	}

private:
	/** Computes spherical samples placed along the pixel view cone */
	sample_t createViewConeSample(const PixelViewCone& pvc) const;

private:
	sample_store_t samples_;
};

}

#endif
