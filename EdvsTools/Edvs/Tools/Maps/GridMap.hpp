/*
 * GridMap.hpp
 *
 *  Created on: Mar 9, 2012
 *      Author: user
 */

#ifndef EDVS_MAPS_GRIDMAP_HPP_
#define EDVS_MAPS_GRIDMAP_HPP_

#include "Map.hpp"
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/progress.hpp>
#include "../SensorModel.hpp"
#include <cmath>

namespace Edvs {


struct GridMap
: public Map
{
public:
	GridMap()
	: pixel_size_(0.01f), map_height_(2.59f), cx_(0.0f), cy_(0.0f) {}

	virtual ~GridMap() {}

	std::string getInfoString() const;

	float evaluate(const PixelViewCone& pvc) {
		return evaluate(pvc.ray_start, pvc.ray_dir);
	}

	void store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event);

	void resample(const ZAlignedPose<double>& state);

	float estimateCurrentVisibleLikelihood();

	void reduce(const Eigen::Vector3f& pos, float radius);

public:
	typedef float coord_t;
	typedef float score_t;
	typedef Eigen::Matrix<coord_t,3,1> vec3_t;

	void setPixelSize(float pixel_size) {
		pixel_size_ = pixel_size;
		createKernel();
	}

	float getPixelSize() const {
		return pixel_size_;
	}

	void setMapHeight(float height) {
		map_height_ = height;
		createKernel();
	}

	float getMapHeight() const {
		return map_height_;
	}

	float getCenterX() const {
		return cx_;
	}

	float getCenterY() const {
		return cy_;
	}

	void setMaps(const Eigen::MatrixXf& occurence);

	void getDimensions(coord_t& x_min, coord_t& x_max, coord_t& y_min, coord_t& y_max) {
		x_max = pixel_size_ * map_probability_.rows() * 0.5;
		x_min = -x_max;
		y_max = pixel_size_ * map_probability_.cols() * 0.5;
		y_min = -y_max;
	}

	Eigen::MatrixXf& getMapProbability() {
		return map_probability_;
	}

	const Eigen::MatrixXf& getMapProbability() const {
		return map_probability_;
	}

	const Eigen::MatrixXf& getOccurrenceMap() const {
		return occurrence_map_;
	}

	const Eigen::MatrixXf& getDistanceMap() const {
		return distance_map_;
	}

	const Eigen::MatrixXf& getVisibilityMap() const {
		return visibility_01_map_;
	}

	Eigen::Matrix<coord_t,2,1> getPoint(const vec3_t& a, const vec3_t& u) const {
		const coord_t lam = (map_height_ - a[2]) / u[2];
		const coord_t x = a[0] + lam * u[0];
		const coord_t y = a[1] + lam * u[1];
		return {x,y};
	}

	Eigen::Matrix<int,2,1> getMapPoint(const vec3_t& a, const vec3_t& u) const {
		Eigen::Matrix<coord_t,2,1> q = getPoint(a, u);
		// assuming map is centered and has a size
		const coord_t px = q[0] / pixel_size_ + cx_;
		const coord_t py = q[1] / pixel_size_ + cy_;
		int ix = static_cast<int>(px + coord_t(0.5));
		int iy = static_cast<int>(py + coord_t(0.5));
		// TODO treat negative px, py correctly
		return {ix,iy};
	}

	score_t evaluate(const vec3_t& a, const vec3_t& u) const {
		Eigen::Matrix<int,2,1> q = getMapPoint(a, u);
		int ix = q[0];
		int iy = q[1];
		if(0 <= ix && 0 <= iy && ix < map_probability_.rows() && iy < map_probability_.cols()) {
			return map_probability_(ix, map_probability_.cols() - 1 - iy);
		}
		return score_t(0);
	}

	GridMap clone() const;

	void setRawMap();

	void computeOccurenceMap(float pixel_focal_length, float scale);

	std::string getRawMapFileName() {
		return raw_map_filename_;
	}

	void setStartState(ZAlignedPose<double> start_state) {
		state_previous_ = start_state;
	}	


private:
	void createKernel();

private:
	std::string raw_map_filename_;

	coord_t pixel_size_;
	coord_t map_height_;
	coord_t cx_, cy_;

	Eigen::MatrixXf event_map_kernel_;

	Eigen::MatrixXf map_probability_;
	float current_visible_likelihood_;

	Eigen::MatrixXf occurrence_map_;

	Eigen::MatrixXf visibility_01_map_;

	Eigen::MatrixXf distance_map_;
	ZAlignedPose<double> state_previous_;

public:
	static boost::shared_ptr<GridMap> CreateGridMap();

	struct RandomMapParameters {
		RandomMapParameters()
		: cnt_lines(15), cnt_circles(4) {}
		unsigned int cnt_lines;
		unsigned int cnt_circles;
	};

	static boost::shared_ptr<GridMap> CreateRandomMap(unsigned int size, const RandomMapParameters& params, int seed=0);

	static boost::shared_ptr<GridMap> LoadImage(const std::string& fn_img);

	static void SaveImage(const std::string& fn_img, const boost::shared_ptr<GridMap>& map);

	static boost::shared_ptr<GridMap> LoadConfig(const std::string& fn_config);

	static void SaveConfig(const std::string& fn_config, const boost::shared_ptr<GridMap>& map);

};

}

#endif
