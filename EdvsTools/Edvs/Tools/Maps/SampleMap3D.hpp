#ifndef INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAP3D_HPP
#define INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAP3D_HPP

#include "Map.hpp"
#include "SampleMapCommon.hpp"
#include <vector>

namespace Edvs {

class SampleMap3D
: public Map
{
public:
	typedef sample_map_detail::Sample<3> sample_t;
	typedef sample_map_detail::SampleStore<3> sample_store_t;

	SampleMap3D();
	~SampleMap3D();

	std::string getInfoString() const;

	float evaluate(const PixelViewCone& pvc);

	void store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event);

	void resample(const ZAlignedPose<double>& state) {}

	void reduce(const Eigen::Vector3f& pos, float radius) {}

	const sample_store_t& getSampleStore() const {
		return samples_;
	}

private:
	/** Computes spherical samples placed along the pixel view cone */
	std::vector<sample_t> createViewConeSamples(const PixelViewCone& pvc) const;

private:
	sample_store_t samples_;
};

}

#endif
