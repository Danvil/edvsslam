#ifndef INCLUDED_EDVS_TOOLS_MAPS_MAP_HPP
#define INCLUDED_EDVS_TOOLS_MAPS_MAP_HPP

#include "../PixelViewCone.hpp"
#include "../SensorModel.hpp"
#include <Eigen/Dense>
#include <vector>

namespace Edvs {

class Map
{
public:
	Map() : sensor_model_(0) {}
	virtual ~Map() {}

	/** Gets a short info string describing the map */
	virtual std::string getInfoString() const = 0;

	EdvsSensorModel<float>* sensor_model_;

	/** Gets event probability for a given pixel view cone */
	virtual float evaluate(const PixelViewCone& pxc) = 0;

	/** Add a set of weighted pixel view cone events to the map */
	virtual void store(const std::vector<PixelViewCone>& v_pxc, unsigned int num_event) = 0;

	/** Add new Visibility Counts to the Visibility Map*/	
	virtual void resample(const ZAlignedPose<double>& state) = 0;

	/** Deletes map outside of sphere */
	virtual void reduce(const Eigen::Vector3f& pos, float radius) = 0;

};

}

#endif
