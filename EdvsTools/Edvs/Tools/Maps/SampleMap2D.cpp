#include "SampleMap2D.hpp"
#include <boost/format.hpp>
#include <algorithm>
#include <cmath>
#include <iostream>

namespace Edvs {

SampleMap2D::SampleMap2D()
{

}

SampleMap2D::~SampleMap2D()
{

}

void SampleMap2D::createFromRawMap(const Eigen::MatrixXf& raw, float sigma, float scale)
{
	// collect all white points
	std::vector<Eigen::Vector2f> points;
	for(unsigned int j=0; j<raw.cols(); j++) {
		for(unsigned int i=0; i<raw.rows(); i++) {
			if(raw(i,j) > 0.5f) {
				Eigen::Vector2f p(static_cast<float>(j) * scale, static_cast<float>(i) * scale);
				points.push_back(p);
			}
		}
	}
	// randomly pick a percentage of white points and use as sample centers
	std::random_shuffle(points.begin(), points.end());
	const float percent = 1.0f / (2.0f * sigma / scale);
	const unsigned int n = static_cast<unsigned int>(percent * static_cast<float>(points.size()));
	// create samples
	samples_ = sample_store_t();
	for(auto it=points.begin(); it!=points.end()+n; ++it) {
		samples_.add(sample_t{ *it, sigma, 1.0f });
	}
}

std::string SampleMap2D::getInfoString() const
{
	return (boost::format("2D Sample Map (#=%d)") % getSampleStore().numSamples()).str();
}

float SampleMap2D::evaluate(const PixelViewCone& pvc)
{
	sample_t x = createViewConeSample(pvc);
	std::vector<sample_t> candidates = samples_.getInRange(x);
	float score = 0.0f;
	for(const sample_t& c : candidates) {
		score += Overlap(c, x) * c.weight;
	}
	return std::min(1.0f, 0.01f * score * x.weight);
}

void SampleMap2D::store(const std::vector<PixelViewCone>& v_pvc, unsigned int num_event)
{
	for(const PixelViewCone& pvc : v_pvc) {
		sample_t sample = createViewConeSample(pvc);
		samples_.add(sample);
	}
}

SampleMap2D::sample_t SampleMap2D::createViewConeSample(const PixelViewCone& pvc) const
{
	constexpr float HEIGHT = 2.54; // FIXME get height
	const float r = HEIGHT * std::tan(0.5f*pvc.opening_angle);
	const auto p = pvc.ray_start + HEIGHT*pvc.ray_dir;
	return sample_t{ {p[0], p[1]}, r, pvc.weight };
}

}
