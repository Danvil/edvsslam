#ifndef INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAPCOMMON_HPP
#define INCLUDED_EDVS_TOOLS_MAPS_SAMPLEMAPCOMMON_HPP

#include <Eigen/Dense>
#include <vector>

namespace Edvs {

namespace sample_map_detail
{
	template<unsigned int N>
	struct Sample
	{
		typedef Eigen::Matrix<float,N,1> vec_t;
		vec_t position;
		float sigma;
		float weight;
	};

	/** Computes overlap of two 2D samples */
	float Overlap(const Sample<2>& a, const Sample<2>& b);

	/** Computes overlap of two 3D samples */
	float Overlap(const Sample<3>& a, const Sample<3>& b);

	template<unsigned int N>
	class SampleStore
	{
	public:
		typedef Sample<N> sample_t;

		unsigned int numSamples() const {
			return samples_.size();
		}

		/** Adds a sample */
		void add(const sample_t& s) {
			samples_.push_back(s);
		}

		/** Adds many samples */
		void add(const std::vector<sample_t>& v) {
			for(const sample_t& s : v) {
				add(s);
			}
		}

		/** Gets samples which are near */
		const std::vector<sample_t>& getInRange(const sample_t& x) const {
			// constexpr float MIN_VAL = 0.001f;
			// std::vector<sample_t> result;
			// for(const sample_t& s : samples_) {
			// 	if(Overlap(s, x) > MIN_VAL) {
			// 		result.push_back(s);
			// 	}
			// }
			// return result;
			return samples_;
		}

		const std::vector<sample_t>& getSamples() const {
			return samples_;
		}

		std::vector<sample_t>& getSamples() {
			return samples_;
		}

	private:
		std::vector<sample_t> samples_;
	};
}

}

#endif
