/*
 * FunctionCache.hpp
 *
 *  Created on: Jun 18, 2012
 *      Author: david
 */

#ifndef FUNCTIONCACHE_HPP_
#define FUNCTIONCACHE_HPP_

template<typename K>
struct FunctionCache
{
	FunctionCache()
	: min_(0), max_(1), data_(2,K(0)), scl_(1)
	{}

	FunctionCache(K min, K max, unsigned int n)
	: min_(min), max_(max), data_(n) {
		scl_ = static_cast<double>(data_.size() - 1) / (max_- min_);
	}

	template<typename Function>
	void fill(Function f) {
//		std::cout << "CACHE" << std::endl;
		for(unsigned int i=0; i<data_.size(); i++) {
			K x = min_ + static_cast<K>(i) / scl_;
			data_[i] = f(x);
//			std::cout << i << " " << x << " " << data_[i] << std::endl;
		}
	}

	K operator()(K x) const {
		double p = (x - min_) * scl_;
		int v = static_cast<int>(p); // round down
		if(v < 0) {
			return data_.front();
		}
		if(v + 1 >= data_.size()) {
			return data_.back();
		}
		double q = p - static_cast<double>(v);
		K v1 = data_[v];
		K v2 = data_[v + 1];
		return (1.0 - q) * v1 + q * v2;
	}

	template<typename Function>
	static FunctionCache<K> Create(K min, K max, unsigned int n, Function f) {
		FunctionCache<K> cache(min, max, n);
		cache.fill(f);
		return cache;
	}

private:
	K min_, max_;
	std::vector<K> data_;
	double scl_;
};

#endif /* FUNCTIONCACHE_HPP_ */
