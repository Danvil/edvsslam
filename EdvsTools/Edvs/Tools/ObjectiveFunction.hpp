/*
 * ObjectiveFunction.hpp
 *
 *  Created on: Jun 26, 2012
 *      Author: david
 */

#ifndef OBJECTIVEFUNCTION_HPP_
#define OBJECTIVEFUNCTION_HPP_

#include <Edvs/Event.hpp>
#include "Maps/Map.hpp"
#include "FunctionCache.hpp"
#include "SensorModel.hpp"
#include "Pose.hpp"
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>

#define PI 3.1415926535
#include <cmath>

namespace Edvs
{

/** This class manages the objective function used for tracking */
template<typename coord_t, typename score_t>
struct ObjectiveFunction
{
	typedef Eigen::Matrix<coord_t,3,1> vec3_t;
	typedef ZAlignedPose<coord_t> pose_t;

	static constexpr score_t cNoiseOffset = 0.05;

private:
	score_t p_gamma_;
	FunctionCache<score_t> obj_cache_;

public:
	ObjectiveFunction() {
		setGamma(1.0);	
	}

	void setGamma(score_t gamma) {
		p_gamma_ = gamma;
//		obj_cache_ = FunctionCache<score_t>::Create(0.0, 5.0*p_gamma_, 256, [this](score_t x) { return std::exp(-x/p_gamma_); });
		obj_cache_ = FunctionCache<score_t>::Create(0.0, 3.0*p_gamma_, 256, [this](score_t x) { return std::exp(-0.5*x*x/p_gamma_/p_gamma_); });
	}

	float getGamma() const {
		return p_gamma_;
	}

	score_t modulate(score_t score) const {
		return cNoiseOffset + (1.0 - cNoiseOffset)*score;
	}

	score_t evaluate(const vec3_t& pos, coord_t rot_sin, coord_t rot_cos, float ex, float ey) const {
		PixelViewCone pvc = sensor_model_.createPixelViewCone(ex, ey);
		pvc = sensor_model_.Transform(pos, rot_sin, rot_cos, pvc);
		return map_->evaluate(pvc);
	}

	score_t evaluate(const vec3_t& pos, coord_t rot, float ex, float ey) const {
		return evaluate(pos, std::sin(rot), std::cos(rot), ex, ey);
	}

	score_t evaluate(const pose_t& pose, float ex, float ey) const {
		return evaluate(pose.pos, pose.rot, ex, ey);
	}

	score_t evaluate(const pose_t& pose, const Event& event) const {
		return evaluate(pose, event.x, event.y);
	}

	/** Returns 1/n*( P(e_1) + P(e_2) + ... + P(e_n) ) */
	score_t meanOfScores(const vec3_t& pos, coord_t rot_sin, coord_t rot_cos, const std::vector<Event>& events) const {
		if(events.empty()) {
			return 0;
		}
		return std::accumulate(events.begin(), events.end(), score_t(0),
				[this,&pos,rot_sin,rot_cos](score_t a, const Event& event) {
					return a + this->evaluate(pos, rot_sin, rot_cos, event.x, event.y);
				}) / static_cast<score_t>(events.size());
	}

	score_t meanOfScores(const vec3_t& pos, coord_t rot, const std::vector<Event>& events) const {
		return meanOfScores(pos, std::sin(rot), std::cos(rot), events);
	}

	score_t meanOfScores(const pose_t& pose, const std::vector<Event>& events) const {
		return meanOfScores(pose.pos, pose.rot, events);
	}

	/** Returns P(e_1,e_2,...,e_n) = P(e_1) * P(e_2) * ... * P(e_n) */
	score_t productOfScoresWithNoise(const vec3_t& pos, coord_t rot_sin, coord_t rot_cos, const std::vector<Event>& events) const {
		return std::accumulate(events.begin(), events.end(), score_t(1),
				[this,&pos,rot_sin,rot_cos](score_t a, const Event& event) {
					return a * this->modulate(this->evaluate(pos, rot_sin, rot_cos, event.x, event.y));
				});
	}

	score_t productOfScoresWithNoise(const vec3_t& pos, coord_t rot, const std::vector<Event>& events) const {
		return productOfScoresWithNoise(pos, std::sin(rot), std::cos(rot), events);
	}

	score_t productOfScoresWithNoise(const pose_t& pose, const std::vector<Event>& events) const {
		return productOfScoresWithNoise(pose.pos, pose.rot, events);
	}

	/** Returns ln(P(e_1,e_2,...,e_n)) = ln(P(e_1)) + ln(P(e_2)) + ... + ln(P(e_n)) */
	score_t logProductOfScoresWithNoise(const vec3_t& pos, coord_t rot_sin, coord_t rot_cos, const std::vector<Event>& events) const {
		static FunctionCache<score_t> log_cache = FunctionCache<score_t>::Create(1.0/256.0, 1.0, 256, [](score_t x) { return std::log(x); });
		return std::accumulate(events.begin(), events.end(), score_t(0),
				[this,&log_cache,&pos,rot_sin,rot_cos](score_t a, const Event& event) {
					return a + log_cache(this->modulate(this->evaluate(pos, rot_sin, rot_cos, event.x, event.y)));
				});
	}

	score_t logProductOfScoresWithNoise(const vec3_t& pos, coord_t rot, const std::vector<Event>& events) const {
		return logProductOfScoresWithNoise(pos, std::sin(rot), std::cos(rot), events);
	}

	score_t logProductOfScoresWithNoise(const pose_t& pose, const std::vector<Event>& events) const {
		return logProductOfScoresWithNoise(pose.pos, pose.rot, events);
	}

	boost::shared_ptr<Map> map_;
	EdvsSensorModel<coord_t> sensor_model_;
};

typedef ObjectiveFunction<float,float> ObjectiveFunctionF;

}

#endif
