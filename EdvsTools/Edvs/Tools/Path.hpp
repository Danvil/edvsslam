/*
 * Path.hpp
 *
 *  Created on: May 24, 2012
 *      Author: david
 */

#ifndef PATH_HPP_
#define PATH_HPP_

#include <Eigen/Dense>
#include <algorithm>
#include <string>
#include <fstream>

template<typename K, unsigned int N>
struct PathPoint
{
	K time;
	Eigen::Matrix<K,N,1> value;
};

template<typename K, unsigned int N, bool UseSpline>
struct Path
{
	typedef PathPoint<K,N> PP;
	typedef Eigen::Matrix<K,N,1> V;

	void add(const PP& p) {
		auto it = std::upper_bound(nodes.begin(), nodes.end(), p, [](const PP& x, const PP& y) { return x.time < y.time; });
		nodes.insert(it, p);
	}

	const std::vector<PP>& getNodes() const {
		return nodes;
	}

	std::size_t length() const {
		return nodes.size();
	}

	K beginTime() const {
		return nodes.front().time;
	}

	K endTime() const {
		return nodes.back().time;
	}

	V position(K t) const {
		if(nodes.size() == 0) {
			return V::Zero();
		}
		else if(nodes.size() == 1 || t <= nodes.front().time) {
			return nodes.front().value;
		}
		else if(t >= nodes.back().time) {
			return nodes.back().value;
		}
		else {
			// we guaranteed nodes.front().time < t && t < nodes.back().time
			typename std::vector<PP>::const_iterator it_2 = std::upper_bound(nodes.begin(), nodes.end(), t, [](K x, const PP& y) { return x < y.time; });
			typename std::vector<PP>::const_iterator it_1 = std::prev(it_2);
			unsigned int i = std::distance(nodes.begin(), it_1);
			K p = (t - it_1->time) / (it_2->time - it_1->time);
			// interpolate
			if(UseSpline) {
				V a, b, c, d;
				find_points(i, a, b, c, d);
				return Spline(a, b, c, d, p);
			}
			else {
				return Linear(nodes[i].value, nodes[i + 1].value, p);
			}
		}
	}

	V operator()(K t) const {
		return position(t);
	}

	K rotation(K t) const {
		V a = position(t);
		V b = position(t + 0.001);
		V d = b - a;
		return std::atan2(d.y(), d.x());
	}

	V velocity(K t) const {
		const K dt = 0.001;// time / static_cast<Real>(1000);
		V p1 = position(t - dt);
		V p2 = position(t + dt);
		return (1 / (2*dt)) * (p2 - p1);
	}

private:
	void find_points(unsigned int i, V& p1, V& b, V& c, V& p2) const {
		const K Q = 0.16667;
		p1 = nodes[i].value;
		p2 = nodes[i + 1].value;
		if(nodes.size() == 2) {
			b = p1 + Q*(p2 - p1);
			c = p2 + Q*(p1 - p2);
		}
		else if(i == 0) {
			const V& p3 = nodes[i + 2].value;
			b = p1 + Q*(p2 - p1);
			c = p2 + Q*(p1 - p3);
		}
		else if(i == nodes.size() - 2) {
			const V& p0 = nodes[i - 1].value;
			b = p1 + Q*(p2 - p0);
			c = p2 + Q*(p1 - p2);
		}
		else {
			const V& p0 = nodes[i - 1].value;
			const V& p3 = nodes[i + 2].value;
			b = p1 + Q*(p2 - p0);
			c = p2 + Q*(p1 - p3);
		}
	}

	static V Linear(const V& a, const V& b, K t) {
		return (1 - t) * a + t * b;
	}

	static V Spline(const V& a, const V& b, const V& c, const V& d, K t) {
		V u = Linear(a, b, t);
		V v = Linear(b, c, t);
		V w = Linear(c, d, t);
		V r = Linear(u, v, t);
		V s = Linear(v, w, t);
		return Linear(r, s, t);
	}

private:
	std::vector<PP> nodes;

};

namespace Edvs
{

	template<typename K, unsigned int N, bool UseSpline>
	struct PathBuilder
	{
		typedef Eigen::Matrix<K,N,1> V;

		PathBuilder(K velocity)
		: velocity_(velocity) {}

		PathBuilder& operator<<(const V& v) {
			if(path_.length() == 0) {
				path_.add({0, v});
			}
			else {
				const PathPoint<K,N>& p = path_.getNodes().back();
				K dx = (v - p.value).norm();
				K dt = dx / velocity_;
				path_.add({p.time + dt, v});
			}
			return *this;
		}

		const Path<K,N,UseSpline>& getPath() const {
			return path_;
		}

	private:
		K velocity_;
		Path<K,N,UseSpline> path_;
	};

	template<typename K, unsigned int N, bool UseSpline>
	void SavePath(const Path<K,N,UseSpline>& path, const std::string& filename)
	{
		std::ofstream fs(filename);
		for(const PathPoint<K,N>& p : path.getNodes()) {
			fs << p.time;
			for(unsigned int i=0; i<N; i++) {
				fs << "\t" << p.value[i];
			}
			fs << "\t" << path.rotation(p.time);
			fs << std::endl;
		}
	}

	template<typename K, unsigned int N, bool UseSpline>
	Path<K,N,UseSpline> LoadPath(const std::string& filename)
	{
		Path<K,N,UseSpline> path;
		std::ifstream fs(filename);
		while(!fs.eof()) {
			K time;
			fs >> time;
			Eigen::Matrix<K,N,1> value;
			for(unsigned int i=0; i<N; i++) {
				fs >> value[i];
			}
			float rot;
			fs >> rot;
			path.add({time, value});
		}
		return path;
	}

	template<typename K, unsigned int N, bool UseSpline>
	Path<K,N,false> Resample(const Path<K,N,UseSpline>& path, K dt)
	{
		Path<K,N,false> neu;
		K tbegin = path.getNodes().front().time;
		K tend = path.getNodes().back().time;
		for(K t=tbegin; t<tend; t+=dt) {
			neu.add({t, path(t)});
		}
		return neu;
	}

	namespace PathFactory
	{
		template<typename K>
		Path<K,2,true> GeneratePathLine(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePathCircle(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePathRectangle(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePathSlalom(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePathEight(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePathSpiral(K length, K velocity);

		template<typename K>
		Path<K,2,true> GeneratePath(const std::string& type, K length, K velocity);

	}
}

#endif
