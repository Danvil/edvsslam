/*
 * MapSettings.hpp
 *
 *  Created on: Jul 10, 2012
 *      Author: david
 */

#ifndef MAPSETTINGS_HPP_
#define MAPSETTINGS_HPP_

#include <string>

namespace Edvs
{

	struct MapSettings
	{
		std::string fn_raw;
		std::string fn_dist;
		double height;
		double pixel_size;

		void load(const std::string& filename);
		void save(const std::string& filename);
	};

}

#endif
