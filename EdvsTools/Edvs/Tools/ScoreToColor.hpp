/*
 * ScoreToColor.hpp
 *
 *  Created on: Jun 28, 2012
 *      Author: david
 */

#ifndef SCORETOCOLOR_HPP_
#define SCORETOCOLOR_HPP_

#include <stdint.h>

namespace Edvs
{

	inline uint32_t toRgb(unsigned int r, unsigned int g, unsigned int b) {
		return (0xffu << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
	}

	// black -> blue -> red -> yellow -> white
	inline uint32_t Score2Color(float score)
	{
		unsigned int cr, cg, cb;
		if(score < 0) {
			cr = 0;
			cg = 0;
			cb = 0;
		}
		else if(score < 0.25f) {
			float t = score / 0.25f;
			cr = 0;
			cg = 0;
			cb = static_cast<unsigned int>(255.0f * t);
		}
		else if(score < 0.5f) {
			float t = (score - 0.25f) / 0.25f;
			cr = static_cast<unsigned int>(255.0f * t);
			cg = 0;
			cb = static_cast<unsigned int>(255.0f * (1.0f - t));
		}
		else if(score < 0.75f) {
			float t = (score - 0.5f) / 0.25f;
			cr = 255;
			cg = static_cast<unsigned int>(255.0 * t);
			cb = 0;
		}
		else if(score <= 1.0f) {
			float t = (score - 0.75f) / 0.25f;
			cr = 255;
			cg = 255;
			cb = static_cast<unsigned int>(255.0f * t);
		}
		else {
			cr = 255;
			cg = 255;
			cb = 255;
		}
		return toRgb(cr,cg,cb);
	}

	// blue -> red -> yellow
	inline uint32_t Score2ColorBlueToYellow(float score)
	{
		unsigned int cr, cg, cb;
		if(score < 0) {
			cr = 0;
			cg = 0;
			cb = 0;
		}
		else if(score < 0.5f) {
			float t = score / 0.5f;
			cr = static_cast<unsigned int>(255.0f * t);
			cg = 0;
			cb = static_cast<unsigned int>(255.0f * (1.0f - t));
		}
		else if(score <= 1.0f) {
			float t = (score - 0.5f) / 0.5f;
			cr = 255;
			cg = static_cast<unsigned int>(255.0f * (1.0f - t));
			cb = 0;
		}
		else {
			cr = 255;
			cg = 255;
			cb = 255;
		}
		return toRgb(cr,cg,cb);
	}

}

#endif
