/*
 * ObjectiveImage.hpp
 *
 *  Created on: Jun 28, 2012
 *      Author: david
 */

#ifndef EDVS_TOOLS_OBJECTIVEIMAGE_HPP_
#define EDVS_TOOLS_OBJECTIVEIMAGE_HPP_

#include "ScoreToColor.hpp"
#include "ObjectiveFunction.hpp"
#include <Edvs/Event.hpp>
#include <vector>
#include <string>

namespace Edvs
{

struct Roi
{
	float u_min, u_max;
	float v_min, v_max;

	float d_u;
	float d_v;

	unsigned int getNumU() const {
		return (u_max - u_min) / d_u;
	}

	unsigned int getNumV() const {
		return (v_max - v_min) / d_v;
	}

	float getU(unsigned int i) const {
		return u_min + static_cast<float>(i) * d_u;
	}

	float getV(unsigned int i) const {
		return v_min + static_cast<float>(i) * d_v;
	}

};

void CreateAndSaveXY(const std::string& filename, const std::vector<Event>& events, const ObjectiveFunctionF& fnc, const Roi& roi, float z, float rot);

}

#endif
