/*
 * EventSimulation.hpp
 *
 *  Created on: Jul 9, 2012
 *      Author: david
 */

#ifndef EVENTSIMULATION_HPP_
#define EVENTSIMULATION_HPP_

#include <Edvs/Event.hpp>
#include "ObjectiveFunction.hpp"
#include "Path.hpp"
#include <stdint.h>

namespace Edvs
{
	struct EventSimulation
	{
		typedef double Real;
		typedef ZAlignedPose<Real> State;

		EventSimulation() {
			thread_count = 1;
		}

		unsigned int thread_count;

		ObjectiveFunctionF objective;

		Path<Real,2,false> path;

		std::vector<Edvs::Event> SimulateAll(int64_t dt);

		State computePathState(Real t) const {
			Eigen::Matrix<Real,2,1> v = path.position(t);
			return {{v.x(),v.y(),Real(0)}, path.rotation(t)};
		}

	private:
		std::vector<Edvs::Event> Simulate(int64_t time, int64_t dt);

		std::vector<unsigned char> retina_state_;

		std::vector<PixelViewCone> retina_pvc_;

		float n_noise_add_;
		float n_noise_del_;
	};
}

#endif
