/*
 * Path.cpp
 *
 *  Created on: May 24, 2012
 *      Author: david
 */

#include "Path.hpp"
#include <boost/math/constants/constants.hpp>
#include <fstream>
#include <cmath>

//template struct Path<float,3,true>;
//template struct Path<double,3,true>;
//template struct Path<float,3,false>;
//template struct Path<double,3,false>;

namespace Edvs
{

	namespace PathFactory
	{
	//			static Real fadein(Real x) {
	//				return x*x*(2 - x);
	//			}
	//
	//			static Real fadeout(Real x) {
	//				return 1 - fadein(1 - x);
	//			}
	//
	//			static Real fadeover(Real x) {
	//				return x*x*(3 - 2*x);
	//			}

	//			if(is_loop_) {
	//				assert(nodes.size() > 2); // TODO looping path with 2 nodes is not supported!
	//				if(i == 0 && t < time) {
	//					p = fadein(p);
	//				}
	//			}
	//			else {
	//				if(i == 0 && nodes.size() == 2) {
	//					p = fadeover(p);
	//				}
	//				else if(i == 0) {
	//					p = fadein(p);
	//				}
	//				else if(i + 2 == nodes.size()) {
	//					p = fadeout(p);
	//				}
	//			}

		template<typename K>
		Path<K,2,true> GeneratePathLine(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const K A = 0.5 * length;
			PathBuilder<K,2,true> builder(velocity);
			builder << V{-A,0} << V{+A,0};
			return builder.getPath();
		}

		template<typename K>
		Path<K,2,true> GeneratePathCircle(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const unsigned int num = 8;
			const K radius = length / (2.0*boost::math::constants::pi<K>()); // circle radius
			PathBuilder<K,2,true> builder(velocity);
			//path_.is_loop_ = true;
			for(unsigned int k=0; k<=num; k++) {
				K phi = static_cast<K>(k) / static_cast<K>(num) * 2.0 * boost::math::constants::pi<K>();
				K x = std::cos(phi)*radius;
				K y = std::sin(phi)*radius;
				builder << V{x,y};
			}
			return builder.getPath();
		}

		template<typename K>
		Path<K,2,true> GeneratePathRectangle(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const K A = 0.5 * (length / K(4)); // half edge side length
			PathBuilder<K,2,true> builder(velocity);
			builder << V{+A,+A};
			builder << V{-A,+A};
			builder << V{-A,-A};
			builder << V{+A,-A};
			return builder.getPath();
		}

		template<typename K>
		Path<K,2,true> GeneratePathSlalom(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const K A = length / (2.0*(std::sqrt(2.0)*0.5 + std::sqrt(5.0)*0.5)); // some constant
			PathBuilder<K,2,true> builder(velocity);
			builder << V{-1.0*A,0};
			builder << V{-0.5*A,+0.5*A};
			builder << V{ 0.0*A,-0.5*A};
			builder << V{+0.5*A,+0.5*A};
			builder << V{+1.0*A,0};
			return builder.getPath();
		}

		template<typename K>
		Path<K,2,true> GeneratePathEight(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const K A = length / (2.0*3.1415); // approximative
			PathBuilder<K,2,true> builder(velocity);
			builder << V{-1.0*A,0};
			builder << V{-0.5*A,+0.5*A};
			builder << V{ 0.0*A, 0.0*A};
			builder << V{+0.5*A,-0.5*A};
			builder << V{+1.0*A,0};
			builder << V{+0.5*A,+0.5*A};
			builder << V{ 0.0*A, 0.0*A};
			builder << V{-0.5*A,-0.5*A};
			builder << V{-1.0*A,0};
			return builder.getPath();
		}

		template<typename K>
		Path<K,2,true> GeneratePathSpiral(K length, K velocity)
		{
			typedef Eigen::Matrix<K,2,1> V;
			const K A = 0.5 * (length / K(11.5)); // half edge side length
			PathBuilder<K,2,true> builder(velocity);
			builder << V{+A,+A};
			builder << V{-A,+A};
			builder << V{-A,-A};
			builder << V{+A,-A};
			builder << V{+A,A-0.1*A};
			builder << V{0,A-0.1*A};
			builder << V{0,-A+0.3*A};
			builder << V{A,-A+0.3*A};
			return builder.getPath();
		}	

		template<typename K>
		Path<K,2,true> GeneratePath(const std::string& type, K length, K velocity) {
			if(type == "line") {
				return GeneratePathLine(length, velocity);
			}
			else if(type == "circle") {
				return GeneratePathCircle(length, velocity);
			}
			else if(type == "rectangle") {
				return GeneratePathRectangle(length, velocity);
			}
			else if(type == "slalom") {
				return GeneratePathSlalom(length, velocity);
			}
			else if(type == "eight") {
				return GeneratePathEight(length, velocity);
			}
			else if(type == "spiral") {
				return GeneratePathSpiral(length, velocity);
			}
			else {
				throw 0;
			}
		}

		template Path<double,2,true> GeneratePathLine(double length, double velocity);
		template Path<double,2,true> GeneratePathCircle(double length, double velocity);
		template Path<double,2,true> GeneratePathRectangle(double length, double velocity);
		template Path<double,2,true> GeneratePathSlalom(double length, double velocity);
		template Path<double,2,true> GeneratePathSpiral(double length, double velocity);
		template Path<double,2,true> GeneratePath(const std::string& type, double length, double velocity);

	}
}
