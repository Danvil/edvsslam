/*
 * EventSimulation.cpp
 *
 *  Created on: Jul 9, 2012
 *      Author: david
 */

#include "EventSimulation.hpp"
#include <boost/random.hpp>
#include <boost/progress.hpp>
#include <memory>
#include <cmath>

#define SIM_USES_MULTITHREADING

#ifdef SIM_USES_MULTITHREADING
#include <boost/threadpool.hpp>
#endif

namespace Edvs
{
	constexpr uint16_t cSize = EdvsSensorModelF::RETINA_SIZE;
	constexpr float cNoiseAdd = 0.05f; // percentage of noise events
	constexpr float cNoiseDel = 0.05f; // probability that an event is actually created

#ifdef SIM_USES_MULTITHREADING
	struct Workload
	{
		std::vector<unsigned char>::iterator retina_state_it;
		std::vector<PixelViewCone>::const_iterator retina_pvc_it;
		uint16_t y0, y1;
		ObjectiveFunctionF objective;
		Eigen::Vector3f position;
		float rot_sin, rot_cos;
		uint8_t device_id;
		std::vector<Edvs::Event> result;
	};

	void ProcessWorkload(Workload* w)
	{
		auto it = w->retina_state_it;
		auto it_pvc = w->retina_pvc_it;
		for(uint16_t y=w->y0; y<w->y1; y++) {
			for(uint16_t x=0; x<cSize; x++, ++it, ++it_pvc) {
				float val = w->objective.map_->evaluate(
					w->objective.sensor_model_.Transform(w->position, w->rot_sin, w->rot_cos, *it_pvc));
				unsigned char is_on_c = (val >= 0.5 ? 1 : 0);
				if(is_on_c != *it) {
					uint8_t parity = static_cast<uint8_t>(is_on_c >= *it ? 1 : 0);
					w->result.push_back(Event{0, x, y, parity, w->device_id});
					*it = is_on_c;
				}
			}
		}
	}
#endif

	std::vector<Edvs::Event> EventSimulation::Simulate(int64_t current_time, int64_t dt)
	{
		constexpr uint32_t DEVICE_ID = 0;

#ifdef SIM_USES_MULTITHREADING
		if(128 % thread_count != 0) {
			std::cerr << "EventSimulation: Thread count must be divisor of 128!" << std::endl;
			throw 0;
		}
#endif

		static boost::mt19937 rnd_engine;

#ifdef SIM_USES_MULTITHREADING
		static boost::threadpool::pool pool(thread_count);
		static std::vector<Workload*> thread_events;
#endif

		State state = computePathState(static_cast<Real>(current_time) / 1000000.0);
		Eigen::Vector3f state_pos = state.pos.cast<float>();
		float rot_cos = std::cos(state.rot);
		float rot_sin = std::sin(state.rot);

		std::vector<Edvs::Event> events;

#ifdef SIM_USES_MULTITHREADING
		if(thread_count == 1) {
#endif
			auto it = retina_state_.begin();
			auto it_pvc = retina_pvc_.begin();
			for(uint16_t y=0; y<cSize; y++) {
				for(uint16_t x=0; x<cSize; x++, ++it, ++it_pvc) {
					float val = objective.map_->evaluate(
						objective.sensor_model_.Transform(state_pos, rot_sin, rot_cos, *it_pvc));
					unsigned char is_on_c = (val >= 0.5 ? 1 : 0);
					if(is_on_c != *it) {
						uint8_t parity = static_cast<uint8_t>(is_on_c >= *it ? 1 : 0);
						events.push_back(Event{0, x, y, parity, DEVICE_ID});
						*it = is_on_c;
					}
				}
			}
#ifdef SIM_USES_MULTITHREADING
		}
		else {
			if(thread_events.size() == 0) {
				for(unsigned int k=0; k<thread_count; k++) {
					Workload* w = new Workload();
					w->y0 = k*cSize/thread_count;
					w->y1 = (k+1)*cSize/thread_count;
					w->objective = objective;
					w->device_id = DEVICE_ID;
					thread_events.push_back(w);
				}
			}
			for(unsigned int k=0; k<thread_count; k++) {
				Workload* w = thread_events[k];
				w->retina_state_it = retina_state_.begin() + cSize*w->y0;
				w->retina_pvc_it = retina_pvc_.begin() + cSize*w->y0;
				w->position = state_pos;
				w->rot_sin = rot_sin;
				w->rot_cos = rot_cos;
				w->result.clear();
				pool.schedule([w]() { ProcessWorkload(w); });
			}
			pool.wait();
			for(unsigned int k=0; k<thread_count; k++) {
				Workload* w = thread_events[k];
				events.insert(events.end(), w->result.begin(), w->result.end());
			}
		}
#endif

		n_noise_add_ += cNoiseAdd * static_cast<float>(events.size());
		n_noise_del_ += cNoiseDel * static_cast<float>(events.size());

		// randomly delete some events
		{
			std::random_shuffle(events.begin(), events.end());
			const std::size_t n_noise_del_int = static_cast<std::size_t>(n_noise_del_);
			n_noise_del_ -= static_cast<float>(n_noise_del_int);
			events.resize(events.size() - n_noise_del_int);
		}

		// create random noise events
		{
			boost::variate_generator<boost::mt19937&, boost::uniform_smallint<>> die_px(rnd_engine, boost::uniform_smallint<>(0, cSize-1));
			const std::size_t n_noise_add_int = static_cast<std::size_t>(n_noise_add_);
			n_noise_add_ -= static_cast<float>(n_noise_add_int);
			for(unsigned int i=0; i<n_noise_add_int; i++) {
				events.push_back(Event{
					0,
					uint16_t(die_px()), uint16_t(die_px()),
					static_cast<bool>(die_px() % 2),
					DEVICE_ID});
			}
		}

		// more random distribution of events in time
		std::random_shuffle(events.begin(), events.end());
		for(std::size_t i=0; i<events.size(); i++) {
			events[i].t = current_time - dt/2 + (dt*i) / events.size();
		}

		return events;
	}

	std::vector<Edvs::Event> EventSimulation::SimulateAll(int64_t dt)
	{
		objective.sensor_model_.kappa_1 = 0.0;
		objective.sensor_model_.kappa_2 = 0.0;
		retina_state_.resize(EdvsSensorModelF::RETINA_SIZE*EdvsSensorModelF::RETINA_SIZE, 0);

		{
			retina_pvc_.reserve(cSize*cSize);
			for(uint16_t y=0; y<cSize; y++) {
				const float yf = static_cast<float>(y);
				for(uint16_t x=0; x<cSize; x++) {
					const float xf = static_cast<float>(x);
					retina_pvc_.push_back(objective.sensor_model_.createPixelViewCone(xf, yf));
				}
			}
		}

		n_noise_add_ = 0.0f;
		n_noise_del_ = 0.0f;
		std::vector<Edvs::Event> events_all;
		int64_t time_begin = static_cast<int64_t>(1000000.0 * path.beginTime()) + dt/2;
		int64_t time_end = static_cast<int64_t>(1000000.0 * path.endTime());
		int64_t ticks = (time_end - time_begin) / dt;
		events_all.reserve(ticks * 100);
		std::cout << "start=" << time_begin << ", end=" << time_end << ", dt=" << dt << ", ticks=" << ticks << std::endl;
		boost::progress_display progress(ticks);
		for(int64_t t=time_begin; t<=time_end; t+=dt, ++progress) {
			std::vector<Edvs::Event> events = Simulate(t, dt);
			std::cout << "t=" << t << ", dt=" << dt << ", n=" << events.size() << std::endl;
			events_all.insert(events_all.end(), events.begin(), events.end());
		}
		return events_all;
	}

}
