import sys
import pylab
import os



path_folders = sys.argv[1]
filename_plot = sys.argv[2]

error = 0.0

paths = []
data = []
ind = []
j=0
#cwd = os.getcwd()

valid_scens=[ 1,          4,  5,  6,  7,  8,  9, 10,
             11, 12, 13, 14, 15, 16, 17, 18, 19, 
             21,     23, 24, 25,     27, 28, 29, 30, 
             31, 32 ,33, 34, 35,         38, 39, 40]

#for i in range(0, 40):
#    paths.append(cwd + '/' + "%02d"%(i+1) + '/final_error.txt')

for i in valid_scens:
    paths.append(path_folders + '/' + "%02d"%(i) + '/final_error.txt')

for path in paths: 
    textfile = open(path)
    ind.append(j-0.5)
    j = j+1
    for line in textfile:
        error = float(line) 
	data.append(error)

print data

f = pylab.figure()
#ax1 = f.add_subplot(111, aspect='equal') # use for default linewidth
ax1 = f.add_subplot(111)#, aspect='equal') # use for default linewidth
#ax1.plot(data[:,1], data[:,2], linewidth=0.1) # use for smaller linewidth
#ax1.plot(data[:], 'bs')
ax1.bar(ind[:], data[:], 1)
f.set_size_inches(10,3)

pylab.xlim(-0.5,32.5)
pylab.ylim(0,0.15)
pylab.savefig(filename_plot, bbox_inches='tight', format='pdf') # higher res: dpi=600
#pylab.savefig(filename_plot, bbox_inches='tight')




