import os
import subprocess

dryrun = False

def execute(cmds, num_threads):
	''' Executes system commands in parallel using up to num_threads'''
	processes = set()
	for cmd in cmds:
		print('Executing command: ' + ' '.join(cmd))
		if dryrun: continue
		#processes.add(subprocess.Popen(cmd, cwd=path))
		processes.add(subprocess.Popen(cmd))
	if len(processes) >= num_threads:
		os.wait()
		processes.difference_update(p for p in processes if p.poll() is not None)
	for p in processes:
		if p.poll() is None: 
			p.wait

base_cmd = "/home/raoul/git/edvs_release/ConvertEvents/ConvertEvents"

cwd = os.getcwd()
cmds = []


for i in range(40):
	path = "./" + "%02d" % (i+1)
	print(path)
	dirList=os.listdir(path)
	os.remove(path + "/events--omnibot=1")
	for fname in dirList:
		print fname
		if(fname=="Logfile-0.txt"):
			cmds.append((base_cmd + " --in=" + path+"/"+fname + " --out=" + path+"/"+"events" + " --omnibot=1").split(" "))

execute(cmds, 4)
			

		
