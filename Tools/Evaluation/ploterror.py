import sys
import pylab

filename_path = sys.argv[1]
filename_plot = sys.argv[2]
data = pylab.loadtxt(filename_path)
f = pylab.figure()
ax1 = f.add_subplot(211) #, aspect='equal') # use for default linewidth
ax2 = f.add_subplot(212)
#ax1.plot(data[:,1], data[:,2], linewidth=0.1) # use for smaller linewidth
#ax1.plot(data[:,0], data[:,1])
ax1.plot(data[:,1], color="#594D57")
ax1.set_xlabel("events")
ax1.set_ylabel("m")
ax1.set_ylim(0,0.5)
#pylab.ylim(0,0.5)

ax2.plot(data[:,2], color="#594D57")
ax2.set_xlabel("events")
ax2.set_ylabel("rad")
ax2.set_ylim(0,0.5)
#pylab.xlim(-2.5,2.5)
#pylab.ylim(0,0.5)
pylab.savefig(filename_plot, bbox_inches='tight') # use for default dpi
#pylab.savefig(filename_plot, bbox_inches='tight', format='pdf') # use for default dpi
#pylab.savefig(filename_plot, bbox_inches='tight', dpi=600) # use for higher dpi
#f.show()



