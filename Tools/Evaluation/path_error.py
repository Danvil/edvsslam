import sys
import math

#usage: path_error.py truth_path result_path error_output_file

#path format: time, x, y, r
def distance(x,y):
	sum = 0.0
	for i in range(len(x)):
		sum += (x[i] - y[i])**2
	return(math.sqrt(sum))

def add_pi(angle):
	return angle+math.pi

def normalize_angle(angle):
	while angle > (2 * math.pi):
		angle -= 2 * math.pi
	while angle < 0.0:
		angle += 2 * math.pi
	return angle

def angle_distance(a1, a2, delta_angle=5.6): #FIXME optimize for the 5.6 constant to reduce error
	a1_n = normalize_angle(a1-delta_angle)
	a2_n = normalize_angle(a2)
	return math.atan2(math.sin(a1_n-a2_n), math.cos(a1_n-a2_n))

def load_path(filename):
	path = []
	for line in open(filename):
		path_entry = [float(number) for number in line.split('\t')]
		path.append(path_entry)
	return path

def calculate_error(truth_path, result_path):
	errors = []
	for result_point in result_path:
		nearest_truth_point = truth_path[0]
		min_dt = math.fabs(result_point[0] - nearest_truth_point[0])
		for truth_point in truth_path:
			if(math.fabs(result_point[0] - truth_point[0]) < min_dt):
				min_dt = math.fabs(result_point[0] - truth_point[0])
				nearest_truth_point = truth_point
		error = [result_point[0], 
				distance([result_point[1], result_point[2]], [nearest_truth_point[1], nearest_truth_point[2]]),
				(math.fabs(angle_distance(result_point[3], nearest_truth_point[3])))] 
		errors.append(error)
	return errors


def save_errors(filename, errors):
	with open(filename, 'w') as error_file:
		error_file.writelines(''.join(str(number) + '\t' for number in error_line) + '\n' for error_line in errors)

save_errors(sys.argv[3], calculate_error(load_path(filename = sys.argv[1]), load_path(filename = sys.argv[2])))

