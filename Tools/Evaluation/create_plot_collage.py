import subprocess
import os

imagemagick_montage_command = "montage -geometry 300x300"

start_folder_num = 1	# first folder to process
end_folder_num = 40	# last folder to process

#list_of_dirs = []

#for x in os.walk("."):
#    print(x[0])
#    if(os.path.exists(x[0]+"/tracked_plot.png")):
#    if(os.path.exists(x[0]+"/errors.png")):
#    	list_of_dirs.append(x[0])

#list_of_dirs.sort()

paths=[]
for i in range(start_folder_num-1, end_folder_num):
#    paths.append(cwd + '/' + "%02d"%(i+1))
    paths.append('./' + "%02d"%(i+1))

#for d in list_of_dirs:
for d in paths:
#    imagemagick_montage_command += " -label " + d[2:] + " " + d+"/tracked_plot.png"
#    imagemagick_montage_command += " -label " + d[2:] + " " + d+"/tracked_plot.png"
    imagemagick_montage_command += " -label " + d[2:] + " " + d+"/errors.png"
#    imagemagick_montage_command += " -label " + d[2:] + " " + d+"/combined.png"

imagemagick_montage_command += " ./collage_errors.png"
print(imagemagick_montage_command)
subprocess.call(imagemagick_montage_command.split(' '))

