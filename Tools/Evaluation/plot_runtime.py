import sys
import os
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.mlab import griddata
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import math

base_path="/home/raoul/Documents/edvs_raoul_mocap_2_results/"

def dir_names(x):
	start_name = "s_100_b_3_ms_0.5"
	dirs = []
	for x_ in x:
		dirs.append(start_name+"_n_"+str(x_))
	return dirs

def load_times(dirs, scenarios):
	times = []
	times_variances = []
	for dir in dirs:
		#input_file = open(base_path + dir + "/" + "%02d"%scenario + "_rmse.tsv")
		#numbers = (float(number) for number in (line for line in (line.split() for line in input_file)))
		#errors.append(numbers)
		sum_of_times = 0.0
		scenario_times = []
		sum_of_squared_time_deltas = 0.0
		scenario_count = len(scenarios)
		for scenario in scenarios:
			with open(base_path + dir + "/" + "%02d"%scenario + "_time.tsv") as f:
				floats = map(float, f)
				one_scen_time= floats[1] / floats[0]
				scenario_times.append(one_scen_time)
				sum_of_times += one_scen_time
			#print base_path + dir + "/" + "%02d"%scenario + "_rmse.tsv"    	
		medium_time = sum_of_times / scenario_count
		times.append(medium_time)

		for scen_time in scenario_times:
			squared_delta_time = (scen_time - medium_time)**2
			sum_of_squared_time_deltas += squared_delta_time

		time_var = math.sqrt(sum_of_squared_time_deltas/scenario_count)
		times_variances.append(time_var)
	return times, times_variances

def plot_times(nums, times,time_var, filename_plot):
	fig = plt.figure()
	ax = fig.add_subplot(211)
	plt.plot(nums, times, color='#594D57')
	plt.errorbar(nums, times, time_var, color='#594D57')
	plt.plot(nums, times, 'ro')
	ax.set_xlim(nums[0]-5,nums[len(nums)-1]+5)
	#ax.set_ylim(0, times[0]+10000)
	fig.set_size_inches(10,10)
	plt.savefig(filename_plot, bbox_inches='tight', format='pdf')
	#plt.show()


particle_nums = [10,25,50,75,100]

valid_scens=[ 1,          4,  5,  6,  7,  8,  9, 10,
             11, 12, 13, 14, 15, 16, 17, 18,  
             21,     23, 24, 25,     27, 28, 29, 30, 
                 32 ,33, 34, 35,         38, 39, 40]


filename_plot = sys.argv[1]

times, time_var = load_times(dir_names(particle_nums), valid_scens)
plot_times(particle_nums, times, time_var, filename_plot)
print times

##################