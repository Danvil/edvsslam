import subprocess
import os

dryrun = False

def execute(paths, cmd, num_threads):
    ''' Executes system commands in parallel using up to num_threads'''
    processes = set()
    for path in paths:
	print("#######################################################")
	print("#######################################################")
        print('Executing command: ' + ' '.join(cmd) + ' in path ' + path)
	print("#######################################################")
	print("#######################################################")
        if dryrun: continue
        processes.add(subprocess.Popen(cmd, cwd=path))
        if len(processes) >= num_threads:
            os.wait()
            processes.difference_update(p for p in processes if p.poll() is not None)
    for p in processes:
        if p.poll() is None: p.wait


#cmd ="mkdir test".split(' ')

cmd = "/home/raoul/git/edvs_release/PathOptimizer/pathoptimizer --fn_ref=path.txt --fn_track=tracked_path.txt --fn_aligned_out=aligned.txt --fn_error_out=errors.txt --gui=0 --run=1".split(' ')

paths = []
cwd = os.getcwd()

for i in range(15):
    paths.append(cwd + '/' + "%02d"%(i+1))

execute(paths, cmd, 3)
