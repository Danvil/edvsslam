import sys
import os
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.mlab import griddata
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

base_path="/home/raoul/Documents/edvs_raoul_mocap_2_results/"

def dir_names(x,y):
	start_name = "s_100_b_3_"
	dirs = []
	for y_ in y:
		for x_ in x:
			dirs.append(start_name+"ms_"+str(y_)+"_n_"+str(x_))

	return dirs

def load_errors(dirs, scenarios):
	errors = []
	for dir in dirs:
		#input_file = open(base_path + dir + "/" + "%02d"%scenario + "_rmse.tsv")
		#numbers = (float(number) for number in (line for line in (line.split() for line in input_file)))
		#errors.append(numbers)
		sum_of_errs = 0
		scenario_count = len(scenarios)
		for scenario in scenarios:
			with open(base_path + dir + "/" + "%02d"%scenario + "_rmse.tsv") as f:
				floats = map(float, f)
				sum_of_errs += floats[0]
		errors.append(sum_of_errs/scenario_count)
			#print base_path + dir + "/" + "%02d"%scenario + "_rmse.tsv"    	
	return errors

def make_array(x,y):
	dx = []
	dy = []
	for y_ in y:
		dx.extend(x)

	for i in range(len(y)):
		for x_ in x:
			dy.append(y[i])
	return dx, dy

def bar_plot_errors(x, y, errors):
	z = []
	for x_ in x:
		for y_ in y:
			z.append(0)

	x, y = make_array(x,y)

	dx = 10
	dy = 0.1

	fig = plt.figure()
	ax = fig.add_subplot(111, projection = '3d')

	ax.bar3d(x, y, z, dx, dy, errors, color='white', zsort='average')
	#ax.scatter(x, y, errors, color='red')

	ax.set_xlabel("Particle Count")
	ax.set_ylabel("Motion Scale Factor")
	ax.set_zlabel("Error")

	plt.savefig("barplot.pdf", bbox_inches='tight', format='pdf')

def density_plot_errors(x, y, errors):
	num_x = len(x)
	num_y = len(y)

	x, y = make_array(x,y)	

	x = np.asarray(x)
	y = np.asarray(y)
	errors = np.asarray(errors)

	# Set up a regular grid of interpolation points
	xi, yi = np.linspace(x.min(), x.max(), num_x), np.linspace(y.min(), y.max(), num_y)
	xi, yi = np.meshgrid(xi, yi)

	# Interpolate
	#rbf = scipy.interpolate.Rbf(x, y, errors, function='linear')
	#zi = rbf(xi, yi)

	zi = griddata(x, y, errors, xi, yi)

	fig = plt.figure()
	ax = fig.add_subplot(111)

	img = ax.imshow(zi, vmin=errors.min(), vmax=errors.max(), origin='lower',
	           extent=[x.min(), x.max(), y.min(), y.max()])
	cbar = plt.colorbar(img)
	ax.set_xlabel("Particle Count")
	ax.set_ylabel("Motion Scale Factor")
	cbar.set_label("Error")
	ax.set_aspect('auto')
	plt.savefig("densityplot.pdf", bbox_inches='tight', format='pdf')

def surface_plot_errors(x, y, errors):
	num_x = len(x)
	num_y = len(y)

	x, y = make_array(x,y)	

	x = np.asarray(x)
	y = np.asarray(y)
	#z = np.asarray(errors)

	# Set up a regular grid of interpolation points
	
	xi = np.linspace(min(x), max(x))
	yi = np.linspace(min(y), max(y))
	xi, yi = np.meshgrid(xi, yi)

	zi = griddata(x, y, errors, xi, yi)

	fig = plt.figure()
	ax = fig.gca(projection='3d')
	
	surf = ax.plot_surface(xi, yi, zi, rstride=5, cstride=5, cmap=cm.jet, linewidth=1, antialiased=True)

	ax.set_zlim3d(np.min(zi), np.max(zi))
	fig.colorbar(surf)
	plt.savefig("surfaceplot.pdf", bbox_inches='tight', format='pdf')
	#plt.show()


#################################################################################

x = [10,25,50,75,100]
y = [0.1,0.25,0.5,0.75,1.0] #, 1.25]

valid_scens=[ 1,          4,  5,  6,  7,  8,  9, 
             11, 12, 13, 14, 15,  17, 18,  
             21,     23, 24, 25,     27, 28,  30, 
                 32 ,33, 34, 35,         38, 39, 40]






errors = load_errors(dir_names(x,y), valid_scens)
surface_plot_errors(x, y, errors)
bar_plot_errors(x, y, errors)
density_plot_errors(x, y, errors)

z = np.asarray(errors)
print x
print y
print z
print z.min()

##################