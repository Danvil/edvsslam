cmd_plot 	= "python ../plotpath.py tracked_path.txt tracked_plot.png".split(' ')
cmd_tracked_plot= "python ../plotpath.py path.txt plot.png".split(' ')
cmd_aligned_plot= "python ../plotpath.py aligned.txt plot.pdf".split(' ')
cmd_filtered_plot= "python ../plotpath.py filtered_ref.txt reference.pdf".split(' ')
cmd_combined_plot= "python ../plotpath.py aligned.txt combined.pdf filtered_ref.txt".split(' ')
cmd_error_plot= "python ../ploterror.py errors.txt errors.png".split(' ')

cmd_track 	= "/home/raoul/git/edvs_release/EdvsTracking/EdvsTracking --scenario=./scenario.xml --mapping=1 --p_num_test_points=20 --p_retina_fov=38.46 --run".split(' ')
cmd_align 	= "/home/raoul/git/edvs_release/PathOptimizer/pathoptimizer --fn_ref=path.txt --fn_track=tracked_path.txt --fn_aligned_out=aligned.txt --fn_error_out=errors.txt --gui=0 --run=1 --alpha=0.05 --phi=0.81 --fn_final_error_out=final_error.txt --fn_filtered_out=filtered_ref.txt".split(' ')

##############################
# change settings here
# folder names: 2 digits, from "01" to "99"
start_folder_num = 1	# first folder to process
end_folder_num = 40	# last folder to process
cmd = cmd_error_plot	# command to run in this folder (see above)
num_processes = 4	# number of processes

##############################

import subprocess
import os

dryrun = False

def execute(paths, cmd, num_threads):
    ''' Executes system commands in parallel using up to num_threads'''
    processes = set()
    for path in paths:
        print('Executing command: ' + ' '.join(cmd) + ' in path ' + path)
        if dryrun: continue
        processes.add(subprocess.Popen(cmd, cwd=path))
        if len(processes) >= num_threads:
            os.wait()
            processes.difference_update(p for p in processes if p.poll() is not None)
    for p in processes:
        if p.poll() is None: p.wait

paths = []
cwd = os.getcwd()

for i in range(start_folder_num, end_folder_num+1):
    paths.append(cwd + '/' + "%02d"%i)

execute(paths, cmd, num_processes)
