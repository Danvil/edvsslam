for D in $(find . -mindepth 1 -maxdepth 1 -type d) ; do
	if [ -e $D/tracked_path.txt ]
	then
	    echo $D
	    python plotpath.py $D/tracked_path.txt $D/tracked_plot.png
	    #cp tracked_plot.png $D/tracked_plot.png
	fi
done
