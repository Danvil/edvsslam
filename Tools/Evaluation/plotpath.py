import sys
import pylab

filename_path1 = sys.argv[1]
filename_plot = sys.argv[2]

filename_path2 = None

if len(sys.argv)==4:
    filename_path2 = sys.argv[3]

data1 = pylab.loadtxt(filename_path1)

if filename_path2:
    data2 = pylab.loadtxt(filename_path2)

f = pylab.figure()
ax1 = f.add_subplot(111, aspect='equal') # use for default linewidth
#ax1.plot(data[:,1], data[:,2], linewidth=0.1) # use for smaller linewidth
ax1.plot(data1[:,1], data1[:,2], 'r')

if filename_path2:
    ax1.plot(data2[:,1], data2[:,2], 'b')

ax1.plot(data1[0,1], data1[0,2], marker='o', markersize=20, color='white',  mew=1.5) 
ax1.plot(data1[0,1], data1[0,2], marker='x', markersize=20, mfc='black', markeredgecolor='black', mew=1.5)

ax1.set_xlabel("m")
ax1.set_ylabel("m")

pylab.xlim(-1.5,1.5)
pylab.ylim(-1.5,1.5)
pylab.savefig(filename_plot, bbox_inches='tight', format='pdf') # higher res: dpi=600
#pylab.savefig(filename_plot, bbox_inches='tight')
#f.show()
