import sys
import pylab
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import Image





filename_path = sys.argv[1]
filename_map = sys.argv[2]
filename_plot = sys.argv[3]

pixelsize=0.01
center = 704/2

data1 = pylab.loadtxt(filename_path)

data1 = np.multiply(data1,1.0/pixelsize)
data1[:,2] = np.multiply(data1[:,2],-1)
data1 = np.add(data1,center)

print type(data1)

f = pylab.figure()
ax1 = f.add_subplot(111, aspect='equal') # use for default linewidth
#ax1.plot(data[:,1], data[:,2], linewidth=0.1) # use for smaller linewidth

image = Image.open(filename_map).convert("L")
arr = np.asarray(image)
ax1.imshow(arr, cmap = cm.Greys_r)
#plt.show()

ax1.plot(data1[:,1], data1[:,2], 'r')

#pylab.xlim(0,704)
#pylab.ylim(0,704)
ax1.axis('off')
#pylab.savefig(filename_plot, bbox_inches='tight', format='pdf') # higher res: dpi=600
pylab.savefig(filename_plot, bbox_inches='tight', dpi=500)
#f.show()
