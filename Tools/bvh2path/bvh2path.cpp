//	Raoul Hoffmann, 4.2.2013
//
//	reads in a BVH file with only a root node, and convert it to a EdvsTracking-style path.txt-file

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/split.hpp>

int main(int argc, char *argv[]) {
	std::string fn_in, fn_out;
	int num_frames;
	float frame_time;

	namespace po = boost::program_options;
	po::options_description desc("Allowed options:");
	desc.add_options()
		("help", "produce help message")
		("in", po::value<std::string>(&fn_in), "filename of input file (bvh type)")
		("out", po::value<std::string>(&fn_out), "filename of output file (path.txt type)")		
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	bool header_parsing_ok = true;
	std::ifstream is(fn_in.c_str());
	std::string line;

	//check header
	getline(is, line);
  	unsigned found = line.find("HIERARCHY");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: HIERARCHY missing" << std::endl; header_parsing_ok = false;
  	}
    getline(is, line);
	found = line.find("ROOT root");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: ROOT missing" << std::endl; header_parsing_ok = false;
  	}
  	getline(is, line);
	found = line.find("{");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: '{' entry missing" << std::endl; header_parsing_ok = false;
  	}
	getline(is, line);
	found = line.find("OFFSET");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: OFFSET entry missing" << std::endl; header_parsing_ok = false;
  	}
	getline(is, line);
	found = line.find("CHANNELS");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: CHANNELS entry missing" << std::endl; header_parsing_ok = false;
  	}
	getline(is, line);
	found = line.find("}");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: '}' entry missing" << std::endl; header_parsing_ok = false;
  	}
  	getline(is, line);
	found = line.find("MOTION");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: MOTION entry missing" << std::endl; header_parsing_ok = false;
  	}

  	//using frame number for eventual warning
  	getline(is, line);

	found = line.find("Frames:");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: Frames entry missing" << std::endl; header_parsing_ok = false;
  	}
  	else {
  		if(line[line.size() - 1] == '\r') {
			line.erase(line.size() - 1, 1);
		}
		std::string frames_string("Frames:    ");
		line.erase(0, frames_string.size());
		num_frames = boost::lexical_cast<int>(line);
		std::cout << "header claims that " << num_frames << " frames exist" << std::endl;
	}

	//get frame timing
  	getline(is, line);

	found = line.find("Frame Time:");
  	if (found==std::string::npos) {
  		std::cout << "BVH Header incomplete: Frame Time entry missing" << std::endl; header_parsing_ok = false;
  		std::cout << "WARNING: setting frame time to 0.01 seconds!" << std::endl;
  		frame_time = 0.01;
  	}
  	else {
  		if(line[line.size() - 1] == '\r') {
			line.erase(line.size() - 1, 1);
		}
		std::string frame_time_string("Frame Time:    ");
		line.erase(0, frame_time_string.size());
		frame_time = boost::lexical_cast<float>(line);
		std::cout << "Time difference between frames is " << frame_time << " seconds" << std::endl;
	}

	if(header_parsing_ok)
	{
		std::cout << "Header is ok, now parsing entries..." << std::endl;
	}
	else
	{
			std::cout << "WARNING: Incomplete header! Better have a look at the BVH-file! attempt to parse entries anyway..." << std::endl;	
	}

	//parsing path entries
	std::vector<float> one_entry;
	std::string string_buffer;
	std::vector<std::vector<float> > entries;

	while(getline(is, line))
	{
		std::stringstream ss(line);
		while(ss >> string_buffer)
		{
			one_entry.push_back(boost::lexical_cast<float>(string_buffer));
		}
		entries.push_back(one_entry);
		one_entry.clear();
	}
	int num_of_parsed_lines = entries.size();

	if(num_of_parsed_lines != num_frames)
	{
		std::cout << "WARNING: number of parsed lines differs from number of entries claimed in header! Check the BVH-file!" << std::endl;	
	}
	std::cout << "\t -> lines parsed: " << num_of_parsed_lines << ", expected: " << num_frames << std::endl;

	std::ofstream ofs(fn_out.c_str());
	float entry_time = 0.0;
	for(unsigned i = 0; i < entries.size(); i++) {
		ofs << entry_time << "\t" << (entries[i][0] / 100.0f) << "\t" << (entries[i][2] / 100.0f) << "\t" << (entries[i][5] * ((2*M_PI)/360)) << std::endl;
		entry_time += frame_time;
	}
	std::cout << "output file " << fn_out << " written!" << std::endl;
	return(0);
}