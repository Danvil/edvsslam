import subprocess
import os

def execute_commands_parallel(cmds, num_threads, dryrun=False):
	''' Executes system commands in parallel using up to num_threads '''
	processes = set()
	for cmd in cmds:
		print 'Executing command:', ' '.join(cmd)
		if dryrun: continue
		processes.add(subprocess.Popen(cmd))
		if len(processes) >= num_threads:
			os.wait()
			diff = [p for p in processes if p.poll() is not None]
			processes.difference_update(diff)
	for p in processes:
		if p.poll() is None:
			p.wait()

def assure_dir(x):
	if not os.path.exists(x):
		os.makedirs(x)
