import sys
import shutil
import tools

#g_execpath = "/home/raoul/git/edvs_release/EdvsTracking/EdvsTracking"
g_execpath = "/home/david/Projects/edvs/release/PathOptimizer/pathoptimizer"

def cmd(gt, tag):
	return [g_execpath,
		"--fn_ref=" + gt,
		"--fn_track=" + tag + "path.tsv",
		"--fn_aligned_out=" + tag + "aligned.tsv",
		"--fn_error_out=" + tag + "error.tsv",
		"--fn_final_error_out=" + tag + "rmse.tsv",
		"--fn_filtered_out=" + tag + "truth_filtered.tsv",
		"--alpha=0.05",
		"--phi=0.81",
		"--gui=0",
		"--run=1"
	]

def cmds_db(db, ids, results):
	v = []
	tools.assure_dir(results)
	for i in ids:
		idstr = "%02d"%i
		gtsrc = db + "/" + idstr + "/path.txt"
		gt = results + "/" + idstr + "_truth.txt"
		shutil.copyfile(gtsrc, gt)
		tag = results + "/" + idstr + "_"
		x = cmd(gt, tag)
		v.append(x)
	return v

def cmds_db_range(db, n, results):
	return cmds_db(db, range(1, n+1), results)

# dbpath = sys.argv[1]
# n = int(sys.argv[2])
# results = sys.argv[3]
# num_threads = int(sys.argv[4]) if len(sys.argv) == 5 else 4

# cmds = cmds_db_range(execpath, dbpath, n, results)
# tools.execute_commands_parallel(cmds, num_threads)
