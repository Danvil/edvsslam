import tools
import sys
import os

def cmd_with_vis(execpath, scn, results, outvis):
	return [execpath,
		"--scenario=" + scn,
		"--results=" + results,
		"--out_vis=" + outvis,
		"--mapping=1",
		"--p_motion_scale=1.0",
		"--p_retina_fov=38.46",
		"--run"
	]

def cmd(execpath, scn, results):
	return [execpath,
		"--scenario=" + scn,
		"--results=" + results,
		"--mapping=1",
		"--p_motion_scale=1.0",
		"--p_retina_fov=38.46",
		"--no-gui",
		"--run"
	]

def assure_dir(x):
	if not os.path.exists(x):
		os.makedirs(x)

def cmds_db(execpath, path, ids, resultpath, savevis):
	v = []
	tools.assure_dir(resultpath)
	for i in ids:
		idstr = "%02d"%i
		scn = path + "/" + idstr + "/scenario.xml"
		resultstag = resultpath + "/" + idstr + "_"
		#tools.assure_dir(resultpath + "/" + idstr)
		if savevis:
			outvis = resultpath + "/" + idstr + "_images/"
			tools.assure_dir(outvis)
			x = cmd_with_vis(execpath, scn, resultstag, outvis)
		else:
			x = cmd(execpath, scn, resultstag)
		v.append(x)
	return v

def cmds_db_range(execpath, path, n, resultpath, savevis):
	return cmds_db(execpath, path, range(1, n+1), resultpath, savevis)

#execpath = "/home/raoul/git/edvs_release/EdvsTracking/EdvsTracking"
execpath = "/home/david/Projects/edvs/release/EdvsTracking/EdvsTracking"
dbpath = sys.argv[1]
n = int(sys.argv[2])
resultpath = sys.argv[3]
num_threads = int(sys.argv[4]) if len(sys.argv) == 5 else 4
savevis = False

cmds = cmds_db_range(execpath, dbpath, n, resultpath, savevis)
tools.execute_commands_parallel(cmds, num_threads)
