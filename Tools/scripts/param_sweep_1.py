import shlex, subprocess
import re
import time

name_tracker = '/home/david/Projects/edvs/release/EdvsTracking/EdvsTracking'

def params_to_name(params):
	q = ''
	callback = lambda pat: pat.group(1).upper()
	for k0,v0 in params.iteritems():
		k1 = re.sub(r'^p', r'P', k0)
		k2 = re.sub(r'_(.)', callback, k1)
		#print k0, '-->', k1, '-->', k2
		v1 = str(v0)
		v2 = re.sub(r'\.', r'_', v1)
		#print v0, '-->', v1, '-->', v2
		q = q + k2 + '_' + v2 + '_'
	return q[0:len(q)-1]

def id_to_str(id):
	if id<10:
		return '0' + str(id)
	else:
		return str(id)
	
def build_cmd(base_dir, id, params):
	q = [name_tracker,
		'--scenario', base_dir + '/' + id_to_str(id) + '/scenario.xml',
		'--result', base_dir + '/results/' + id_to_str(id) + '_' + params_to_name(params) + '.txt',
		'--no-gui']
	for k,v in params.iteritems():
		q.extend(['--' + k, str(v)])
	return q

process_queue = []
thread_count = 1

def process(args):
	global process_queue
	global thread_count
	print 'Executing command:'
	print ' '.join(args)
	while len(process_queue) > thread_count - 1:
		process_queue = [x for x in process_queue if x.poll() == None]
		time.sleep(0.1)	
	process_queue.append(subprocess.Popen(args))

base_dir = '/home/david/Documents/DataSets/edvs_dataset/'

def edvs_tracking_sweep_impl(id, params, sweep, i):
	if i == len(sweep):
		process(build_cmd(base_dir, id, params))
	else:
		k,q = sweep[i]
		for v in q:
			params[k] = v
			edvs_tracking_sweep_impl(id, params, sweep, i+1) 

def edvs_tracking_sweep(id):
	params_sweep = {
		'p_map_soften': [1,3,5],
		'p_num_particles': [25,100,400],
		'p_batch_size': [1,3,15],
		'p_num_events_resample': [1,50,250],
		'p_retina_fov': [43.67],
	}
	
	q = list(params_sweep.iteritems())
	edvs_tracking_sweep_impl(id, params, q, 0)

#process(build_cmd(base_dir, 12, params))

#edvs_tracking_sweep(12)

thread_count = 5
params = {
	'p_map_soften': 3.0,
	'p_num_particles': 100,
	'p_batch_size': 10,
	'p_num_events_resample': 100,
	'p_retina_fov': 43.67
}
for id in range(10,14):
	process(build_cmd(base_dir, id, params))
