
import hashlib
import tools
import db_align

g_params_map = {
	'n' : 'p_num_particles',
	'b' : 'p_batch_size',
	'ms' : 'p_motion_scale',
	's' : 'p_num_events_resample'	
}
g_execpath = '/home/david/Projects/edvs/release/EdvsTracking/EdvsTracking'
g_num_threads = 16
g_simulate = True

def params_to_name(params):
	m = ''
	for k,v in params.iteritems():
		m = m + k + '_' + str(v) + '_'
	return m[0:len(m)-1]

def params_to_name_md5(params):
	m = hashlib.md5()
	for k,v in params.iteritems():
		m.update(k)
		m.update(str(v))
	return m.hexdigest()

def params_to_file(path, params):
	tools.assure_dir(path)
	with open(path + "/params.txt", "w") as f:
		for k,v in params.iteritems():
			f.write(k + '=' + str(v) + '\n')

def cmd(scn, results, params):
	x = [g_execpath,
		"--scenario=" + scn,
		"--results=" + results,
		"--mapping=1",
		"--p_retina_fov=38.46",
		"--no-gui",
		"--run"
	]
	for k,v in params.iteritems():
		x.append('--' + g_params_map[k] + '=' + str(v))
	return x

def db_cmds(db_path, ids, dst_path, params):
	v = []
	tools.assure_dir(dst_path)
	for i in ids:
		idstr = "%02d"%i
		scn = db_path + "/" + idstr + "/scenario.xml"
		resultstag = dst_path + "/" + idstr + "_"
		v.append(cmd(scn, resultstag, params))
	return v

def unroll_params_impl(ranges, params_list):
	for k,v in ranges.iteritems():
		if len(v) > 1:
			for p in v:
				tmp = dict(ranges)
				tmp[k] = [p]
				unroll_params_impl(tmp, params_list)
			return
	params = {}
	for k,v in ranges.iteritems():
		params[k] = v[0]
	params_list.append(params)

def unroll_params(ranges):
 	params_list = []
	unroll_params_impl(ranges, params_list)
	return params_list

# def sweep_track(db_path, ids, dst_path, ranges):
# 	params_list = unroll_params(ranges)
# 	cmds = []
# 	for params in params_list:
# 		tmp_path = params_to_name(params)
# 		params_to_file(tmp_path, params)
# 		v = db_cmds(db_path, ids, dst_path + '/' + tmp_path, x)
# 		cmds.extend(v)
# 	print 'Executing', len(cmds), 'commands'
# 	tools.execute_commands_parallel(cmds, g_num_threads, g_simulate)

# def sweep_align(db_path, ids, dst_path, ranges):
# 	params_list = unroll_params(ranges)
# 	cmds = []
# 	for params in params_list:
# 		tmp_path = params_to_name(params)
# 		v = db_align.cmds_db(db_path, ids, dst_path + '/' + tmp_path)
# 		cmds.extend(v)
# 	print 'Executing', len(cmds), 'commands'
# 	tools.execute_commands_parallel(cmds, g_num_threads, g_simulate)

def do_sweep(ranges, fnc):
	params_list = unroll_params(ranges)
	cmds = []
	for params in params_list:
		v = fnc(params)
		cmds.extend(v)
	print 'Executing', len(cmds), 'commands'
	tools.execute_commands_parallel(cmds, g_num_threads, g_simulate)

def track_impl(db_path, ids, dst_path, params):
	tmp_path = params_to_name(params)
	params_to_file(tmp_path, params)
	return db_cmds(db_path, ids, dst_path + '/' + tmp_path, params)

def align_impl(db_path, ids, dst_path, params):
	tmp_path = params_to_name(params)
	return db_align.cmds_db(db_path, ids, dst_path + '/' + tmp_path)

def sweep_track(db_path, ids, dst_path, ranges):
	return do_sweep(ranges, lambda p: track_impl(db_path,ids,dst_path,p))

def sweep_align(db_path, ids, dst_path, ranges):
	return do_sweep(ranges, lambda p: align_impl(db_path,ids,dst_path,p))

db_path = '/home/david/Documents/DataSets/edvs_raoul_mocap_2'
param_range = {
	'n' : [50,100,250,500],
	'b' : [3],
	'ms' : [0.5,1.0,2.0],
	's' : [100]
}
n = 40
ids = range(1,n+1)
sweep_track(db_path, ids, '.', param_range)
sweep_align(db_path, ids, '.', param_range)
