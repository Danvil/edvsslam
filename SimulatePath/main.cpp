/*
 * main.cpp
 *
 *  Created on: Jun 29, 2012
 *      Author: david
 */

#include <Edvs/Tools/Path.hpp>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char** argv)
{
	std::string p_filename = "path.txt";
	std::string p_type = "line";
	double p_path_len = 3.0;
	double p_path_vel = 0.2;
	double p_dt = 0.1;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("filename", po::value<std::string>(&p_filename), "filename for output path file")
		("type", po::value<std::string>(&p_type)->default_value(p_type), "type of path (line, circle, rectangle, slalom, spiral)")
		("length", po::value<double>(&p_path_len)->default_value(p_path_len), "length of path in meter")
		("velocity", po::value<double>(&p_path_vel)->default_value(p_path_vel), "velocity in meter/second")
		("dt", po::value<double>(&p_dt)->default_value(p_dt), "sampling delta time in seconds")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help") || !vm.count("filename")) {
		std::cout << desc << std::endl;
		return 1;
	}

	std::cout << "Generating path '" << p_type << "'..." << std::endl;
	Path<double,2,true> path_spline = Edvs::PathFactory::GeneratePath<double>(p_type, p_path_len, p_path_vel);
	std::cout << "Resampling path..." << std::endl;
	Path<double,2,false> path = Edvs::Resample(path_spline, p_dt);
	std::cout << "Saving path in '" << p_filename << "'..." << std::endl;
	Edvs::SavePath(path, p_filename);
	std::cout << "Finished." << std::endl;

	return 0;
}
