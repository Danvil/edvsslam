PROJECT(ScenarioTransformPath)

set(EIGEN_INCLUDE_DIR "/usr/include/eigen3" CACHE STRING "Eigen3 include folder")

include_directories(
	${EdvsTools_SOURCE_DIR}
	${EIGEN_INCLUDE_DIR}
)

ADD_EXECUTABLE( ${PROJECT_NAME}
	main.cpp
)

TARGET_LINK_LIBRARIES(${PROJECT_NAME}
	Edvs
	EdvsTools
	boost_program_options
)