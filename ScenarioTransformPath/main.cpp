/*
 * main.cpp
 *
 *  Created on: Jun 20, 2012
 *      Author: david
 */

#include <Edvs/Tools/Scenario.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <string>

int main(int argc, char** argv)
{
	std::string p_fn_frame;
	std::string p_fn_input;
	std::string p_fn_output;
	double p_time_offset;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("frame", po::value<std::string>(&p_fn_frame), "filename for camera to map frame")
		("input", po::value<std::string>(&p_fn_input), "filename for input marker pose trajectory")
		("output", po::value<std::string>(&p_fn_output), "filename for output marker ground position trajectory")
		("time_offset", po::value<double>(&p_time_offset)->default_value(0), "time offset to apply to trajectory")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help") || !vm.count("frame") || !vm.count("input") || !vm.count("output")) {
		std::cout << desc << std::endl;
		return 1;
	}

	Edvs::Scenario::Calibration calib = Edvs::Scenario::LoadCalibration(p_fn_frame);
	std::cout << calib << std::endl;

	unsigned int n = Edvs::Scenario::TransformPath(p_fn_input, p_fn_output, calib, p_time_offset);
	std::cout << "Converted " << n << " points" << std::endl;

	return 1;
}
