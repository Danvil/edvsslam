/*
 * EdvsPF_v1.h
 *
 *  Created on: Jun 17, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_EdvsPF_v1_H_
#define EDVS_TRACKING_EdvsPF_v1_H_

#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Tracker.hpp>
#include <boost/shared_ptr.hpp>

namespace Edvs {

class EdvsPF
: public Tracker
{
public:
	EdvsPF();
	virtual ~EdvsPF() {}

	void reset(const State& initial_state);

	void step(const std::vector<Edvs::Event>& events);

	ParticleSet getParticles() const {
		return particles_;
	}

	void setParticles(const ParticleSet& particles) {
		particles_ = particles;
		on_particles_(particles_);
	}

	void addNoise(Real pos_sigma, Real rot_sigma_rad);

	unsigned int p_num_particles_;

	unsigned int p_resample_event_count_;

private:
	ParticleSet particles_;
	std::vector<Real> scores_diff_;
	int64_t last_time_;
	int64_t last_resample_time_;
	unsigned int num_events_since_resample_;
	Real total_score_max_inv_;
};

}

#endif
