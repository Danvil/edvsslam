/*
 * EventBasedTracker.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#include "EventBasedTracker.h"
#include "../Tools/TrackingTools.hpp"
#include <Edvs/Tracking/Tools/Time.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace Edvs {

const unsigned int cParticleCount = 250;
const Real cExpDecay = std::exp(-2.0); // FIXME how to choose this constant?
const Real cInitialPosSigma = 0.01;
const Real cInitialRotSigma = boost::math::constants::pi<Real>() * 5.0 / 180.0;
const Real cInitialVelPosSigma = 0.01;
const Real cInitialVelRotSigma = boost::math::constants::pi<Real>() * 5.0 / 180.0;
const Real cAccelPosSigma = 8 * 2.0;
const Real cAccelRotSigma = 8 * boost::math::constants::pi<Real>() * 180.0 / 180.0;
const unsigned int cEventCountUntilResample = 1000;

EventBasedTracker::EventBasedTracker()
{
//	for(unsigned int i=0; i<100; i++) {
//		solution_.add(i * 1000000/100,
//				State{Vec3{static_cast<Real>(i) * 0.01,0,0},0,Vec3::Zero(),0},
//				1.0);
//	}

	last_time_ = 0;

	auto rnd_pos = Tools::GaussianGenerator<Real>(0, cInitialPosSigma);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, cInitialRotSigma);
	auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cInitialVelPosSigma);
	auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cInitialVelRotSigma);
	rnd_pos.engine().seed(static_cast<unsigned int>(std::time(0)));

	// init particles
	for(unsigned int i=0; i<cParticleCount; i++) {
		State state{
			Vec3{rnd_pos(), rnd_pos(), 0}, rnd_rot(),
			Vec3{rnd_vel_pos(), rnd_vel_pos(), 0}, rnd_vel_rot()
		};
		// adjust initial guess
		state.px() += 0.1;
		//state.py() += 0.1;

		//std::cout << state << std::endl;
		particles_.push_back(Particle{state, 1.0});
		alpha_.push_back(1);
	}
}

void EventBasedTracker::notify(const std::vector<Edvs::Event>& events)
{
	if(events.size() == 0) {
		return;
	}

	int64_t current_time = static_cast<int64_t>(events.front().time);
	if(last_time_ == 0) {
		last_time_ = current_time;
	}
	int64_t dt_mus = current_time - last_time_;
	last_time_ = current_time;

	auto rnd_acc_pos = Tools::GaussianGenerator<Real>(0, cAccelPosSigma);
	auto rnd_acc_rot = Tools::GaussianGenerator<Real>(0, cAccelRotSigma);

//	std::cout << "dt=" << dt_mus << ", events=" << events.size() << std::endl;
	Real dt = static_cast<Real>(dt_mus) / 1000000.0;
	// update particles
	for(unsigned int i=0; i<particles_.size(); i++) {
		Particle& p = particles_[i];
		Real& alpha = alpha_[i];
		// apply motion model (position is changed by velocity and velocity by a random acceleration)
		p.state.pos += dt * p.state.vel_pos;
		p.state.rot += dt * p.state.vel_rot;
		Real v_len = p.state.vel_pos.norm();
		p.state.vel_pos[0] += dt * rnd_acc_pos();
		p.state.vel_pos[1] += dt * rnd_acc_pos();
		p.state.vel_rot += dt * rnd_acc_rot();
		// update score using new particle state
		Score partial_score = objective_.sumOfScores(p.state.pos.x(), p.state.pos.y(), p.state.pos.z(), p.state.rot, events) / static_cast<Score>(events.size());
		alpha = alpha / (alpha + cExpDecay);
		p.score = alpha * p.score + (1 - alpha) * partial_score;
	}
	// progress particles // TODO progress while evaluating?
	for(Particle& p : particles_) {
		p.state.pos += dt * p.state.vel_pos;
		p.state.rot += dt * p.state.vel_rot;
	}
	// resample if necessary
	cnt_events_until_resample_ += events.size();
	if(cnt_events_until_resample_ > cEventCountUntilResample) {
		cnt_events_until_resample_ -= cEventCountUntilResample;
		//std::cout << "Event Based Resampling ::  time: " << current_time << std::endl;
		// compute weights
		std::vector<Real> weights(particles_.size());
		for(unsigned int i=0; i<particles_.size(); i++) {
			weights[i] = particles_[i].score;
		}
		std::vector<Real> cdf = WeightsToCDF(weights);
		// pick by weight
		vector<size_t> pick = RandomPickCDF(cdf, cParticleCount);
		assert(pick.size() == cParticleCount);
		std::vector<Particle> new_particles(cParticleCount);
		std::vector<Real> new_alpha(cParticleCount);
		for(unsigned int i=0; i<new_particles.size(); i++) {
			new_particles[i] = particles_[pick[i]];
			new_alpha[i] = alpha_[pick[i]];
		}
		particles_ = new_particles;
		alpha_ = new_alpha;
		// solution
//		Particle mean = get_mean_10p_best(particles_);
		auto best_it = std::max_element(particles_.begin(), particles_.end(), [](const Particle& x, const Particle& y) { return x.score < y.score;  });
		Particle mean = *best_it;
//		std::cout << "best: score=" << mean.score << ", alpha=" << alpha_[best_it - particles_.begin()] << std::endl;
		//solution_.add(Time::GetCurrent(), state, score);
		solution_.add(current_time, mean);
		// notitfy about new particles
		notifySolutionObservers(solution_);
//		std::cout << "Time: " << solution_.data.back().time << "  Best: score=" << score << ", state=" << state << std::endl;
		on_particles_(particles_);
		on_particles_evaluate_(particles_, static_cast<Timestamp>(current_time));
	}

}

}
