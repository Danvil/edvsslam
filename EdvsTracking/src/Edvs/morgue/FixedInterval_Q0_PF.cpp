/*
 * FixedIntervalParticleFilter.cpp
 *
 *  Created on: 06/04/2012
 *      Author: David Weikersdorfer
 */

#include "FixedInterval_Q0_PF.hpp"
#include <Edvs/Tracking/Tools/Time.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace Edvs {

const Real cMaxPosVelocity = 0.7;
const Real cMaxRotVelocity = boost::math::constants::pi<Real>() * (90.0 / 180.0);
const Real cMaxPosAcceleration = 1.0;
const Real cMaxRotAcceleration = boost::math::constants::pi<Real>() * (100 / 180.0);

FixedInterval_Q0_PF::FixedInterval_Q0_PF()
{
	num_particles = 250;

	use_velocity_ = false;

	resample_delta_time_mus = 10000;

	resample_delta_events = 1000;

	last_resample_ = 0;

	// FIXME: if starting point unknown -> evenly distributed over whole map instead of gaussian at start point
	auto rnd_pos = Tools::GaussianGenerator<Real>(0, 0.1*cMaxPosVelocity);
	rnd_pos.engine().seed(static_cast<unsigned int>(std::time(0)));	// FIXME: seed random engine somewhere central

	ParticleSet particles_initial;

	// init particles
	for(unsigned int i=0; i<num_particles; i++) {
		State state = State::Zero();
		state.pos.x() = 0.1;// + rnd_pos();
		state.pos.y() = 0.0;// + rnd_pos();
		auto id = Q0::add_sample(particles_initial);
		Q0::set_state(particles_initial, id, state);
		Q0::set_score(particles_initial, id, 0.5);
	}

	tracker_ = Q0::make_iterative_condensation(particles_initial, num_particles);

}

void FixedInterval_Q0_PF::notify(const std::vector<Event>& events)
{
	// FIXME: prone to dropouts in data (no data in time interval longer than 10 ms)

	// do nothing if no events given
	if(events.size() == 0) {
		return;
	}

	// initialize time counter
	if(last_resample_ == 0) {
		last_resample_ = events.front().time;
	}

	// collect events into big sample for evaluation
	if(use_time_for_resampling_) {
		while(true) {
			auto last_event = std::find_if(events.begin(), events.end(),
					[this](const Event& e) { return e.time > last_resample_ + resample_delta_time_mus; });
			accum_events_.insert(accum_events_.end(), events.begin(), last_event);
			if(last_event == events.end()) {
				break;
			}
			resample();
		}
	}
	else {
		while(true) {
			assert(accum_events_.size() < resample_delta_events);
			std::size_t d = resample_delta_events - accum_events_.size();
			accum_events_.insert(accum_events_.end(), events.begin(), events.begin() + std::min(d, events.size()));
			if(accum_events_.size() < resample_delta_events) {
				break;
			}
			resample();
		}
	}

}

void FixedInterval_Q0_PF::resample()
{
	//std::cout << "Fix. Interval Resampling ::  time: " << last_resample_ << "  events: " << accum_events_.size() << std::endl;

	// evaluation State -> Score
	auto eval_fnc = [this](const State& u) {
		return objective_.sumOfScores(u.pos.x(), u.pos.y(), 0, u.rot, accum_events_);
	};

	int64_t dt = accum_events_.back().time - last_resample_;
	Real dt_s = static_cast<Real>(dt) / 1000000.0;

	// motion model State -> State
	auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cMaxPosAcceleration);
	auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cMaxRotAcceleration);
	auto rnd_pos = Tools::GaussianGenerator<Real>(0, cMaxPosVelocity);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, cMaxRotVelocity);
	std::function<State(const State& x)> motion_fnc;
	if(use_velocity_) {
		motion_fnc = [dt_s, &rnd_vel_pos, &rnd_vel_rot](State x) {
			Vec3 x_vel_pos_old = x.vel_pos;
			Real x_vel_rot_old = x.vel_rot;
			x.vel_pos.x() += dt_s * rnd_vel_pos();
			x.vel_pos.y() += dt_s * rnd_vel_pos();
			x.vel_rot += dt_s * rnd_vel_rot();
			x.pos += dt_s * 0.5 * (x.vel_pos + x_vel_pos_old);
			x.rot += dt_s * 0.5 * (x.vel_rot + x_vel_rot_old);
			return x;
		};
	}
	else {
		motion_fnc = [dt_s, &rnd_pos, &rnd_rot](State x) {
			x.pos.x() += dt_s * rnd_pos();
			x.pos.y() += dt_s * rnd_pos();
			x.rot += dt_s * rnd_rot();
			return x;
		};
	}

	tracker_.iterate(motion_fnc, eval_fnc);

	const ParticleSet& particles = tracker_.get_particles();

	// solution
	unsigned int best_10p = (10 * Q0::num_samples(particles)) / 100;
	ParticleSet best = Q0::pick_best(particles, best_10p, std::greater<Score>());

//	// compute mean
//	State mean = Q0::get_state(best, 0);
//	for(unsigned int i=1; i<Q0::num_samples(best); i++) {
//		const State& state = Q0::get_state(best, i);
//		mean.pos += state.pos;
//		mean.rot += state.rot;
//		mean.vel_pos += state.vel_pos;
//		mean.vel_rot += state.vel_rot;
//	}
//	double scl = 1.0 / static_cast<double>(Q0::num_samples(best));
//	mean.pos *= scl;
//	mean.rot *= scl;
//	mean.vel_pos *= scl;
//	mean.vel_rot *= scl;
	State mean = Q0::get_state(particles, Q0::find_best_by_score(particles, std::greater<Score>()));

	last_resample_ = accum_events_.back().time;
	accum_events_.clear();

	Score score = eval_fnc(mean);
	solution_.add(last_resample_, mean, score);
	notifySolutionObservers(solution_);
//	std::cout << "Time: " << solution_.data.back().time << "  Best: score=" << score << ", state=" << state << std::endl;

	std::vector<Particle> particle_vec(Q0::num_samples(particles));
	for(unsigned int i=0; i<particle_vec.size(); i++) {
		particle_vec[i] = { Q0::get_state(particles, i), Q0::get_score(particles, i) };
	}

	// visualize particles
	on_particles_(particle_vec);
	on_particles_evaluate_(particle_vec, static_cast<Timestamp>(last_resample_));
}

}
