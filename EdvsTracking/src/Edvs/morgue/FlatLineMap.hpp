/*
 * Map.hpp
 *
 *  Created on: Mar 1, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_MAP_HPP_
#define EDVS_TRACKING_MAP_HPP_

#include "IMap.hpp"
#include <eigen3/Eigen/Dense>
#include <vector>

namespace Edvs {

struct Line2D
{
	Vector2f start;
	Vector2f direction;

	float distanceToPoint(const Vector2f& p) {
		Vector2f x = p - start;
		// TODO assume direction has norm 1
		float lambda = x.dot(direction) / direction.squaredNorm();
		return (x - lambda * direction).norm();
	}

};

class FlatLineMap
: public IMap
{
public:
	virtual ~FlatLineMap() {}

	const std::vector<Line2D>& getLines() const {
		return lines_;
	}

	std::vector<Line2D>& getLines() {
		return lines_;
	}

private:
	std::vector<Line2D> lines_;
};

}

#endif
