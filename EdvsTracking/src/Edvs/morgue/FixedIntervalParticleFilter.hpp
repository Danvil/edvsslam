/*
 * FixedIntervalParticleFilter.hpp
 *
 *  Created on: Mar 1, 2012
 *      Author: Martin
 */

#ifndef EDVS_TRACKING_TRACKER_FIXEDINTERVALPARTICLEFILTER_HPP_
#define EDVS_TRACKING_TRACKER_FIXEDINTERVALPARTICLEFILTER_HPP_

#include <Edvs/Tracking/Tracker.hpp>

namespace Edvs {

class FixedIntervalParticleFilter
: public Tracker
{
public:
	FixedIntervalParticleFilter();
	virtual ~FixedIntervalParticleFilter() {}

	void notify(const std::vector<Event>& events);

private:
	ParticleSet particles_;
	int64_t last_resample_;
	std::vector<Event> accum_events_;

};

}

#endif
