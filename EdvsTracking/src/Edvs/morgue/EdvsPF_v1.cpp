/*
 * EdvsPF_v1.cpp
 *
 *  Created on: Jun 17, 2012
 *      Author: david
 */

#include "EdvsPF_v1.h"
#include "../Tools/TrackingTools.hpp"
#include <Edvs/Tracking/Tools/Time.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace Edvs {

const Real cStrangeConstant = 2.0 / 10000.0;
const Real cMaxPosVelocity = 0.7;
const Real cMaxRotVelocity = boost::math::constants::pi<Real>() * (120.0 / 180.0);
const Real cInitialPosSigma = 0.02;
const Real cInitialRotSigma = boost::math::constants::pi<Real>() * 5.0 / 180.0;
//const Real cInitialVelPosSigma = 0.01;
//const Real cInitialVelRotSigma = boost::math::constants::pi<Real>() * 5.0 / 180.0;
//const Real cAccelPosSigma = 8 * 2.0;
//const Real cAccelRotSigma = 8 * boost::math::constants::pi<Real>() * 180.0 / 180.0;
const int64_t cResampleDeltaTime = 5000000;
const Real cResampleDistributionDiff = 0;
const bool cSpreadIfLost = false;

EdvsPF::EdvsPF()
{
	p_num_particles_ = 100;
	p_resample_event_count_ = 100;
}

void EdvsPF::reset(const State& initial_state)
{
	last_time_ = 0;
	last_resample_time_ = 0;
	num_events_since_resample_ = 0;

	solution_.data.clear();

	auto rnd_pos = Tools::GaussianGenerator<Real>(0, cInitialPosSigma);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, cInitialRotSigma);
//	auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cInitialVelPosSigma);
//	auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cInitialVelRotSigma);

	// init particles
	particles_.resize(p_num_particles_);
	for(unsigned int i=0; i<p_num_particles_; i++) {
		State state = initial_state;
		state.pos += Vec3{rnd_pos(), rnd_pos(), 0};
		state.rot += rnd_rot();
		//state.vel_pos += Vec3{rnd_vel_pos(), rnd_vel_pos(), 0}, rnd_vel_rot()
		//std::cout << state << std::endl;
		particles_[i] = Particle{state, 1.0};
	}
	scores_diff_ = std::vector<Real>(p_num_particles_, 1.0);
	total_score_max_inv_ = 1;
}

void EdvsPF::step(const std::vector<Edvs::Event>& events)
{
	if(events.size() == 0) {
		return;
	}

	int64_t current_time = static_cast<int64_t>(events.back().time);
	if(last_time_ == 0) {
		last_time_ = current_time;
	}
	int64_t dt_mus = current_time - last_time_;
	last_time_ = current_time;

	Real dt_s = static_cast<Real>(dt_mus) / 1000000.0;

	auto rnd_pos = Tools::GaussianGenerator<Real>(0, events.size() * cStrangeConstant * cMaxPosVelocity);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, events.size() * cStrangeConstant * cMaxRotVelocity);

//	std::cout << "dt=" << dt_mus << ", events=" << events.size() << std::endl;
	Real dt = static_cast<Real>(dt_mus) / 1000000.0;
	// update particles
	double particle_scores_max = 0.0;
	double particle_scores_sum = 0.0;
//	std::vector<unsigned int> non_noise_cnts(particles_.size(), 0);
	for(unsigned int i=0; i<particles_.size(); i++) {
		Particle& p = particles_[i];
		// apply motion model (position is changed by velocity and velocity by a random acceleration)
		State& x = p.state;
		x.pos.x() += rnd_pos();
		x.pos.y() += rnd_pos();
		x.rot += rnd_rot();
		// update score using new particle state
		double ps = 0.01 + 0.99 * objective_.meanOfScores(p.state.pos.cast<float>(), p.state.rot, events);
		double& score = scores_diff_[i];
//		std::cout << score << "*" << ps << ", " << std::flush;
		score *= ps;
		particle_scores_max = std::max(particle_scores_max, score);
		particle_scores_sum += score;
	}

	num_events_since_resample_ += events.size();
	// compute scale scores such that the maximum score is 1
	// compute deviation of scores from uniform distribution
	double error_sqrt = 0.0;
	double sum_over_cnt = particle_scores_sum / static_cast<double>(particles_.size());
	double inv_max = 1.0 / particle_scores_max;
	for(unsigned int i=0; i<particles_.size(); i++) {
		double& score = scores_diff_[i];
		// compute deviation from uniform distribution
		double d = score - sum_over_cnt;
		error_sqrt += d * d;
		// score is normalized such that the max is 1
		score *= inv_max;
		particles_[i].score = score;
	}
	// error is the MSE of the individual deviations from normal distribution
	double error = std::sqrt(error_sqrt / static_cast<double>(particles_.size())) / particle_scores_sum;

	total_score_max_inv_ *= inv_max;

	on_particles_(particles_);

	// spread if lost
	if(cSpreadIfLost) {
		const Real cScoreMaxInvExpThreshold1 = 200;
		const Real cScoreMaxInvExpThreshold2 = 300;
		// are we lost?
		const Real q = std::log(total_score_max_inv_);
		if(q > cScoreMaxInvExpThreshold1) {
			// compute aberrance
			const Real aberrance = (q - cScoreMaxInvExpThreshold1) / (cScoreMaxInvExpThreshold2 - cScoreMaxInvExpThreshold1);
			// compute spread distance
			const Real dt_ms_last = static_cast<double>(current_time - last_resample_time_)/1000.0;
			// message
			std::cout << "AddNoise: time=" << current_time
					<< ", max_score_inv=" << total_score_max_inv_
					<< ", ln(max_score_inv)=" << q
					<< ", aberrance=" << aberrance
					<< ", dt=" << dt_ms_last << " ms"
					<< std::endl;
			// add noise
			const Real pos_sigma = dt_ms_last * aberrance * 0.005; // m/ms
			const Real rot_sigma = dt_ms_last * aberrance * M_PI / 180.0 * 1.0; // deg/ms
			addNoise(pos_sigma, rot_sigma);
			// reset like with resampling
			std::fill(scores_diff_.begin(), scores_diff_.end(), 1.0);
			total_score_max_inv_ = 1.0;
			on_particles_(particles_);
			on_particles_evaluate_(particles_, static_cast<Timestamp>(current_time));
			num_events_since_resample_ = 0;
			last_resample_time_ = current_time;
			return;
		}
	}

//	DEBUG
//	std::cout << error << std::endl;
//	for(unsigned int i=0; i<scores_diff_.size(); i++) {
//		std::cout << scores_diff_[i] << " ";
//	}
//	std::cout << std::endl;
//	DEBUG

	// resample if necessary
	if(
			(cResampleDistributionDiff != 0 && error >= cResampleDistributionDiff)
			|| (p_resample_event_count_ != 0 && num_events_since_resample_ >= p_resample_event_count_)
			|| (cResampleDeltaTime != 0 && current_time - last_resample_time_ >= cResampleDeltaTime)) {
		std::cout
				<< "Resample: time=" << current_time
				<< ", dt=" << static_cast<double>(current_time - last_resample_time_)/1000.0 << "ms" << " (max=" << cResampleDeltaTime/1000.0 << ")"
				<< ", #events=" << num_events_since_resample_
				<< ", error=" << error << " (max=" << cResampleDistributionDiff << ")"
				<< ", max_score_inv=" << total_score_max_inv_
				<< std::endl;
		//std::cout << "Event Based Resampling ::  time: " << current_time << std::endl;
		// compute weights from scores
		std::vector<Real> cdf = WeightsToCDF(scores_diff_);
		// pick by weight
		vector<size_t> pick = RandomPickCDF(cdf, p_num_particles_);
		assert(pick.size() == p_num_particles_);
		std::vector<Particle> new_particles(p_num_particles_);
		for(unsigned int i=0; i<new_particles.size(); i++) {
			new_particles[i] = particles_[pick[i]];
		}
		// set scores to 1
		particles_ = new_particles;
		scores_diff_.resize(p_num_particles_);
		std::fill(scores_diff_.begin(), scores_diff_.end(), 1.0);
		total_score_max_inv_ = 1.0;
		// solution
//		Particle mean = get_mean_10p_best(particles_);
		auto best_it = std::max_element(particles_.begin(), particles_.end(), [](const Particle& x, const Particle& y) { return x.score < y.score;  });
		Particle mean = *best_it;
		//solution_.add(Time::GetCurrent(), state, score);
		solution_.add(current_time, mean);
		std::cout << "Solution: " << mean.state.pos.x() << " " << mean.state.pos.y() << " " << mean.state.rot << std::endl;
		// notitfy about new particles
		notifySolutionObservers(solution_);
//		std::cout << "Time: " << solution_.data.back().time << "  Best: score=" << score << ", state=" << state << std::endl;
		on_particles_(particles_);
		on_particles_evaluate_(particles_, static_cast<Timestamp>(current_time));
		// prepare next step
		last_resample_time_ = current_time;
		num_events_since_resample_ = 0;
		return;
	}

}

void EdvsPF::addNoise(Real pos_sigma, Real rot_sigma_rad)
{
	auto noise_pos = Edvs::Tools::GaussianGenerator<Real>(0, pos_sigma);
	auto noise_rot = Edvs::Tools::GaussianGenerator<Real>(0, rot_sigma_rad);
	for(Edvs::Particle& p : particles_) {
		p.state.pos.x() += noise_pos();
		p.state.pos.y() += noise_pos();
		p.state.rot += noise_rot();
	}
}


}
