/*
 * FixedIntervalParticleFilter.cpp
 *
 *  Created on: Mar 1, 2012
 *      Author: david
 */

#include "FixedIntervalParticleFilter.hpp"
#include "../Tools/TrackingTools.hpp"
#include <Edvs/Tracking/Tools/Time.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>


namespace Edvs {

const unsigned int cParticleCount = 250;
const Real cVelPosSigma = 0.01; 											// FIXME
const Real cVelRotSigma = boost::math::constants::pi<Real>() * (5.0 / 180.0); // FIXME
const int64_t cTimeUntilResampleMus = 10000;

FixedIntervalParticleFilter::FixedIntervalParticleFilter()
{
	last_resample_ = 0;

	// FIXME: if starting point unknown -> evenly distributed over whole map instead of gaussian at start point
	auto rnd_pos = Tools::GaussianGenerator<Real>(0, cVelPosSigma);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, cVelRotSigma);
	auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cVelPosSigma);
	auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cVelRotSigma);
	rnd_pos.engine().seed(static_cast<unsigned int>(std::time(0)));	// FIXME: seed random engine somewhere central

	// init particles
	for(unsigned int i=0; i<cParticleCount; i++) {
		State state{
			Vec3{rnd_pos(), rnd_pos(), 0},
			0,//rnd_rot(),
			Vec3{rnd_vel_pos(), rnd_vel_pos(), 0},
			0//rnd_vel_rot()
		};
		// adjust start position
		state.px() += 0.1;
		//state.py() += 0.1;

		//std::cout << state << std::endl;
		particles_.push_back(Particle{state, 0.5});
	}
}

void FixedIntervalParticleFilter::notify(const std::vector<Event>& events)
{
	bool resample = false;

	if(events.size() == 0) {
		return;
	}

	// update time counter
	if(last_resample_ == 0) {
		last_resample_ = events.front().time;
	}

	// collect events into big sample for evaluation
	std::vector<Event>::const_iterator last_event;
	for( std::vector<Event>::const_iterator it = events.begin(); it < events.end() ;it++){
		last_event = it;
		if(it->time - last_resample_ > cTimeUntilResampleMus){
			resample = true;
			break;
		}
	}
	accum_events_.insert(accum_events_.end(), events.begin(), last_event);

	// FIXME: prone to dropouts in data (no data in time interval longer than 10 ms)
	// resample if necessary
	if(resample) {
		last_resample_ += cTimeUntilResampleMus;
		//std::cout << "Fix. Interval Resampling ::  time: " << last_resample_ << "  events: " << accum_events_.size() << std::endl;


 	 	// Condensation algorithm like M. Isard and A. Blake

		// TODO: weights as a private variable
		// get weights
		std::vector<Real> weights(particles_.size());
		for(unsigned int i=0; i<particles_.size(); i++) {
			weights[i] = particles_[i].score;
		}
		std::vector<Real> cdf = WeightsToCDF(weights);

		// pick by weight
		vector<size_t> pick = RandomPickCDF(cdf, cParticleCount);
		assert(pick.size() == cParticleCount);
		std::vector<Particle> new_particles(cParticleCount);
		for(unsigned int i=0; i<new_particles.size(); i++) {
			new_particles[i] = particles_[pick[i]];
		}
		particles_ = new_particles;

		// add motion model randomness
		auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cVelPosSigma);
		auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cVelRotSigma);
		for(unsigned int i=0; i<particles_.size(); i++) {
			particles_[i].state.vel_pos[0] += rnd_vel_pos();
			particles_[i].state.vel_pos[1] += rnd_vel_pos();
			particles_[i].state.vel_rot += rnd_vel_rot();
		}

		// progress particles
		for(Particle& p : particles_) {
			p.state.pos += (cTimeUntilResampleMus/1000000.0) * p.state.vel_pos;
			p.state.rot += (cTimeUntilResampleMus/1000000.0) * p.state.vel_rot;
		}

		// reset accumulation process
		accum_events_.clear();
		accum_events_.insert(accum_events_.end(), last_event, events.end());
		for(Particle& p : particles_){
			p.score = 0.5;
		}

		//update particle scores
		for(unsigned int i=0; i<particles_.size(); i++) {
			Particle& p = particles_[i];
			p.score += objective_.sumOfScores(p.state.pos.x(), p.state.pos.y(), p.state.pos.z(), p.state.rot, accum_events_);
		}

		// solution
		Particle mean = get_mean_10p_best(particles_);
		solution_.add(last_resample_, mean);
		notifySolutionObservers(solution_);
//		std::cout << "Time: " << solution_.data.back().time << "  Best: score=" << score << ", state=" << state << std::endl;

		// visualize particles
		on_particles_(particles_);
		on_particles_evaluate_(particles_, static_cast<Timestamp>(last_resample_));
/*


		// Own idea

		// progress particles
		for(Particle& p : particles_.particles()) {
			p.state.pos += (cTimeUntilResampleMus/1000000.0) * p.state.vel_pos;
			p.state.rot += (cTimeUntilResampleMus/1000000.0) * p.state.vel_rot;
		}

		//update particle scores
		for(unsigned int i=0; i<particles_.size(); i++) {
			Particle& p = particles_[i];
			for(const Edvs::Event& e : accum_events_) {
				// get event score
				p.score += map_->evaluate(retinaPixelToWorldRay(p.state, e.x, e.y));
			}
		}

		// compute weights
		std::vector<Real> weights(particles_.size());
		for(unsigned int i=0; i<particles_.size(); i++) {
			weights[i] = particles_[i].score;
		}
		std::vector<Real> cdf = WeightsToCDF(weights);

		// pick by weight
		vector<size_t> pick = RandomPickCDF(cdf, cParticleCount);
		assert(pick.size() == cParticleCount);
		std::vector<Particle> new_particles(cParticleCount);
		for(unsigned int i=0; i<new_particles.size(); i++) {
			new_particles[i] = particles_[pick[i]];
		}
		particles_.particles() = new_particles;

		// add motion model randomness
		auto rnd_vel_pos = Tools::GaussianGenerator<Real>(0, cVelPosSigma);
		auto rnd_vel_rot = Tools::GaussianGenerator<Real>(0, cVelRotSigma);
		for(unsigned int i=0; i<particles_.size(); i++) {
			particles_[i].state.vel_pos[0] += rnd_vel_pos();
			particles_[i].state.vel_pos[1] += rnd_vel_pos();
			//particles_[i].state.vel_rot += rnd_vel_rot();
		}

		// solution
		Score score;
		State state = particles_.getMean10PBest(score);

		//solution_.add(Time::GetCurrent(), state, score);
		solution_.add(last_resample_, state, score);
		notifySolutionObservers(solution_);

//		std::cout << "Time: " << solution_.data.back().time << "  Best: score=" << score << ", state=" << state << std::endl;

		// visualize particles
		on_particles_(particles_);
		on_particles_evaluate_(particles_, static_cast<Timestamp>(last_resample_));

		// reset accumulation process
		accum_events_.clear();
		accum_events_.insert(accum_events_.end(), last_event, events.end());
		for(Particle& p : particles_.particles()){
			p.score = 0.5;
		}

*/

	}


}

}
