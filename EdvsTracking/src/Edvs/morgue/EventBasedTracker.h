/*
 * EventBasedTracker.h
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_EVENTBASEDTRACKER_H_
#define EDVS_TRACKING_EVENTBASEDTRACKER_H_

#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Tracker.hpp>
#include <boost/shared_ptr.hpp>

namespace Edvs {

class EventBasedTracker
: public Tracker
{
public:
	EventBasedTracker();
	virtual ~EventBasedTracker() {}

	void notify(const std::vector<Edvs::Event>& events);

private:
	ParticleSet particles_;
	std::vector<Real> alpha_;
	unsigned int cnt_events_until_resample_;
	int64_t last_time_;
};

}

#endif
