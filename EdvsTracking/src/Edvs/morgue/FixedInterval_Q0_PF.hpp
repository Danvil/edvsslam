/*
 * FixedInterval_Q0_PF.hpp
 *
 *  Created on: 06/04/2012
 *      Author: David Weikersdorfer
 */

#ifndef EDVS_TRACKING_TRACKER_FixedInterval_Q0_PF_HPP_
#define EDVS_TRACKING_TRACKER_FixedInterval_Q0_PF_HPP_

#include <Edvs/Tracking/Tracker.hpp>
#include <QuestZero/Common/StateScoreVector.h>
#include <QuestZero/Tracking/experimental/CondensationIterative.h>

namespace Edvs {

class FixedInterval_Q0_PF
: public Tracker
{
public:
	FixedInterval_Q0_PF();
	virtual ~FixedInterval_Q0_PF() {}

	void notify(const std::vector<Event>& events);

	unsigned int num_particles;

	bool use_velocity_;

	bool use_time_for_resampling_;

	int64_t resample_delta_time_mus;

	unsigned int resample_delta_events;

private:
	void resample();

private:
	typedef Q0::StateScoreVector<State,Score> ParticleSet;

	// Condensation algorithm like M. Isard and A. Blake
	Q0::CondensationIterative<ParticleSet> tracker_;

	int64_t last_resample_;
	std::vector<Event> accum_events_;

};

}

#endif
