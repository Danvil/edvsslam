/*
 * EventObserver.hpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef EVENTOBSERVER_HPP_
#define EVENTOBSERVER_HPP_

#include <Edvs/Event.hpp>
#include <vector>

namespace Edvs {

class EventObserver
{
public:
	virtual ~EventObserver() {}

	virtual void notify(const std::vector<Edvs::Event>& events) = 0;

};

}

#endif
