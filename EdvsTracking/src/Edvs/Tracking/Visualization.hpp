/*
 * Visualization.hpp
 *
 *  Created on: Mar 1, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_VISUALIZATION_HPP_
#define EDVS_TRACKING_VISUALIZATION_HPP_

#include <QtGui/QImage>
#include <QtGui/QPaintDevice>
#include <boost/shared_ptr.hpp>
#include <string>
#include <map>

namespace Candy { class Engine; }

namespace Edvs
{

class Visualization
{
public:
	Visualization(const std::string& name)
	: name_(name), is_enabled_(true), is_repaint_required_(true) {}

	virtual ~Visualization() {}

	/** Gets the name for the visualization */
	const std::string& getName() const {
		return name_;
	}

	void setName(const std::string& name) {
		name_ = name;
	}

	void enable() {
		is_enabled_ = true;
	}

	void disable() {
		is_enabled_ = false;
	}

	void setEnabled(bool x) {
		is_enabled_ = x;
	}

	bool isEnabled() const {
		return is_enabled_;
	}

	bool requiresRepaint() const {
		return is_repaint_required_;
	}

	void clearRequireRepaint() {
		is_repaint_required_ = false;
	}

protected:
	void requireRepaint() {
		is_repaint_required_ = true;
	}

private:
	std::string name_;

	bool is_enabled_;

	bool is_repaint_required_;

};

class VisualizationImage
: public Visualization
{
public:
	VisualizationImage(const std::string& name)
	: Visualization(name), display_scale_(1.0f) {}

	virtual ~VisualizationImage() {}

	void setScale(float s) {
		display_scale_ = s;
	}

	float getScale() const {
		return display_scale_;
	}

	void setImagePaintDevice(unsigned int width, unsigned int height) {
		image_ .reset(new QImage(width, height, QImage::Format_ARGB32));
	}

	/** Gets current visualization images */
	QImage* getImage() const {
		return image_.get();
	}

private:
	float display_scale_;

	boost::shared_ptr<QImage> image_;

};

class Visualization2D
: public Visualization
{
public:
	Visualization2D(const std::string& name)
	: Visualization(name) {}

	virtual ~Visualization2D() {}

	virtual void paint(QPainter& painter) = 0;

};

class Visualization3D
: public Visualization
{
public:
	Visualization3D(const std::string& name)
	: Visualization(name) {}

	virtual ~Visualization3D() {}

	virtual void render() = 0;

	boost::shared_ptr<Candy::Engine> candy_engine_;
	
};

}

#endif
