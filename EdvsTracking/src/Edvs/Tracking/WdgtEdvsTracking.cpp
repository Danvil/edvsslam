#include "WdgtEdvsTracking.h"
#include "WdgtVisualizer.h"
#include "Tools/Time.hpp"
#include "Tools/Random.hpp"
#include <QtGui/QDesktopWidget>
#include <QtGui/QFileDialog>

const char* cDeviceName = "/dev/ttyUSB1";
const unsigned int cSpinThreadWait = 0; // in ms
const unsigned int cVisThreadWait = 50; // in ms
const unsigned int cTickDebugInterval = 3000; // in ms
const std::string cDatabasePath = "/home/david/Documents/DataSets/2012-06-01 EDVS Roboter/";

EdvsTracking::EdvsTracking(bool show_gui, QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);

	Edvs::Time::Restart();

	// set up tracking control
	tracking_.reset(new Tracking(show_gui));

	// resize widget
	QSize app_size = QApplication::desktop()->size();
	app_size.setWidth(std::min(app_size.width(), 1920));
	app_size.setHeight(app_size.height() / 2);
	this->resize(app_size);

	// connect gui events
	connect(ui.pushButtonRunPause, SIGNAL(clicked()), this, SLOT(onClickedRunPause()));
	connect(ui.pushButtonStep, SIGNAL(clicked()), this, SLOT(onClickedStep()));
	connect(ui.pushButtonReset, SIGNAL(clicked()), this, SLOT(onClickedReset()));

	connect(ui.doubleSpinBoxTrackerGamma, SIGNAL(valueChanged(double)), this, SLOT(onChangedTrackerParameter()));
	connect(ui.spinBoxTrackerNumParticles, SIGNAL(valueChanged(int)), this, SLOT(onChangedTrackerParameter()));
	connect(ui.spinBoxTrackerResampleEventCount, SIGNAL(valueChanged(int)), this, SLOT(onChangedTrackerParameter()));
	connect(ui.spinBoxTrackerBatchSize, SIGNAL(valueChanged(int)), this, SLOT(onChangedTrackerParameter()));
	onChangedTrackerParameter();

	connect(ui.doubleSpinBoxAddNoiseMeanPos, SIGNAL(valueChanged(double)), this, SLOT(onChangedAddNoiseMeanPos(double)));
	connect(ui.doubleSpinBoxAddNoiseSigmaPos, SIGNAL(valueChanged(double)), this, SLOT(onChangedAddNoiseSigmaPos(double)));
	connect(ui.doubleSpinBoxAddNoiseMeanRot, SIGNAL(valueChanged(double)), this, SLOT(onChangedAddNoiseMeanRot(double)));
	connect(ui.doubleSpinBoxAddNoiseSigmaRot, SIGNAL(valueChanged(double)), this, SLOT(onChangedAddNoiseSigmaRot(double)));
	connect(ui.pushButtonAddNoise, SIGNAL(clicked()), this, SLOT(onClickedAddNoise()));

	connect(ui.pushButtonComputeObjective, SIGNAL(clicked()), this, SLOT(onClickedComputeObjective()));
	connect(ui.horizontalSliderObjectiveRot, SIGNAL(valueChanged(int)), this, SLOT(onChangedObjectiveRot(int)));

	connect(ui.checkBoxVisDisableAll, SIGNAL(stateChanged(int)), this, SLOT(onChangedVisDisableAll(int)));
	connect(ui.checkBoxVisMap, SIGNAL(stateChanged(int)), this, SLOT(onChangedVisMap(int)));
	connect(ui.checkBoxVisRetina, SIGNAL(stateChanged(int)), this, SLOT(onChangedVisRetina(int)));
	connect(ui.checkBoxVisRetinaObjective, SIGNAL(stateChanged(int)), this, SLOT(onChangedVisRetinaObjective(int)));
	connect(ui.checkBoxVisPaths, SIGNAL(stateChanged(int)), this, SLOT(onChangedVisPaths(int)));

	connect(ui.pushButtonSaveMap, SIGNAL(clicked()), this, SLOT(onClickedSaveMap()));

	// set up tick timer
	connect(&timer_notify_, SIGNAL(timeout()), this, SLOT(tickNotify()));
	timer_notify_.setInterval(cSpinThreadWait);
	timer_notify_.start();

	// set up debug tick timer
	connect(&timer_debug_, SIGNAL(timeout()), this, SLOT(tickDebug()));
	timer_debug_.setInterval(cTickDebugInterval);
	timer_debug_.start();

	// set up visualization timer
	if(show_gui) {
		connect(&timer_vis_, SIGNAL(timeout()), this, SLOT(tickVis()));
		timer_vis_.setInterval(cVisThreadWait);
		timer_vis_.start();
	}

}

EdvsTracking::~EdvsTracking()
{

}

void EdvsTracking::loadScenario(const std::string& filename)
{
	tracking_->loadScenario(filename);
	ui.labelNoteMap->setText(QString::fromStdString(tracking_->getMapInfoString()));
	ui.labelNotePath->setText(QString::fromStdString(tracking_->getPathInfoString()));
	ui.labelNoteEvents->setText(QString::fromStdString(tracking_->getEventsInfoString()));
}

void EdvsTracking::loadMap(const std::string& filename)
{
	tracking_->loadMap(filename);
	ui.labelNoteMap->setText(QString::fromStdString(tracking_->getMapInfoString()));
}

void EdvsTracking::loadPath(const std::string& filename)
{
	tracking_->loadPath(filename);
	ui.labelNotePath->setText(QString::fromStdString(tracking_->getPathInfoString()));
}

void EdvsTracking::loadEvents(const std::string& filename)
{
	tracking_->loadEventSource(filename, false);
	ui.labelNoteEvents->setText(QString::fromStdString(tracking_->getEventsInfoString()));
}

void EdvsTracking::setExploration(const std::string& robot_address)
{
	tracking_->setExploration(robot_address);
}


void EdvsTracking::disableVisualizations()
{
	ui.checkBoxVisDisableAll->setChecked(true);
}

void EdvsTracking::setResultsTag(const std::string& tag)
{
	tracking_->setResultsTag(tag);
}

void EdvsTracking::setVisOutputPath(const std::string& path)
{
	p_save_vis_path_ = path;
}

void EdvsTracking::setParameters(const TrackingParameters& params)
{
	tracking_->setParameters(params);
	ui.doubleSpinBoxTrackerGamma->setValue(params.gamma);
	ui.spinBoxTrackerNumParticles->setValue(params.num_particles);
	ui.spinBoxTrackerResampleEventCount->setValue(params.num_events_resample);
	ui.spinBoxTrackerBatchSize->setValue(params.batch_size);
}

void EdvsTracking::run()
{
	if(tracking_->isRunning() && !tracking_->isPaused()) {
		tracking_->pause();
	}
	else {
		tracking_->run();
	}
	if(tracking_->isRunning() && !tracking_->isPaused()) {
		ui.pushButtonRunPause->setText("Pause");
	}
	else {
		ui.pushButtonRunPause->setText("Run");
	}
}

void EdvsTracking::onClickedRunPause()
{
	run();
}

void EdvsTracking::onClickedStep()
{
	tracking_->step();
}

void EdvsTracking::onClickedReset()
{
	tracking_->reset();
}

void EdvsTracking::onChangedTrackerParameter()
{
	TrackingParameters params = tracking_->getParameters();
	params.gamma = ui.doubleSpinBoxTrackerGamma->value();
	params.num_particles = ui.spinBoxTrackerNumParticles->value();
	params.num_events_resample = ui.spinBoxTrackerResampleEventCount->value();
	params.batch_size = ui.spinBoxTrackerBatchSize->value();
	tracking_->setParameters(params);
}

void EdvsTracking::onClickedSaveMap()
{
	tracking_->saveMap();
}


void EdvsTracking::tickNotify()
{
	// tick
	bool is_finished = tracking_->tick();
	ui.labelNoteMap->setText(QString::fromStdString(tracking_->getMapInfoString()));
	// quit if finished and no gui
	if(!is_finished && !this->isVisible()) {
		QApplication::quit();
	}
}

void EdvsTracking::tickDebug()
{
	tracking_->tickDebug();
}

void EdvsTracking::tickVis()
{
	for(auto v : tracking_->getVisualizations()) {
		// find (and create if not found) the vis widget to use for visualization
		Edvs::WdgtVisualizer* wdgt_vis;
		std::string name = v->getName();
		auto it = vis_wdgts_.find(name);
		if(it == vis_wdgts_.end()) {
			wdgt_vis = new Edvs::WdgtVisualizer(v);
			if(!p_save_vis_path_.empty()) {
				wdgt_vis->enableSaveToFile(p_save_vis_path_);
			}
			// add as mdi sub window
			QMdiSubWindow * sub_wdgt = new QMdiSubWindow();
			sub_wdgt->setWidget(wdgt_vis);
			sub_wdgt->setWindowTitle(QString::fromStdString(name));
			sub_wdgt->setAttribute(Qt::WA_DeleteOnClose);
			sub_wdgt->setMinimumSize(256,256);
			ui.mdiArea->addSubWindow(sub_wdgt);
			vis_wdgts_[name] = sub_wdgt;
			sub_wdgt->show();
			std::cout << "Created visualization widget '" << name << "'" << std::endl;
		}
		else {
			wdgt_vis = dynamic_cast<Edvs::WdgtVisualizer*>(it->second->widget());
		}
		assert(wdgt_vis != 0);
		// only continue if repaint is required
		if(!v->requiresRepaint()) {
			continue;
		}
		v->clearRequireRepaint();
		// call repaint
		wdgt_vis->repaint();
	}
}

void EdvsTracking::onChangedAddNoiseMeanPos(double val)
{

}

void EdvsTracking::onChangedAddNoiseSigmaPos(double val)
{

}

void EdvsTracking::onChangedAddNoiseMeanRot(double val)
{

}

void EdvsTracking::onChangedAddNoiseSigmaRot(double val)
{

}

void EdvsTracking::onClickedAddNoise()
{
	tracking_->addNoise(
			ui.doubleSpinBoxAddNoiseMeanPos->value(), ui.doubleSpinBoxAddNoiseSigmaPos->value(),
			ui.doubleSpinBoxAddNoiseMeanRot->value(), ui.doubleSpinBoxAddNoiseSigmaRot->value());
}

void EdvsTracking::onClickedComputeObjective()
{
	tracking_->computeObjectiveFunction();
}

void EdvsTracking::onChangedObjectiveRot(int val)
{
	tracking_->setObjectiveRotation(val);
}

void EdvsTracking::onChangedVisDisableAll(int val)
{
	if(val == Qt::Checked) {
		tracking_->setEnabledVisualization("GridMap", false);
		tracking_->setEnabledVisualization("Retina", false);
		tracking_->setEnabledVisualization("RetinaObjective", false);
		tracking_->setEnabledVisualization("Paths", false);
	}
	else {
		tracking_->setEnabledVisualization("GridMap", ui.checkBoxVisMap->checkState());
		tracking_->setEnabledVisualization("Retina", ui.checkBoxVisRetina->checkState());
		tracking_->setEnabledVisualization("RetinaObjective", ui.checkBoxVisRetinaObjective->checkState());
		tracking_->setEnabledVisualization("Paths", ui.checkBoxVisPaths->checkState());
	}
	ui.checkBoxVisMap->setEnabled(val == Qt::Unchecked);
	ui.checkBoxVisRetina->setEnabled(val == Qt::Unchecked);
	ui.checkBoxVisRetinaObjective->setEnabled(val == Qt::Unchecked);
	ui.checkBoxVisPaths->setEnabled(val == Qt::Unchecked);
}

void EdvsTracking::onChangedVisMap(int val)
{
	tracking_->setEnabledVisualization("GridMap", val == Qt::Checked);
}

void EdvsTracking::onChangedVisRetina(int val)
{
	tracking_->setEnabledVisualization("Retina", val == Qt::Checked);
}

void EdvsTracking::onChangedVisRetinaObjective(int val)
{
	tracking_->setEnabledVisualization("RetinaObjective", val == Qt::Checked);
}

void EdvsTracking::onChangedVisPaths(int val)
{
	tracking_->setEnabledVisualization("Paths", val == Qt::Checked);
}
