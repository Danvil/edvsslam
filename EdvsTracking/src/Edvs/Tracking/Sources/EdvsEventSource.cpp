/*
 * EdvsEventSource.cpp
 *
 *  Created on: Mar 2, 2012
 *      Author: david
 */

#include "EdvsEventSource.hpp"

namespace Edvs {

EdvsEventSource::EdvsEventSource(const std::string& uri, bool use_file_mode)
: last_event_time_(0), time_add_(0), use_file_mode_(use_file_mode)
{
	event_stream_ = Edvs::EventStream(uri);
	if(!use_file_mode) {
		event_capture_ = Edvs::EventCapture(event_stream_,
			boost::bind(&EdvsEventSource::onEvents, this, _1));
	}
}

EdvsEventSource::~EdvsEventSource()
{
}

void EdvsEventSource::spin(time_t t, Timespan dt)
{
//	std::cout << "events: t=" << t << std::endl;
	// get events
	if(use_file_mode_) {
		std::vector<Edvs::Event> latest_events;
		// read events until time
		while(true) {
			auto it = std::lower_bound(events_.begin(), events_.end(), t,
				[](const Edvs::Event& e, uint64_t t) {
					return e.t < t;
				});
			// move processed events
			latest_events.insert(latest_events.end(), events_.begin(), it);
			events_.erase(events_.begin(), it);
			// need more events?
			if(!events_.empty()) {
				break;
			}
			// get events
			std::vector<Edvs::Event> v;
			while(!event_stream_.eos()) {
				v.resize(1024);
				event_stream_.read(v);
				if(v.size() > 0) break;
			}
			events_.insert(events_.end(), v.begin(), v.end());
			// break if eos is reached
			if(event_stream_.eos()) {
				break;
			}
		}
		// notify observers
//		if(latest_events.size() > 0) {
//			std::cout << "New events: t=" << latest_events.back().t << ", n=" << latest_events.size() << std::endl;
//		}
//		else {
//			std::cout << "New events: n=" << latest_events.size() << std::endl;
//		}
//		std::cout << "Event Source: t=" << t << ", n=" << latest_events.size() << std::endl;
		observer_signal_(latest_events);
	}
	else {
		// get stored events
		std::vector<Edvs::Event> latest_events;
		{
			boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
			latest_events = events_;
			events_.clear();
		}
		// filter if necessary
		constexpr unsigned int max_events = 1000;
		std::vector<Edvs::Event> filtered_events;
		int num_events = latest_events.size();

		if(num_events > max_events) {
			filtered_events.reserve(max_events);
			int i = 0;
			for(const Edvs::Event& e : latest_events)
			{
				i += max_events;
				if(i >= num_events)
				{
					filtered_events.push_back(e);
					i -= num_events;
				}
			}
			std::size_t dropped = latest_events.size() - filtered_events.size();
			std::cout << "Dropped " << dropped << " events!" << std::endl;
		}
		else {
			filtered_events = latest_events;
		}
		// notify observers
//		std::cout << "Event Source: t=" << t << ", n=" << filtered_events.size() << std::endl;
		observer_signal_(filtered_events);
	}
}

bool EdvsEventSource::eof() const
{
	return event_stream_.eos();
}

void EdvsEventSource::onEvents(const std::vector<Edvs::Event>& events)
{
	// store events for later
	boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
	events_.insert(events_.end(), events.begin(), events.end());
}

}
