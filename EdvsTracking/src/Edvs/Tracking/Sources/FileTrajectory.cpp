/*
 * FileTrajectory.cpp
 *
 *  Created on: Mar 3, 2012
 *      Author: user
 */

#include "FileTrajectory.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>

namespace Edvs {

FileTrajectory::FileTrajectory() {
}

FileTrajectory::~FileTrajectory() {
	// TODO Auto-generated destructor stub
}

std::vector< std::vector<double> > FileTrajectory::trajectory(){
	return trajectory_;
}

struct TrajectoryEntry
{
	unsigned long time;
	double x, y, phi;
};

TrajectoryEntry ParseTrajectoryEntry(const std::string& line, bool read_time)
{
	TrajectoryEntry e;
	// tab separated values
	// time x y orientation  (time maybe always 0, if no time ins included in the data)
	//3976	98.4259809	148.546181  3.67834
	std::vector<std::string> tokens;
	boost::split(tokens, line, boost::is_any_of("\t"));
	try {
		if(read_time){
			e.time = 1000000.0*boost::lexical_cast<double>(tokens[0]);
			e.x = boost::lexical_cast<double>(tokens[1]);
			e.y = boost::lexical_cast<double>(tokens[2]);
			e.phi = boost::lexical_cast<double>(tokens[3]);
		}
		else{
			e.time = 0;
			e.x = boost::lexical_cast<double>(tokens[0]);
			e.y = boost::lexical_cast<double>(tokens[1]);
			e.phi = boost::lexical_cast<double>(tokens[2]);
		}
	}
	catch(boost::bad_lexical_cast& exc) {
		std::cerr << "Bad line:" << line << std::endl;
		for(std::string& t : tokens) {
			try {
				boost::lexical_cast<double>(t);
			}
			catch(boost::bad_lexical_cast&) {
				std::cerr << "Bad token: " << t << std::endl;
			}
		}
		throw exc;
	}
	return e;
}

void FileTrajectory::load(const std::string& filename, bool read_time, float scl) {
	std::ifstream is(filename.c_str());
	std::string line;
	std::vector<double> temp;
	temp.resize(4);
	while(getline(is, line)) {
		if(line[line.size() - 1] == '\r') {
			line.erase(line.size() - 1, 1);
		}
		// parse line
		TrajectoryEntry e = ParseTrajectoryEntry(line, read_time);
		e.x *= scl;
		e.y *= scl;
		temp[0] = e.time;
		temp[1] = e.x;
		temp[2] = e.y;
		temp[3] = e.phi;
		trajectory_.push_back(temp);
		solution_.add(e.time, State{Vec3{e.x, e.y, 0}, e.phi}, 1.0);
	}
	std::cout << "Parsed " << trajectory_.size() << " trajectory steps" << std::endl;
	notifySolutionObservers(solution_);
}

} /* namespace Edvs */
