/*
 * FileTrajectory.hpp
 *
 *  Created on: Mar 3, 2012
 *      Author: user
 */

#ifndef FILETRAJECTORY_HPP_
#define FILETRAJECTORY_HPP_

#include <Edvs/Tracking/SolutionSource.hpp>
#include <Edvs/Tracking/Spinable.hpp>
#include <string>
#include <vector>

namespace Edvs {

class FileTrajectory
: public SolutionSource
{
public:
	FileTrajectory();
	virtual ~FileTrajectory();

	const Solution& getSolution() const {
		return solution_;
	}

	void load(const std::string& filename, bool read_time, float scl);

	void spin(time_t, Timespan) {
		notifySolutionObservers(solution_);
	}

	std::vector< std::vector<double> > trajectory();

private:
	std::vector< std::vector<double> > trajectory_;				//!< trajectory
	Solution solution_;
};

} /* namespace Edvs */
#endif /* FILETRAJECTORY_HPP_ */
