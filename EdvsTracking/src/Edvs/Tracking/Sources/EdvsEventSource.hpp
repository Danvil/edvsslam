/*
 * EdvsEventSource.h
 *
 *  Created on: Mar 2, 2012
 *      Author: david
 */

#ifndef EDVSLIVESOURCE_H_
#define EDVSLIVESOURCE_H_

#include "../EventObserver.hpp"
#include "../Spinable.hpp"
#include <Edvs/EventStream.hpp>
#include <Edvs/EventCapture.hpp>
#include <Edvs/Tracking/Spinable.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/signal.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

namespace Edvs {

class EdvsEventSource
: public Spinable
{
public:
	EdvsEventSource(const std::string& uri, bool use_file_mode);

	virtual ~EdvsEventSource();

	void spin(time_t time, Timespan dt);

	bool eof() const;

	boost::signals::connection registerEventObserver(const boost::shared_ptr<EventObserver>& observer) {
		return observer_signal_.connect(boost::bind(&EventObserver::notify, observer, _1));
	}

	boost::signals::connection registerEventListener(boost::function<void(const std::vector<Edvs::Event>&)> func) {
		return observer_signal_.connect(func);
	}

private:
	void onEvents(const std::vector<Edvs::Event>& events);

	uint64_t last_event_time_;
	uint64_t time_add_;

private:
	Edvs::EventStream event_stream_;
	Edvs::EventCapture event_capture_;

	std::vector<Edvs::Event> events_;

	boost::mutex events_mtx_;

	boost::signal<void(const std::vector<Edvs::Event>&)> observer_signal_;

	bool use_file_mode_;

};

}
#endif
