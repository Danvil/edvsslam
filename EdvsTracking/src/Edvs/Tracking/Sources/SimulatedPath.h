/*
 * SimulatedPath.h
 *
 *  Created on: Jun 22, 2012
 *      Author: david
 */

#ifndef EDVS_SOURCES_SIMULATEDPATH_H_
#define EDVS_SOURCES_SIMULATEDPATH_H_

#include <Edvs/Tracking/SolutionSource.hpp>

namespace Edvs {

class SimulatedPath
: public SolutionSource
{
public:
	SimulatedPath();
	virtual ~SimulatedPath() {}

	void setPath(const Path<Real,3,false>& path);

	const Path<Real,3,false>& getPath() const {
		return path_;
	}

	const Solution& getSolution() const {
		return solution_;
	}

	State computePathState(Real t) const {
		return State{path_.position(t), path_.rotation(t)};
	}

	void spin(time_t time, Timespan dt);

private:
	void generateSolution();

private:
	Path<Real,3,false> path_;
	Solution solution_;
};

}
#endif
