/*
 * SimulatedPath.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#include "SimulatedPath.h"
#include <Edvs/Tracking/Tools/TrackingTools.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <Edvs/Tools/FunctionCache.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>
#include <cmath>

namespace Edvs {

SimulatedPath::SimulatedPath()
{
}

void SimulatedPath::setPath(const Path<Real,3,false>& path)
{
	path_ = path;
	generateSolution();
}


void SimulatedPath::generateSolution()
{
	const unsigned long cDtMus = 10000;
	const Real cDT = static_cast<Real>(cDtMus)/1000000.0;
	// add initial state to solution
	Real tbegin = path_.beginTime();
	Real tend = path_.endTime();
	for(Real t=tbegin; t<=tend; t+=cDT) {
		solution_.add(t*1000000, computePathState(t), 1.0);
	}
	// notify observers
	notifySolutionObservers(solution_);
}


void SimulatedPath::spin(time_t, Timespan)
{
	// notify observers
	notifySolutionObservers(solution_);
}

}
