/*
 * Tracking.cpp
 *
 *  Created on: Jun 25, 2012
 *      Author: david
 */

#include "Tracking.hpp"
#include "SaveSolution.hpp"
#include "Visualizations/RetinaDisplay.hpp"
#include "Visualizations/TrajectoryVisualization.hpp"
#include "Visualizations/GridMapVis.h"
#include "Visualizations/GridMap3DVis.hpp"
#include "Visualizations/SampleMap2DVis.hpp"
#include "Visualizations/SampleMap3DVis.hpp"
#include "Visualizations/ParticleSetVis.h"
#include "Visualizations/TrackerVisualization.hpp"
#include "Sources/SimulatedPath.h"
#include "Sources/FileTrajectory.hpp"
#include "Sources/EdvsEventSource.hpp" 
#include "Tracker/EventBasedSLAM.hpp" 
#include <Edvs/Tools/impl/lodepng.h>
#include <Edvs/Tools/Maps/GridMap3D.hpp>
#include <Edvs/Tools/Maps/SampleMap2D.hpp>
#include <Edvs/Tools/Maps/SampleMap3D.hpp>
#include "Tools/Random.hpp"
#include <Edvs/Tools/Path.hpp>
#include <Edvs/Tools/Scenario.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/make_shared.hpp>
#include <ctime>
#include <cstdlib>

const Edvs::Timespan cSpinDT = 4000; // mus per spin
const bool cObjFncVisAutoUpdate = false;
const bool cShowParticleScoreHistogram = false;

std::clock_t performance_begin;

/**
0 = 2D grid map
1 = 3D grid map
2 = 2d sample map
3 = 3d sample map
*/
constexpr int cMapModeSelection = 0;

typedef Edvs::EventBasedSLAM tracker_t;

constexpr float GRIDMAP2D_SCALE = 0.20f;
constexpr float GRIDMAP2D_PIXEL_SIZE = 0.01f;
constexpr unsigned int GRIDMAP2D_SIZE_X = 704;
constexpr unsigned int GRIDMAP2D_SIZE_Y = 704;

TrackingParameters::TrackingParameters()
: enable_mapping(false),
  gamma(1.0),
  num_particles(100),
  batch_size(3),
  motion_scale(1.0f),
  num_events_resample(100),
  decay_constant(0.03),
  fov(53.0f),
  kappa_1(0.0f), kappa_2(0.0f)
{
}


Tracking::Tracking(bool is_create_vis)
: progress_(1), is_create_vis_(is_create_vis)
{
	if(is_create_vis_) {
		// map visualization
		switch(cMapModeSelection) {
			case 1: vis_map_.reset(new Edvs::GridMap3DVis()); break;
			case 2: vis_map_.reset(new Edvs::SampleMap2DVis()); break;
			case 3: vis_map_.reset(new Edvs::SampleMap3DVis()); break;
			default: vis_map_.reset(new Edvs::GridMapVis()); break;
		}
		add(boost::shared_ptr<Edvs::Spinable>(vis_map_));
		add(boost::dynamic_pointer_cast<Edvs::Visualization>(vis_map_));

		// retina visualization
		vis_retina_.reset(new Edvs::RetinaDisplay());
		add(vis_retina_);

		// tracker visualization
		vis_tracker_.reset(new Edvs::TrackerVisualization("Tracker '???'"));
		add(vis_tracker_);

		vis_obj_fnc_.reset(new Edvs::ObjectiveFunctionVisualization());
		vis_obj_fnc_->default_rot_ = 0;
		add(vis_obj_fnc_);

		vis_retina_obj_.reset(new Edvs::RetinaObjective());
		add(boost::shared_ptr<Edvs::Spinable>(vis_retina_obj_));
		add(boost::shared_ptr<Edvs::Visualization>(vis_retina_obj_));

		if(cShowParticleScoreHistogram) {
			vis_particle_scores_.reset(new Edvs::ParticleScoreVisualization());
			add(vis_particle_scores_);
		}

		vis_video_.reset(new Edvs::VideoVis());
		add(vis_video_);
		vis_video_->setEnabled(true);

		vis_exploration_.reset(new Edvs::ExplorationVis());
		add(vis_exploration_);
		vis_exploration_->setEnabled(true);

		// vis_video_ref_.reset(new Edvs::VideoReferenceVis());
		// add(vis_video_ref_);
		// vis_video_ref_->setEnabled(true);
	}

	start_state.pos.x() = 0.0f;
	start_state.pos.y() = 0.0f;
	start_state.rot = 0.0f;

	// set up edvs sensor model
	objective_.sensor_model_.setOpeningAngleDeg(params_.fov);
	if(vis_retina_obj_) {
		vis_retina_obj_->setObjective(objective_);
	}

	is_running_ = false;

	path_info_str_ = "none";
	events_info_str_ = "none";

	changeTracker();

	reset();
}

bool Tracking::tick()
{
	if(!is_running_) {
		return true;
	}
	if(is_paused_ && !do_tick_) {
		return true;
	}

	last_time_ += cSpinDT;
	for(auto x : spinables_) {
		x->spin(last_time_, cSpinDT);
	}

	// update
	if(cObjFncVisAutoUpdate && last_time_ > objvis_last_update_time_ + 10000) {
		if(vis_obj_fnc_) {
			vis_obj_fnc_->update();
		}
		objvis_last_update_time_ = last_time_;
	}

	if(!tracker_->getSolution().data.empty()) {
		if(vis_retina_obj_) {
			vis_retina_obj_->setState(tracker_->getSolution().data.back().state);
		}
	}

//	if(is_running_ && ref_solution_source_ && !ref_solution_source_->getSolution().data.empty()) {
//		vis_obj_fnc_->default_rot_ = ref_solution_source_->getSolution().getStateAt(last_time_).rot;
//	}
//	else {
//		vis_obj_fnc_->default_rot_ = 0;
//	}

	// if(progress_.count() < progress_.expected_count()) {
	// 	progress_ += cSpinDT;
	// }

	do_tick_ = false;
	return true;
}

void save_map(const std::string& filename, const Eigen::MatrixXf map)
{
	int cols = map.cols();
	int rows = map.rows();
	
	std::ofstream fs(filename);
	for(int y = 0; y < cols; y++)
    {
        for(int x = 0; x < rows; x++)
        {
    		fs << map(x,y) << "\t";
        }

        fs << std::endl;
    }
}

void Tracking::tickDebug()
{
	static int num = 0;
	num ++;

	boost::format fmt_path(results_tag_ + "path_%05d.tsv");
	boost::format fmt_map(results_tag_ + "map_%05d.png");
	boost::format fmt_norm(results_tag_ + "norm_%05d.png");
	boost::format fmt_bumps(results_tag_ + "bumps_%05d.tsv");
	boost::format fmt_plan(results_tag_ + "plan_%05d.tsv");

	boost::format fmt_iwashere_map(results_tag_ + "iwashere_map_%05d.tsv");
	boost::format fmt_exploration_map(results_tag_ + "exploration_map_%05d.tsv");
	boost::format fmt_frontier_map(results_tag_ + "frontier_map_%05d.tsv");
	boost::format fmt_accessibility_map(results_tag_ + "accessibility_map_%05d.tsv");
	boost::format fmt_cost_map(results_tag_ + "cost_map_%05d.tsv");

	//save maps:
	save_map((fmt_iwashere_map % num).str(), exploration_->get_search_strategy().get_iwashere_map());
	save_map((fmt_exploration_map % num).str(), exploration_->get_search_strategy().get_exploration_map());
	save_map((fmt_frontier_map % num).str(), exploration_->get_search_strategy().get_frontier_map());
	save_map((fmt_accessibility_map % num).str(), exploration_->get_search_strategy().get_accessibility_map());
	save_map((fmt_cost_map % num).str(), exploration_->get_search_strategy().get_cost_map());

	// save result trajectory
	std::string fn_results_path = (fmt_path % num).str();
	std::cout << "Saving result path to '" << fn_results_path << std::endl;
	{
		std::ofstream fs(fn_results_path);
		const auto& path = tracker_->getSolution().data;
		for(int i=0; i<path.size(); i++) {
			const Edvs::SolutionItem& x = path[i];
			fs << static_cast<float>(x.time)/1000000.0f << "\t" << x.state.pos.x() << "\t" << x.state.pos.y() << "\t" << x.state.rot << std::endl;
		}
	}
	// save map
	std::string fn_results_map = (fmt_map % num).str();
	std::cout << "Saving result map to '" << fn_results_map << std::endl;
	{
		auto map_grid2 = boost::dynamic_pointer_cast<Edvs::GridMap>(map_);
		if(map_grid2) {
			const Eigen::MatrixXf& map = map_grid2->getMapProbability();
			const unsigned int w = map.rows();
			const unsigned int h = map.cols();
			// resize image
			QImage img(w, h, QImage::Format_ARGB32);
			// paint map
			uint32_t* dst = reinterpret_cast<uint32_t*>(img.bits());
			for(unsigned int y=0; y<h; y++) {
				for(unsigned int x=0; x<w; x++) {
					const float mp = map(x,y);
					uint32_t iv = static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, mp * 255.0f)));
					iv = 255 - iv;
					*dst = (iv | (iv<<8) | (iv<<16) | (255<<24)); // black -> white
					dst++;
				}
			}
			img.save(QString::fromStdString(fn_results_map));
		}
	}
	// save map
	std::string fn_results_norm = (fmt_norm % num).str();
	std::cout << "Saving norm map to '" << fn_results_norm << std::endl;
	{
		auto map_grid2 = boost::dynamic_pointer_cast<Edvs::GridMap>(map_);
		if(map_grid2) {
			const Eigen::MatrixXf& map = map_grid2->getDistanceMap();
			float mmin = map.minCoeff();
			float mmax = map.maxCoeff();
			const unsigned int w = map.rows();
			const unsigned int h = map.cols();
			// resize image
			QImage img(w, h, QImage::Format_ARGB32);
			// paint map
			uint32_t* dst = reinterpret_cast<uint32_t*>(img.bits());
			for(unsigned int y=0; y<h; y++) {
				for(unsigned int x=0; x<w; x++) {
					const float mp = (map(x,y) - mmin) / (mmax - mmin);
					uint32_t iv = static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, mp * 255.0f)));
					iv = 255 - iv;
					*dst = (iv | (iv<<8) | (iv<<16) | (255<<24)); // black -> white
					dst++;
				}
			}
			img.save(QString::fromStdString(fn_results_norm));
		}
	}
	// save bumps
	std::string fn_results_bumps = (fmt_bumps % num).str();
	std::cout << "Saving bumps to '" << fn_results_bumps << std::endl;
	{
		std::ofstream fs(fn_results_bumps);
		for(const Bump& x : exploration_->get_bumps()) {
			fs << x.position.x() << "\t" << x.position.y() << "\t"
			   << x.normal.x() << "\t" << x.normal.y() << std::endl;
		}
	}	
	// save plan
	std::string fn_results_plan = (fmt_plan % num).str();
	std::cout << "Saving plan to '" << fn_results_plan << std::endl;
	{
		std::ofstream fs(fn_results_plan);
		fs << exploration_->get_search_strategy().get_next_target()[0] << "\t"
			<< exploration_->get_search_strategy().get_next_target()[1] << std::endl; 
		for(const grid_coord_t& g : exploration_->get_search_strategy().get_path()) {
			fs << g[0] << "\t" << g[1] << std::endl;
		}
	}	

	if(!result_saved_ && event_source_->eof()) {
		// show performance
		std::clock_t performance_end = std::clock();
		double elapsed_secs = double(performance_end - performance_begin) / CLOCKS_PER_SEC;
		std::cout << "Elapsed: " << elapsed_secs << "s" << std::endl;
		{
			std::ofstream ofs(results_tag_ + "time.tsv");
			ofs << elapsed_secs << std::endl;
			ofs << num_events_ << std::endl;
		}
		// ready
		result_saved_ = true;
		throw(0); // quit program
	}
}

void Tracking::run()
{
	if(!connect()) {
		return;
	}
	is_running_ = true;
	is_paused_ = false;
	do_tick_ = false;
	performance_begin = std::clock();
}

void Tracking::step()
{
	if(!connect()) {
		return;
	}
	is_running_ = true;
	is_paused_ = true;
	do_tick_ = true;
	objvis_last_update_time_ = 0; // update objective function in any case
}

void Tracking::pause()
{
	if(is_running_) {
		is_paused_ = true;
	}
}

void Tracking::reset()
{
	is_running_ = false;
	is_paused_ = false;
	do_tick_ = false;
	last_time_ = 0;
	objvis_last_update_time_ = 0;
	if(vis_retina_obj_) {
		vis_retina_obj_->reset();
	}

	disconnect();
	connect();
}

bool Tracking::connect()
{
	if(is_connected_) {
		return true;
	}

	if(!tracker_) {
		std::cerr << "Need tracker to run!" << std::endl;
		return false;
	}
	if(!event_source_) {
		std::cerr << "Need a event source for tracker initialization!" << std::endl;
		return false;
	}
	if(!map_) {
		std::cerr << "Need a map source for tracker initialization!" << std::endl;
		return false;
	}

	Edvs::Tools::Engine().seed(static_cast<unsigned int>(std::time(0)));

	result_saved_ = false;

	connection_a_ = event_source_->registerEventObserver(tracker_);
	if(vis_tracker_) {
		vis_tracker_->setTracker(tracker_);
		if(ref_solution_source_) {
			vis_tracker_->setReferenceSolution(ref_solution_source_->getSolution());
		}
		vis_tracker_->notifySolution(Edvs::Solution());
		vis_tracker_->notifyParticles({});
	}
	objective_.map_ = map_;
	objective_.setGamma(params_.gamma);
	if(vis_retina_obj_) {
		vis_retina_obj_->setObjective(objective_);
	}
	tracker_->setObjective(objective_);
	tracker_->reset(start_state);
	if(vis_obj_fnc_) {
		vis_obj_fnc_->reset();
	}
	if(vis_obj_fnc_) {
		event_source_->registerEventObserver(vis_obj_fnc_);
	}
	if(vis_retina_obj_) {
		event_source_->registerEventObserver(vis_retina_obj_);
	}

//	{
//		last_time_ = 0;
//		Edvs::FileSource* q = dynamic_cast<Edvs::FileSource*>(event_source_.get());
//		if(q) {
//			last_time_ = q->getStartTime();
//		}
//	}
//
	is_connected_ = true;

	return true;
}

void Tracking::disconnect()
{
	if(connection_a_.connected()) {
		connection_a_.disconnect();
	}
	is_connected_ = false;
}

void Tracking::loadScenario(const std::string& fn)
{
	std::cout << "Loading scenario file... " << std::flush;
	Edvs::Scenario::ScenarioSettings scn;
	scn.load(fn);
	std::string base_path = boost::filesystem::path(fn).parent_path().string() + "/";
	std::cout << base_path << std::endl;
	loadPath(base_path + "path.txt");
	loadMap(base_path + scn.fn_map + "/map.xml");
//	loadEventSource(base_path + "events?dt=1000", true);
	loadEventSource(base_path + "events", false);
	start_state.pos.x() = scn.start_x;
	start_state.pos.y() = scn.start_y;
	start_state.rot = scn.start_rot;
}

void Tracking::loadPath(const std::string& fn)
{
	std::cout << "Loading path file... " << std::flush;

	if(is_running_) {
		std::cerr << "Need to stop tracker before path can be changed!" << std::endl;
		return;
	}

	boost::shared_ptr<Edvs::FileTrajectory> ref_solution_src(new Edvs::FileTrajectory());
	ref_solution_src->load(fn, true, 1.0);

	remove(ref_solution_source_);
	ref_solution_source_ = ref_solution_src;
	add(ref_solution_source_);
	if(vis_tracker_) {
		vis_tracker_->setReferenceSolution(ref_solution_source_->getSolution());
	}
	if(vis_video_ref_) {
		vis_video_ref_->setReferenceSolution(ref_solution_source_->getSolution());
	}

	start_state = ref_solution_source_->getSolution().data.front().state;

	path_info_str_ = (boost::format("%d steps") % ref_solution_src->getSolution().data.size()).str();

	std::cout << "Done." << std::endl;
}

void Tracking::loadMap(const std::string& fn)
{
	if(!params_.enable_mapping && cMapModeSelection != 0) {
		std::cerr << "Tracking with prebuild map only works with 2D grid map!" << std::endl;
		throw 0;
	}
	if(cMapModeSelection == 0) {
		boost::shared_ptr<Edvs::GridMap> map;
		if(!params_.enable_mapping) {
			std::cout << "Loading Grid Map from config file ..." << std::endl;
			// load map from file
			map = Edvs::GridMap::LoadConfig(fn);
			map->computeOccurenceMap(objective_.sensor_model_.getPixelFocalLength(), GRIDMAP2D_SCALE);
			// deletes part of the map
			// map->reduce(Eigen::Vector3f(start_state.pos.x(), start_state.pos.y(), 0.0f), 0.0f);
		}
		else {
			std::cout << "Creating empty 2D Grip Map ..." << std::endl;
			//map = boost::make_shared<Edvs::GridMap>();
			map = Edvs::GridMap::LoadConfig(fn);
			map->setPixelSize(GRIDMAP2D_PIXEL_SIZE);
			map->setMaps(Eigen::MatrixXf::Zero(GRIDMAP2D_SIZE_X, GRIDMAP2D_SIZE_Y));
		}
		setMap(map);
	}
	else if(cMapModeSelection == 1) {
		std::cout << "Creating empty 3D Grid Map ..." << std::endl;
		boost::shared_ptr<Edvs::GridMap3D> map(new Edvs::GridMap3D());
		setMap(map);
	}
	else if(cMapModeSelection == 2) {
		std::cout << "Creating empty 2D Sample Map ..." << std::endl;
		boost::shared_ptr<Edvs::SampleMap2D> map(new Edvs::SampleMap2D());
		setMap(map);
	}
	else if(cMapModeSelection == 3) {
		std::cout << "Creating empty 3D Sample Map ..." << std::endl;
		boost::shared_ptr<Edvs::SampleMap3D> map(new Edvs::SampleMap3D());
		setMap(map);
	}
	else {
		std::cerr << "Invalid cMapModeSelection!" << std::endl;
		throw 0;
	}
	std::cout << "Done." << std::endl;
}

void Tracking::setMap(const boost::shared_ptr<Edvs::Map>& map)
{
	map_ = map;

	tracker_->setMap(map_);

	objective_.map_ = map_;
	map_->sensor_model_ = &objective_.sensor_model_;
	objective_.setGamma(params_.gamma);

	if(vis_map_) {
		vis_map_->setMap(map_);
	}
	if(vis_video_) {
		vis_video_->setObjective(&objective_);
	}
	if(vis_video_ref_) {
		vis_video_ref_->setObjective(&objective_);
	}
	if(vis_retina_obj_) {
		vis_retina_obj_->setObjective(objective_);
	}
	if(vis_obj_fnc_) {
		vis_obj_fnc_->setObjectiveFunction(objective_);
	}

	is_connected_ = false;
}

void Tracking::loadEventSource(const std::string& uri, bool use_file_mode)
{
	std::cout << "Loading event source file... " << std::flush;

	remove(event_source_);

	event_source_.reset(new Edvs::EdvsEventSource(uri, use_file_mode));
	add(event_source_);
	if(vis_retina_) {
		event_source_->registerEventObserver(vis_retina_);
	}
	if(vis_video_) {
		event_source_->registerEventObserver(vis_video_);
	}
	if(vis_video_ref_) {
		event_source_->registerEventObserver(vis_video_ref_);
	}

	num_events_ = 0;
	event_source_->registerEventListener(
		[this](const std::vector<Edvs::Event>& events) {
			num_events_ += events.size();
		});

	events_info_str_ = "live stream";
	std::cout << "Done." << std::endl;

	tracker_t* p = dynamic_cast<tracker_t*>(tracker_.get()); 
}

void Tracking::setExploration(const std::string& robot_address)
{
	boost::shared_ptr<Edvs::Exploration> exploration(new Edvs::Exploration(robot_address));
	exploration_ = exploration;

	run_exploration_.reset(new Edvs::RunExploration(exploration));

	if(run_exploration_) {
		run_exploration_->setMap(map_);

		tracker_->registerSolutionObserver(boost::bind(
					static_cast<void(Edvs::RunExploration::*)(const Edvs::Solution& solution)>(&Edvs::RunExploration::notify),
					run_exploration_, _1));	
	}

	if(vis_exploration_)
	{
		vis_exploration_->setExploration(exploration);
	}
}

void Tracking::changeTracker()
{
	if(is_running_) {
		std::cerr << "Need to stop tracker before tracker can be changed!" << std::endl;
		return;
	}

	// FIXME change tracker name
	tracker_.reset(new tracker_t());
	if(vis_tracker_) {
		tracker_->on_particles_.connect(boost::bind(&Edvs::TrackerVisualization::notifyParticles, vis_tracker_, _1));
	}
	if(vis_particle_scores_) {
		tracker_->on_particles_.connect(boost::bind(&Edvs::ParticleScoreVisualization::setParticles, vis_particle_scores_, _1));
	}
	if(vis_tracker_) {
		tracker_->registerSolutionObserver(boost::bind(&Edvs::TrackerVisualization::notifySolution, vis_tracker_, _1));
	}
	if(vis_video_) {
		tracker_->registerSolutionObserver(boost::bind(
			static_cast<void(Edvs::VideoVis::*)(const Edvs::Solution& solution)>(&Edvs::VideoVis::notify),
			vis_video_, _1));
	}
	if(vis_video_ref_) {
		//tracker_->registerSolutionObserver(boost::bind(&Edvs::VideoReferenceVis::notify, vis_video_ref_, _1));
		tracker_->registerSolutionObserver(boost::bind(
				static_cast<void(Edvs::VideoReferenceVis::*)(const Edvs::Solution& solution)>(&Edvs::VideoReferenceVis::notify),
				vis_video_ref_, _1));
	}
	if(vis_exploration_) {
		tracker_->registerSolutionObserver(boost::bind(
			static_cast<void(Edvs::ExplorationVis::*)(const Edvs::Solution& solution)>(&Edvs::ExplorationVis::notify),
			vis_exploration_, _1));
	}

	// boost::shared_ptr<Edvs::SaveSolution> save_solution(new Edvs::SaveSolution(fn_result_));
	// tracker_->registerSolutionObserver(boost::bind(&Edvs::SaveSolution::notify, save_solution, _1));
	
	if(vis_tracker_) {
		vis_tracker_->setTracker(tracker_);
		vis_tracker_->setName("edvs_event_new");
	}

	is_connected_ = false;

	// compare event based trajectory with ref. trajectory
//	comp_trajec_event_.reset(new Edvs::CompareTrajectories("Compare Event"));
//	tracker_event->registerSolutionObserver(boost::bind(&Edvs::CompareTrajectories::notify, comp_trajec_event_, _1));
//	comp_trajec_event_->setMinimumSize(600, 400);
//	comp_trajec_event_->show();
//	comp_trajec_event_->setRefTrajectory(ref_solution);
////	visualizations_.push_back(comp_trajec_event_); // FIXME this is a hack
////	ui.mdiArea->addSubWindow(comp_trajec_event_.get());
//
//	// plot certainty of the event based particle set
//	plot_certainty_event_.reset(new Edvs::PlotStandardDeviation("Certainty Event"));
//	tracker_event->on_particles_evaluate_.connect(boost::bind(&Edvs::PlotStandardDeviation::notify, plot_certainty_event_, _1, _2));
//	plot_certainty_event_->setMinimumSize(600, 400);
//	plot_certainty_event_->show();
////	ui.mdiArea->addSubWindow(plot_certainty_event_.get());
//
//	// compare fixed interval trajectory with reference and visualize the ref. trajectory
//	comp_trajec_pf_.reset(new Edvs::CompareTrajectories("Compare PF"));
//	tracker_pf->registerSolutionObserver(boost::bind(&Edvs::CompareTrajectories::notify, comp_trajec_pf_, _1));
//	comp_trajec_pf_->setMinimumSize(600, 400);
//	comp_trajec_pf_->show();
//	comp_trajec_pf_->setRefTrajectory(ref_solution);
////	ui.mdiArea->addSubWindow(comp_trajec_pf_.get());
//
//	// plot certainty of the fix. interval particle set
//	plot_certainty_pf_.reset(new Edvs::PlotStandardDeviation("Certainty PF"));
//	tracker_pf->on_particles_evaluate_.connect(boost::bind(&Edvs::PlotStandardDeviation::notify, plot_certainty_pf_, _1, _2));
//	plot_certainty_pf_->setMinimumSize(600, 400);
//	plot_certainty_pf_->show();
////	ui.mdiArea->addSubWindow(plot_certainty_pf_.get());

}

void Tracking::saveMap()
{
	// FIXME implement
	//Edvs::SaveImageUchar4("currentImage.png", map_->getDistMap());
	throw 0;
}

void Tracking::setResultsTag(const std::string& tag)
{
	results_tag_ = tag;
}

void Tracking::setParameters(const TrackingParameters& params)
{
	params_ = params;
	objective_.sensor_model_.setOpeningAngleDeg(params_.fov);
	objective_.sensor_model_.kappa_1 = params_.kappa_1;
	objective_.sensor_model_.kappa_2 = params_.kappa_2;

	// recompute map
	objective_.setGamma(params_.gamma);
	if(vis_retina_obj_) {
		vis_retina_obj_->setObjective(objective_);
	}
	if(vis_retina_obj_) {
		vis_obj_fnc_->setObjectiveFunction(objective_);
	}

	tracker_t* tracker = dynamic_cast<tracker_t*>(tracker_.get()); 
	tracker->setObjective(objective_);
	Edvs::EventBasedSLAMParameters slam_params = tracker->getParameters();
	slam_params.enable_mapping = params_.enable_mapping;
	slam_params.num_particles = params_.num_particles;
	slam_params.batch_size = params_.batch_size;
	slam_params.motion_scale = params_.motion_scale;
	slam_params.resample_event_count = params_.num_events_resample;
	slam_params.decay_constant = params_.decay_constant;
	tracker->setParameters(slam_params);
}

void Tracking::setTrackerParticleCount(unsigned int val)
{
	tracker_t* tracker = dynamic_cast<tracker_t*>(tracker_.get());
	if(tracker) {
		Edvs::EventBasedSLAMParameters slam_params = tracker->getParameters();
		slam_params.num_particles = val;
		tracker->setParameters(slam_params);
	}
}

void Tracking::setTrackerResampleEventCount(unsigned int val)
{
	tracker_t* tracker = dynamic_cast<tracker_t*>(tracker_.get());
	if(tracker) {
		Edvs::EventBasedSLAMParameters slam_params = tracker->getParameters();
		slam_params.resample_event_count = val;
		tracker->setParameters(slam_params);
	}
}

void Tracking::addNoise(double pos_mean, double pos_sigma, double rot_mean, double rot_sigma)
{
	if(!is_running_) {
		std::cerr << "Tracker must be running!" << std::endl;
		return;
	}
	tracker_t* pf = dynamic_cast<tracker_t*>(tracker_.get());
	if(!pf) {
		std::cerr << "Add noise is only implemented for EdvsPF!" << std::endl;
		return;
	}
	// add noise to particles
	double dir = Edvs::Tools::UniformRealGenerator(0.0, 2.0*M_PI)();
	auto noise_pos = Edvs::Tools::GaussianGenerator(0.0, pos_sigma);
	auto noise_rot = Edvs::Tools::GaussianGenerator(rot_mean, rot_sigma);
	Edvs::ParticleSet particles = pf->getParticles();
	for(Edvs::Particle& p : particles) {
		p.state.pos.x() += std::cos(dir) * pos_mean + noise_pos();
		p.state.pos.y() += std::sin(dir) * pos_mean + noise_pos();
		p.state.rot += noise_rot() / 180.0 * M_PI;
	}
	pf->setParticles(particles);
}

void Tracking::computeObjectiveFunction()
{
	vis_obj_fnc_->update();
}

void Tracking::setObjectiveRotation(double angle_deg)
{
	vis_obj_fnc_->default_rot_ = angle_deg * M_PI / 180.0;
	vis_obj_fnc_->update();
}

void Tracking::setEnabledVisualization(const std::string& str, bool x)
{
	if(str == "GridMap") {
		if(vis_map_)
			boost::dynamic_pointer_cast<Edvs::Visualization>(vis_map_)->setEnabled(x);
	}
	else if(str == "Retina") {
		if(vis_retina_)
			vis_retina_->setEnabled(x);
	}
	else if(str == "RetinaObjective") {
		if(vis_retina_obj_)
			vis_retina_obj_->setEnabled(x);
	}
	else if(str == "Paths") {
		if(vis_tracker_)
			vis_tracker_->setEnabled(x);
	}
	else {
		std::cerr << "Unknown visualization '" << str << "'" << std::endl;
	}
}
