/*
 * Common.hpp
 *
 *  Created on: Mar 7, 2012
 *      Author: Martin
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_
#define _USE_MATH_DEFINES

#include <Edvs/Tools/Pose.hpp>
#include <Edvs/Tools/Path.hpp>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <math.h>

using std::string;
using std::vector;

namespace Edvs {

typedef double Real;
typedef Real Score;
typedef unsigned long Timestamp;
typedef int64_t Timespan;
typedef int64_t time_t;

typedef Eigen::Matrix<Real,2,1> Vec2;
typedef Eigen::Matrix<Real,3,1> Vec3;
typedef Eigen::Matrix<Real,2,2> Mat2;
typedef Eigen::Matrix<Real,3,3> Mat3;

//template<typename T>
//std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) {
//	os << "{";
//	for(unsigned int i=0; i<v.size(); i++) {
//
//	}
//	os << "}";
//	return os;
//}

typedef ZAlignedPose<Real> State;

struct TimedScore {
	Timestamp time;
	Score partial_score;
	Score total_score;
};

struct Particle
{
	State state;
	Score score;
};

typedef vector<Particle> ParticleSet;

inline
std::vector<Score> get_scores(const ParticleSet& particles)
		{
	std::vector<Score> scores(particles.size());
	std::transform(particles.begin(), particles.end(), scores.begin(),
			[](const Particle& p) { return p.score; });
	return scores;
}

inline
const State& get_best(const ParticleSet& particles)
{
	auto maxel = std::min_element(particles.begin(), particles.end(),
			[](const Particle& x, const Particle& y) { return x.score > y.score; });
	return maxel->state;
}

template<typename It> inline
Particle compute_mean(It begin, It end)
{
	State mean = State::Zero();
	Score mean_score = 0;
	for(auto it=begin; it!=end; ++it) {
		const Particle& p = *it;
		mean.pos += p.state.pos;
		mean.rot += p.state.rot;
		mean_score += p.score;
	}
	const unsigned int n = std::distance(begin, end);
	mean.pos /= static_cast<Real>(n);
	mean.rot /= static_cast<Real>(n);
	mean_score /= static_cast<Real>(n);
	return {mean, mean_score};
}

inline
Particle get_mean_10p_best(const ParticleSet& particles)
{
	// find 10 percent best particles
	unsigned int n = particles.size() / 10;
	vector<Particle> best10p(n);
	std::partial_sort_copy(particles.begin(), particles.end(), best10p.begin(), best10p.end(),
			[](const Particle& x, const Particle& y) { return x.score > y.score; });
	// build mean over selected particles
	State mean = State::Zero();
	Score mean_score = 0;
	for(const Particle& p : best10p) {
		mean.pos += p.state.pos;
		mean.rot += p.state.rot;
		mean_score += p.score;
	}
	mean.pos /= static_cast<Real>(n);
	mean.rot /= static_cast<Real>(n);
	mean_score /= static_cast<Real>(n);
	return {mean, mean_score};
}

struct SolutionItem {
	Timestamp time;
	State state;
	Real score;
};

struct Solution
{
	vector<SolutionItem> data;

	State getStateAt(Timestamp time) const {
		auto it = std::find_if(data.begin(), data.end(), [time](const SolutionItem& item) {
			return item.time >= time;
		});
		if(it == data.end()) {
			return data.back().state;
		}
		else {
			return it->state;
		}
	}

	void add(Timestamp time, const Particle& p) {
		SolutionItem i;
		i.time = time;
		i.state = p.state;
		i.score = p.score;
		data.push_back(i);
	}

	void add(Timestamp time, const State& state, Real score) {
		SolutionItem i;
		i.time = time;
		i.state = state;
		i.score = score;
		data.push_back(i);
	}

	State getStateByTime(Timestamp time) const {
		// auto it = std::find_if(data.begin(), data.end(), [time](const SolutionItem& item) {
		// 	return item.time >= time;
		// });
		// if (it == data.begin()) {
		// 	return it->state;
		// }
		// else {
		// 	Timestamp nextT = it->time;
		// 	State stateNext = it->state;
		// 	Real rotNext = it->state.rot; 
		// 	it--;
		// 	Timestamp prevT = it->time;
		// 	State statePrev = it->state;
		// 	Real rotPrev = it->state.rot;

		// 	Real interpolFac = Real((time - prevT)) / Real((nextT - prevT));
		// 	//std::cout << "t " << time  << " it " << it->time << " dT " << deltaTime << std::endl;
		// 	//std::cout << "interpolF " << interpolFac<<std::endl;
			
		// 	Eigen::Matrix<Real,3,1> interpolatedPos = statePrev.pos + interpolFac * (stateNext.pos - statePrev.pos);
		// 	//std::cout << "pos0: " << statePrev.pos << " pos1: " << stateNext.pos << " interp: " << interpolatedPos << std::endl;

		// 	Real deltaRotAbs = std::abs(rotNext - rotPrev);
		// 	if(deltaRotAbs > M_PI) {
		// 		if(rotNext > rotPrev) {
		// 			rotPrev += 2*M_PI;
		// 		}
		// 		else {
		// 			rotNext += 2*M_PI;
		// 		}
		// 	}
		// 	Real interpolatedRot = rotPrev + interpolFac * (rotNext - rotPrev);
		// 	//std::cout << "rot0: " << rotPrev << " rot1: " << rotNext << " interp: " << interpolatedRot << std::endl;
		// 	return State {interpolatedPos, interpolatedRot};			
		// }

		auto path = convertToPath();
		Real t = static_cast<Real>(time) / 1000000.0;
		Eigen::Matrix<Real,3,1> v = path.position(t);
		return {{v.x(),v.y(),Real(0)}, path.rotation(t)};
	}

	Path<Real,3,false> convertToPath() const {
		Path<Real,3,false> path;
		Real total_time = static_cast<Real>(data.back().time) / 1000000.0;
		Real dt = total_time / data.size();
		Real time = 0;
		for(const SolutionItem& u : data) {
			path.add({time, Vec3{u.state.pos.x(), u.state.pos.y(), 0.0}});
			time += dt;
		}
		return path;
	}

};

inline float FastInverseSqrt(float x) {
	uint32_t i = *((uint32_t *)&x);			// evil floating point bit level hacking
	i = 0x5f3759df - (i >> 1);				// use a magic number
	float s = *((float *)&i);				// get back guess
	return s * (1.5f - 0.5f * x * s * s);	// one newton iteration
}

}

#endif /* COMMON_HPP_ */
