/*
 * TrajectoryVisualization.cpp
 *
 *  Created on: Mar 2, 2012
 *      Author: user
 */

#include "TrajectoryVisualization.hpp"
#include "Painters.hpp"
#include <QPainter>

namespace Edvs {

//TrajectoryVisualization::TrajectoryVisualization()
//: Visualization("Trajectory")
TrajectoryVisualization::TrajectoryVisualization(std::string name)
: Visualization2D(name)
{
}

TrajectoryVisualization::~TrajectoryVisualization()
{
}

void TrajectoryVisualization::notify(const Solution& solution)
{
	solution_ = solution;
	requireRepaint();
}

void TrajectoryVisualization::paint(QPainter& painter)
{
	painter.fillRect(painter.window(), Qt::black);
	PreparePainter(painter);
	PaintAxes(painter);
	PaintSolution(painter, solution_);
}

}
