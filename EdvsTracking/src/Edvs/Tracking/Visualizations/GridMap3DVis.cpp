/*
 * SampleMap3DVis.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: david
 */

#include <Edvs/Tools/Maps/GridMap3D.hpp>
#include "GridMap3DVis.hpp"
#include "Painters.hpp"
#include <Candy/OpenGL.h>
#include <Candy.h>
#include <Candy/Primitives.h>
#include <QPainter>
#include <stdint.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>

using namespace octomap;

namespace Edvs {

GridMap3DVis::GridMap3DVis()
: Visualization3D("GridMap3DVis")
{
}

GridMap3DVis::~GridMap3DVis()
{
}

void GridMap3DVis::setMap(const boost::shared_ptr<Map>& map)
{
	map_ = boost::dynamic_pointer_cast<GridMap3D>(map);
}

void GridMap3DVis::spin(time_t, Timespan)
{
	if(!isEnabled()) {
		return;
	}
	requireRepaint();
}

inline void opengl_vertex3f(const Eigen::Vector3f& v) {
	glVertex3f(v.x(), v.y(), v.z());
}

inline void render_particle(const Eigen::Vector3f& pos, float r, float q, const Eigen::Vector3f& right, const Eigen::Vector3f& up)
{
	const float a = 0.01f*q;
	glColor4f(a,a,a,1.0f);
	opengl_vertex3f(pos - r*right - r*up);
	opengl_vertex3f(pos + r*right - r*up);
	opengl_vertex3f(pos + r*right + r*up);
	opengl_vertex3f(pos - r*right + r*up);
}

void GridMap3DVis::render()
{	
	constexpr float r = 0.01;

	enum RenderStyle { Boxes, Particles };
	const RenderStyle style = Particles;

	if(style == Boxes) {
		for(auto it=map_->tree_.begin_leafs(), end=map_->tree_.end_leafs(); it!=end; ++it) {
			glPushMatrix();
			glTranslatef(it.getX(), it.getY(), it.getZ());
			Candy::Primitives::RenderBox(r);
			glPopMatrix();
			//manipulate node, e.g.:
			// std::cout << "Node center: " << it.getCoordinate() << std::endl;
			// std::cout << "Node size: " << it.getSize() << std::endl;
			// std::cout << "Node value: " << it->getValue() << std::endl;
		}
	}
	else if(style == Particles) {
		auto camera = candy_engine_->getView()->getCamera();
		Eigen::Vector3f right(camera->getRight().x, camera->getRight().y, camera->getRight().z);
		Eigen::Vector3f up(camera->getUp().x, camera->getUp().y, camera->getUp().z);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glBlendFunc(GL_ONE, GL_ONE);
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glBegin(GL_QUADS);
		for(auto it=map_->tree_.begin_leafs(), end=map_->tree_.end_leafs(); it!=end; ++it) {
			Eigen::Vector3f pos(it.getX(), it.getY(), it.getZ());
			render_particle(pos, r, it->getOccupancy(), right, up);
		}
		glEnd();
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}
}

}
