/*
 * Painters.cpp
 *
 *  Created on: Mar 12, 2012
 *      Author: david
 */

#include "Painters.hpp"
#include <Edvs/Tools/Maps/GridMap.hpp>

namespace Edvs
{

const float cPixelPerMeter = 300;
const float cRangeMeter = 1.5;
const float cAxisTicksDist = 0.5f;
const float cAxisTicksSize = 0.05f;

void PreparePainter(QPainter& painter)
{
	QTransform t;
	t.scale(cPixelPerMeter, -cPixelPerMeter);
	painter.setTransform(t);
	int ws = static_cast<int>(cPixelPerMeter * cRangeMeter);
	painter.setWindow(-ws,-ws,2*ws,2*ws);
}

void PaintAxes(QPainter& painter)
{
	QPen pen(Qt::SolidLine);
	pen.setColor(qRgb(255,255,255));
	painter.setPen(pen);
	for(int i=-10; i<=+10; i++) {
		float p = static_cast<float>(i) * cAxisTicksDist;
		float q = p + cAxisTicksDist;
		painter.drawLine(QPointF(0,p), QPointF(0,q));
		painter.drawLine(QPointF(-cAxisTicksSize,p), QPointF(+cAxisTicksSize,p));
		painter.drawLine(QPointF(p,0), QPointF(q,0));
		painter.drawLine(QPointF(p,-cAxisTicksSize), QPointF(p,+cAxisTicksSize));
	}

}

void PaintStatePos(QPainter& painter, const State& s)
{
	painter.setPen(QColor(qRgb(0, 255, 0)));
	painter.drawPoint(QPointF{s.pos[0], s.pos[1]});

}

void PaintStatePos(QPainter& painter, const State& s, float p)
{
	painter.setPen(QColor(Score2ColorBlueToYellow(p)));
	painter.drawPoint(QPointF{s.pos[0], s.pos[1]});
}

void PaintStateVelocity(QPainter& painter, const State& s)
{
//	const float cVelScl = 0.05f;
//
//	painter.setPen(QColor(qRgb(0, 255, 0)));
//	painter.drawLine(
//			QPointF{s.pos[0], s.pos[1]},
//			QPointF{
//					s.pos[0] + cVelScl * s.vel_pos[0],
//					s.pos[1] + cVelScl * s.vel_pos[1]});
//
}

void PaintStateVelocity(QPainter& painter, const State& s, float p)
{
//	const float cVelScl = 0.05f;
//
//	painter.setPen(QColor(Score2ColorBlueToYellow(p)));
//	painter.drawLine(
//			QPointF{s.pos[0], s.pos[1]},
//			QPointF{
//					s.pos[0] + cVelScl * s.vel_pos[0],
//					s.pos[1] + cVelScl * s.vel_pos[1]});
//
}

void PaintStateDirection(QPainter& painter, const State& s, float len)
{
	painter.drawLine(
			QPointF{s.pos[0], s.pos[1]},
			QPointF{
					s.pos[0] + len * std::cos(s.rot),
					s.pos[1] + len * std::sin(s.rot)});

}

void PaintRetina(QPainter& painter, const State& s, const ObjectiveFunctionF* objf)
{
	boost::shared_ptr<GridMap> map = boost::dynamic_pointer_cast<GridMap>(objf->map_);
	const float retina_half_size =
		static_cast<float>(objf->sensor_model_.RETINA_SIZE/2)
		* map->getMapHeight()
		/ objf->sensor_model_.getPixelFocalLength();
	float sr = retina_half_size*std::sin(s.rot);
	float cr = retina_half_size*std::cos(s.rot);
	QPointF a(cr, sr);
	QPointF b(sr, -cr);
	QPointF c(s.pos[0], s.pos[1]);
	QVector<QPointF> pnts = {
		c + a + b,
		c - a + b,
		c - a - b,
		c + a - b
	};
	QVector<QLineF> lines = {
		QLineF(pnts[0], pnts[1]),
		QLineF(pnts[1], pnts[2]),
		QLineF(pnts[2], pnts[3]),
		QLineF(pnts[3], pnts[0]),
		QLineF(c + a, c + 1.15f*a)
	};
	painter.drawLines(lines);
}

void PaintEvents(QPainter& painter, const State& s, const std::vector<Event> events, const ObjectiveFunctionF* objf, bool fast)
{
	boost::shared_ptr<GridMap> map = boost::dynamic_pointer_cast<GridMap>(objf->map_);
	const float retina_half_size =
		static_cast<float>(objf->sensor_model_.RETINA_SIZE/2)
		* map->getMapHeight()
		/ objf->sensor_model_.getPixelFocalLength();
	const float r = 
		0.5f
		* map->getMapHeight()
		/ objf->sensor_model_.getPixelFocalLength();
	for(const Event& e : events) {
		PixelViewCone pvc = objf->sensor_model_.Transform(
			s.pos.cast<float>(), std::sin(s.rot), std::cos(s.rot),
			objf->sensor_model_.createPixelViewCone(e.x, e.y)
		);
		Eigen::Vector2f p = map->getPoint(pvc.ray_start, pvc.ray_dir);
		if(fast)
		{
			painter.drawPoint(QPointF(p.x(), p.y()));
		}
		else
		{
			painter.drawEllipse(QPointF(p.x(), p.y()), r, r);
		}
	}
}

void PaintParticles(QPainter& painter, const ParticleSet& particles)
{
	if(particles.size() == 0) {
		return;
	}
//	double score_max = particles[0].score;
//	double score_min = particles[0].score;
//	for(const Particle& p : particles) {
//		score_max = std::max(p.score, score_max);
//		score_min = std::min(p.score, score_min);
//	}
//	if(score_max == score_min) {
//		score_max = score_min + 1;
//	}
	float score_min = 0.0f;
	float score_max = 1.0f;
//	for(const Particle& p : particles) {
//		PaintStateVelocity(painter, p.state);
//		PaintStateVelocity(painter, p.state, (p.score-score_min)/(score_max - score_min));
//	}
	for(const Particle& p : particles) {
//		PaintStatePos(painter, p.state);
		PaintStatePos(painter, p.state, (p.score-score_min)/(score_max - score_min));
	}
}

void PaintSolution(QPainter& painter, const Solution& solution, int skip)
{
	// paint path
	int n = static_cast<int>(solution.data.size()) / skip;
	if(n <= 2) { return; }
	QVector<QPointF> lines(2*n);
	lines[0] = QPointF{solution.data[0].state.pos[0], solution.data[0].state.pos[1]};
	for(int i=1; i<n-1; i++) {
		// paint path
		int j = i*skip;
		QPointF pnt{solution.data[j].state.pos[0], solution.data[j].state.pos[1]};
		lines[2*i+1] = pnt;
		lines[2*i+2] = pnt;
	}
	lines[2*n-1] = QPointF{solution.data[(n-1)*skip].state.pos[0], solution.data[(n-1)*skip].state.pos[1]};
	painter.drawLines(lines);
}

void PaintSolutionTime(QPainter& painter, const Solution& solution, unsigned long time)
{
	auto it = std::find_if(solution.data.begin(), solution.data.end(), [time](const SolutionItem& item) {
			return item.time >= time;
	});
	QVector<QLineF> lines;
	for(int i=0; i<(it - solution.data.begin()); i++) {
		State state = solution.data[i].state;
		// paint path
		lines.push_back(QLineF(
				QPointF{solution.data[i].state.pos[0], solution.data[i].state.pos[1]},
				QPointF{solution.data[i+1].state.pos[0], solution.data[i+1].state.pos[1]}));
	}
	painter.drawLines(lines);
}

void PaintParticlesDistribution(QPainter& painter, const ParticleSet& particles)
{
	Vec3 mean = Vec3::Zero();		// [x, y, rot]
	Vec3 sum = Vec3::Zero();		// [x, y, rot]
	Vec3 temp = Vec3::Zero();
	Vec3 sd = Vec3::Zero();
	Mat3 Cov = Mat3::Zero();

	// compute mean of particle pose
	for(int i=0; i<particles.size() ;i++){
		sum[0] += particles[i].state.px();
		sum[1] += particles[i].state.py();
		sum[2] += particles[i].state.rot;
	}
	mean = sum / particles.size();
	//std::cout << "mean: " << mean.transpose() << std::endl;

	//compute covariance matrix
	for(int i=0; i<particles.size() ;i++){
		temp[0] = particles[i].state.px() - mean[0];
		temp[1] = particles[i].state.py() - mean[1];
		temp[2] = particles[i].state.rot - mean[2];
		Cov += temp * temp.transpose();
	}
	Cov /= particles.size();

	// get standard deviations
	sd[0] = std::sqrt( static_cast<Real>(Cov(0, 0)) );
	sd[1] = std::sqrt( static_cast<Real>(Cov(1, 1)) );
	sd[2] = std::sqrt( static_cast<Real>(Cov(2, 2)) );
	//std::cout << "standard deviations: " << sd.transpose() << std::endl;

	// plot
	static QPen pen(qRgb(0, 220, 130));
	painter.setPen(pen);
	painter.drawEllipse(QPointF(static_cast<Real>(mean[0]), static_cast<Real>(mean[1])), static_cast<Real>(sd[0]), static_cast<Real>(sd[1]));
	painter.drawEllipse(QPointF(static_cast<Real>(mean[0]), static_cast<Real>(mean[1])), static_cast<Real>(sd[0]*2), static_cast<Real>(sd[1]*2));

}

}
