/*
 * SampleMap2DVis.hpp
 *
 *  Created on: Nov 27, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_SAMPLE3DMAPVIS_HPP_
#define EDVS_VIS_SAMPLE3DMAPVIS_HPP_

#include "MapVis.hpp"

namespace Edvs {

class SampleMap3D;

// FIXME only static map is supported

class SampleMap3DVis
: public MapVis, public Visualization3D
{
public:
	SampleMap3DVis();
	virtual ~SampleMap3DVis();

	void setMap(const boost::shared_ptr<Map>& map);

	void spin(time_t time, Timespan dt);

	void render();

private:
	boost::shared_ptr<SampleMap3D> map_;
};

}
#endif
