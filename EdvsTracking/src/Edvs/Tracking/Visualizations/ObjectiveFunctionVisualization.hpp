/*
 * ObjectiveFunctionVisualization.hpp
 *
 *  Created on: Jun 25, 2012
 *      Author: david
 */

#ifndef OBJECTIVEFUNCTIONVISUALIZATION_HPP_
#define OBJECTIVEFUNCTIONVISUALIZATION_HPP_

#include "../Visualization.hpp"
#include "../EventObserver.hpp"
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/threadpool.hpp>

namespace Edvs
{

class ObjectiveFunctionVisualization
: public VisualizationImage, public EventObserver
{
public:
	ObjectiveFunctionVisualization();
	virtual ~ObjectiveFunctionVisualization() {}

	void setObjectiveFunction(const ObjectiveFunctionF& objfnc) {
		objective_ = objfnc;
	}

	void reset() {
		events_.clear();
	}

	void notify(const std::vector<Edvs::Event>& events);

	void paint(QPainter& painter) {}

	void update(float rot);

	void update() {
		update(default_rot_);
	}

	float default_rot_;

private:
	ObjectiveFunctionF objective_;
	std::vector<Edvs::Event> events_;

	boost::threadpool::pool pool_;

};

}

#endif
