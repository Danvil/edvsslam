#ifndef VISUALIZATION_SAVER_
#define VISUALIZATION_SAVER_

#include <QImage>
#include <string>

namespace Edvs {

class VisualizationSaver
{
public:
	VisualizationSaver(std::string name);

	~VisualizationSaver();

	void save(QImage image);

private:
	unsigned int counter_;
	std::string name_;
};

}

#endif