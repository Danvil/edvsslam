/*
 * ParticleScoreVisualization.cpp
 *
 *  Created on: Jun 26, 2012
 *      Author: david
 */

#include "ParticleScoreVisualization.hpp"

constexpr int cHeight = 128;

namespace Edvs
{

ParticleScoreVisualization::ParticleScoreVisualization()
: VisualizationImage("Particle Scores")
{
	setImagePaintDevice(100, 2*cHeight);
}

void ParticleScoreVisualization::paintImage()
{
	assert(particles_prev_.size() <= particles_.size());
	if(particles_.size() != getImage()->width()) {
		setImagePaintDevice(std::max<int>(1,particles_.size()), 2*cHeight);
	}
	QRgb color_white = qRgb(255,255,255);
	QRgb color_black = qRgb(0,0,0);
	for(unsigned int j=0; j<particles_.size(); j++) {
		Real score = particles_[j].score;
		int y = static_cast<int>(score * static_cast<Real>(cHeight) + 0.5);
		y = std::min(y, cHeight);
		for(int i=0; i<y; i++) {
			getImage()->setPixel(j, cHeight-1-i, color_black);
		}
		for(int i=y; i<cHeight; i++) {
			getImage()->setPixel(j, cHeight-1-i, color_white);
		}
	}
	for(unsigned int j=0; j<particles_prev_.size(); j++) {
		Real score = particles_prev_[j].score;
		int y = static_cast<int>(score * static_cast<Real>(cHeight) + 0.5);
		y = std::min(y, cHeight);
		for(int i=0; i<y; i++) {
			getImage()->setPixel(j, cHeight + cHeight-1-i, color_black);
		}
		for(int i=y; i<cHeight; i++) {
			getImage()->setPixel(j, cHeight + cHeight-1-i, color_white);
		}
	}
	requireRepaint();
}

}


