/*
 * RetinaObjective.hpp
 *
 *  Created on: Jun 26, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_RETINAOBJECTIVE_HPP_
#define EDVS_TRACKING_RETINAOBJECTIVE_HPP_

#include "../Common.hpp"
#include "../Visualization.hpp"
#include "../EventObserver.hpp"
#include "../Spinable.hpp"
#include <Edvs/Tools/ObjectiveFunction.hpp>

namespace Edvs
{

class RetinaObjective
: public VisualizationImage, public EventObserver, public Spinable
{
public:
	RetinaObjective();
	virtual ~RetinaObjective() {}

	void reset() {
		events_.clear();
	}

	void setObjective(const ObjectiveFunctionF& objective) {
		objective_ = objective;
	}

	void setState(const State& state) {
		state_ = state;
	}

	void notify(const std::vector<Edvs::Event>& events);

	void spin(time_t time, Timespan dt);

private:
	State state_;
	std::vector<Edvs::Event> events_;
	ObjectiveFunctionF objective_;

};

}

#endif
