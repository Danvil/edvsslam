/*
 * GridMapVis.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#include "GridMapVis.h"
#include <stdint.h>
#include <Eigen/Dense>

namespace Edvs {

GridMapVis::GridMapVis()
: VisualizationImage("GridMapVis")
{
}

GridMapVis::~GridMapVis()
{
}

void GridMapVis::setMap(const boost::shared_ptr<Map>& map)
{
	map_ = boost::dynamic_pointer_cast<GridMap>(map);
	updateImage();
}

void GridMapVis::spin(time_t, Timespan)
{
	if(!isEnabled()) {
		return;
	}
	updateImage();
}

void WriteMatToImg(const Eigen::MatrixXf& mat, uint32_t* dst, unsigned int linewidth, unsigned int MULT) {
	const int w = mat.rows() / MULT;
	const int h = mat.cols() / MULT;
	for(unsigned int y=0; y<h; y++) {
		uint32_t* dst_line = dst;
		for(unsigned int x=0; x<w; x++) {
			const float v = mat(x*MULT,y*MULT);
			uint32_t iv = 255 - static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, v * 255.0f)));
			*dst_line = ((iv) | (iv<<8) | (iv<<16) | (255<<24));
			dst_line++;
		}
		dst += linewidth;
	}
}

void GridMapVis::updateImage()
{
	const Eigen::MatrixXf& mo = map_->getOccurrenceMap();
	float mo_max = mo.maxCoeff() + 0.001f;
	const Eigen::MatrixXf& mz = map_->getDistanceMap();
	float mz_max = mz.maxCoeff() + 0.001f;
	const Eigen::MatrixXf& mv = map_->getVisibilityMap();
	const int CONV = 2; // FIXME get correct conversion!!!
	const int w = mz.rows();
	const int h = mz.cols();

	if(w == 0 || h == 0) {
		return;
	}

	if(false) {
		// resize image
		setImagePaintDevice(w, h);
		// convert and paint map data
		uint32_t* dst = reinterpret_cast<uint32_t*>(getImage()->bits());
		for(unsigned int y=0; y<h; y++) {
			for(unsigned int x=0; x<w; x++) {
				const float vo = mo(x*CONV,y*CONV) / mo_max;
				const float vz = mz(x,y) / mz_max;
				const bool vv = (mv(x,y) > 0.0f);
				uint32_t ivo = static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, vo * 255.0f)));
				uint32_t ivz = static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, vz * 255.0f)));
				// B | (G<<8) | (R<<16)
				// if(vv) {
				// 	iv = 255 - iv;
				// 	*dst = (iv | (iv<<8) | (iv<<16) | (255<<24)); // black -> white
			 // 		//*dst = (255 - iv + (iv<<8) + (iv<<16) + (255<<24)); // blue -> yellow
				// }
				// else {
				// 	iv = 255 - iv;
				// 	*dst = (192 + iv/8 | (((iv*6)/8)<<8) | (((iv*6)/8)<<16) | (255<<24));
				// }
				*dst = ((vv ? 255 : 0) | ((ivz)<<8) | ((ivo)<<16) | (255<<24));
				dst++;
			}
		}
	}
	else {
		unsigned int lw = 3*w;
		setImagePaintDevice(lw, h);
		uint32_t* dst = reinterpret_cast<uint32_t*>(getImage()->bits());
		WriteMatToImg(mo / mo_max, dst, lw, CONV);
		WriteMatToImg(mz / mz_max, dst + w, lw, 1);
		WriteMatToImg(map_->getMapProbability(), dst + 2*w, lw, CONV);
	}

	// //getImage()->save("/tmp/map.png");
	requireRepaint();
}

}
