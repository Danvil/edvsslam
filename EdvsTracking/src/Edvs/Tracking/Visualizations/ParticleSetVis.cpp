/*
 * ParticleSetVis.cpp
 *
 *  Created on: Mar 12, 2012
 *      Author: david
 */

#include "ParticleSetVis.h"
#include "Painters.hpp"
#include <QPainter>

namespace Edvs {

ParticleSetVis::ParticleSetVis(std::string name)
: Visualization2D(name)
{
}

ParticleSetVis::~ParticleSetVis()
{
}

void ParticleSetVis::notify(const ParticleSet& particles)
{
	particles_ = particles;
	requireRepaint();
}

void ParticleSetVis::paint(QPainter& painter)
{
	painter.fillRect(painter.window(), Qt::black);
	PreparePainter(painter);

	PaintAxes(painter);
	PaintParticles(painter, particles_);
	PaintParticlesDistribution(painter, particles_);
}

}
