#ifndef EDVS_VIS_EXPLORATIONVIS_H_
#define EDVS_VIS_EXPLORATIONVIS_H_

#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/SolutionObserver.hpp>
#include <QtGui/QImage>
#include <Exploration/Exploration.hpp>

namespace Edvs {

// FIXME only static map is supported

class ExplorationVis
: public VisualizationImage,
  public SolutionObserver
{
public:
	ExplorationVis();
	virtual ~ExplorationVis();

	void notify(const Solution& solution);

	void setExploration(boost::shared_ptr<Edvs::Exploration> exploration)
	{
		exploration_ = exploration;
	}

private:
	//void draw_hexagon(QPainter& painter, float x, float y, float radius);
	void updateImage();

private:
	Solution solution_;
	boost::shared_ptr<Edvs::Exploration> exploration_;
};

}
#endif