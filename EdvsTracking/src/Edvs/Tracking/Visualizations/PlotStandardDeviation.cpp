/*
 * PlotStandardDeviation.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: martin
 */

#include <Edvs/Tracking/Visualizations/PlotStandardDeviation.hpp>

namespace Edvs {

PlotStandardDeviation::PlotStandardDeviation(const std::string name, QWidget *parent) : QwtPlot( parent )
{
	// set up qwt plot
    QwtPlot::setTitle( QString::fromStdString(name) );
    QwtPlot::setAxisTitle(xBottom, "time (micro seconds)");
    QwtPlot::setAxisTitle(yLeft, "difference (meter)");
    QwtLegend* legend = new QwtLegend();
    QwtPlot::insertLegend(legend);
    // curves
    curve1_ = new QwtPlotCurve("Curve 1");
    // set parameters of plots
    curve1_->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve1_->setPen(QPen(QColor(255,0,0),2));
    curve1_->setStyle(QwtPlotCurve::Lines);
    curve1_->attach(this);
}

PlotStandardDeviation::~PlotStandardDeviation()
{
	delete curve1_;
}

void PlotStandardDeviation::notify(const ParticleSet& particles, Timestamp time)
{
	Vec3 sum = Vec3::Zero();		// [x, y, rot]
	// compute mean of particle pose
	for(int i=0; i<particles.size() ;i++){
		sum[0] += particles[i].state.px();
		sum[1] += particles[i].state.py();
		sum[2] += particles[i].state.rot;
	}
	Vec3 mean = sum / static_cast<Real>(particles.size());
	//std::cout << "mean: " << mean.transpose() << std::endl;

	//compute covariance matrix
	Mat3 Cov = Mat3::Zero();
	for(int i=0; i<particles.size() ;i++){
		Vec3 temp = Vec3::Zero();
		temp[0] = particles[i].state.px() - mean[0];
		temp[1] = particles[i].state.py() - mean[1];
		temp[2] = particles[i].state.rot - mean[2];
		Cov += temp * temp.transpose();
	}
	Cov /= static_cast<Real>(particles.size());

	Real certainty = std::sqrt(Cov.determinant());

	time_.push_back( time );
	certainty_.push_back( static_cast<double>(certainty) );

    // update plot
    curve1_->setSamples(time_, certainty_);
    QwtPlot::replot();
}

void PlotStandardDeviation::saveDataToFile(const std::string& file_name)
{
	std::fstream f;
	f.open(file_name, std::ios::out|std::ios::trunc);
	for(int i=0; i<certainty_.size() ;i++){
		f << time_[i] << " " << certainty_[i] << std::endl;
		//std::cout << time_[i] << " " << certainty_[i] << std::endl;
	}
	f.close();
}

} /* namespace Edvs */
