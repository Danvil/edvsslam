/*
 * TrackerVisualization.cpp
 *
 *  Created on: Jun 4, 2012
 *      Author: user
 */

#include "TrackerVisualization.hpp"
#include "Painters.hpp"
#include <QPainter>

namespace Edvs {

TrackerVisualization::TrackerVisualization(std::string name)
: Visualization2D(name)
{
}

void TrackerVisualization::setTracker(const boost::shared_ptr<Tracker>& tracker)
{
	tracker_ = tracker;
	requireRepaint();
}

void TrackerVisualization::setReferenceSolution(const Solution& reference)
{
	reference_solution_ = reference;
	requireRepaint();
}

void TrackerVisualization::notifySolution(const Solution& solution)
{
	actual_solution_.data = std::vector<SolutionItem>(
			solution.data.end() - std::min<std::size_t>(solution.data.size(), 1000),
			solution.data.end());
	requireRepaint();
}

void TrackerVisualization::notifyParticles(const ParticleSet& particles)
{
	actual_particles_ = particles;
	// repaint for particles only if vis is enabled
	if(isEnabled()) {
		requireRepaint();
	}
}

void TrackerVisualization::paint(QPainter& painter)
{
	painter.fillRect(painter.window(), Qt::black);
//	painter.setRenderHint(QPainter::Antialiasing, true);
	PreparePainter(painter);

	// paint map as background image
	if(tracker_ && tracker_->getObjective().map_) {
		//float x_min, x_max, y_min, y_max;
		//tracker_->getObjective().map_->getDimensions(x_min, x_max, y_min, y_max);
		//painter.drawImage(QRectF(x_min, y_min, x_max - x_min, y_max - y_min), tracker_->getObjective().map_->getQImage());
		// FIXME
	}

	PaintAxes(painter);

	if(actual_solution_.data.size() > 0) {
		painter.setPen(Qt::red);
		PaintSolution(painter, actual_solution_);
	}

	if(reference_solution_.data.size() > 0) {
		painter.setPen(Qt::blue);
		PaintSolution(painter, reference_solution_, 10);
	}

	// paint expected position
	if(actual_solution_.data.size() > 0 && reference_solution_.data.size() > 0) {
		State expected = reference_solution_.getStateByTime(actual_solution_.data.back().time);
		painter.setPen(Qt::yellow);
		painter.setBrush(Qt::transparent);
		painter.drawEllipse(QPointF(expected.pos.x(), expected.pos.y()), 0.05, 0.05);
		PaintStateDirection(painter, expected, 0.1);
	}

	// paint current best position
	if(actual_solution_.data.size() > 0) {
		auto it = std::find_if(actual_solution_.data.begin(), actual_solution_.data.end(),
				[this](const SolutionItem& item) { return item.time >= actual_solution_.data.back().time; });
		if(it != actual_solution_.data.end()) {
			State actual = it->state;
			painter.setPen(Qt::yellow);
			painter.setBrush(Qt::transparent);
			painter.drawEllipse(QPointF(actual.pos.x(), actual.pos.y()), 0.02, 0.02);
			PaintStateDirection(painter, actual, 0.1);
		}
	}

	// paint particles only if visualization is enabled
	if(isEnabled()) {
		PaintParticles(painter, actual_particles_);
//		PaintParticlesDistribution(painter, actual_particles_);
	}

//	if(tracker_ && tracker_->getObjective().map_) {
//		// paint windows for particles
//		painter.setPen(Qt::green);
//		painter.setBrush(Qt::NoBrush);
//		for(const Particle& p : actual_particles_) {
//			// FIXME
//			// FIXME
//			// FIXME
//
//			QPolygonF poly;
//
//			Ray<float> ray = tracker_->getObjective().sensor_model_.computeEventRay(p.state.pos.cast<float>(), p.state.rot, 0, 0);
//			Eigen::Vector2f v00 = tracker_->getObjective().map_->getPoint(ray);
//	//		std::cout << v.transpose() << std::endl;
//			poly.append(QPointF(v00.x(), v00.y()));
//
//			ray = tracker_->getObjective().sensor_model_.computeEventRay(p.state.pos.cast<float>(), p.state.rot, 128, 0);
//			Eigen::Vector2f v10 = tracker_->getObjective().map_->getPoint(ray);
//	//		std::cout << v.transpose() << std::endl;
//			poly.append(QPointF(v10.x(), v10.y()));
//
//			ray = tracker_->getObjective().sensor_model_.computeEventRay(p.state.pos.cast<float>(), p.state.rot, 128, 128);
//			Eigen::Vector2f v11 = tracker_->getObjective().map_->getPoint(ray);
//	//		std::cout << v.transpose() << std::endl;
//			poly.append(QPointF(v11.x(), v11.y()));
//
//			ray = tracker_->getObjective().sensor_model_.computeEventRay(p.state.pos.cast<float>(), p.state.rot, 0, 128);
//			Eigen::Vector2f v01 = tracker_->getObjective().map_->getPoint(ray);
//	//		std::cout << v.transpose() << std::endl;
//			poly.append(QPointF(v01.x(), v01.y()));
//
//			painter.drawPolygon(poly);
//
//			// paint a small corner to indicate (0,0)
//			{
//				Eigen::Vector2f a = v00 + 0.1*(v01 - v00);
//				Eigen::Vector2f b = v00 + 0.1*(v10 - v00);
//				painter.drawLine(QPointF(a.x(), a.y()), QPointF(b.x(), b.y()));
//			}
//		}
//	}

}

}
