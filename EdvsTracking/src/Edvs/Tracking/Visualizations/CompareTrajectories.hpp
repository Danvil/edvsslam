/*
 * CompareTrajectories.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: user
 */

#ifndef COMPARETRAJECTORIES_HPP_
#define COMPARETRAJECTORIES_HPP_

#include <Edvs/Tracking/SaveData.hpp>
#include <Edvs/Tracking/SolutionObserver.hpp>
#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Visualizations/Painters.hpp>
#include <QPainter>
#include <fstream>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_legend.h"


namespace Edvs
{

class CompareTrajectories
: public QwtPlot, public SolutionObserver, public SaveData
{
public:
	CompareTrajectories(const std::string name = "Compare_Trajec", QWidget *parent = 0);
	virtual ~CompareTrajectories();
	void notify(const Solution& solution);
	void setRefTrajectory(const Solution& solution);
	void saveDataToFile(const std::string& file_name = "comp_trajec.txt");

private:
	QwtPlotCurve *curve1_;
	Solution ref_trajectory_;
	QVector<double> diff_;
	QVector<double> time_;
};

} /* namespace Edvs */
#endif /* COMPARETRAJECTORIES_HPP_ */
