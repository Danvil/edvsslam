#include "VisualizationSaver.hpp"
#include <boost/format.hpp>
#include <QString>

namespace Edvs
{
	VisualizationSaver::VisualizationSaver(std::string name){
		counter_ = 0;
		name_ = name;
	}

	VisualizationSaver::~VisualizationSaver(){}

	void VisualizationSaver::save(QImage image)
	{
		counter_ ++;
		boost::format filename_format(name_ + "%05d.png"); //
		std::string filename = (filename_format % counter_).str();
		QString Qfilename = QString::fromUtf8(filename.c_str());
		image.save(QString(Qfilename));
	}
}