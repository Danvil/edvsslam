/*
 * ObjectiveFunctionVisualization.cpp
 *
 *  Created on: Jun 25, 2012
 *      Author: david
 */

#include "ObjectiveFunctionVisualization.hpp"
#include "../Common.hpp"
#include "../Visualizations/Painters.hpp"
#include <Edvs/Tools/FunctionCache.hpp>
#include <boost/progress.hpp>

#define USE_THREADING

namespace Edvs
{

constexpr uint64_t cDT_MS = 10;
constexpr int cSizePx = 48;//192;
constexpr Real cSizeMeter = 3.0;
constexpr Real cPixelSize = cSizeMeter / static_cast<Real>(cSizePx);
constexpr unsigned int cNumEvents = 0; //128;

ObjectiveFunctionVisualization::ObjectiveFunctionVisualization()
: VisualizationImage("Objective Function"),
  pool_(12)
{
	setImagePaintDevice(cSizePx, cSizePx);
}

void ObjectiveFunctionVisualization::notify(const std::vector<Edvs::Event>& events)
{
	if(events.empty()) {
		return;
	}
	// add new events
	events_.insert(events_.end(), events.begin(), events.end());
	// only keep last events
	const uint64_t cDT = cDT_MS*1000;
	uint64_t target = (events_.back().t > cDT) ? events_.back().t - cDT : 0;
	auto it = std::find_if(events_.begin(), events_.end(), [target](const Edvs::Event& event) {
		return event.t >= target;
	});
	events_.erase(events_.begin(), it);
}

/** Randomly shuffles the first elements */
template<class fwditer>
fwditer random_unique(fwditer begin, fwditer end, size_t num_random) {
    size_t left = std::distance(begin, end);
    while (num_random--) {
        fwditer r = begin;
        std::advance(r, rand()%left);
        std::swap(*begin, *r);
        ++begin;
        --left;
    }
    return begin;
}

void ObjectiveFunctionVisualization::update(float rot)
{
	const int height = getImage()->height();
	const int width = getImage()->width();
	// randomly pick some events
	std::vector<Event> picked_events = events_;
	if(cNumEvents > 0 && cNumEvents < picked_events.size()) {
		random_unique(picked_events.begin(), picked_events.end(), cNumEvents);
		picked_events.resize(cNumEvents);
	}
	// state
	const float rot_cos = std::cos(rot);
	const float rot_sin = std::sin(rot);
	// compute scores
	std::vector<Real> scores(width * height);
	// FIXME
	// WARNING this must be here, to call objective_.logLikelihood at least once to init static log cache
	// FIXME
	Real score_min = objective_.logProductOfScoresWithNoise(Eigen::Vector3f::Zero(), rot_sin, rot_cos, picked_events);
	Real score_max = score_min;
#ifdef USE_THREADING
	for(int i=0; i<height; i++) {
		const float y = cPixelSize * static_cast<Real>(height/2 - i);
		Real* target = &(scores[i*width]);
		pool_.schedule([this, y, rot_sin, rot_cos, &picked_events, width, target]() {
			for(int j=0; j<width; j++) {
				const float x = cPixelSize * static_cast<Real>(j - width/2);
				Real s = objective_.logProductOfScoresWithNoise(Eigen::Vector3f{x, y, 0.0f}, rot_sin, rot_cos, picked_events);
				s /= picked_events.size();
//				score_min = std::min(score_min, s);
//				score_max = std::max(score_max, s);
				target[j] = s;
			}
		});
	}
	pool_.wait();
#else
	std::cout << "Computing objective function ..." << std::endl;
	std::cout << "events: #=" << events_.size() << ", #used=" << picked_events.size() << " time=[" << events_.front().time << "," << events_.back().time << "]" << std::endl;
	boost::progress_display progress(height);
	for(int i=0; i<height; i++, ++progress) {
		Real y = cPixelSize * static_cast<Real>(height/2 - i);
		for(int j=0; j<width; j++) {
			Real x = cPixelSize * static_cast<Real>(j - width/2);
			Real s = objective_.logLikelihood(x, y, rot_sin, rot_cos, picked_events);
			s /= picked_events.size();
//			score_min = std::min(score_min, s);
//			score_max = std::max(score_max, s);
			scores[j + i*width] = s;
		}
	}
	std::cout << " Finished." << std::endl;
#endif
	// find min and max
	score_min = -3;
	score_max = 0;
//	std::cout << "min=" << score_min << " max=" << score_max << std::endl;
	// create image
	const Real maxmin_inv = 1.0 / (score_max - score_min);
	for(int i=0; i<height; i++) {
		for(int j=0; j<width; j++) {
			Real s = (scores[j + i*width] - score_min) * maxmin_inv;
			getImage()->setPixel(j, i, Score2Color(s));
		}
	}
	// draw lines for 0 and 1 meters
	const int meter_in_px = static_cast<int>(static_cast<Real>(cSizePx) / cSizeMeter);
	const QRgb color_black = qRgb(0,0,0);
	for(int i=0; i<height; i++) {
		getImage()->setPixel(width/2, i, color_black);
		getImage()->setPixel(width/2 + meter_in_px, i, color_black);
		getImage()->setPixel(width/2 - meter_in_px, i, color_black);
	}
	for(int j=0; j<width; j++) {
		getImage()->setPixel(j, height/2, color_black);
		getImage()->setPixel(j, height/2 + meter_in_px, color_black);
		getImage()->setPixel(j, height/2 - meter_in_px, color_black);
	}
	requireRepaint();
}


}
