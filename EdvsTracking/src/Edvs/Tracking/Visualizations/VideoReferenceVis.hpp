/*
 * VideoReferenceVis.h
 *
 *  Created on: Jan 14, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_VideoReferenceVis_H_
#define EDVS_VIS_VideoReferenceVis_H_

#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/SolutionObserver.hpp>
#include <Edvs/Tracking/EventObserver.hpp>
#include <QtGui/QImage>

namespace Edvs {

// FIXME only static map is supported

class VideoReferenceVis
: public VisualizationImage,
  public SolutionObserver,
  public EventObserver
{
public:
	VideoReferenceVis();
	virtual ~VideoReferenceVis();

	void setMap(const boost::shared_ptr<Map>& map);

	void setObjective(const ObjectiveFunctionF* objf);

	void notify(const Solution& solution);

	void notify(const std::vector<Edvs::Event>& events);

	void setReferenceSolution(const Solution& reference_solution);

private:
	void updateImage(uint64_t next_timestep);

private:
	const ObjectiveFunctionF* objf_;
	boost::shared_ptr<GridMap> map_;
	std::vector<Event> events_;
	Solution solution_;
	Solution reference_solution_;
	uint64_t next_timestep_;

};

}
#endif
