/*
 * SampleMap2DVis.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: david
 */

#include <Edvs/Tools/Maps/SampleMap2D.hpp>
#include "SampleMap2DVis.hpp"
#include "Painters.hpp"
#include <QPainter>
#include <stdint.h>

namespace Edvs {

SampleMap2DVis::SampleMap2DVis()
: Visualization2D("SampleMap2DVis")
{
}

SampleMap2DVis::~SampleMap2DVis()
{
}

void SampleMap2DVis::setMap(const boost::shared_ptr<Map>& map)
{
	map_ = boost::dynamic_pointer_cast<SampleMap2D>(map);
}

void SampleMap2DVis::spin(time_t, Timespan)
{
	if(!isEnabled()) {
		return;
	}
	requireRepaint();
}

void SampleMap2DVis::paint(QPainter& painter)
{
	painter.fillRect(painter.window(), Qt::black);
	PreparePainter(painter);
	// paint samples as points with circles
	for(const SampleMap2D::sample_t& s : map_->getSampleStore().getSamples()) {
		const Eigen::Vector2f& q = s.position;
		const QPointF p(q.x(), q.y());
		const float r = s.sigma;
		const float w = s.weight;
		const unsigned char c = std::min<int>(255, static_cast<unsigned int>(std::max(0.0f, w) * 255.0f));
		painter.setPen(QColor(c,c,255-c));
		painter.drawPoint(p);
		painter.drawEllipse(p, r, r);
	}
}

}
