#include "ExplorationVis.hpp"
#include <Eigen/Dense>
#include <QtGui/QPainter>
#include <QtGui/QPen>
#include <QtGui/QBrush>
#include "Painters.hpp"

namespace Edvs {

ExplorationVis::ExplorationVis()
: VisualizationImage("ExplorationVis")
{
	setImagePaintDevice(1, 1);
}

ExplorationVis::~ExplorationVis()
{
}

void ExplorationVis::notify(const Solution& solution)
{
	solution_ = solution;
	updateImage();
}

// void ExplorationVis::draw_hexagon(QPainter& painter, float x, float y, float radius)
// 	{
// 		constexpr float cos30 = 0.866025404;
// 		static const QPointF hex_points[6] = {
// 	     	QPointF(x + (cos30 * radius), y + (0.5 * radius)),
// 	     	QPointF(x, y + radius),
// 	     	QPointF(x - (cos30 * radius), y + (0.5 * radius)),
// 	     	QPointF(x - (cos30 * radius), y - (0.5 * radius)),
// 	     	QPointF(x, y - radius),
// 	     	QPointF(x + (cos30 * radius), y - (0.5 * radius))
//      	};
//      	painter.drawConvexPolygon(hex_points, 6);
//  	};

void ExplorationVis::updateImage()
{
	if(!isEnabled()) {
		return;
	}
	if(solution_.data.empty())
	{
		return;
	}

	constexpr int vis_width = 704;
	constexpr int vis_height = 704;

	setImagePaintDevice(vis_width, vis_height);

	QPainter painter(getImage());
	QTransform t;
	t.scale(1.0, -1.0);
	painter.setTransform(t);
	painter.setWindow(
		-static_cast<int>(vis_height / 2),
		-static_cast<int>(vis_width / 2),
		vis_width,
		vis_height);
	
	painter.fillRect(painter.window(), Qt::white);
	// QBrush brush;
	// brush.setColor(qRgb(255,200,255));
	// painter.setBrush(brush);
	// painter.drawRect(-static_cast<int>(vis_height / 2),
	// 	-static_cast<int>(vis_width / 2),
	// 	vis_width,
	// 	vis_height);
	
	QPen pen(Qt::SolidLine);
	pen.setColor(qRgb(0,0,0));
	painter.setPen(pen);
	
//	draw_hexagon(painter, 0.0, 0.0, 15.0);

	constexpr float draw_scale = 0.1/0.01;

    // //draw accessibility
    // Eigen::MatrixXf accessibility_map = exploration_->get_accessibility_map();
    // for(int y = 0; y < accessibility_map.rows(); y++)
    // {
    //     for(int x = 0; x < accessibility_map.cols(); x++)
    //     {
    //         //if(accessibility_map(x,y) == 0.0){
    //         //    painter.setBrush(QColor(Qt::green));
    //         //}
    //         if(accessibility_map(x,y) == 1.0){
    //             painter.setBrush(QColor(Qt::red));
    //         painter.drawEllipse(QPoint((x + ((y%2)*0.5))*draw_scale - 352.0, y*draw_scale - 352.0), 3, 3);
    //         }
            
    //         //painter.drawEllipse(QPoint((x + ((y%2)*0.5))*draw_scale - 352.0, y*draw_scale - 352.0), 3, 3);

    //     }
    // }

    //draw frontier
    Eigen::MatrixXf frontier_map = exploration_->get_frontier_map();
    for(int y = 0; y < frontier_map.rows(); y++)
    {
        for(int x = 0; x < frontier_map.cols(); x++)
        {
            if(frontier_map(x,y) == 1.0){
               painter.fillRect((x + ((y%2)*0.5))*draw_scale - 352.0 - 4, y*draw_scale - 352.0 - 4, 8, 8,
                QColor(64,64,64));
            }
        }
    }

    // i was here map
    // Eigen::MatrixXf iwashere_map = exploration_->get_search_strategy().get_iwashere_map();
    // float iwashere_max = iwashere_map.maxCoeff();
    // for(int y = 0; y < iwashere_map.rows(); y++)
    //     {
    //         for(int x = 0; x < iwashere_map.cols(); x++)
    //         {
    //             float val = iwashere_map(x,y) / iwashere_max;
    //             if(val > 0.05f) {
    //                 int iv = (int) (255.0f*val);
    //                 painter.fillRect((x + ((y%2)*0.5))*draw_scale - 352.0 - 2, y*draw_scale - 352.0 - 2, 4, 4,
		  //          	QColor(256-iv,iv,0));
    //             }
    //         }
    //     }


    //draw cost 
    Eigen::MatrixXf cost_map = exploration_->get_search_strategy().get_cost_map();
    for(int y = 0; y < cost_map.cols(); y++)
    {
        for(int x = 0; x < cost_map.rows(); x++)
        {
        	float cost = 2.0f*cost_map(x,y);
        	QColor color;
        	if(cost < 0.0f) {
        		color = QColor(Qt::red);
        	}
        	else {
        		cost = std::min(cost, 1.0f);
        		int ci = static_cast<int>(cost * 255.0f);
        		color = QColor(128-ci/2, 128+ci/2, 128-ci/2);
        	}
            painter.fillRect((x + ((y%2)*0.5))*draw_scale - 352.0 - 2, y*draw_scale - 352.0 - 2, 4, 4, color);
        }
    }

    painter.setBrush(QColor(Qt::black));
    painter.setPen(QColor(Qt::black));

    for(const Bump& b : exploration_->get_bumps())
    {
        painter.drawEllipse(QPointF(b.position[0]*100.0f-2.0f, b.position[1]*100.0f-2.0f), 4, 4);
        painter.drawLine(QPointF(
				b.position[0]*100.0f,
				b.position[1]*100.0f), 
    			QPointF(
    			(b.position[0] + b.normal[0]*0.2f)*100.0f,
    			(b.position[1] + b.normal[1]*0.2f)*100.0f));
    }

    painter.setBrush(QColor(0,128,255));
    painter.setPen(QColor(0,128,255));
    for(const grid_coord_t& gx : exploration_->get_search_strategy().get_path()) {
    	Eigen::Vector2f x = grid_to_world(gx);
		painter.drawEllipse(QPointF(x[0]*100.0f, x[1]*100.0f), 3, 3);
    }

    painter.setBrush(QColor(Qt::blue));
    painter.setPen(QColor(Qt::blue));
    Eigen::Vector2f target = exploration_->get_search_strategy().get_next_target();
    painter.drawEllipse(QPointF(target[0]*100.0f, target[1]*100.0f), 5, 5);

    painter.setBrush(QColor(Qt::magenta));
    painter.setPen(QColor(Qt::magenta));
    Eigen::Vector2f pos = exploration_->get_search_strategy().get_current_position();
    painter.drawEllipse(QPointF(pos[0]*100.0f, pos[1]*100.0f), 5, 5);


	// paint path
	// {
	// 	QPen pen(Qt::SolidLine);
	// 	pen.setColor(qRgb(0,0,255));
	// 	painter.setPen(pen);
	// }
	// Solution sol;
	// sol.data.insert(sol.data.begin(),
	// 	solution_.data.end() - std::min<unsigned>(1000,solution_.data.size()),
	// 	solution_.data.end());
	// PaintSolution(painter, sol);


    //painter.drawLine(0,0,50,50);
    //std::cout << "EXPLORATION VIS REPAINT" << std::endl;
	
	requireRepaint();
}

}
