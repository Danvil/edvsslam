/*
 * PlotStandardDeviation.hpp
 *
 *  Created on: Apr 11, 2012
 *      Author: martin
 */

#ifndef PLOTSTANDARDDEVIATION_HPP_
#define PLOTSTANDARDDEVIATION_HPP_

#include <Edvs/Tracking/SaveData.hpp>
#include <Edvs/Tracking/Common.hpp>
#include <fstream>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_legend.h"

namespace Edvs {

class PlotStandardDeviation
: public QwtPlot, public SaveData
{
public:
	PlotStandardDeviation(const std::string name = "Certainty", QWidget *parent = 0);
	virtual ~PlotStandardDeviation();

	void notify(const ParticleSet& particles, Timestamp time);
	void saveDataToFile(const std::string& file_name = "certainty.txt");

private:
	QwtPlotCurve *curve1_;
	QVector<double> time_;
	QVector<double> certainty_;
};

} /* namespace Edvs */
#endif /* PLOTSTANDARDDEVIATION_HPP_ */
