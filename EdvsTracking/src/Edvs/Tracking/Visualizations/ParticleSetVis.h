/*
 * ParticleSetVis.h
 *
 *  Created on: Mar 12, 2012
 *      Author: david
 */

#ifndef PARTICLESVIS_H_
#define PARTICLESVIS_H_

#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Visualization.hpp>

namespace Edvs {

class ParticleSetVis
: public Visualization2D
{
public:
	ParticleSetVis(std::string name = "ParticleSetVis");
	virtual ~ParticleSetVis();

	void notify(const ParticleSet& particles);

	void paint(QPainter& painter);

private:
	ParticleSet particles_;

};

}
#endif
