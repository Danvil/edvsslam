/*
 * CompareTrajectories.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: user
 */

#include "Edvs/Tracking/Visualizations/CompareTrajectories.hpp"

namespace Edvs {

CompareTrajectories::CompareTrajectories(const std::string name, QWidget *parent)
: QwtPlot(parent)
{
	// set up qwt plot
    QwtPlot::setTitle( QString::fromStdString(name) );
    QwtPlot::setAxisTitle(xBottom, "time (micro seconds)");
    QwtPlot::setAxisTitle(yLeft, "difference (meter)");
    QwtLegend* legend = new QwtLegend();
    QwtPlot::insertLegend(legend);
    // curves
    curve1_ = new QwtPlotCurve("Curve 1");
    // set parameters of plots
    curve1_->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve1_->setPen(QPen(QColor(255,0,0),2));
    curve1_->setStyle(QwtPlotCurve::Lines);

    // set up visualization
}

CompareTrajectories::~CompareTrajectories() {
}

void CompareTrajectories::setRefTrajectory(const Solution& solution)
{
	ref_trajectory_ = solution;
}

void CompareTrajectories::notify(const Solution& solution)
{
	Timestamp delta_time, interpol_time;
	Real delta_x, delta_y, interpol_x, interpol_y, difference;
	unsigned int i, k=0;
	Solution current_reference;

	diff_.clear();
	time_.clear();

	if(ref_trajectory_.data.empty()){
		std::cout << "no reference trajectory to compare" <<std::endl;
		return;
	}

	//TODO: save interpolation so it does not have to be recomputed (alllocating memory is bad !!)

	current_reference.add(0, ref_trajectory_.data.front().state, 42.0);

	// interpolate
	for(i=0; i<solution.data.size() ;i++){

		if(solution.data[i].time > ref_trajectory_.data[k].time){
			while( k < (ref_trajectory_.data.size()-1) && ref_trajectory_.data[k+1].time < solution.data[i].time ){
				k++;
				current_reference.add(0, ref_trajectory_.data[k].state, 42.0);
			}
		}

		// TODO: carefully think about borders of solution and reference . is code stable in every possible/likely situation ?

		delta_time = ref_trajectory_.data[k+1].time - ref_trajectory_.data[k].time;
		delta_x = ref_trajectory_.data[k+1].state.px() - ref_trajectory_.data[k].state.px();
		delta_y = ref_trajectory_.data[k+1].state.py() - ref_trajectory_.data[k].state.py();
		interpol_time = solution.data[i].time - ref_trajectory_.data[k].time;
		interpol_x = (interpol_time/delta_time)*delta_x + ref_trajectory_.data[k].state.px();
		interpol_y = (interpol_time/delta_time)*delta_y + ref_trajectory_.data[k].state.py();
		difference = std::sqrt( std::pow(interpol_x - solution.data[i].state.px(), 2) + std::pow(interpol_y - solution.data[i].state.py(), 2) );

		diff_.push_back( static_cast<double>(difference) );
		time_.push_back( static_cast<double>(solution.data[i].time) );
	}

    // copy the data into the curves
    curve1_->setSamples(time_, diff_);

    // add curves to plot
    curve1_->attach(this);
    QwtPlot::replot();

    //std::cout << current_reference.data.size() <<std::endl;

    // update visualization of reference trajectory
//	if(!canPaint()) return;
//	QPainter painter(getPaintDevice());
//	painter.fillRect(painter.window(), Qt::black);
//	PreparePainter(painter);
//
//	PaintAxes(painter);
//	PaintSolution(painter, current_reference);
//	requireRepaint();
}


void CompareTrajectories::saveDataToFile(const std::string& file_name)
{
	std::fstream f;
	f.open(file_name, std::ios::out|std::ios::trunc);
	for(int i=0; i<diff_.size() ;i++){
		f << time_[i] << " " << diff_[i] << std::endl;
		//std::cout << time_[i] << " " << diff_[i] << std::endl;
	}
	f.close();
}

} /* namespace Edvs */
