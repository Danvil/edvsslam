/*
 * Painters.hpp
 *
 *  Created on: Mar 12, 2012
 *      Author: david
 */

#ifndef PAINTERS_HPP_
#define PAINTERS_HPP_

#include <Edvs/Event.hpp>
#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tools/ScoreToColor.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <QPainter>

namespace Edvs
{

	void PreparePainter(QPainter& painter);

	void PaintAxes(QPainter& painter);

	void PaintStatePos(QPainter& painter, const State& state);

	void PaintStateVelocity(QPainter& painter, const State& state);

	void PaintStateDirection(QPainter& painter, const State& s, float len);

	void PaintRetina(QPainter& painter, const State& s, const ObjectiveFunctionF* map);

	void PaintEvents(QPainter& painter, const State& s, const std::vector<Event> events, const ObjectiveFunctionF* map, bool fast=true);

	void PaintParticles(QPainter& painter, const ParticleSet& particles);

	void PaintSolution(QPainter& painter, const Solution& solution, int skip=1);

	void PaintSolutionTime(QPainter& painter, const Solution& solution, unsigned long time);

	void PaintParticlesDistribution(QPainter& painter, const ParticleSet& particles);

}

#endif
