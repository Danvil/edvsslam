/*
 * RetinaDisplay.cpp
 *
 *  Created on: Mar 1, 2012
 *      Author: david
 */

#include <Edvs/Tracking/Tools/Time.hpp>
#include "RetinaDisplay.hpp"

namespace Edvs
{

const double cDecayPerSec = 500.0;
const QRgb cColorOn = qRgb(255, 255, 0);
const QRgb cColorOff = qRgb(100, 155, 255);

RetinaDisplay::RetinaDisplay()
: VisualizationImage("Retina"),
  last_decay_(0),
  last_event_time_(0),
  decay_remainder_(0.0)
{
	setImagePaintDevice(128, 128);
	getImage()->fill(qRgb(0,0,0));
}

QRgb Add(QRgb a, QRgb b) {
	return qRgb(
			std::min(qRed(a) + qRed(b), 255),
			std::min(qGreen(a) + qGreen(b), 255),
			std::min(qBlue(a) + qBlue(b), 255)
	);
}

void RetinaDisplay::notify(const std::vector<Edvs::Event>& events)
{
	if(!isEnabled()) {
		return;
	}
	if(events.empty()) {
		return;
	}
	for(const Edvs::Event& e : events) {
		// an event is translated into a color dot
		if(getImage()->rect().contains(e.x, e.y)) {
			QRgb color = Add(getImage()->pixel(e.x, e.y), e.parity ? cColorOn : cColorOff);
			getImage()->setPixel(e.x, e.y, color);
		}
	}
	last_event_time_ = events.back().t;
	updateImage();
	requireRepaint();
}

void RetinaDisplay::updateImage()
{
	// apply decay
	const int64_t dt = last_event_time_ - last_decay_;
	double decay = cDecayPerSec * double(dt) / 1000000.0 + decay_remainder_;

	decay = std::min(255.0, std::max(0.0, decay));
	
	const int decay_int = static_cast<int>(decay);
	decay_remainder_ = decay - static_cast<double>(decay_int);
	// FIXME make if faster by not using pixel/setPixel!
	for(int y=0; y<getImage()->height(); y++) {
		for(int x=0; x<getImage()->width(); x++) {
			QRgb color = getImage()->pixel(x, y);
			int cr = qRed(color) - decay_int;
			int cg = qGreen(color) - decay_int;
			int cb = qBlue(color) - decay_int;
			color = qRgb(cr < 0 ? 0 : cr, cg < 0 ? 0 : cg, cb < 0 ? 0 : cb);
			getImage()->setPixel(x, y, color);
		}
	}
	last_decay_ = last_event_time_;
}

}

