/*
 * GridMapVis.h
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_GRIDMAPVIS_H_
#define EDVS_VIS_GRIDMAPVIS_H_

#include "MapVis.hpp"
#include <Edvs/Tools/Maps/GridMap.hpp>
#include <QtGui/QImage>

namespace Edvs {

// FIXME only static map is supported

class GridMapVis
: public MapVis, public VisualizationImage
{
public:
	GridMapVis();
	virtual ~GridMapVis();

	void setMap(const boost::shared_ptr<Map>& map);

	void spin(time_t time, Timespan dt);

private:
	void updateImage();

private:
	boost::shared_ptr<GridMap> map_;

};

}
#endif
