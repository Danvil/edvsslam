/*
 * ParticleScoreVisualization.hpp
 *
 *  Created on: Jun 26, 2012
 *      Author: david
 */

#ifndef PARTICLESCOREVISUALIZATION_HPP_
#define PARTICLESCOREVISUALIZATION_HPP_

#include "../Common.hpp"
#include "../Visualization.hpp"

namespace Edvs
{

class ParticleScoreVisualization
: public VisualizationImage
{
public:
	ParticleScoreVisualization();
	virtual ~ParticleScoreVisualization() {}

	void setParticles(const ParticleSet& particles) {
		particles_prev_ = particles_;
		particles_ = particles;
		paintImage();
	}

private:
	void paintImage();

private:
	ParticleSet particles_;
	ParticleSet particles_prev_;

};

}

#endif
