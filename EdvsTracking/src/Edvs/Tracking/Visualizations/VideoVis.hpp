/*
 * VideoVis.h
 *
 *  Created on: Jan 14, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_VideoVis_H_
#define EDVS_VIS_VideoVis_H_

#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/SolutionObserver.hpp>
#include <Edvs/Tracking/EventObserver.hpp>
#include <QtGui/QImage>

namespace Edvs {

// FIXME only static map is supported

class VideoVis
: public VisualizationImage,
  public SolutionObserver,
  public EventObserver
{
public:
	VideoVis();
	virtual ~VideoVis();

	void setObjective(const ObjectiveFunctionF* objf);

	void notify(const Solution& solution);

	void notify(const std::vector<Edvs::Event>& events);

private:
	void updateImage();

private:
	const ObjectiveFunctionF* objf_;
	boost::shared_ptr<GridMap> map_;
	std::vector<Event> events_;
	Solution solution_;
	uint64_t next_timestep_;

};

}
#endif
