/*
 * TrajectoryVisualization.hpp
 *
 *  Created on: Mar 2, 2012
 *      Author: user
 */

#ifndef TRAJECTORYVISUALIZATION_HPP_
#define TRAJECTORYVISUALIZATION_HPP_

#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/SolutionObserver.hpp>

namespace Edvs {

class TrajectoryVisualization
: public SolutionObserver, public Visualization2D
{
public:
	TrajectoryVisualization(std::string name = "Trajectory");
	virtual ~TrajectoryVisualization();

	void notify(const Solution& solution);

	void paint(QPainter& painter);

private:
	Solution solution_;

};

}

#endif
