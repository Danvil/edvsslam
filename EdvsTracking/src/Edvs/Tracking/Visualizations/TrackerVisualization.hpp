/*
 * TrackerVisualization.hpp
 *
 *  Created on: jun 4, 2012
 *      Author: user
 */

#ifndef TrackerVisualization_HPP_
#define TrackerVisualization_HPP_

#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Tracker.hpp>

namespace Edvs {

class TrackerVisualization
: public Visualization2D
{
public:
	TrackerVisualization(std::string name = "Tracker");

	virtual ~TrackerVisualization() {}

	void setTracker(const boost::shared_ptr<Tracker>& tracker);

	void setReferenceSolution(const Solution& reference);

	void notifySolution(const Solution& solution);

	void notifyParticles(const ParticleSet& particles);

	void paint(QPainter& painter);

private:
	boost::shared_ptr<Tracker> tracker_;
	Solution reference_solution_;
	Solution actual_solution_;
	ParticleSet actual_particles_;

};

}

#endif
