/*
 * SampleMap2DVis.hpp
 *
 *  Created on: Nov 27, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_GRID3DMAPVIS_HPP_
#define EDVS_VIS_GRID3DMAPVIS_HPP_

#include "MapVis.hpp"

namespace Edvs {

class GridMap3D;

// FIXME only static map is supported

class GridMap3DVis
: public MapVis, public Visualization3D
{
public:
	GridMap3DVis();
	virtual ~GridMap3DVis();

	void setMap(const boost::shared_ptr<Map>& map);

	void spin(time_t time, Timespan dt);

	void render();

private:
	boost::shared_ptr<GridMap3D> map_;
};

}
#endif
