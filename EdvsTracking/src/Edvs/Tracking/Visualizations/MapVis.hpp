/*
 * MapVis.hpp
 *
 *  Created on: Nov 24, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_MAPVIS_H_
#define EDVS_VIS_MAPVIS_H_

#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/Spinable.hpp>
#include <Edvs/Tools/Maps/Map.hpp>
#include <QtGui/QImage>

namespace Edvs {

// FIXME only static map is supported

class MapVis
: public Spinable
{
public:
	virtual ~MapVis() {}

	virtual void setMap(const boost::shared_ptr<Map>& map) = 0;

};

}
#endif
