/*
 * RetinaObjective.cpp
 *
 *  Created on: Jun 26, 2012
 *      Author: david
 */

#include "RetinaObjective.hpp"

namespace Edvs
{

RetinaObjective::RetinaObjective()
: VisualizationImage("Retina Objective")
{
	setImagePaintDevice(128,128);
}

void RetinaObjective::notify(const std::vector<Edvs::Event>& events)
{
	events_.insert(events_.end(), events.begin(), events.end());
}

void RetinaObjective::spin(time_t time, Timespan dt)
{
	if(!isEnabled()) {
		return;
	}
	// update vis
	std::vector<Real> retina_scores(128*128);
	Real rot_sin = std::sin(state_.rot);
	Real rot_cos = std::cos(state_.rot);
	for(int i=0; i<128; i++) {
		for(int j=0; j<128; j++) {
			Real q = objective_.evaluate(state_.pos.cast<float>(), rot_sin, rot_cos, j, i);
			retina_scores[j+i*128] = q;
			//int g = static_cast<int>(96.0f + 64.0f * q);
			int g = static_cast<int>(255.0f * q);
			QRgb color = qRgb(g,g,g);
			getImage()->setPixel(j, i, color);
		}
	}
	// paint events
	for(const Event& e : events_) {
		if(0 <= e.x && e.x < 128 && 0 <= e.y && e.y < 128) {
			// int g = 255.0 * retina_scores[e.x+e.y*128];
			// QRgb color = qRgb(255-g,g,0);
			QRgb color = qRgb(0,255,255);
			getImage()->setPixel(e.x, e.y, color);
		}
	}
	events_.clear();
	// repaint
	requireRepaint();
}


}
