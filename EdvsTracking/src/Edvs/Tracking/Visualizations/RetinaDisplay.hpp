/*
 * RetinaDisplay.hpp
 *
 *  Created on: Mar 1, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_VISUALIZATIONS_RETINADISPLAY_HPP_
#define EDVS_TRACKING_VISUALIZATIONS_RETINADISPLAY_HPP_

#include <Edvs/Tracking/EventObserver.hpp>
#include <Edvs/Tracking/Visualization.hpp>

namespace Edvs
{

class RetinaDisplay
: public VisualizationImage, public EventObserver
{
public:
	virtual ~RetinaDisplay() {}

	RetinaDisplay();

	void notify(const std::vector<Edvs::Event>& events);

private:
	void updateImage();

private:
	int64_t last_decay_;
	int64_t last_event_time_;
	double decay_remainder_;

};

}

#endif
