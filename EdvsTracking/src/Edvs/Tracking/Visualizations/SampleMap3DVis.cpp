/*
 * SampleMap3DVis.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: david
 */

#include <Edvs/Tools/Maps/SampleMap3D.hpp>
#include "SampleMap3DVis.hpp"
#include "Painters.hpp"
#include <Candy/OpenGL.h>
#include <Candy/Primitives.h>
#include <QPainter>
#include <stdint.h>

namespace Edvs {

SampleMap3DVis::SampleMap3DVis()
: Visualization3D("SampleMap3DVis")
{
}

SampleMap3DVis::~SampleMap3DVis()
{
}

void SampleMap3DVis::setMap(const boost::shared_ptr<Map>& map)
{
	map_ = boost::dynamic_pointer_cast<SampleMap3D>(map);
}

void SampleMap3DVis::spin(time_t, Timespan)
{
	if(!isEnabled()) {
		return;
	}
	requireRepaint();
}

void SampleMap3DVis::render()
{
	// paint samples as points with circles
	for(const SampleMap3D::sample_t& s : map_->getSampleStore().getSamples()) {
		const Eigen::Vector3f& q = s.position;
		const float r = s.sigma;
		const float w = s.weight;
		// render sphere
		glPushMatrix();
		glTranslatef(q.x(), q.y(), q.z());
		Candy::Primitives::RenderSphere(r, 6, 6);
		glPopMatrix();
	}
}

}
