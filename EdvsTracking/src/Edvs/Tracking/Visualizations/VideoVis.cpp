/*
 * VideoVis.cpp
 *
 *  Created on: Jan 14, 2012
 *      Author: david
 */

#include "VideoVis.hpp"
#include "Painters.hpp"
#include <stdint.h>
#include <Eigen/Dense>
#include <QtGui/QPainter>

namespace Edvs {

VideoVis::VideoVis()
: VisualizationImage("VideoVis")
{
	setImagePaintDevice(1, 1);
	next_timestep_ = 0;
}

VideoVis::~VideoVis()
{
}

void VideoVis::setObjective(const ObjectiveFunctionF* objf)
{
	objf_ = objf;
	map_ = boost::dynamic_pointer_cast<GridMap>(objf_->map_);
}

void VideoVis::notify(const Solution& solution)
{
	solution_ = solution;
//	updateImage();
}

void VideoVis::notify(const std::vector<Edvs::Event>& events)
{
	events_.insert(events_.end(), events.begin(), events.end());
	updateImage();
}

void VideoVis::updateImage()
{
	if(!isEnabled()) {
		return;
	}
	if(solution_.data.empty())
	{
		return;
	}
	uint64_t current_time = solution_.data.back().time;
	if(next_timestep_ == 0) {
		next_timestep_ = current_time;
	}
	if(current_time >= next_timestep_) {
//		std::cout << "VideoVis: time=" << next_timestep_ << std::endl;
		next_timestep_ += 1000000 / 50;
	}
	else {
		return;
	}

	if(!map_) {
		return;
	}

	Eigen::MatrixXf& map = map_->getMapProbability();
	// resize image
	const int w = map.rows();
	const int h = map.cols();
	setImagePaintDevice(w, h);
	// paint map
	uint32_t* dst = reinterpret_cast<uint32_t*>(getImage()->bits());
	for(unsigned int y=0; y<h; y++) {
		for(unsigned int x=0; x<w; x++) {
			const float mp = map(x,y);
			uint32_t iv = static_cast<uint32_t>(std::max(0.0f, std::min(255.0f, mp * 255.0f)));
			iv = 255 - iv;
			*dst = (iv | (iv<<8) | (iv<<16) | (255<<24)); // black -> white
			dst++;
		}
	}

	// prepare painter
	QPainter painter(getImage());
	const float ppm = 1.0f / map_->getPixelSize();
	QTransform t;
	t.scale(ppm, -ppm);
	painter.setTransform(t);
	painter.setWindow(
		-static_cast<int>(map_->getCenterX()),
		-static_cast<int>(map_->getCenterY()),
		w,
		h);
	// paint path
	{
		QPen pen(Qt::SolidLine);
		pen.setColor(qRgb(0,0,255));
		painter.setPen(pen);
	}
	// Solution sol;
	// sol.data.insert(sol.data.begin(),
	// 	solution_.data.end() - std::min<unsigned>(1000,solution_.data.size()),
	// 	solution_.data.end());
	// PaintSolution(painter, sol);
	PaintSolution(painter, solution_, 10);

	// paint retina
	{
		QPen pen(Qt::SolidLine);
		pen.setColor(qRgb(255,0,0));
		pen.setWidthF(0.027f);
		painter.setPen(pen);
	}
	const float retina_half_size = 64.0f * map_->getMapHeight() / 138.4f; // FIXME correct parameters
	PaintRetina(painter, solution_.data.back().state, objf_);

	// paint events
	PaintEvents(painter, solution_.data.back().state, events_, objf_);
	events_.clear();

	// finished
	requireRepaint();
}

}
