/*
 * SampleMap2DVis.hpp
 *
 *  Created on: Nov 23, 2012
 *      Author: david
 */

#ifndef EDVS_VIS_SAMPLE2DMAPVIS_HPP_
#define EDVS_VIS_SAMPLE2DMAPVIS_HPP_

#include "MapVis.hpp"

namespace Edvs {

class SampleMap2D;

// FIXME only static map is supported

class SampleMap2DVis
: public MapVis, public Visualization2D
{
public:
	SampleMap2DVis();
	virtual ~SampleMap2DVis();

	void setMap(const boost::shared_ptr<Map>& map);

	void spin(time_t time, Timespan dt);

	void paint(QPainter& painter);

private:
	boost::shared_ptr<SampleMap2D> map_;
};

}
#endif
