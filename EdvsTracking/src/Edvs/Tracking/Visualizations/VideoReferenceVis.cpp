/*
 * VideoReferenceVis.cpp
 *
 *  Created on: Jan 14, 2012
 *      Author: david
 */

#include "VideoReferenceVis.hpp"
#include "Painters.hpp"
#include <stdint.h>
#include <Eigen/Dense>
#include <QtGui/QPainter>

namespace Edvs {

VideoReferenceVis::VideoReferenceVis()
: VisualizationImage("VideoReferenceVis")
{
	setImagePaintDevice(1, 1);
	next_timestep_ = 0;
}

VideoReferenceVis::~VideoReferenceVis()
{
}

void VideoReferenceVis::setObjective(const ObjectiveFunctionF* objf)
{
	objf_ = objf;
	map_ = boost::dynamic_pointer_cast<GridMap>(objf_->map_);
}

void VideoReferenceVis::setMap(const boost::shared_ptr<Map>& map)
{
	map_ = boost::dynamic_pointer_cast<GridMap>(map);
}

void VideoReferenceVis::notify(const Solution& solution)
{
	solution_ = solution;
	if(!isEnabled()) {
		return;
	}
	uint64_t current_time = solution_.data.back().time;
	if(next_timestep_ == 0) {
		next_timestep_ = current_time;
	}
	if(current_time >= next_timestep_) {
//		std::cout << "VideoReferenceVis: time=" << next_timestep_ << std::endl;
		updateImage(current_time);
		next_timestep_ += 1000000 / 50;
	}
}

void VideoReferenceVis::notify(const std::vector<Edvs::Event>& events)
{
	events_.insert(events_.end(), events.begin(), events.end());
}

void VideoReferenceVis::setReferenceSolution(const Solution& reference_solution)
{
	reference_solution_ = reference_solution;
}

void VideoReferenceVis::updateImage(uint64_t next_timestep)
{
	if(!map_) {
		return;
	}
	Eigen::MatrixXf& map = map_->getMapProbability();
	// resize image
	const int w = map.rows();
	const int h = map.cols();
	setImagePaintDevice(w, h);

	//std::cout << map_ -> getRawMapFileName() << std::endl;
	QImage tempImg = QImage(QString::fromStdString(map_->getRawMapFileName())).scaledToHeight(h);
	//std::cout << tempImg.format() << std::endl;
	tempImg = tempImg.convertToFormat(QImage::Format_RGB32);
	//std::cout << tempImg.format() << std::endl;
	tempImg.invertPixels();
	*getImage() = tempImg;
	
	// prepare painter
	QPainter painter(getImage());
	const float ppm = 1.0f / map_->getPixelSize();
	QTransform t;
	t.scale(ppm, -ppm);
	painter.setTransform(t);
	painter.setWindow(
		-static_cast<int>(map_->getCenterX()),
		-static_cast<int>(map_->getCenterY()),
		w,
		h);
	// paint path
	{
		QPen pen(Qt::SolidLine);
		pen.setColor(qRgb(255,0,0));
		painter.setPen(pen);
	}

	painter.setPen(Qt::red);
	PaintSolution(painter, reference_solution_, next_timestep);

	// paint retina
	{
		QPen pen(Qt::SolidLine);
		pen.setColor(qRgb(255,0,0));
		pen.setWidthF(0.027f);
		painter.setPen(pen);
	}

	State timestepState = reference_solution_.getStateByTime(next_timestep); //interpolation
	//State timestepState = reference_solution_.getStateAt(next_timestep);	//no interpolation

	const float retina_half_size = 64.0f * map_->getMapHeight() / 138.4f; // FIXME correct parameters
	PaintRetina(painter, timestepState, objf_);

	// paint events
	PaintEvents(painter, timestepState, events_, objf_);
	events_.clear();

	// finished
	requireRepaint();
}

}
