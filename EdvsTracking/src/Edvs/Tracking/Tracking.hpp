/*
 * Tracking.hpp
 *
 *  Created on: Jun 25, 2012
 *      Author: david
 */

#ifndef TRACKING_HPP_
#define TRACKING_HPP_

#include "SolutionSource.hpp"
#include "Sources/EdvsEventSource.hpp"
#include "Tracker.hpp"
#include "Spinable.hpp"
#include "Visualization.hpp"
#include "Visualizations/RetinaDisplay.hpp"
#include "Visualizations/GridMapVis.h"
#include "Visualizations/TrackerVisualization.hpp"
#include "Visualizations/ObjectiveFunctionVisualization.hpp"
#include "Visualizations/RetinaObjective.hpp"
#include "Visualizations/ParticleScoreVisualization.hpp"
#include "Visualizations/VideoVis.hpp"
#include "Visualizations/VideoReferenceVis.hpp"
#include "Visualizations/ExplorationVis.hpp"
#include "RunExploration.hpp"
#include <Edvs/Tools/SensorModel.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tools/Maps/Map.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/progress.hpp>
#include <vector>

struct TrackingParameters
{
	TrackingParameters();
	bool enable_mapping;
	double gamma;
	unsigned int num_particles;
	unsigned int batch_size;
	float motion_scale;
	unsigned int num_events_resample;
	double decay_constant;
	double fov;
	double kappa_1, kappa_2;
};

class Tracking
{
public:
	Tracking(bool is_create_vis);

	bool isRunning() const {
		return is_running_;
	}

	bool isPaused() const {
		return is_paused_;
	}

	bool tick();

	void tickDebug();

	void run();

	void step();

	void pause();

	void reset();

	void loadScenario(const std::string& fn);

	void loadPath(const std::string& fn);

	void loadMap(const std::string& fn);

	void loadEventSource(const std::string& fn, bool use_file_mode);

	void setExploration(const std::string& robot_address);

	void changeTracker();

	void setResultsTag(const std::string& tag);

	void setParameters(const TrackingParameters& params);

	void saveMap();

	const TrackingParameters& getParameters() const {
		return params_;
	}

	void setTrackerParticleCount(unsigned int val);

	void setTrackerResampleEventCount(unsigned int val);

	void addNoise(double pos_mean, double pos_sigma, double rot_mean, double rot_sigma);

	const std::vector<boost::shared_ptr<Edvs::Visualization>>& getVisualizations() const {
		return visualizations_;
	}

	void computeObjectiveFunction();

	void setObjectiveRotation(double angle_deg);

	void setEnabledVisualization(const std::string& str, bool x);

	std::string getPathInfoString() const {
		return path_info_str_;
	}

	std::string getMapInfoString() const {
		return map_->getInfoString();
	}

	std::string getEventsInfoString() const {
		return events_info_str_;
	}

	Edvs::State start_state;

private:
	bool connect();

	void disconnect();

	void add(const boost::shared_ptr<Edvs::Spinable>& v) {
		auto it = std::find(spinables_.begin(), spinables_.end(), v);
		if(it == spinables_.end()) {
			spinables_.push_back(v);
		}
	}

	void remove(const boost::shared_ptr<Edvs::Spinable>& v) {
		auto it = std::find(spinables_.begin(), spinables_.end(), v);
		if(it != spinables_.end()) {
			spinables_.erase(it);
		}
	}

	void add(const boost::shared_ptr<Edvs::Visualization>& v) {
		auto it = std::find(visualizations_.begin(), visualizations_.end(), v);
		if(it == visualizations_.end()) {
			visualizations_.push_back(v);
		}
	}

	void remove(const boost::shared_ptr<Edvs::Visualization>& v) {
		auto it = std::find(visualizations_.begin(), visualizations_.end(), v);
		if(it != visualizations_.end()) {
			visualizations_.erase(it);
		}
	}

	void setMap(const boost::shared_ptr<Edvs::Map>& map);

private:
	TrackingParameters params_;

	bool is_create_vis_;

	bool is_running_;
	bool is_paused_;
	bool is_connected_;
	bool do_tick_;

	boost::signals::connection connection_a_;

	std::vector<boost::shared_ptr<Edvs::Spinable>> spinables_;
	std::vector<boost::shared_ptr<Edvs::Visualization>> visualizations_;

	boost::shared_ptr<Edvs::Map> map_;

	Edvs::ObjectiveFunctionF objective_;

	boost::shared_ptr<Edvs::SolutionSource> ref_solution_source_;

	boost::shared_ptr<Edvs::EdvsEventSource> event_source_;

	boost::shared_ptr<Edvs::Tracker> tracker_;

	boost::shared_ptr<Edvs::Exploration> exploration_;

	boost::shared_ptr<Edvs::RetinaDisplay> vis_retina_;
	boost::shared_ptr<Edvs::MapVis> vis_map_;
	boost::shared_ptr<Edvs::TrackerVisualization> vis_tracker_;
	boost::shared_ptr<Edvs::ObjectiveFunctionVisualization> vis_obj_fnc_;
	boost::shared_ptr<Edvs::RetinaObjective> vis_retina_obj_;
	boost::shared_ptr<Edvs::ParticleScoreVisualization> vis_particle_scores_;
	boost::shared_ptr<Edvs::VideoVis> vis_video_;
	boost::shared_ptr<Edvs::VideoReferenceVis> vis_video_ref_;
	boost::shared_ptr<Edvs::ExplorationVis> vis_exploration_;

	boost::shared_ptr<Edvs::RunExploration> run_exploration_;

	time_t last_time_;

	uint64_t objvis_last_update_time_;

	std::string path_info_str_;
	std::string events_info_str_;

	std::string results_tag_;
	bool result_saved_;

	boost::progress_display progress_;

	unsigned int num_events_;

};

#endif
