/*
 * PlotScore.h
 *
 *  Created on: Mar 22, 2012
 *      Author: user
 */

#ifndef PLOTSCORE_H_
#define PLOTSCORE_H_

#include "Edvs/Tracking/Common.hpp"
#include <Edvs/Tracking/SolutionObserver.hpp>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_legend.h"


class PlotScore
: public QwtPlot, public Edvs::SolutionObserver
{
public:
	PlotScore(QWidget *parent = 0);
	~PlotScore();
	void notify(const Edvs::Solution& solution);

private:
	QwtPlotCurve *curve1_;
};

#endif /* PLOTSCORE_H_ */
