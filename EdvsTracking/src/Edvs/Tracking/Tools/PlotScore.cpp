/*
 * PlotScore.cpp
 *
 *  Created on: Mar 22, 2012
 *      Author: user
 */

#include "Edvs/Tracking/Tools/PlotScore.h"

PlotScore::PlotScore(QWidget *parent) : QwtPlot( parent )
{
    // Show a title
    QwtPlot::setTitle( "best score over time" );

    QwtPlot::setAxisTitle(xBottom, "solutions");
    QwtPlot::setAxisTitle(yLeft, "score");

    QwtLegend* legend = new QwtLegend();
    QwtPlot::insertLegend(legend);

    // curves
    curve1_ = new QwtPlotCurve("Curve 1");

    // set parameters of plots
    curve1_->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve1_->setPen(QPen(QColor(255,0,0),2));
    curve1_->setStyle(QwtPlotCurve::Lines);

}

PlotScore::~PlotScore() {
}

void PlotScore::notify(const Edvs::Solution& solution)
{

	int size_data = static_cast<int>(solution.data.size());
	double score[ size_data ];
	double time[ size_data ];

	for(unsigned int i=0; i<size_data ;i++){
		score[i] = solution.data[i].score;
		//time[i] = solution.data[i].time;
		time[i] = i;
	}

    // copy the data into the curves
    curve1_->setSamples(time, score, size_data);

    // add curves to plot
    curve1_->attach(this);

    QwtPlot::replot();
}

