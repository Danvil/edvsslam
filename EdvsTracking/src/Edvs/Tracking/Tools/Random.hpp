/*
 * Random.hpp
 *
 *  Created on: 16 Nov 2011
 *      Author: david
 */

#ifndef HELIOS_TOOLS_RANDOM_HPP_
#define HELIOS_TOOLS_RANDOM_HPP_

#include <eigen3/Eigen/Dense>
#include <boost/random.hpp>
#include <boost/assert.hpp>
#include <algorithm>
#include <vector>

namespace Edvs {
namespace Tools {

//typedef boost::minstd_rand EngineType;
typedef boost::mt19937 EngineType;

/** Global random number engine */
EngineType& Engine();

float Uniform01();

/** Creates a uniform probability distribution over the interval [a,b] */
template<typename K>
boost::variate_generator<EngineType&, boost::uniform_real<K> > UniformRealGenerator(K a, K b) {
	return boost::variate_generator<EngineType&, boost::uniform_real<K> >(
			Engine(),
			boost::uniform_real<K>(a, b));
}

/** Creates a gaussian probability distribution  */
template<typename K>
boost::variate_generator<EngineType&, boost::normal_distribution<K> > GaussianGenerator(K mu, K sigma) {
	return boost::variate_generator<EngineType&, boost::normal_distribution<K> >(
			Engine(),
			boost::normal_distribution<K>(mu, sigma));
}

/** Creates a uniform probability distribution over integers in {a, ..., b} */
template<typename K>
boost::variate_generator<EngineType&, boost::uniform_int<K> > UniformIntegerGenerator(K a, K b) {
	return boost::variate_generator<EngineType&, boost::uniform_int<K> >(
			Engine(),
			boost::uniform_int<K>(a, b));
}

/** Creates a uniform probability distribution over {0,1} */
inline
boost::variate_generator<EngineType&, boost::uniform_smallint<> > UniformBinaryGenerator() {
	static auto rnd = boost::variate_generator<EngineType&, boost::uniform_smallint<> >(
			Engine(),
			boost::uniform_smallint<>(0, 1));
	return rnd;
}

/** Interprets each value of the matrix as a probability and draws a random sample */
Eigen::MatrixXf Sample(const Eigen::MatrixXf& v);

/** Like Matrix Sample(const Matrix&) but inplace */
void Sample(Eigen::MatrixXf& v);

void Sample(std::vector<float>& v);

/** Creates a random matrix using the given probability distribution */
template<typename P>
Eigen::MatrixXf CreateRandomMatrix(unsigned int n, unsigned int m, P p)
{
	Eigen::MatrixXf v(n, m);
	std::generate(v.data(), v.data() + v.size(), p);
	return v;
}

/** Picks a random (unique) subset from elements in v */
template<typename K>
std::vector<K> RandomSubset(const std::vector<K>& v, unsigned int n) {
	BOOST_ASSERT(n <= v.size());
	std::vector<K> tmp = v;
	// TODO use boost random
	//	boost::uniform_int<std::size_t> rnd_node_rng(0, input.dim());
	//	boost::variate_generator<EngineType&, boost::uniform_int<std::size_t> > rnd_node(RndEngine(), rnd_node_rng);
	std::random_shuffle(tmp.begin(), tmp.end());
	tmp.resize(n);
	return tmp;
}

/** Picks a random (unique) subset from the numbers {0, ..., q-1} */
std::vector<unsigned int> RandomSubset(unsigned int q, unsigned int n);

/** Creates a random binary std::vector */
std::vector<bool> RandomBinaryVector(unsigned int n);

/** Randomly draws an element from a discrete distribution
 * @param v discrete (possibly unnormalized) probability distribution
 * @param sum normalization factor for the distribution
 */
inline
unsigned int SampleCDF(const std::vector<float>& v) {
	BOOST_ASSERT(v.size() > 0);
	auto rnd = UniformRealGenerator(0.0f, v[v.size() - 1]);
	float q = rnd();
	auto it = std::lower_bound(v.begin(), v.end(), q);
	unsigned int index = it - v.begin();
	if(index == v.size()) { index --; }
	BOOST_ASSERT(0 <= index && index < v.size());
	return index;
}

}}

#endif
