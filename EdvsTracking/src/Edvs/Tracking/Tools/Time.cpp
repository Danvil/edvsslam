/*
 * Time.cpp
 *
 *  Created on: Mar 2, 2012
 *      Author: david
 */

#include "Time.hpp"

namespace Edvs
{

Danvil::Timer s_timer;

void Time::Restart()
{
	s_timer.start();
}

uint64_t Time::GetCurrent()
{
	return s_timer.getElaspedTicks();
}


}
