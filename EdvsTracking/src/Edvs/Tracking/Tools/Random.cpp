/*
 * Random.cpp
 *
 *  Created on: 16 Nov 2011
 *      Author: david
 */

#include "Random.hpp"

namespace Edvs {
namespace Tools {

EngineType& Engine()
{
	static EngineType rnd;
	return rnd;
}

float Uniform01()
{
	return UniformRealGenerator(0.0f, 1.0f)();
//	static uint32_t x = 1;
//	x = (x*48271) % 2147483647;
//	return float(x - 1)/2147483645.0f;
}

void Sample(float* src, float* src_end)
{
	while(src != src_end) {
		float rnd = Uniform01();//UniformRealGenerator(0.0f, 1.0f);
		float& v = *(src++);
		v = (rnd < v) ? 1.0f : 0.0f;
	}
}

void Sample(Eigen::MatrixXf& v)
{
	Sample(v.data(), v.data() + v.size());
}

Eigen::MatrixXf Sample(const Eigen::MatrixXf& v)
{
	Eigen::MatrixXf q = v;
	Sample(q);
	return q;
//	auto rnd = UniformRealGenerator(0.0f, 1.0f);
//	Eigen::MatrixXf u(v.rows(), v.cols());
//	const float* src = v.data();
//	const float* src_end = v.data() + v.size();
//	float* dst = u.data();
//	while(src != src_end) {
//		*(dst++) = (rnd() < *(src++)) ? 1.0f : 0.0f;
//	}
//	return u;
}

void Sample(std::vector<float>& v)
{
	Sample(v.begin().base(), v.end().base());
}

std::vector<unsigned int> RandomSubset(unsigned int q, unsigned int n)
{
	BOOST_ASSERT(n <= q);
	if(q > 3*n) {
		// random unique drawing (good if q is much bigger than n)
		std::vector<unsigned int> u;
		u.reserve(n);
		auto rnd = UniformIntegerGenerator<unsigned int>(0, q-1);
		for(unsigned int i=0; i<n; i++) {
			while(true) {
				unsigned int x = rnd();
				if(std::find(u.begin(), u.end(), x) == u.end()) {
					u.push_back(x);
					break;
				}
			}
		}
		return u;
	}
	else {
		// random subset of shuffeld q (good if q is close to n)
		std::vector<unsigned int> full(q);
		for(unsigned int i=0; i<q; i++) {
			full[i] = i;
		}
		return RandomSubset(full, n);
	}
}

std::vector<bool> RandomBinaryVector(unsigned int n) {
	auto rnd = UniformBinaryGenerator();
	std::vector<bool> v(n);
	std::generate(v.begin(), v.end(), [&]() { return rnd(); });
	return v;
}

}}
