/*
 * Time.hpp
 *
 *  Created on: Mar 2, 2012
 *      Author: david
 */

#ifndef TIME_HPP_
#define TIME_HPP_

#include <Edvs/Tracking/Tools/Timer.hpp>

namespace Edvs
{
	namespace Time
	{
		void Restart();

		uint64_t GetCurrent();

	}

}

#endif /* TIME_HPP_ */
