/*
 * TrackingTools.cpp
 *
 *  Created on: Mar 7, 2012
 *      Author: Martin
 */

#include "Edvs/Tracking/Tools/TrackingTools.hpp"

typedef boost::mt19937 RndEngineType;
RndEngineType sRndEngine;

namespace Edvs {

void NormalizeSum(vector<Real>& weights)
{
	Real sum = std::accumulate(weights.begin(), weights.end(), 0.0);
	Real sum_inv = 1.0 / sum;
	std::for_each(weights.begin(), weights.end(), [sum_inv](Real& s) { s *= sum_inv; });
}

vector<Real> WeightsToCDF(const vector<Real>& weights)
{
	vector<Real> cdf(weights.size());
	auto src = weights.begin();
	auto src_end = weights.end();
	auto dst = cdf.begin();
	Real sum = Real(0);
	while(src != src_end) {
		sum += *src;
		*dst = sum;
		++src;
		++dst;
	}
	return cdf;
}

vector<size_t> RandomPickCDF(const vector<Real>& cdf, unsigned int n)
{
	if(std::isnan(cdf.back())) {
		std::cerr << "RandomPickCDF: scores are nan!" << std::endl;
	}
	boost::uniform_real<Real> range(0.0f, cdf.back());
	boost::variate_generator<RndEngineType&, boost::uniform_real<Real> > die(sRndEngine, range);
	vector<size_t> picks;
	picks.reserve(n);
	auto it_begin = cdf.begin();
	auto it_end = cdf.end();
	for(unsigned int i=0; i<n; i++) {
		Real x = die();
		auto it_picked = std::lower_bound(it_begin, it_end, x);
		picks.push_back(std::distance(it_begin, it_picked));
	}
	return picks;
}

ParticleSet CreateRandomParticles(unsigned int n)
{
	// TODO:  change this part to the new values depending on the map
	vector<Real> cInitUniformSize = { 0.4, 0.4, 0.0, 0.4*90.0/180.0*3.1415 };
	vector<Real> cNullstate = {0, 0, -1.0f, 0};

	boost::variate_generator<RndEngineType&, boost::uniform_real<Real> > die0(sRndEngine,
			boost::uniform_real<Real>(cNullstate[0] - cInitUniformSize[0], cNullstate[0] + cInitUniformSize[0]));
	boost::variate_generator<RndEngineType&, boost::uniform_real<Real> > die1(sRndEngine,
			boost::uniform_real<Real>(cNullstate[1] - cInitUniformSize[1], cNullstate[1] + cInitUniformSize[1]));
	boost::variate_generator<RndEngineType&, boost::uniform_real<Real> > die2(sRndEngine,
			boost::uniform_real<Real>(cNullstate[2] - cInitUniformSize[2], cNullstate[2] + cInitUniformSize[2]));
	boost::variate_generator<RndEngineType&, boost::uniform_real<Real> > die3(sRndEngine,
			boost::uniform_real<Real>(cNullstate[3] - cInitUniformSize[3], cNullstate[3] + cInitUniformSize[3]));

	ParticleSet particles;
	for(unsigned int i=0; i<n; i++) {
		State u {Vec3{die0(), die1(), die2()}, die3()};
		particles.push_back(Particle{u, 0.5});
	}
	return particles;
}


}
