/*
 * TrackingTools.hpp
 *
 *  Created on: Mar 7, 2012
 *      Author: Martin
 */

#ifndef TRACKINGTOOLS_HPP_
#define TRACKINGTOOLS_HPP_

#include "Edvs/Tracking/Common.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/timer.hpp>
#include <boost/random.hpp>
#include <boost/utility.hpp>

using std::string;
using std::vector;
using std::cout;
using std::endl;

namespace Edvs {

// Rescales a vector of weights s.t. the sum is 1
void NormalizeSum(vector<Real>& weights);

// Creates a cdf vector (vector with added up weights) from the weight vector.
vector<Real> WeightsToCDF(const vector<Real>& weights);

// Creates a vector with n random particles.
// Chance to pick a particle is proportional to its weight.
// Can pick a particle several times.
vector<size_t> RandomPickCDF(const vector<Real>& cdf, unsigned int n);

// Create a random set of particles
ParticleSet CreateRandomParticles(unsigned int n);

}

#endif
