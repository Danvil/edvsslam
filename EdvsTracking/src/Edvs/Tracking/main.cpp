#include "WdgtEdvsTracking.h"
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>

int main(int argc, char *argv[])
{
	std::string p_results_tag = ".";
	std::string p_out_vis = "";
	TrackingParameters params;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("scenario", po::value<std::string>(), "filename for scenario xml file")
		("fn_path", po::value<std::string>(), "filename for path file")
		("fn_map", po::value<std::string>(), "filename for map file")
		("event-source", po::value<std::string>(), "event stream URI")
		("exploration_addr", po::value<std::string>(), "address of robot used for exploration, in format: 192.168.201.62:56000")
		("results", po::value(&p_results_tag)->default_value(p_results_tag), "tag for result files (path, map, ...)")
		("out_vis", po::value(&p_out_vis)->default_value(p_out_vis), "output path for visualization images (leave empty to disable)")
		("no-gui", "specify to hide the gui while tracking (implies 'run')")
		("run", "run tracker immediately")
		("mapping", po::value(&params.enable_mapping)->default_value(params.enable_mapping), "set to true for enabling mapping")
		("p_gamma", po::value(&params.gamma)->default_value(params.gamma), "soften parameter for objective function")
		("p_num_particles", po::value(&params.num_particles)->default_value(params.num_particles), "number of particles")
		("p_batch_size", po::value(&params.batch_size)->default_value(params.batch_size), "number of events integrated")
		("p_motion_scale", po::value(&params.motion_scale)->default_value(params.motion_scale), "number of test points")
		("p_num_events_resample", po::value(&params.num_events_resample)->default_value(params.num_events_resample), "number of events until resampling happen")
		("p_decay_constant", po::value(&params.decay_constant)->default_value(params.decay_constant), "soften parameter for objective function")
		("p_retina_fov", po::value(&params.fov)->default_value(params.fov), "retina sensor field of view")
		("p_retina_kappa_1", po::value(&params.kappa_1)->default_value(params.kappa_1), "retina sensor distortion parameter 1")
		("p_retina_kappa_2", po::value(&params.kappa_2)->default_value(params.kappa_2), "retina sensor distortion parameter 2")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	QApplication a(argc, argv);

	bool show_gui = vm.count("no-gui") == 0;

	EdvsTracking w(show_gui);
	if(show_gui) {
		w.show();
	}
	w.disableVisualizations();

	w.setResultsTag(p_results_tag);
	w.setVisOutputPath(p_out_vis);
	w.setParameters(params);

	if(vm.count("scenario")) {
		w.loadScenario(vm["scenario"].as<std::string>());
	}

	if(vm.count("fn_path")) {
		w.loadPath(vm["fn_path"].as<std::string>());
	}
	if(vm.count("fn_map")) {
		w.loadMap(vm["fn_map"].as<std::string>());
	}
	if(vm.count("event-source")) {
		w.loadEvents(vm["event-source"].as<std::string>());
	}
	if(vm.count("exploration_addr")) {
		w.setExploration(vm["exploration_addr"].as<std::string>());
	}

	if(!show_gui || vm.count("run")) {
		w.run();
	}

	return a.exec();

}
