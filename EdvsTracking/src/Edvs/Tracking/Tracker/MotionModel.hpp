#ifndef EDVS_TRACKING_MOTIONMODEL_HPP
#define EDVS_TRACKING_MOTIONMODEL_HPP

#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <iostream>

namespace Edvs {

	namespace detail {

		struct DiffuseMotionModelApprox
		{
			static constexpr unsigned int N = 1024;

			DiffuseMotionModelApprox()
			: rnd_index_(boost::mt11213b(), boost::uniform_smallint<>(0, N-1))
			{}

			void setSigma(Real s_pos, Real s_rot) {
				auto BatchMovePos = Tools::GaussianGenerator<Real>(0, s_pos);
				val_pos_.resize(N);
				std::generate(val_pos_.begin(), val_pos_.end(), [&BatchMovePos]() {
					return BatchMovePos();
				});
				auto BatchMoveRot = Tools::GaussianGenerator<Real>(0, s_rot);
				val_rot_.resize(N);
				std::generate(val_rot_.begin(), val_rot_.end(), [&BatchMoveRot]() {
					return BatchMoveRot();
				});
			}

			void operator()(State& state) const {
				unsigned int i1 = lcg_rand();
				unsigned int i2 = lcg_rand();
				unsigned int i3 = lcg_rand();
				state.pos.x() += val_pos_[i1];
				state.pos.y() += val_pos_[i2];
				state.rot += val_rot_[i3];
			}

		private:
			static uint32_t lcg_rand()
			{
				static uint32_t a = 1331128;
			    a = (static_cast<uint64_t>(a) * 1103515245UL + 12345UL) % 2147483648UL;
			    return a % N;
			}

		private:
			std::vector<Real> val_pos_;
			std::vector<Real> val_rot_;
			mutable boost::variate_generator<boost::mt11213b, boost::uniform_smallint<>> rnd_index_;
		};

		struct DiffuseMotionModel
		{
			void setSigma(Real s_pos, Real s_rot) {
				dist_pos_ = boost::normal_distribution<Real>(0, s_pos);
				dist_rot_ = boost::normal_distribution<Real>(0, s_rot);
		//		std::cout << s_pos << "|" << dist_pos_ << std::endl;
		//		std::cout << s_rot << "|" << dist_rot_ << std::endl;
		//		std::cout << dist_pos_(Edvs::Tools::Engine()) << std::endl;
		//		std::cout << dist_rot_(Edvs::Tools::Engine()) << std::endl;
		//		std::cout << rnd_pos() << std::endl;
		//		std::cout << rnd_rot() << std::endl;
			}

			void operator()(State& state) const {
				boost::variate_generator<Edvs::Tools::EngineType&,boost::normal_distribution<Real>> rnd_pos(Edvs::Tools::Engine(), dist_pos_);
				boost::variate_generator<Edvs::Tools::EngineType&,boost::normal_distribution<Real>> rnd_rot(Edvs::Tools::Engine(), dist_rot_);
		//		std::cout << "(" << state.pos.x() << "," << state.pos.y() << "," << state.rot << ") =>";
				state.pos.x() += rnd_pos();
				state.pos.y() += rnd_pos();
				state.rot += rnd_rot();
		//		std::cout << "(" << state.pos.x() << "," << state.pos.y() << "," << state.rot << ")" << std::endl;
			}

		private:
			mutable boost::normal_distribution<Real> dist_pos_;
			mutable boost::normal_distribution<Real> dist_rot_;
		};

	}
}

#endif
