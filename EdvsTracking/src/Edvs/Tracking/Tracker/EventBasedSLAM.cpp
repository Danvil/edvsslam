/*
 * EVentBasedSLAM.cpp
 *
 *  Created on: Jan 13, 2012
 *      Author: david
 */

#include "EventBasedSLAM.hpp"
#include "Edvs/Tracking/Tools/TrackingTools.hpp"
#include <Edvs/Tracking/Tools/Time.hpp>
#include <Edvs/Tracking/Tools/Random.hpp>
#include <boost/math/constants/constants.hpp>
#include <algorithm>
#include <iostream>

#ifdef PROFILE_ONLY_TRACKER
#	include "valgrind/callgrind.h"
#endif

constexpr float STATE_SMOOTH_CONST = 1.0f;

namespace Edvs {

EventBasedSLAMParameters::EventBasedSLAMParameters()
: initial_pos_sigma(0.001),
  initial_rot_sigma(0.5*3.1415/180.0),
  num_particles(100),
  percent_significant(0.05),
  batch_size(3),
  num_test_points(50.0),
  motion_scale(1.0f),
  resample_event_count(1000),
  decay_constant(0.03),
  enable_mapping(false)
{}

EventBasedSLAM::EventBasedSLAM()
{
	onParameterUpdate();
}

void EventBasedSLAM::onParameterUpdate()
{
	// The estimated movement per event:
	// \Delta_x = W_px / G_0 = h/f/G_0
	// \Delta_\theta = 4/S/G_0
	const Real height = 2.59f; // FIXME where to get the height? (objective_.map_) ? objective_.map_->map_height_ : 2.59; // HACK
	const Real delta_pos = height / objective_.sensor_model_.getPixelFocalLength() / static_cast<Real>(parameters_.num_test_points) * static_cast<Real>(parameters_.motion_scale);
	const Real delta_rot = 4.0 / static_cast<Real>(EdvsSensorModelF::RETINA_SIZE) / static_cast<Real>(parameters_.num_test_points) * static_cast<Real>(parameters_.motion_scale);
	// batching multiplies with square root of number of batches
	// This is because addition of two normally distributed random variables with variance s^2
	// yields a normally distributed random variable with variance s'^2 = 2*s^2.
	// Thus s' = sqrt(2) * s.
	const Real batch_mult = std::sqrt(static_cast<Real>(parameters_.batch_size));
	motion_model_.setSigma(batch_mult * delta_pos, batch_mult * delta_rot);

	resample_check_.threshold_ = parameters_.resample_event_count;

	num_significant_ = std::max<unsigned int>(1, static_cast<unsigned int>(
			static_cast<float>(parameters_.num_particles) * parameters_.percent_significant));
}

void EventBasedSLAM::reset(const State& initial_state)
{
#ifdef PROFILE_ONLY_TRACKER
	CALLGRIND_START_INSTRUMENTATION;
	CALLGRIND_TOGGLE_COLLECT;
#endif

	solution_.data.clear();

	auto rnd_pos = Tools::GaussianGenerator<Real>(0, parameters_.initial_pos_sigma);
	auto rnd_rot = Tools::GaussianGenerator<Real>(0, parameters_.initial_rot_sigma);

	// init particles
	particles_.resize(parameters_.num_particles);
	for(unsigned int i=0; i<parameters_.num_particles; i++) {
		State state = initial_state;
		state.pos += Vec3{rnd_pos(), rnd_pos(), 0};
		state.rot += rnd_rot();
		particles_[i] = Particle{state, 1.0};
	}
	boost::shared_ptr<GridMap> tempMap = boost::dynamic_pointer_cast<GridMap>(objective_.map_);
	tempMap->setStartState(initial_state);
	smoothed_state_ = initial_state;

#ifdef PROFILE_ONLY_TRACKER
	CALLGRIND_TOGGLE_COLLECT;
#endif
}

void EventBasedSLAM::step(const std::vector<Edvs::Event>& events)
{
#ifdef PROFILE_ONLY_TRACKER
	CALLGRIND_TOGGLE_COLLECT;
#endif

	// add new events to event queue
	events_queue_.insert(events_queue_.end(), events.begin(), events.end());
	// process event queue
	while(events_queue_.size() >= parameters_.batch_size) {
		// get batch from event queue
		std::vector<Edvs::Event> batch(events_queue_.begin(), events_queue_.begin() + parameters_.batch_size);
		// erase batch from event queue
		events_queue_.erase(events_queue_.begin(), events_queue_.begin() + parameters_.batch_size);
		// process batch
		processBatch(batch);
	}

#ifdef PROFILE_ONLY_TRACKER
	CALLGRIND_TOGGLE_COLLECT;
#endif
}

float clamp_angle(float x) {
	while(x > 2.0f*PI) x -= 2.0f*PI;
	while(x < 0.0f) x += 2.0f*PI;
	return x;
}

float merge_angle(float a, float b, float s) {
	a = clamp_angle(a);
	b = clamp_angle(b);
	float d = b - a;
	if(d < -PI) {
		b += 2.0f*PI;
	}
	if(d > +PI) {
		b -= 2.0f*PI;
	}
	return clamp_angle(a + s*(b - a));
}

State merge_states(const State& a, const State& b, float s) {
	State c;
	c.pos = (1.0f - s)*a.pos + s*b.pos;
	c.rot = merge_angle(a.rot, b.rot, s);
	return c;
}

void EventBasedSLAM::processBatch(const std::vector<Edvs::Event>& events)
{
	// FIXME compute from parameters
	// FIXME get parameters from somewhere
	const double cDecayConstant = 1.0 - std::pow(1.0 - parameters_.decay_constant, parameters_.batch_size);

	// update particles
	int i=0;
	for(Particle& p : particles_) {
		// apply motion model
		motion_model_(p.state);
		// compute score
		double ps = objective_.meanOfScores(p.state.pos.cast<float>(), p.state.rot, events);
		// update score using exponential decay model
		p.score = (1.0 - cDecayConstant) * p.score + cDecayConstant * ps;

		if(std::isnan(p.score)) {
			std::cerr << "score " << i << " is nan!" << std::endl;
		}
		if(std::isinf(p.score)) {
			std::cerr << "score " << i << " is inf!" << std::endl;
		}
		if(p.score == 0.0f) {
			std::cerr << "score " << i << " is 0!" << std::endl;
		}
		if(p.score < 0.0f) {
			std::cerr << "score " << i << " is <0!" << std::endl;
		}
		i++;
	}

	// mapping
	if(parameters_.enable_mapping) {
		// take only best particles to update map
		// assume the particle list is approximately sorted
		// sorting is done while resampling
		// std::partial_sort(
		// 	particles_.begin(), particles_.begin() + num_significant_,
		// 	[](const Particle& a, const Particle& b) {
		// 		return a.score > b.score;
		// 	});
		auto it_begin = particles_.begin();
		auto it_end = particles_.begin() + num_significant_;

		// compute pixel view cones for map update
		std::vector<PixelViewCone> v_pvc;
		v_pvc.reserve(num_significant_ * events.size());
		float scores_sum = std::accumulate(
			it_begin, it_end,
			0.0,
			[](Real a, const Particle& p) {
				return a + p.score;
			});

		for(auto it=it_begin; it!=it_end; ++it) {
			const Particle& p = *it;
			const float rot = p.state.rot;
			const float rot_sin = std::sin(rot);
			const float rot_cos = std::cos(rot);
			const float weight = p.score / scores_sum;
			for(const Event& e : events) {
				PixelViewCone pvc = objective_.sensor_model_.Transform(
					p.state.pos.cast<float>(), rot_sin, rot_cos,
					objective_.sensor_model_.createPixelViewCone(e.x, e.y)
				);
				pvc.weight = weight;
				v_pvc.push_back(pvc);
			}
		}

		// update map
		objective_.map_->store(v_pvc, events.size());
	}

	// resample if necessary
	if(resample_check_(particles_, events)) {
		// debug message
//		std::cout
//				<< "Resample: time=" << current_time
//				<< ", dt=" << static_cast<double>(current_time - last_resample_time_)/1000.0 << "ms" << " (max=" << cResampleDeltaTime/1000.0 << ")"
//				<< ", #events=" << num_events_since_resample_
//				<< ", error=" << error << " (max=" << cResampleDistributionDiff << ")"
//				<< std::endl;
	// get list with particle scores
		std::vector<Real> particle_scores(particles_.size());
		for(std::size_t i=0; i<particles_.size(); i++) {
			particle_scores[i] = particles_[i].score;
		}

		// do some particle filter magic
		auto ps_minmax = std::minmax_element(particle_scores.begin(), particle_scores.end());
		Real ps_min = *ps_minmax.first;
		Real ps_max = *ps_minmax.second;
//		std::cout << ps_min << " " << ps_max << std::endl;
		if(ps_min < ps_max) {
			for(std::size_t i=0; i<particle_scores.size(); i++) {
				particle_scores[i] = std::pow((particle_scores[i] - ps_min) / (ps_max - ps_min), 8.0f);
//				std::cout << particle_scores[i] << ", ";
			}
//			std::cout << std::endl;
		}

		// compute weights from scores
		std::vector<Real> cdf = WeightsToCDF(particle_scores);
		// pick by weight
		std::vector<size_t> pick = RandomPickCDF(cdf, parameters_.num_particles);
		std::vector<Particle> new_particles(pick.size());
		for(unsigned int i=0; i<new_particles.size(); i++) {
			new_particles[i] = particles_[pick[i]];
		}
		particles_ = new_particles;
		// sort particle list (ordering should be preserved approximatelly until next)
		std::sort(
		particles_.begin(), particles_.end(),
			[](const Particle& a, const Particle& b) {
				return a.score > b.score;
			});
		// latest event time
		uint64_t current_time = events.back().t;
		// solution
		Particle mean = compute_mean(particles_.begin(), particles_.begin() + num_significant_);
		// smooth using exponential decay
		mean.state = merge_states(smoothed_state_, mean.state, STATE_SMOOTH_CONST);
		smoothed_state_ = mean.state;
		solution_.add(current_time, mean);
		// notitfy about new particles
		notifySolutionObservers(solution_);
		// notify particles
		on_particles_(particles_);
		on_particles_evaluate_(particles_, static_cast<Timestamp>(current_time));
		// update map with new state estimate
		if(parameters_.enable_mapping) {
			// notify map about new state
			objective_.map_->resample(mean.state);
			// update num test points
			auto map_grid2 = boost::dynamic_pointer_cast<Edvs::GridMap>(objective_.map_);
			if(map_grid2) {
				float estimated_event_count = map_grid2->estimateCurrentVisibleLikelihood();
				float old_estimate = parameters_.num_test_points;
				constexpr float decay = 0.05f; // FIXME constant ???
				parameters_.num_test_points = (1.0f - decay)*old_estimate + decay*estimated_event_count;
//				std::cout << "event_count: " << "old=" << old_estimate << ", cur=" << estimated_event_count << " => new=" << parameters_.num_test_points << std::endl;
				onParameterUpdate();
			}
		}
	}
}

void EventBasedSLAM::addNoise(Real pos_sigma, Real rot_sigma_rad)
{
	auto noise_pos = Edvs::Tools::GaussianGenerator<Real>(0, pos_sigma);
	auto noise_rot = Edvs::Tools::GaussianGenerator<Real>(0, rot_sigma_rad);
	for(Edvs::Particle& p : particles_) {
		p.state.pos.x() += noise_pos();
		p.state.pos.y() += noise_pos();
		p.state.rot += noise_rot();
	}
}


}
