/*
 * EventBasedSLAM.hpp
 *
 * Event-based simulated localization and mapping
 *
 *  Created on: Jan 12, 2013
 *      Author: david
 */

#ifndef EDVS_TRACKING_EventBasedSLAM_HPP_
#define EDVS_TRACKING_EventBasedSLAM_HPP_

#include "MotionModel.hpp"
#include "Resampling.hpp"
#include <Edvs/Tracking/Common.hpp>
#include <Edvs/Tracking/Tracker.hpp>
#include <Edvs/Tools/MapImage.hpp>
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>

namespace Edvs {

struct EventBasedSLAMParameters
{
	// initial spread of particles (position in cm)
	double initial_pos_sigma;
	// initial spread of particles (rotation in rad) 
	double initial_rot_sigma;

	// number of particles for the tracker
	unsigned int num_particles;
	// percentage of particles taken for map update and mean computation
	float percent_significant;
	// number of events processed in a batch
	unsigned int batch_size;
	// number of imaginary test points (depends on the map!!)
	float num_test_points;
	float motion_scale;
	// number of events between particle resamples
	unsigned int resample_event_count;
	// decay constant for particle score update
	double decay_constant;

	// true if mapping is enabled
	bool enable_mapping;

	EventBasedSLAMParameters();
};

class EventBasedSLAM
: public Tracker
{
public:
	EventBasedSLAM();

	virtual ~EventBasedSLAM() {
	}

	const EventBasedSLAMParameters& getParameters() const {
		return parameters_;
	}

	void setParameters(const EventBasedSLAMParameters& params) {
		parameters_ = params;
		onParameterUpdate();
	}

	void reset(const State& initial_state);

	void step(const std::vector<Edvs::Event>& events);

	ParticleSet getParticles() const {
		return particles_;
	}

	void setParticles(const ParticleSet& particles) {
		particles_ = particles;
		on_particles_(particles_);
	}

	void addNoise(Real pos_sigma, Real rot_sigma_rad);

private:
	void onParameterUpdate();

	void processBatch(const std::vector<Edvs::Event>& events);

private:
	EventBasedSLAMParameters parameters_;

	detail::DiffuseMotionModel motion_model_;
	detail::ResampleCheckNumEvents resample_check_;

	std::vector<Edvs::Event> events_queue_;

	ParticleSet particles_;

	Edvs::State smoothed_state_;

	unsigned int num_significant_;

};

}

#endif
