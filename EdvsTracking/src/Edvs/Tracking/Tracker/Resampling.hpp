#ifndef EDVS_TRACKING_RESAMPLING_HPP
#define EDVS_TRACKING_RESAMPLING_HPP

#include <Edvs/Event.hpp>
#include "Edvs/Tracking/Common.hpp"
#include <vector>

namespace Edvs
{
	namespace detail
	{

		struct ResampleCheckNumEvents
		{
			ResampleCheckNumEvents(unsigned int threshold=1000)
			: threshold_(threshold),
			  num_(0) {}

			bool operator()(const std::vector<Particle>& particles, const std::vector<Edvs::Event>& events) {
				num_ += events.size();
				if(num_ >= threshold_) {
					num_ -= threshold_;
					return true;
				}
				else {
					return false;
				}
			}

			unsigned int threshold_;

		private:
			unsigned int num_;
		};

		struct ResampleCheckTime
		{
			ResampleCheckTime(uint64_t threshold=1000)
			: threshold_(threshold),
			  last_(0) {}

			bool operator()(const std::vector<Particle>& particles, const std::vector<Edvs::Event>& events) {
				uint64_t current = events.back().t;
				assert(current >= last_);
				if(current - last_ >= threshold_) {
					last_ = current;
					return true;
				}
				else {
					return false;
				}
			}

			uint64_t threshold_;

		private:
			uint64_t last_;
		};

		struct ResampleCheckError
		{
			ResampleCheckError(Real threshold=0.01)
			: threshold_(threshold) {}

			bool operator()(const std::vector<Particle>& particles, const std::vector<Edvs::Event>& events) {
				const std::size_t n = particles.size();
				// just take scores if first call
				if(scores_.size() != n) {
					scores_.resize(n);
					for(std::size_t i=0; i<n; i++) {
						scores_[i] = particles[i].score;
					}
					return false;
				}
				// compute error wrt stored scores and store new scores
				Real error = 0.0f;
				for(std::size_t i=0; i<n; i++) {
					const Real s = particles[i].score;
					const Real e = s - scores_[i];
					error += e*e;
					scores_[i] = s;
				}
				// error is the MSE of the individual deviations from normal distribution
				error = std::sqrt(error / static_cast<Real>(n));
				return error > threshold_;
			}

			Real threshold_;

		private:
			std::vector<Real> scores_;
		};

	}
}

#endif
