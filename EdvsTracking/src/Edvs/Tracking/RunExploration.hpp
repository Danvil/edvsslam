#ifndef RUN_EXPLORATION_H
#define RUN_EXPLORATION_H
#include <Edvs/Tracking/SolutionObserver.hpp>
#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Exploration/Exploration.hpp>

namespace Edvs {

class RunExploration
: public SolutionObserver
{
public:
	RunExploration(boost::shared_ptr<Edvs::Exploration> exploration);
	virtual ~RunExploration();
	void notify(const Solution& solution);
	void setMap(const boost::shared_ptr<Map>& map);

private:
	Solution solution_;
	boost::shared_ptr<GridMap> map_;

	boost::shared_ptr<Exploration> exploration_;
	uint64_t last_time_;
};

}

#endif
