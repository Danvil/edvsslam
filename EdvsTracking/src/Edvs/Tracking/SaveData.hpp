/*
 * SaveData.hpp
 *
 *  Created on: Apr 11, 2012
 *      Author: martin
 */

#ifndef SAVEDATA_HPP_
#define SAVEDATA_HPP_

#include "Common.hpp"

namespace Edvs {

class SaveData
{
public:
	virtual ~SaveData() {}

	virtual void saveDataToFile(const std::string& file_name) = 0;

};

}


#endif /* SAVEDATA_HPP_ */
