
#include "SaveSolution.hpp"
#include <stdexcept>

namespace Edvs
{

SaveSolution::SaveSolution(const std::string& path)
: path_(path),
  ofs_(path)
{
	if(!ofs_.is_open()) {
		throw std::runtime_error("Could not open file: '" + path + "'");
	}
}

void SaveSolution::notify(const Solution& solution)
{
	const auto& i = solution.data.back();
	ofs_ << i.time/1000000.0f << "\t" << i.state.pos.x() << "\t" << i.state.pos.y() << "\t" << i.state.rot << std::endl;
}

}
