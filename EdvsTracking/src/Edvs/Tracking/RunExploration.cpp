#include "RunExploration.hpp"
#include <iostream>

namespace Edvs {

	RunExploration::RunExploration(boost::shared_ptr<Edvs::Exploration> exploration)
	: exploration_(exploration), last_time_(0)
	{}

	RunExploration::~RunExploration()
	{}

	void RunExploration::notify(const Solution& solution)
	{
		constexpr uint64_t delta_time = 100000;
		solution_ = solution;

		if(solution_.data.back().time - last_time_ > delta_time)
		{
			exploration_->step(map_->getDistanceMap(), solution_.data.back().state.pos, solution_.data.back().state.rot);
			last_time_ = solution_.data.back().time;
		}
		//std::cout << "RunExploration notified! pos: time = "  << solution_.data.back().time << std::endl;
	}
	
	void RunExploration::setMap(const boost::shared_ptr<Map>& map)
	{
		map_ = boost::dynamic_pointer_cast<GridMap>(map);
	}
}