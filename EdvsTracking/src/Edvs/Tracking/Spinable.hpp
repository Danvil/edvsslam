/*
 * Spinable.hpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef SPINABLE_HPP_
#define SPINABLE_HPP_

#include "Common.hpp"

namespace Edvs
{

class Spinable
{
public:
	virtual ~Spinable() {}

	virtual void spin(time_t time, Timespan dt) = 0;

};

}

#endif
