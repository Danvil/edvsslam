#ifndef WDGTEDVSTRACKING_H
#define WDGTEDVSTRACKING_H

#include "Tracking.hpp"
#include <Edvs/Tracking/Visualizations/CompareTrajectories.hpp>
#include <Edvs/Tracking/Visualizations/PlotStandardDeviation.hpp>
#include <Edvs/Tracking/Visualization.hpp>
#include <Edvs/Tracking/Spinable.hpp>
#include <Edvs/Tracking/SaveData.hpp>
#include <QtGui/QMainWindow>
#include <QtGui/QMdiSubWindow>
#include <QtCore/QTimer>
#include "ui_WdgtEdvsTracking.h"
#include <vector>
#include <map>

class EdvsTracking
: public QMainWindow
{
	Q_OBJECT

public:
	EdvsTracking(bool show_gui=true, QWidget *parent = 0);

	~EdvsTracking();

	void loadScenario(const std::string& filename);
	void loadMap(const std::string& filename);
	void loadPath(const std::string& filename);
	void loadEvents(const std::string& filename);
	void setExploration(const std::string& robot_address);
	void disableVisualizations();

	void setResultsTag(const std::string& tag);
	void setVisOutputPath(const std::string& path);
	void setParameters(const TrackingParameters& params);

	void run();

public Q_SLOTS:
	void tickNotify();
	void tickVis();
	void tickDebug();

	void onClickedRunPause();
	void onClickedStep();
	void onClickedReset();

	void onChangedTrackerParameter();

	void onChangedAddNoiseMeanPos(double val);
	void onChangedAddNoiseSigmaPos(double val);
	void onChangedAddNoiseMeanRot(double val);
	void onChangedAddNoiseSigmaRot(double val);
	void onClickedAddNoise();

	void onClickedComputeObjective();
	void onChangedObjectiveRot(int val);

	void onChangedVisDisableAll(int val);
	void onChangedVisMap(int val);
	void onChangedVisRetina(int val);
	void onChangedVisRetinaObjective(int val);
	void onChangedVisPaths(int val);

	void onClickedSaveMap();

public:
	boost::shared_ptr<Tracking> tracking_;

private:
	Ui::EdvsTrackingClass ui;

	QTimer timer_notify_;
	QTimer timer_vis_;
	QTimer timer_debug_;

	std::map<std::string,QMdiSubWindow*> vis_wdgts_;

	std::string p_save_vis_path_;

//	boost::shared_ptr<Edvs::CompareTrajectories> comp_trajec_pf_;
//	boost::shared_ptr<Edvs::CompareTrajectories> comp_trajec_event_;
//
//	boost::shared_ptr<Edvs::PlotStandardDeviation> plot_certainty_pf_;
//	boost::shared_ptr<Edvs::PlotStandardDeviation> plot_certainty_event_;

};

#endif
