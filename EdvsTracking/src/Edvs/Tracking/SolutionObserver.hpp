/*
 * SolutionObserver.hpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef EDVS_SOLUTIONLISTENER_HPP_
#define EDVS_SOLUTIONLISTENER_HPP_

#include "Common.hpp"

namespace Edvs {

class SolutionObserver
{
public:
	virtual ~SolutionObserver() {}

	virtual void notify(const Solution& solution) = 0;

};

}

#endif
