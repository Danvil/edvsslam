/*
 * SolutionSource.hpp
 *
 *  Created on: Mar 11, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_SOLUTIONSOURCE_HPP_
#define EDVS_TRACKING_SOLUTIONSOURCE_HPP_

#include "SolutionObserver.hpp"
#include "Spinable.hpp"
#include <boost/bind.hpp>
#include <boost/signal.hpp>
#include <boost/shared_ptr.hpp>

namespace Edvs {

class SolutionSource
: public Spinable
{
public:
	virtual ~SolutionSource() {}

	virtual const Solution& getSolution() const = 0;

	template<typename Function>
	boost::signals::connection registerSolutionObserver(Function f) {
		return observer_signal_.connect(f);
	}

	boost::signals::connection registerSolutionObserver(const boost::shared_ptr<SolutionObserver>& observer) {
		return observer_signal_.connect(boost::bind(&SolutionObserver::notify, observer, _1));
	}

protected:
	void notifySolutionObservers(const Solution& solution) {
		observer_signal_(solution);
	}

private:
	boost::signal<void(const Solution&)> observer_signal_;
};

}

#endif
