/*
 * SaveSolution.hpp
 *
 *  Created on: Feb 21, 2013
 *      Author: david
 */

#ifndef EDVS_TRACKING_SAVESOLUTION_HPP_
#define EDVS_TRACKING_SAVESOLUTION_HPP_

#include "Common.hpp"

namespace Edvs {

class SaveSolution
{
public:
	SaveSolution(const std::string& path);

	void notify(const Solution& solution);

private:
	std::string path_;
	std::ofstream ofs_;
};

}

#endif
