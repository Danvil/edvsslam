/*
 * Tracker.hpp
 *
 *  Created on: Feb 29, 2012
 *      Author: david
 */

#ifndef EDVS_TRACKING_TRACKER_HPP_
#define EDVS_TRACKING_TRACKER_HPP_

#include "Common.hpp"
#include "Visualization.hpp"
#include "EventObserver.hpp"
#include "SolutionSource.hpp"
#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <boost/shared_ptr.hpp>

namespace Edvs {

class Tracker
: public EventObserver, public SolutionSource
{
public:
	virtual ~Tracker() {}

	void setMap(const boost::shared_ptr<Map>& map){
		map_ = map;
	}

	void setObjective(const ObjectiveFunctionF& objective) {
		objective_ = objective;
	}

	const ObjectiveFunctionF& getObjective() const {
		return objective_;
	}

	const Solution& getSolution() const {
		return solution_;
	}

	virtual ParticleSet getParticles() const {
		return {};
	}

	void spin(time_t, Timespan) {}

	void notify(const std::vector<Edvs::Event>& events) {
		step(events);
	}

	virtual void reset(const State& initial_state) = 0;

	virtual void step(const std::vector<Edvs::Event>& events) = 0;

	boost::signal<void(const ParticleSet&)> on_particles_;
	boost::signal<void(const ParticleSet&, Timestamp time)> on_particles_evaluate_;

protected:
	ObjectiveFunctionF objective_;
	Solution solution_;
	boost::shared_ptr<Map> map_;

};

}

#endif
