/*
 * WdgtVisualizer.hpp
 *
 *  Created on: Jun 4, 2012
 *      Author: david
 */

#ifndef WDGTVISUALIZER_HPP_
#define WDGTVISUALIZER_HPP_

#include "Visualization.hpp"
#include <Candy/System/GLSystemQtWindow.h>
#include <Candy.h>
#include <QtGui/QLabel>
#include <QtGui/QPainter>
#include <QtGui/QScrollArea>
#include <QtGui/QGridLayout>
#include "Visualizations/VisualizationSaver.hpp"

namespace Edvs {

class WdgtVisualizer
: public QLabel
{
	Q_OBJECT

public:
	WdgtVisualizer(const boost::shared_ptr<Visualization>& vis, QWidget *parent = 0)
	: vis_(vis) {
		img_label_ = 0;
		gl_wdgt_ = 0;
		if(dynamic_cast<VisualizationImage*>(vis_.get())) {
			img_label_ = new QLabel();
			scroll_area_ = new QScrollArea();
			scroll_area_->setWidget(img_label_);
			QGridLayout* gridLayout = new QGridLayout();
			gridLayout->addWidget(scroll_area_, 0, 0);
			setLayout(gridLayout);
		}
		if(dynamic_cast<Visualization3D*>(vis_.get())) {
			std::cout << "Creating OpenGL Widget ..." << std::endl;

			boost::shared_ptr<Candy::View> view_ = Candy::View::FactorDefaultPerspectiveView();
			boost::shared_ptr<Candy::Scene> scene_ = view_->getScene();
			boost::shared_ptr<Candy::Engine> engine_(new Candy::Engine(view_));

			// boost::shared_ptr<Candy::DirectionalLight> light1(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(+1.0f, +1.0f, -1.0f)));
			// light1->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
			// scene_->addLight(light1);
			// boost::shared_ptr<Candy::DirectionalLight> light2(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(-1.0f, -1.0f, -1.0f)));
			// light2->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
			// scene_->addLight(light2);

			engine_->setClearColor(Danvil::Color::Black);
			scene_->EnableThreePointLighting(engine_);
			scene_->setShowCoordinateCross(true);

			gl_wdgt_ = new Candy::GLSystemQtWindow(0, engine_);
			QGridLayout* gridLayout = new QGridLayout();
			gridLayout->addWidget(gl_wdgt_, 0, 0);
			setLayout(gridLayout);

			boost::shared_ptr<Visualization3D> vis3d = boost::dynamic_pointer_cast<Visualization3D>(vis_);
			boost::shared_ptr<Candy::IRenderable> renderling(new Candy::ObjectRenderling(
						[vis3d]() { vis3d->render(); }));
			scene_->addItem(renderling);

			vis3d->candy_engine_ = engine_;
		}
		if(dynamic_cast<Visualization2D*>(vis_.get())) {
			// nothing
		}
	}
	
	~WdgtVisualizer() {
		delete img_label_;
		delete scroll_area_;
	}

	void enableSaveToFile(const std::string& path) {
		vis_saver_.reset(new VisualizationSaver(path + "/" + vis_->getName()));
	}

	void disableSaveToFile() {
		vis_saver_.reset();
	}

	void paintEvent(QPaintEvent*) {
		if(img_label_ != 0) {
			boost::shared_ptr<VisualizationImage> visimg = boost::dynamic_pointer_cast<VisualizationImage>(vis_);
			// repaint using image
			QImage img = *visimg->getImage();
			if(vis_saver_) {
				vis_saver_->save(img);
			}
			// scale wrt to desired scale factor
			float scale = visimg->getScale();
			if(scale != 1.0f) {
				img = img.scaled(QSize(img.width()*scale, img.height()*scale));
			}
			// the available display area
			int display_width = scroll_area_->maximumViewportSize().width() - 1;
			int display_height = scroll_area_->maximumViewportSize().height() - 1;
			// magnify image if display area is larger than image
			if(img.width() < display_width && img.height() < display_height) {
				if(display_height - img.height() > display_width - img.width()) {
					// scale to fit width
					img_label_->setPixmap(QPixmap::fromImage(img.scaledToWidth(display_width)));
				}
				else{
					// scale to fit height
					img_label_->setPixmap(QPixmap::fromImage(img.scaledToHeight(display_height)));
				}
			}
			else{
				// display normally
				img_label_->setPixmap(QPixmap::fromImage(img));
			}
			// adjust label size to show the whole image
			img_label_->adjustSize();
		}
		else if(gl_wdgt_ != 0) {
			boost::shared_ptr<Visualization3D> vis3d = boost::dynamic_pointer_cast<Visualization3D>(vis_);
			// FIXME
		}
		else {
			boost::shared_ptr<Visualization2D> vis2d = boost::dynamic_pointer_cast<Visualization2D>(vis_);
			// repaint into widget
			QPainter painter(this);
			vis2d->paint(painter);

			if(vis_saver_) {
				QImage imageToPNG(this->width(), this->height(), QImage::Format_RGB32);
				QPainter painterToPNG;
				painterToPNG.begin(&imageToPNG);
				vis2d->paint(painterToPNG);
				painterToPNG.end();
				vis_saver_->save(imageToPNG);
			}
		}
	}

private:
	boost::shared_ptr<Visualization> vis_;
	QLabel* img_label_;
	QScrollArea* scroll_area_;
	Candy::GLSystemQtWindow* gl_wdgt_;
	boost::shared_ptr<VisualizationSaver> vis_saver_;
};

}

#endif
