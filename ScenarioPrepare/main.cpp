/*
 * main.cpp
 *
 *  Created on: Jul 17, 2012
 *      Author: david
 */

#include <Edvs/Tools/Scenario.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <string>

int main(int argc, char** argv)
{
	std::string p_fn_scenario_path;
	std::string p_fn_input;
	std::string p_fn_output;
	double p_time_offset;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("scenario", po::value<std::string>(&p_fn_scenario_path), "link to scenario directory")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help") || !vm.count("scenario")) {
		std::cout << desc << std::endl;
		return 1;
	}

	// get base path
	std::string p_fn_scenario = p_fn_scenario_path + "/scenario.xml";

	// load scenario file
	Edvs::Scenario::ScenarioSettings settings;
	settings.load(p_fn_scenario);

	// load calibration
	std::string fn_frame = p_fn_scenario_path + "/" + settings.fn_calib + "/frame.txt";
	std::cout << "Loading frame ..." << std::endl;
	std::cout << "Calibration File = '" << fn_frame << "'" << std::endl;
	Edvs::Scenario::Calibration calib = Edvs::Scenario::LoadCalibration(fn_frame);
	std::cout << calib << std::endl;

	// convert path
	std::string fn_input = p_fn_scenario_path + "/path_cam_pose.txt";
	std::string fn_output = p_fn_scenario_path + "/path.txt";
	std::cout << "Converting points ... " << std::endl;
	std::cout << "Input File = '" << fn_input << "'" << std::endl;
	std::cout << "Output File = '" << fn_output << "'" << std::endl;
	unsigned int n = Edvs::Scenario::TransformPath(fn_input, fn_output, calib, settings.time_offset_s);
	std::cout << "Converted " << n << " points" << std::endl;

	return 1;
}
