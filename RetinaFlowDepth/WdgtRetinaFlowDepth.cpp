#include "WdgtRetinaFlowDepth.h"
#include <Eigen/Dense>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <iostream>


const unsigned int RetinaSize = 128;
const int cDecay = 4;
const int cDisplaySize = 512;
const int cUpdateInterval = 1;

// blue/yellow color scheme
// const QRgb cColorMid = qRgb(0, 0, 0);
// const QRgb cColorOn = qRgb(255, 255, 0);
// const QRgb cColorOff = qRgb(0, 0, 255);

// black/white color scheme
const QRgb cColorMid = qRgb(128, 128, 128);
const QRgb cColorOn = qRgb(255, 255, 255);
const QRgb cColorOff = qRgb(0, 0, 0);

WdgtRetinaFlowDepth::WdgtRetinaFlowDepth(const Edvs::EventStream& dh, QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

	image_ = QImage(RetinaSize, RetinaSize, QImage::Format_RGB32);

	timer_.connect(&timer_, SIGNAL(timeout()), this, SLOT(Update()));
	timer_.setInterval(cUpdateInterval);
	timer_.start();

	// start capture
	edvs_event_stream_ = dh;
	edvs_event_capture_ = Edvs::EventCapture(edvs_event_stream_,
		std::bind(&WdgtRetinaFlowDepth::OnEvent, this, std::placeholders::_1));
}

WdgtRetinaFlowDepth::~WdgtRetinaFlowDepth()
{
}

void WdgtRetinaFlowDepth::OnEvent(const std::vector<Edvs::Event>& newevents)
{
	// just store events
	// protect common vector with a mutex to avoid race conditions
	boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
	events_.insert(events_.end(), newevents.begin(), newevents.end());

	// for(const auto& e : newevents) {
	// 	std::cout << e.t << ", ";
	// }
	// std::cout << std::endl;
	if(!newevents.empty())
		std::cout << newevents.back().t << std::endl;
}

int DecayComponent(int current, int target, int decay)
{
	if(current-decay >= target) {
		return current - decay;
	}
	if(current+decay <= target) {
		return current + decay;
	}
	return target;
}

QRgb DecayColor(QRgb color, QRgb target, int decay)
{
	return qRgb(
		DecayComponent(qRed(color), qRed(target), cDecay),
		DecayComponent(qGreen(color), qGreen(target), cDecay),
		DecayComponent(qBlue(color), qBlue(target), cDecay)
	);
}

float Rescale(float d, float min, float max)
{
	return (d - min) / (max - min);
}

Eigen::Vector3f BlueRedColor(float d)
{
	return { d, 0.0f, 1.0f - d };
}

Eigen::Vector3f Colorize(float d)
{
	if(d > 1.0f) {
		return {1.0f, 1.0f, 0.0f};
	}
	else if(d < 0.0f) {
		return {0.0f, 1.0f, 1.0f};
	}
	else {
		return {d, 1.0f - d, 0.0f};
	}
}

QRgb Vec3ToColor(const Eigen::Vector3f& v)
{
	unsigned char crub = std::max(0, std::min(255, static_cast<int>(255.0f*v[0])));
	unsigned char cgub = std::max(0, std::min(255, static_cast<int>(255.0f*v[1])));
	unsigned char cbub = std::max(0, std::min(255, static_cast<int>(255.0f*v[2])));
	return qRgb(crub, cgub, cbub);
}

void WdgtRetinaFlowDepth::Update()
{
	// apply decay
	// write events
	{
		boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
		for(const Edvs::Event& e : events_) {
			retina_.onEvent(e);
		}
		events_.clear();
	}

	// image_ = RetinaImage(retina_,
	// 	[](const Edvs::Retina::Pixel& p) {
	// 		float f = std::min(1.0f, p.flow2);
	// 		unsigned char q = static_cast<int>(255.0f*f);
	// 		return qRgb(q,q,255-q);
	// 	});

	// image_ = RetinaImage(retina_,
	// 	[this](const Edvs::Retina::Pixel& p) {
	// 		float dtf = 1.0f / static_cast<float>(
	// 			1.0f + static_cast<float>(retina_.last_time - p.last_time)/100000.0f);
	// 		unsigned char q = static_cast<int>(255.0f*dtf);
	// 		return qRgb(q,q,255-q);
	// 	});

	image_ = RetinaImage(retina_,
		[this](const Edvs::Retina::Pixel& p) {
			float c = retina_.age_decay(p, 0.3f);
			float d = p.flow;
			//float d = p.r / p.flow;
			return Vec3ToColor(
				c*Colorize(
					Rescale(d, 0.0f, 1000.0f)));
		});

	// rescale so that we see more :)
	QImage big = image_.scaled(cDisplaySize, cDisplaySize);
	// display
	ui.label->setPixmap(QPixmap::fromImage(big));
}
