#ifndef INCLUDED_WDGTRETINAFLOWDEPTH_H
#define INCLUDED_WDGTRETINAFLOWDEPTH_H

#include "Retina.hpp"
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include "ui_WdgtRetinaFlowDepth.h"
#include <Edvs/EventStream.hpp>
#include <Edvs/EventCapture.hpp>
#include <boost/thread.hpp>
#include <vector>

class WdgtRetinaFlowDepth : public QWidget
{
    Q_OBJECT

public:
	WdgtRetinaFlowDepth(const Edvs::EventStream& dh, QWidget *parent = 0);
	~WdgtRetinaFlowDepth();

	void OnEvent(const std::vector<Edvs::Event>& events);

public Q_SLOTS:
	void Update();

private:
	Edvs::EventStream edvs_event_stream_;
	Edvs::EventCapture edvs_event_capture_;
	std::vector<Edvs::Event> events_;
	Edvs::Retina retina_;
	boost::mutex events_mtx_;
	QTimer timer_;
	QImage image_;

private:
    Ui::WdgtRetinaFlowDepthClass ui;
};

#endif
