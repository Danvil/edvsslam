#ifndef INCLUDED_RETINA
#define INCLUDED_RETINA

#include <Edvs/Event.hpp>
#include <QtGui/QImage>
#include <iostream>
#include <cstdint>

namespace Edvs
{
	struct Retina
	{
		struct Pixel {
			uint64_t last_time;
			float flow_x, flow_y;
			float flow;
			float r;
		};

		unsigned int ROWS, COLS;

		std::vector<Pixel> pixels;

		int64_t last_time;

		Retina()
		: ROWS(128), COLS(128), pixels(128*128) {
			create();
		}

		void create() {
			last_time = 0;
			int cx = ROWS/2;
			int cy = COLS/2;
			for(int i=0; i<ROWS; i++) {
				for(int j=0; j<COLS; j++) {
					Retina::Pixel& p = (*this)(j,i);
					p.last_time = 0;
					p.flow_x = 0.0f;
					p.flow_y = 0.0f;
					p.flow = 0.0f;
					p.r = static_cast<float>((i-cy)*(i-cy) + (j-cx)*(j-cx));
				}
			}
		}

		const Pixel& at(int x, int y) const {
			return pixels[x + y*COLS];
		}

		Pixel& at(int x, int y) {
			return pixels[x + y*COLS];
		}

		const Pixel& operator()(int x, int y) const {
			return at(x,y);
		}

		Pixel& operator()(int x, int y) {
			return at(x,y);
		}

		bool isInside(int x, int y, int d=1) const {
			return d <= x && x+d < COLS
				&& d <= y && y+d < ROWS;
		}

		static constexpr float MUS = 1000000.0f;

		void onEvent(const Edvs::Event& e) {
			constexpr int R = 2;
			int x = e.x;
			int y = e.y;
			int64_t t = e.t;
			Pixel& p = (*this)(x, y);
			p.last_time = e.t;
			last_time = std::max<int64_t>(last_time, e.t);
			if(!isInside(x,y,R)) {
				return;
			}
			int64_t tx0 = at(x-R,y).last_time;
			int64_t tx2 = at(x+R,y).last_time;
			int64_t ty0 = at(x,y-R).last_time;
			int64_t ty2 = at(x,y+R).last_time;
			p.flow_x = static_cast<float>(R)
				* MUS / (static_cast<float>(1 + std::min(t - tx0, t - tx2)));
			p.flow_y = static_cast<float>(R)
				* MUS / (static_cast<float>(1 + std::min(t - ty0, t - ty2)));
			p.flow = std::sqrt(p.flow_x*p.flow_x + p.flow_y*p.flow_y);
			std::cout << "f_x=" << p.flow_x << ", f_y=" << p.flow_y << ", f=" << p.flow << std::endl;
		}

		float age_decay(const Pixel& p, float A=1.0f) const {
			return 1.0f / static_cast<float>(
				1.0f + static_cast<float>(last_time - p.last_time)/(A*MUS));
		}

	};


	inline Retina CreateRetina()
	{
		Retina retina;
		return retina;
	}

	template<typename COLFNC>
	QImage RetinaImage(const Retina& retina, COLFNC colfnc)
	{
		QImage img(retina.COLS, retina.ROWS, QImage::Format_RGB32);
		for(unsigned int i=0; i<retina.ROWS; i++) {
			for(unsigned int j=0; j<retina.COLS; j++) {
				img.setPixel(j, i, colfnc(retina(j,i)));
			}
		}
		return img;
	}

}

#endif
