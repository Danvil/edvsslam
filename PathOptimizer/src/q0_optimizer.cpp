#include "pathoptimization.h"
#include <q0/q0.hpp>
#include <q0/algorithms.hpp>
#include <q0/domains/cartesian.hpp>
#include <q0/domains/so2.hpp>
#include <q0/domains/tuple.hpp>
#include <q0/algorithms/apso.hpp>
#include <boost/function.hpp>
#include <functional>
#include <iostream>

typedef q0::domains::tuple<
	q0::domains::cartesian<float,1,q0::domains::cartesian_constraint_box>, //time
	q0::domains::cartesian<float,2,q0::domains::cartesian_constraint_box>, //shift
	q0::domains::so2<float>	//theta
> domain_t;

typedef typename q0::domains::state_type<domain_t>::type state_t;

OptimizationParameters state_to_params(const OptimizationParameters& p0, const state_t& s)
{
	OptimizationParameters p = p0;
	p.time = std::get<0>(s);
	p.shift_x = std::get<1>(s)[0];
	p.shift_y = std::get<1>(s)[1];
	p.theta = std::get<2>(s);
	return p;
}

void print(const state_t& x, float s)
{
	std::cout << "{"
		<< "t=" << std::get<0>(x)
		<< ", x=" << std::get<1>(x)[0]
		<< ", y=" << std::get<1>(x)[1]
		<< ", r=" << std::get<2>(x)
		<< "} -> " << s << std::endl;
}

OptimizationParameters optimize_q0(
	const OptimizationParameters& params,
	const std::function<float(const OptimizationParameters& params)>& eval,
	const std::function<void(const OptimizationParameters& params)>& tick)
{
	domain_t dom;
	std::get<0>(dom).set_box_extends(-10,+10); // seconds
	std::get<1>(dom).set_box_extends(-2,+2);
	
	unsigned int f_eval_count = 0;

	boost::function<float(const state_t&)> q0eval = 
		[&params,&f_eval_count,eval](const state_t& x) {
			f_eval_count ++;
			return eval(state_to_params(params, x));
		};

	boost::function<bool(const state_t&,float)> q0tester =
		[&params,&f_eval_count,tick](const state_t& x, float s) {
			tick(state_to_params(params, x));
//			print(x, s);
			return f_eval_count >= 1000;
		};

	q0::algorithms::apso algo;
	//algo.parameters.num = 45;
	algo.parameters.num = 90;

	auto p = q0::minimize(dom, q0eval, algo, q0::stop_condition<state_t,float>(q0tester));

	print(p.state, p.score);
	std::cout << "Number of evaluations: " << f_eval_count << std::endl;
	return state_to_params(params, p.state);
}
