#ifndef PATHOPTIMIZATION_H
#define PATHOPTIMIZATION_H

#include <vector>
#include <string>
#include <Eigen/Dense>

struct PathPoint
{
    float time;
    Eigen::Matrix<float, 2, 1> pos;
    float rot;
};

struct FilterParameters
{
    float max_x;
    float min_x;
    float max_y;
    float min_y;
    float max_v;
    bool mirror_x;
};

struct OptimizationParameters
{
    float time; //time shift btw ground_truth and
    float theta;    // rotation around 0.0, 0.0
    float shift_x;  // shift added in x-direction
    float shift_y;  // shift added in y-direction
    float scale;    // scaling aroud 0.0, 0.0
    float alpha;    // camera tilt direction
    float phi;      // angle of camera tilt
    float error;    //error of this set of params
};

enum path {reference, filtered, tracked, optimized};

class PathOptimization
{
public:
    PathOptimization();

    void reset_optimization_params();
    void reset_optimization_delta_params();
    void reset_filter_params();
    bool load_path(std::string filename, path path_name);
    void print_path(path path_name);
    void save_path_file();
    void save_errors_file();

    void calculate_optimized();
    void calculate_filtered();

    //void downsample(path path_name);
    float sum_of_quadratic_errors(int step_size = 10, bool use_to_time=false, float to_time = 0);
    
    void print_params(const OptimizationParameters& params);

    void run_optimization(float rate, int steps, const bool do_global_optim, const bool do_local_optim);

    PathPoint get_path_point(path path_name, int index);
    int get_size(path path_name);

    OptimizationParameters get_optimization_parameters(){return optimization_parameters_;}
    void set_optimization_parameters(OptimizationParameters new_optimization_parameters){optimization_parameters_=new_optimization_parameters;}

    FilterParameters get_filter_parameters(){return filter_parameters_;}
    void set_filter_parameters(FilterParameters new_filter_parameters){filter_parameters_=new_filter_parameters;}

    void set_fn_aligned(std::string fn){fn_aligned_path_ = fn;}
    void set_fn_filtered(std::string fn){fn_filtered_path_ = fn;}

    void set_fn_error(std::string fn){fn_error_path_ = fn;}
    void set_fn_final_error(std::string fn){fn_final_error_ = fn;}

    void save_final_error();
    
private:
    std::vector<PathPoint> reference_path_;
    std::vector<PathPoint> filtered_reference_path_;

    std::vector<PathPoint> tracked_path_;
    std::vector<PathPoint> optimized_path_;

    OptimizationParameters optimization_parameters_;
    OptimizationParameters delta_parameters_;
    FilterParameters filter_parameters_;

    int sample_size_;

    float ceiling_height_;
    std::string fn_aligned_path_;
    std::string fn_filtered_path_;
    std::string fn_error_path_;
    std::string fn_final_error_;
};

#endif // PATHOPTIMIZATION_H
