#include "pathoptimization.h"
#include <functional>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>

PathOptimization::PathOptimization()
{
	this->reset_optimization_params();
	this->reset_optimization_delta_params();
	this->reset_filter_params();

	ceiling_height_ = 3.0f; //This should be read in from either map.xml or a so far nonexistens room parameter file "room.xml"
	sample_size_ = 300;
}

void PathOptimization::reset_optimization_params()
{
	optimization_parameters_.time = 0.0;
	optimization_parameters_.theta = 0.0;
	optimization_parameters_.shift_x = 0.0;
	optimization_parameters_.shift_y = 0.0;
	optimization_parameters_.scale = 1.0;
	optimization_parameters_.alpha = 0.0;
	optimization_parameters_.phi = 0.0;
	optimization_parameters_.error = 0.0;
}

void PathOptimization::reset_optimization_delta_params()
{
	 delta_parameters_.time = 0.01;
	 delta_parameters_.theta = 0.01;
	 delta_parameters_.shift_x = 0.01;
	 delta_parameters_.shift_y = 0.01;
	 delta_parameters_.scale = 0.01;
	 delta_parameters_.alpha = 0.01;
	 delta_parameters_.phi = 0.01;
	 delta_parameters_.error = 0.01;
}

void PathOptimization::reset_filter_params()
{
	filter_parameters_.max_x = 2.5;
	filter_parameters_.min_x = -2.5;
	filter_parameters_.max_y = 2.5;
	filter_parameters_.min_y = -2.5;
	filter_parameters_.max_v = 0.5;
	filter_parameters_.mirror_x = true;
}

bool PathOptimization::load_path(std::string filename, path path_name)
{
	std::ifstream is(filename.c_str());
	if(!is){
		return false;
	}
	else
	{
		if(path_name == reference){
			reference_path_.clear();}
		else if (path_name == tracked){
			tracked_path_.clear();}

		std::string line;

		bool first_run = true;
		float start_time;

		while(getline(is, line))
		{
			PathPoint point;
			std::stringstream ss(line);
			ss >> point.time;
			ss >> point.pos[0];
			ss >> point.pos[1];
			ss >> point.rot;

			if(first_run)
			{
				first_run= false;	
				start_time = point.time;
			}

			if(path_name == reference){
				reference_path_.push_back(point);}
			else if (path_name == tracked){
				point.time -= start_time;
				tracked_path_.push_back(point);}
		}

		this->calculate_filtered();
		this->calculate_optimized();
		return true;
	}

}

void PathOptimization::print_path(path path_name)
{
	int path_size;
	std::vector<PathPoint> output_path;

	if(path_name == reference){
		path_size = reference_path_.size();
		output_path = reference_path_;}
	else if (path_name == tracked){
		path_size = tracked_path_.size();
		output_path = tracked_path_;}

	for(unsigned i = 0; i < path_size; i++)
	{
		std::cout << output_path[i].time << " " << output_path[i].pos[0] << " " << output_path[i].pos[1] << " " << output_path[i].rot << std::endl;
	}
}

void PathOptimization::save_path_file()
{
  	std::ofstream path_file;
  	path_file.open(fn_aligned_path_);

  	for(unsigned i = 0; i < optimized_path_.size(); i++)
  	{
		path_file << optimized_path_[i].time << "\t" << optimized_path_[i].pos[0] << "\t" 
				<< optimized_path_[i].pos[1] << "\t" << optimized_path_[i].rot << std::endl;
  	}
  	path_file.close();


  	std::ofstream path_file2;
  	path_file2.open(fn_filtered_path_);

  	

  	for(unsigned i = 0; i < filtered_reference_path_.size(); i++)
  	{
		path_file2 << filtered_reference_path_[i].time << "\t" << filtered_reference_path_[i].pos[0] << "\t" 
				<< filtered_reference_path_[i].pos[1] << "\t" << filtered_reference_path_[i].rot << std::endl;
  	}
  	path_file2.close();
}

void PathOptimization::save_errors_file()
{
	struct ErrorEntry
	{
		float time;
		float error;
	};

	float time_resolution = 0.5;
	float start_time = 0.0;
	float last_time = 0.0;

	std::vector<ErrorEntry> errors;
	ErrorEntry one_entry;

	for(unsigned i = 0; i < optimized_path_.size(); i++)
  	{
  			//last_time = filtered_reference_path_[i].time;

	        float min_dt = fabs(optimized_path_[i].time - filtered_reference_path_[0].time);
			unsigned min_index = 0;

  			for(unsigned i_t = 1; i_t < filtered_reference_path_.size(); i_t++)
			{
                if(fabs(optimized_path_[i].time - filtered_reference_path_[i_t].time) < min_dt)
				{
                    min_dt = fabs(optimized_path_[i].time - filtered_reference_path_[i_t].time);
					min_index = i_t;
				}
			}

			//one_entry.error = this->sum_of_quadratic_errors(10, true, filtered_reference_path_[i].time);
			one_entry.error = (filtered_reference_path_[min_index].pos - optimized_path_[i].pos).norm();
  			one_entry.time = optimized_path_[i].time;
  			errors.push_back(one_entry);
   	}
/*
	for(unsigned i = 0; i < filtered_reference_path_.size(); i++)
  	{
  		
  		if((filtered_reference_path_[i].time - last_time) > time_resolution)
  		{	
  			last_time = filtered_reference_path_[i].time;

	        float min_dt = fabs(filtered_reference_path_[i].time - optimized_path_[0].time);
			unsigned min_index = 0;

  			for(unsigned i_t = 1; i_t < optimized_path_.size(); i_t++)
			{
                if(fabs(filtered_reference_path_[i].time - optimized_path_[i_t].time) < min_dt)
				{
                    min_dt = fabs(filtered_reference_path_[i].time - optimized_path_[i_t].time);
					min_index = i_t;
				}
			}

			//one_entry.error = this->sum_of_quadratic_errors(10, true, filtered_reference_path_[i].time);
			one_entry.error = (optimized_path_[min_index].pos - filtered_reference_path_[i].pos).norm();

  			one_entry.time = filtered_reference_path_[i].time;
  			errors.push_back(one_entry);
  		}
  */
  		
 /* 		if((filtered_reference_path_[i].time - last_time) > time_resolution)
  		{	
  			last_time = filtered_reference_path_[i].time;

  			one_entry.error = this->sum_of_quadratic_errors(10, true, filtered_reference_path_[i].time);
  			one_entry.time = filtered_reference_path_[i].time;
  			errors.push_back(one_entry);
  		}
  		
	}*/

	std::ofstream error_file;
  	error_file.open(fn_error_path_);

  	for(unsigned i = 0; i < errors.size(); i++)
  	{
		error_file << errors[i].time << "\t" << errors[i].error << std::endl;
  	}
  	error_file.close();

}

    void PathOptimization::save_final_error()
	{
        std::ofstream error_file;
        error_file.open(fn_final_error_);
        error_file << this->sum_of_quadratic_errors() << std::endl;
        error_file.close();
    }


void PathOptimization::calculate_optimized()
{
	int path_size = tracked_path_.size();
	optimized_path_.clear();

	for(unsigned i = 0; i < path_size; i++)
	{
		PathPoint new_optimized_point;
		PathPoint original_point = tracked_path_[i];

        new_optimized_point.time = original_point.time + optimization_parameters_.time;

		float tilt_shift_distance = ceiling_height_ * tan(optimization_parameters_.alpha);

		//apply rotation, scale and shift
		new_optimized_point.rot = original_point.rot + optimization_parameters_.theta;
		new_optimized_point.pos[0] = ( original_point.pos[0] * std::cos(optimization_parameters_.theta) * optimization_parameters_.scale )
								  - ( original_point.pos[1] * std::sin(optimization_parameters_.theta) * optimization_parameters_.scale )
								  + ( optimization_parameters_.shift_x )
								  + ( tilt_shift_distance * cos(new_optimized_point.rot + optimization_parameters_.phi) );
		new_optimized_point.pos[1] = ( original_point.pos[0] * std::sin(optimization_parameters_.theta) * optimization_parameters_.scale )
								  + ( original_point.pos[1] * std::cos(optimization_parameters_.theta) * optimization_parameters_.scale )
								  + ( optimization_parameters_.shift_y )
								  + ( tilt_shift_distance * sin(new_optimized_point.rot + optimization_parameters_.phi) );


		optimized_path_.push_back(new_optimized_point);
	}

}

void PathOptimization::calculate_filtered()
{
	int ref_path_size = reference_path_.size();
	filtered_reference_path_.clear();
	PathPoint new_filtered_point;
	PathPoint current_filtered_point;
	PathPoint previous_filtered_point;

	std::vector<float> speeds;
	speeds.push_back(0.0);
	for(unsigned i = 1; i < ref_path_size; i++)
	{
		current_filtered_point = reference_path_[i];
		previous_filtered_point = reference_path_[i-1];
		speeds.push_back((current_filtered_point.pos - previous_filtered_point.pos).norm() / (current_filtered_point.time - previous_filtered_point.time));
	}

	//filter out-of-range entries
	for(unsigned i = 0; i < ref_path_size; i++)
	{
		new_filtered_point = reference_path_[i];

		if( new_filtered_point.pos[0] < filter_parameters_.max_x &&
			new_filtered_point.pos[0] > filter_parameters_.min_x &&
			new_filtered_point.pos[1] < filter_parameters_.max_y &&
			new_filtered_point.pos[1] > filter_parameters_.min_y &&
			speeds[i] < filter_parameters_.max_v
		  )
		{
			if(filter_parameters_.mirror_x)
			{
				new_filtered_point.pos[1] *= -1.0f;
			}
			filtered_reference_path_.push_back(new_filtered_point);
		}
	}
}
/*
void PathOptimization::downsample(path path_name)
{
	switch(path_name)
	{
	case reference:
		std::random_shuffle(reference_path_.begin(), reference_path_.end());
		reference_path_.erase(reference_path_.begin()+sample_size_, reference_path_.end());
		break;
	case filtered:
		std::random_shuffle(filtered_reference_path_.begin(), filtered_reference_path_.end());
		filtered_reference_path_.erase(filtered_reference_path_.begin()+sample_size_, filtered_reference_path_.end());
	case tracked:
		std::random_shuffle(tracked_path_.begin(), tracked_path_.end());
		tracked_path_.erase(tracked_path_.begin()+sample_size_, tracked_path_.end());
	case optimized:
		std::random_shuffle(optimized_path_.begin(), optimized_path_.end());
		optimized_path_.erase(optimized_path_.begin()+sample_size_, optimized_path_.end());
	//    default:
	//        PathPoint defaultpoint;
	//        return(defaultpoint); break;
	}
}
*/

float PathOptimization::sum_of_quadratic_errors(int step_size, bool use_to_time, float to_time)
{
	float sum_of_squared_distances = 0.0;

	//int steps_reference = filtered_reference_path_.size() / sample_size_;
	int steps_reference = step_size;
	int num_points = 0;
	for(unsigned i_r=0; i_r<filtered_reference_path_.size(); i_r+=steps_reference) {
		const float target_time = filtered_reference_path_[i_r].time;
		if(use_to_time) {
			if(target_time > to_time) {
				break;
			}
		}

		// find nearest time in other (use binary search for speed)
		auto it = std::lower_bound(optimized_path_.begin(), optimized_path_.end(), target_time,
			[](const PathPoint& p, float t) {
				return p.time < t;
			});
		// take this or previous - whichever is nearer
		float time_dist = std::abs(it->time - target_time);
		if(it > optimized_path_.begin()) {
			auto itp = it;
			std::advance(itp, -1);
			float td = std::abs(itp->time - target_time);
			if(td < time_dist) {
				it = itp;
				time_dist = td;
			}
		}

		// add to RMSE if a near point was found
		if(time_dist <= 0.02f) {
			num_points ++;
			sum_of_squared_distances += (it->pos - filtered_reference_path_[i_r].pos).squaredNorm();
		}
	}
	
	return std::sqrt(sum_of_squared_distances/num_points);

	/*
	//this->downsample(filtered);
	//this->downsample(optimized);
	float sum_of_squared_distances = 0.0;

	// for(const auto& x1 : optimized_path_) //umbauen
	//      x1.pos

	int steps_tracked = optimized_path_.size() / sample_size_;
	int steps_reference = filtered_reference_path_.size() / sample_size_;

	for(unsigned i_t = 0; i_t < optimized_path_.size(); i_t+=steps_tracked)
	{
		float min_squared_distance = (optimized_path_[i_t].pos -filtered_reference_path_[0].pos).squaredNorm();
		for(unsigned i_r = 0; i_r < filtered_reference_path_.size(); i_r+=steps_reference)
		{
			if((optimized_path_[i_t].pos - filtered_reference_path_[i_r].pos).squaredNorm() < min_squared_distance)
			{
				min_squared_distance = (optimized_path_[i_t].pos - filtered_reference_path_[i_r].pos).squaredNorm();
			}
		}
		sum_of_squared_distances += min_squared_distance;
	}
	sum_of_squared_distances /= sample_size_;
	sum_of_squared_distances = sqrt(sum_of_squared_distances);
	return(sum_of_squared_distances);
	*/
}

// TODO move to own header
OptimizationParameters optimize_q0(
	const OptimizationParameters& params,
	const std::function<float(const OptimizationParameters& params)>& eval,
	const std::function<void(const OptimizationParameters& params)>& tick);

/** return a + s*b */
OptimizationParameters add_scaled(const OptimizationParameters& a, const OptimizationParameters& s, const OptimizationParameters& b) {
	OptimizationParameters c;
	c.time = a.time + s.time * b.time;
	c.theta = a.theta + s.theta * b.theta;
	c.shift_x = a.shift_x + s.shift_x * b.shift_x;
	c.shift_y = a.shift_y + s.shift_y * b.shift_y;
	c.scale = a.scale + s.scale * b.scale;
	c.alpha = a.alpha + s.alpha * b.alpha;
	c.phi = a.phi + s.phi * b.phi;
	c.error = a.error + s.error * b.error;
	return c;
}

void PathOptimization::print_params(const OptimizationParameters& params)
{
	std::cout 	<< " time: "<< params.time 
				<< " theta: " << params.theta
				<< " shift-x: " << params.shift_x 
				<< " shift-y: " << params.shift_y
				<< " scale: " << params.scale
				<< " alpha: " << params.alpha
				<< " phi: " << params.phi
				<< " error: " << params.error << std::endl;
}

void PathOptimization::run_optimization(float rate, int steps, const bool do_global_optim, const bool do_local_optim)
{
	const unsigned num_global_optim = 5;
	//const bool do_global_optim = true;
	//const bool do_local_optim = true;

	float lowest_error = this->sum_of_quadratic_errors();

	OptimizationParameters globally_optimized_parameters = optimization_parameters_;
	OptimizationParameters start_parameters = optimization_parameters_;

	if(do_global_optim) {
		// q0 optimizer
		for(unsigned i = 0; i< num_global_optim; i++)
		{
			optimization_parameters_ = optimize_q0(optimization_parameters_,
				[this](const OptimizationParameters& params) {
					this->optimization_parameters_ = params;
					this->calculate_optimized();
					return this->sum_of_quadratic_errors();
				},
				[this](const OptimizationParameters& params) { //e.g. gui update
					this->optimization_parameters_ = params;
					this->calculate_optimized();
				 });
			float current_error = this->sum_of_quadratic_errors();
	//		std::cout << "curr err run " << i << ": " << current_error << std::endl;
			//globally_optimized_parameters = optimization_parameters_;
			if(current_error < lowest_error)
			{
				lowest_error = current_error;
				globally_optimized_parameters = optimization_parameters_;
				globally_optimized_parameters.error = current_error;
			}
			optimization_parameters_ = start_parameters;
		}
	
	//	std::cout << "lowest:" << this->sum_of_quadratic_errors() << std::endl;
		
		optimization_parameters_ = globally_optimized_parameters;
	}

	if(do_local_optim) {
		// built in optimizer by raoul

		OptimizationParameters params_rate;
		// params_rate.time = delta_parameters_.time * rate;
		// params_rate.shift_x = delta_parameters_.shift_x * rate;
		// params_rate.shift_y = delta_parameters_.shift_y * rate;
		// params_rate.theta = delta_parameters_.theta * rate;
		params_rate.time = rate;
		params_rate.shift_x = rate;
		params_rate.shift_y = rate;
		params_rate.theta = rate;
		params_rate.scale = 0.0;
		params_rate.alpha = 0.0;
		params_rate.phi = 0.0;
		params_rate.error = 0.0;

		float last_error = optimization_parameters_.error;
		for(unsigned s = 0; s < steps; s++)
		{
			OptimizationParameters optimization_parameters_start = optimization_parameters_;
			this->calculate_optimized();
			float start_error = this->sum_of_quadratic_errors();
			optimization_parameters_start.error = start_error;

			OptimizationParameters param_errors;
			param_errors.scale=0.0; //params that will not be optimized
			param_errors.alpha=0.0;
			param_errors.phi=0.0; 
			param_errors.error = 0;

			optimization_parameters_.time = optimization_parameters_start.time + delta_parameters_.time;
			this->calculate_optimized();
			param_errors.time = (start_error - this->sum_of_quadratic_errors())/delta_parameters_.time;
			optimization_parameters_ = optimization_parameters_start;

			optimization_parameters_.theta = optimization_parameters_start.theta + delta_parameters_.theta;
			this->calculate_optimized();
			param_errors.theta = (start_error - this->sum_of_quadratic_errors())/delta_parameters_.theta;
			optimization_parameters_ = optimization_parameters_start;

			optimization_parameters_.shift_x = optimization_parameters_start.shift_x + delta_parameters_.shift_x;
			this->calculate_optimized();
			param_errors.shift_x = (start_error - this->sum_of_quadratic_errors())/delta_parameters_.shift_x;
			optimization_parameters_ = optimization_parameters_start;

			optimization_parameters_.shift_y = optimization_parameters_start.shift_y + delta_parameters_.shift_y;
			this->calculate_optimized();
			param_errors.shift_y = (start_error - this->sum_of_quadratic_errors())/delta_parameters_.shift_y;
			
			//optimization_parameters_ = optimization_parameters_start;

			optimization_parameters_ = add_scaled(optimization_parameters_start, params_rate, param_errors);
			optimization_parameters_.error =  this->sum_of_quadratic_errors();
			this->calculate_optimized();
			std::cout << "before: \t";
			print_params(optimization_parameters_start);
			std::cout << "now:    \t";
			print_params(optimization_parameters_);		
			std::cout << "errors: \t";
			print_params(param_errors);		
			std::cout << "local optim run " << s << ": " << optimization_parameters_.error << std::endl;

			/*if(optimization_parameters_.error > last_error)
			{
				break;
			}*/
			//last_error = optimization_parameters_.error;
			//std::cout << "Delta Error after Step " << s << " is +dir " << error_positive - current_error << " and in -dir "<<  error_negative - current_error << " " << std::endl;
		}
	}
	this->calculate_optimized();
	this->calculate_filtered();

	this->save_path_file();
	this->save_errors_file();
	this->save_final_error();
}

PathPoint PathOptimization::get_path_point(path path_name, int index)
{
	switch(path_name)
	{
	case reference:
		return(reference_path_[index]); break;
	case filtered:
		return(filtered_reference_path_[index]); break;
	case tracked:
		return(tracked_path_[index]);   break;
	case optimized:
		return(optimized_path_[index]); break;
//    default:
//        PathPoint defaultpoint;
//        return(defaultpoint); break;

	}
}

int PathOptimization::get_size(path path_name)
{
	if(path_name == reference){
		return(reference_path_.size());}
	else if (path_name == filtered){
		return(filtered_reference_path_.size());}
	else if (path_name == tracked){
		return(tracked_path_.size());}
	else if (path_name == optimized){
		return(optimized_path_.size());}
	else{ return(0);}
}
