#include <QtGui/QApplication>
#include "mainwindow.h"

#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;
	po::options_description desc("Allowed options:");
	desc.add_options()
		("help", "show help message")
		("fn_ref", po::value<std::string>(), "filename reference path")
		("fn_track", po::value<std::string>(), "filename tracked path")
		("fn_aligned_out", po::value<std::string>(), "filename to save aligned tracked path")
		("fn_filtered_out", po::value<std::string>(), "filename to save filtered reference path")
		("fn_error_out", po::value<std::string>(), "filename to save error over time file")
		("fn_final_error_out", po::value<std::string>(), "filename to save final error file")
		("scale", po::value<float>(), "tilt direction in rad")
		("alpha", po::value<float>(), "tilt angle in rad")
		("phi", po::value<float>(), "tilt direction in rad")
		("gui", po::value<int>(), "show gui")
		("run", po::value<int>(), "run optimization immediately, then quit")

	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	QApplication a(argc, argv);
    MainWindow w;

    if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	if(vm.count("fn_ref")) {
		w.path_optimization_->load_path(vm["fn_ref"].as<std::string>(), reference);
	}

	if(vm.count("fn_track")) {
		w.path_optimization_->load_path(vm["fn_track"].as<std::string>(), tracked);
	}

	if(vm.count("fn_aligned_out")) {
		w.path_optimization_->set_fn_aligned(vm["fn_aligned_out"].as<std::string>());
	}

	if(vm.count("fn_filtered_out")) {
		w.path_optimization_->set_fn_filtered(vm["fn_filtered_out"].as<std::string>());
	}

	if(vm.count("fn_error_out")) {
		w.path_optimization_->set_fn_error(vm["fn_error_out"].as<std::string>());
	}

	if(vm.count("fn_final_error_out")) {
		w.path_optimization_->set_fn_final_error(vm["fn_final_error_out"].as<std::string>());
	}

	w.path_optimization_->reset_optimization_params();
    w.path_optimization_->reset_optimization_delta_params();
	auto optim_params = w.path_optimization_->get_optimization_parameters();

	if(vm.count("scale")) {
		optim_params.scale = vm["scale"].as<float>();
	}    
	
	if(vm.count("alpha")) {
		optim_params.alpha = vm["alpha"].as<float>();
	}    
	
	if(vm.count("phi")) {
		optim_params.phi = vm["phi"].as<float>();
	}    

	int show_gui = 1;
	if(vm.count("gui")) {
		 show_gui = vm["gui"].as<int>();
	}
	if(show_gui != 0)
	{
		w.show();
	}

	if(vm.count("run"))
	{
		int do_run = vm["run"].as<int>();
		if(do_run == 1)
		{	
			w.path_optimization_->set_optimization_parameters(optim_params);
			w.path_optimization_->calculate_optimized();
			w.path_optimization_->run_optimization(0.5, 100, true, false); //default rate, default steps... get from program options
																//second false = no local -> GLOBAL ONLY
			return(0);
		}
			
	}

	w.path_optimization_->set_optimization_parameters(optim_params);	
	w.draw();

    return a.exec();
}
