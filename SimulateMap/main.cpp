/*
 * main.cpp
 *
 *  Created on: Jul 26, 2012
 *      Author: david
 */

#include <Edvs/Tools/Path.hpp>
#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tools/EventSimulation.hpp>
#include <boost/program_options.hpp>
#include <string>

int main(int argc, char** argv)
{
	std::string p_dir_map;
	unsigned int p_size;
	unsigned int p_seed;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("dir", po::value<std::string>(&p_dir_map)->required(), "path to result map directory")
		("size", po::value<unsigned int>(&p_size)->default_value(1000), "size of map")
		("seed", po::value<unsigned int>(&p_seed)->default_value(0), "seed for random number generator")
	;

	po::variables_map vm;

	try {
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
	}
	catch(...) {
		std::cout << desc << std::endl;
		return 1;
	}

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	Edvs::GridMap::RandomMapParameters params;

	boost::shared_ptr<Edvs::GridMap> map = Edvs::GridMap::CreateRandomMap(p_size, params,  p_seed);
	map->setMapHeight(2.59f);
	map->setPixelSize(0.002f);
	Edvs::GridMap::SaveConfig(p_dir_map + "/map.xml", map);

	return 1;

}
