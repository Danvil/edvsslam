function mmConv_9(fileName)
%% parameters

streams = 1;
fileSuffix='.dvs';
tolerated_diff = 5 * 1000000;

PlotStyle{1} = 'b.';
PlotStyle{2} = 'r.';
PlotStyle{3} = 'g.';
PlotStyle{4} = 'y.';
PlotStyle{5} = 'c.';
PlotStyle{6} = 'k.';

%% open file

f=fopen([fileName fileSuffix], 'rb');        % open file
d=fread(f, inf, '*uint8');                   % read all data
fclose(f);

l=size(d,1);                                 % determine exact file length

%% translate binary data into "events"

% upper bound estimate of number of events
estUpperEventCount=ceil(l/7)    %#ok<*NOPRT>

% pre-allocate memory
eventS = zeros(estUpperEventCount,1)-1;          
eventX = zeros(estUpperEventCount,1);
eventY = zeros(estUpperEventCount,1);
eventP = zeros(estUpperEventCount,1);
eventT = zeros(estUpperEventCount,1);

n=1;                            % index into binary data
c=0;                            % counter of events extracted
currentSensor=1;
while n<l                       % iterate over binary data
    if (d(n) < 10)              % special char, indicating data block from new sensor
        currentSensor=d(n);
        n=n+1;
    end
    
    c=c+1;                      % new event, remember data

    eventS(c) = currentSensor;
    eventX(c) = d(n)-32;          % x-position of event
    eventY(c) = d(n+1)-32;        % y-position of event
    eventP(c) = (bitand((d(n+2)-32), 64) == 64); % polarity of event
    eventT(c) = 128*128*128*double(bitand((d(n+2)-32), 15)) + 128*128*double(d(n+3)-32) + 128*double(d(n+4)-32) + double(d(n+5)-32);
    
    n = n+6;                      % advance counter by 5 (bytes read)

    if (d(n) ~= 10)
        disp(['sync error at byte position ' num2str(n)]);

        % discard previous event
        eventS(c) = -1;
        eventX(c) = 0;
        eventY(c) = 0;
        eventP(c) = 0;
        eventT(c) = 0;
        c=c-1;
        while (d(n) ~= 10)
            n=n+1;
        end
    end

    n=n+1;                      % advance to next block start
end

%% split all events into six streams

figure(1);
clf;

plot_a = subplot(3,1,1);
hold on;
title 'time difference between successive events'
plot_b = subplot(3,1,2);
hold on;
title 'original timestamps of events'
plot_c = subplot(3,1,3);
hold on;
title 'continuous timestamp of events'

RealEventCount = zeros(1, streams);
ElapsedTimeS = zeros(1, streams);
ActiveTime = zeros(1, streams);

for s = 1:(streams)
    eventXS = eventX(eventS==(s-1));
    eventYS = eventY(eventS==(s-1));
    eventPS = eventP(eventS==(s-1));
    eventTS = eventT(eventS==(s-1));
    
    % fix timestamp
    timeOffset = 0;
    eventTSC = eventTS;
    for n=2:size(eventTS,1)
        if (eventTS(n) < eventTS(n-1) - tolerated_diff)
            timeOffset = timeOffset + 32*1024*1024;
        end
        eventTSC(n) = eventTS(n)+timeOffset;
    end
    TimeDiff = eventTSC(2:end) - eventTSC(1:end-1);    
    
    % plot data
    plot(plot_a, TimeDiff, PlotStyle{s})
    plot(plot_b, eventTS, PlotStyle{s})
    plot(plot_c, eventTSC, PlotStyle{s})
    
    % get xcharacteristics of data stream
    RealEventCount(s) = size(eventTS, 1);
    ElapsedTimeS(s) = max(eventTSC)/1000000;
    ActiveTime(s) = (max(eventTSC) - min(eventTSC))/1000000;
    
    % save data
    fw = fopen([fileName '-' num2str(s) fileSuffix], 'w');
    % normal version to save data; should be used whenever possible
    e = [eventXS, eventYS, eventPS, eventTSC];    
    fprintf(fw, '%3d %3d %1d %10d\n', e');
    % VERY slow; use only if not enough RAM for  "e = [eventXS, eventYS, eventPS, eventTSC];"
%     for k = 1:size(eventXS, 1)
%         fprintf(fw, '%3d %3d %1d %10d\n', eventXS(k), eventYS(k), eventPS(k), eventTSC(k)');
%     end
    fclose(fw);    
    
end

linkaxes([plot_a plot_b plot_c], 'x');

%% show characteristics of data
RealEventCount  %#ok<NOPRT>
ElapsedTimeS    %#ok<NOPRT>
ActiveTime      %#ok<NOPRT>
