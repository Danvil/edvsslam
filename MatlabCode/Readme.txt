These are several matlab scripts and functions used to capture, process and visualize data of the eDVs128 sensor.

##########################################################################

"calibBarrel.m"

This function allows to find the parameters used to correct the barrel distortion of the eDVS data.
It will display the distorted data and the corrected data next to each other and play them in an endless loop.
It is possible to change the parameters with sliders during the runtime and therefore search for the best parameters to correct the distortion.

##########################################################################

"AllSteps.m"

This script utilizes the "CorrectCameraDistortion.m" function to correct the camera distortion for a given data set and save the new data set as -file and txt-file.


##########################################################################

"CorrectCameraDistortion.m"

This function corrects the barrel distortion of a given data set and save the new data set as -file and txt-file. 

##########################################################################

"mmConv_9.m"

This function reads the data file recorded by the java program responsible for recording the eDVS data.
It then transforms the data file into a format required by the other matlab functions.
It can also split up the data into several files for each camera, if several cameras have been recorded at once.

##########################################################################

"Visdistorted.m"

Visualizes eDVs data which is still distorted, therefore the barrel distortion has not been corrected. 

##########################################################################

"VisUndistorted.m"

Vislualizes eDVS data with corrected barrel distortion.

##########################################################################

