function calibBarrel(Data)

% use only distorted data as input for this script !!

if nargin < 1
    disp('missing parameter for VisSingleUndistCam(Data)')
    return
end

polarity = 1;
maxRes = 250;

Events = Data;

% colormap to show discrete colours :: red / yellow / black / blue / green  <->  -5 / -2 / 0 / 2 / 5
myCMap = [ 1 , 0 , 0 ;
    1 , 1 , 0 ;
    0 , 0 , 0 ;
    0 , 0 , 1 ;
    0 , 1 , 0 ];

Vis = zeros(maxRes);
VisO = zeros(128);

global quitProg
global stepByStep
quitProg = 0;
stepByStep = 0;
Fighandle = figure(1);
clf(Fighandle);
set(Fighandle,'Position',[50,50,1800,800], 'Colormap',myCMap, 'WindowKeyPressFcn',@KeyContrProg);
% set(Fighandle,'Colormap',myCMap, 'WindowKeyPressFcn',@KeyContrProg);

%% GUI for parameters

global recalcEvents;
recalcEvents = 0;

global k1;
global k2;
global k3;
% k1 = 0.0016;
% k2 = 0.000075;
% k3 = 0.0000002;
% k1 = 0.0017;
% k2 = 0.000075;
% k3 = 0.000000;
k1 = 0.0013;
k2 = 0.000017;
k3 = 0.000000;

figure(2);
clf

global hSliderK1;
global hSliderK2;
global hSliderK3;

global hMaxImpactK1;
global hMaxImpactK2;
global hMaxImpactK3;

hSliderK1 = uicontrol('style','text','Position',[20,20,200,25],'String',['K1: ' num2str(k1, '%.10f')]);
hMaxImpactK1 = uicontrol('style','text','Position',[600,20,200,25],'String',['max impact K1 : ' num2str(sqrt(2*63.5^2)*k1*63.5, '%.3g')]);
uicontrol('style','slider','Min',0,'Max',0.01,'SliderStep',[1/100,1/10],'Value',k1,'Position',[250,23,300,17],'Callback',{@SliderK1});

hSliderK2 = uicontrol('style','text','Position',[20,60,200,25],'String',['K2: ' num2str(k2, '%.10f')]);
hMaxImpactK2 = uicontrol('style','text','Position',[600,60,200,25],'String',['max impact K2 : ' num2str(sqrt(2*63.5^2)^2*k2*63.5, '%.3g')]);
uicontrol('style','slider','Min',0,'Max',0.0001,'SliderStep',[1/100,1/10],'Value',k2,'Position',[250,63,300,17],'Callback',{@SliderK2});

hSliderK3 = uicontrol('style','text','Position',[20,100,200,25],'String',['K3: ' num2str(k3, '%.10f')]);
hMaxImpactK3 = uicontrol('style','text','Position',[600,100,200,25],'String',['max impact K3 : ' num2str(sqrt(2*63.5^2)^3*k3*63.5, '%.3g')]);
uicontrol('style','slider','Min',0,'Max',0.00001,'SliderStep',[1/500,1/10],'Value',k3,'Position',[250,103,300,17],'Callback',{@SliderK3});

Events = Rectify(Data);

%% iterate over events and visualize them
tic
vis_delta = 0.05 * 1000000;
next_vis = Data(1,4) + vis_delta;

last_shown_event = 0;

while 1
    if quitProg == 1
        disp('program aborted')
        return
    end
    
    for k = 1:1:size(Events, 1)
        
        if quitProg == 1
            disp('program aborted')
            return
        end
        
        if recalcEvents == 1
            recalcEvents = 0;
            Events = Rectify(Data);
        end
        
        while toc*1000000 < Events(k, 4)                                    % limit maximum processing rate to real speed
        end
        
        if Data(k,4) > next_vis
            next_vis = next_vis + vis_delta;
            
            Vis = Vis * 0;
            VisO = VisO * 0;
            figure(1)
            subplot(1,2,1)
            
            c = last_shown_event+1;
            while c <= k
                x = Data(c,1) + 1;
                y = Data(c,2) + 1;
                if polarity < 1
                    Vis(y, x) = 5;
                else
                    VisO(y, x) = Data(c,3)*10 - 5;
                end
                c = c + 1;
            end
            
            imagesc(VisO);
            caxis([-5 5]);
            %         colormap(myCMap);
            title([num2str(Events(k,4)/1000000) 's (Event-Time)']);
            axis equal
            drawnow;            
            
            
            subplot(1,2,2)
            
            %         ImagePointer = ((Events(last_shown_event+1:k, 2)-1)*128 + Events(last_shown_event+1:k, 1));
            c = last_shown_event+1;
            while c <= k
                x = ceil(Events(c,1)) + floor((maxRes-128)/2);
                y = ceil(Events(c,2)) + floor((maxRes-128)/2);
                if polarity < 1
                    Vis(y, x) = 5;
                else
                    Vis(y, x) = Events(c,3)*10 - 5;
                end
                c = c + 1;
            end
            
            % display image
            imagesc(Vis);
            caxis([-5 5]);
            %         colormap(myCMap);
            title([num2str(Events(k,4)/1000000) 's (Event-Time)']);
            axis equal
            drawnow;
            
            last_shown_event = k;
            
            if stepByStep == 1                                                      % wait for key press
                while 1
                    butt = waitforbuttonpress;
                    if butt == 1
                        if get(Fighandle, 'CurrentCharacter') == 'p'
                            stepByStep = 0;
                        elseif get(Fighandle, 'CurrentCharacter') == 'q'
                            disp('program aborted')
                            return
                        end
                        break
                    end
                end
            end
            
        end
        
    end
   
% reset parameters for next run
tic
next_vis = Data(1,4) + vis_delta;
last_shown_event = 0;    
    
end

end

%% functions

function Events = Rectify(Events)
global k1;
global k2;
global k3;

for k = 1:size(Events, 1)
    x = Events(k, 1);
    y = Events(k, 2);
    r = sqrt( (x-63.5)^2 + (y-63.5)^2 );
    tempX = (x-63.5)*(1 + k1*r + k2*r^2 + k3*r^3) + 63.5;
    tempY = (y-63.5)*(1 + k1*r + k2*r^2 + k3*r^3) + 63.5;
    Events(k, 1) = tempX;
    Events(k, 2) = tempY;
end

end

%% control functions

function KeyContrProg(hObj,eventData) %#ok<INUSL>
global quitProg
global stepByStep

if eventData.Character == 'p'
    if stepByStep == 0
        stepByStep = 1;
    else
        stepByStep = 0;
    end
end

if eventData.Character == 'q'
    quitProg = 1;
end

end

function SliderK1(hObj,event)
global hSliderK1;
global hMaxImpactK1;
global k1;

global recalcEvents;
recalcEvents = 1;

step = get(hObj,'SliderStep')*get(hObj,'Max');

k1 = round(get(hObj,'Value')/step(1))*step(1);
set(hObj,'Value',k1);
set(hSliderK1,'String',['K1: ' num2str(k1, '%.10f')]);
set(hMaxImpactK1,'String',['max impact K1 : ' num2str(sqrt(2*63.5^2)*k1*63.5, '%.3g')]);
end

function SliderK2(hObj,event)
global hSliderK2;
global hMaxImpactK2;
global k2;

global recalcEvents;
recalcEvents = 1;

step = get(hObj,'SliderStep')*get(hObj,'Max');

k2 = round(get(hObj,'Value')/step(1))*step(1);
set(hObj,'Value',k2);
set(hSliderK2,'String',['K2: ' num2str(k2, '%.10f')]);
set(hMaxImpactK2,'String',['max impact K2 : ' num2str(sqrt(2*63.5^2)^2*k2*63.5, '%.3g')]);
end

function SliderK3(hObj,event)
global hSliderK3;
global hMaxImpactK3;
global k3;

global recalcEvents;
recalcEvents = 1;

step = get(hObj,'SliderStep')*get(hObj,'Max');

k3 = round(get(hObj,'Value')/step(1))*step(1);
set(hObj,'Value',k3);
set(hSliderK3,'String',['K3: ' num2str(k3, '%.10f')]);
set(hMaxImpactK3,'String',['max impact K3 : ' num2str(sqrt(2*63.5^2)^3*k3*63.5, '%.3g')]);
end


