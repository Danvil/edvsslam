function VisUndistorted(Data)
%% check input
if nargin < 2
    polarity = 1;
end
if nargin < 1
    disp('missing parameter for VisPanorama_2(Panorama, polarity, X_Res, Y_Res)')
    return
end

%% parameters

% colormap to show discrete colours :: red / yellow / black / blue / green  <->  -5 / -2 / 0 / 2 / 5
myCMap = [ 1 , 0 , 0 ;                                                      
           1 , 1 , 0 ;
           0 , 0 , 0 ;
           0 , 0 , 1 ;
           0 , 1 , 0 ];
      
expanding = 100;       
       
Vis = zeros(128 + 2*expanding);

global quitProg
global stepByStep
quitProg = 0;
stepByStep = 0;
Fighandle = figure(1);
clf(Fighandle);
set(Fighandle,'Colormap', myCMap, 'WindowKeyPressFcn', @KeyContrProg);

%% iterate over Panorama data and visualize it
vis_delta = 0.075*1000000;      % time per frame
next_vis = Data(1,4) + vis_delta;
last_shown_event = 0;

tic

for k = 1:1:size(Data, 1)
    
    % program control
    if quitProg == 1
        disp('program aborted')
        return
    end    
    
    % limit maximum processing rate to real speed
    while toc * 1000000 < Data(k, 4)
    end
    
    if Data(k, 4) > next_vis
        next_vis = next_vis + vis_delta;
        Vis = Vis * 0;        

        % write events into image
        c = last_shown_event+1;
        while c <= k
            if polarity < 1
                Vis( floor(Data(c,2))+expanding+1, floor(Data(c,1))+expanding+1) = 5;
            else
                Vis( floor(Data(c,2))+expanding+1, floor(Data(c,1))+expanding+1) = Data(c,3)*10 - 5;
            end
            c = c + 1;
        end
        
        % display image
        imagesc(Vis);
        caxis([-5 5]);
        title([num2str(Data(k,4)/1000000) 's (Event-Time)']);
        axis equal
        drawnow;
        last_shown_event = k;

        % wait for key press (program control)
        if stepByStep == 1                                                      
            while 1
                butt = waitforbuttonpress;
                if butt == 1
                    if get(Fighandle, 'CurrentCharacter') == 'p'
                        stepByStep = 0;
                    elseif get(Fighandle, 'CurrentCharacter') == 'q'
                        disp('program aborted')
                        return
                    end
                    break
                end
            end
        end
 
    end
    
end

end

function KeyContrProg(hObj,eventData) %#ok<INUSL>
global quitProg
global stepByStep

if eventData.Character == 'p'
    if stepByStep == 0
        stepByStep = 1;
    else
        stepByStep = 0;
    end
end

if eventData.Character == 'q'
    quitProg = 1;
end

end


