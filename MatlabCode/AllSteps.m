%% parameters
input_data = '01.06-Slalom_2';
data_type = 'dvs';
save_name = '01.06-Slalom_2';
files = 1;

k0 = 0.0;
k1 = 0.0013;
k2 = 0.000016;
k3 = 0.000000;

% Cam data : [x, y, polarity, timestamp]

%% Correct barrel distortion
for k = 1:files
    Data = importdata([input_data '-' num2str(k) '.dvs']);
%     save([ save_name '-Dist.mat' ], 'Data', '-mat');
%     save([ save_name '-Dist.txt' ], 'Data', '-ascii');
    CorrectCameraDistortion(Data, [save_name '-Undist'], k0, k1, k2, k3)
end
disp 'corrected distortion'

clearvars