function CorrectCameraDistortion(UndistData, save_name, k0, k1, k2, k3)
%% check input parameters
if nargin < 2
    disp 'missing parameter for CorrectCameraDistortion(UndistData, save_name, k0, k1, k2, k3)'
    return
end

%% correct the barrel distortion
for k = 1:size(UndistData, 1)
    x = UndistData(k, 1);
    y = UndistData(k, 2);
    r = sqrt( (x-63.5)^2 + (y-63.5)^2 );
    UndistData(k, 1) = (x-63.5)*(1 + k1*r + k2*r^2 + k3*r^3) + 63.5;
    UndistData(k, 2) = (y-63.5)*(1 + k1*r + k2*r^2 + k3*r^3) + 63.5;
end

%% correct keystone distortion
% for k = 1:size(UndistData, 1)
%     x = UndistData(k, 1);
%     y = UndistData(k, 2);
%     UndistData(k, 1) = (x - 63.5)*(1 + k0*(-(y - 63.5))) + 63.5;
% end

%% save the corrected data as mat and txt file
save([ save_name '.mat' ],'UndistData','-mat');
save([ save_name '.txt' ],'UndistData','-ascii');

end
