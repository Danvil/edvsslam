/*
 * main.cpp
 *
 *  Created on: Mar 29, 2012
 *      Author: martin
 */

#include <stdio.h>
#include <iostream>
#include <boost/lexical_cast.hpp>

using std::cout;
using std::endl;
using std::string;

int main(void)
{
	// file names without extension
	const string file_trajec_eb = "trajectory_error_event_based";
	const string file_trajec_fi = "trajectory_error_fix_int";
	const string file_cert_eb = "certainty_event_based";
	const string file_cert_fi = "certainty_fix_int";

	const unsigned int cI = 2;

	string iteration;
	string old_name;
	string new_name;

	for(unsigned int i=0; i<cI ;i++){
		system("./../EdvsTracking/EdvsTracking");

		iteration = boost::lexical_cast<string>( i+1 );

		// rename files
		old_name = file_trajec_eb + ".txt";
		new_name = file_trajec_eb + "_" + iteration + ".txt";
		rename(old_name.c_str(), new_name.c_str());

		old_name = file_trajec_fi + ".txt";
		new_name = file_trajec_fi + "_" + iteration + ".txt";
		rename(old_name.c_str(), new_name.c_str());

		old_name = file_cert_eb + ".txt";
		new_name = file_cert_eb + "_" + iteration + ".txt";
		rename(old_name.c_str(), new_name.c_str());

		old_name = file_cert_fi + ".txt";
		new_name = file_cert_fi + "_" + iteration + ".txt";
		rename(old_name.c_str(), new_name.c_str());

		cout << "finished iteration " << iteration << endl;
	}

	return 0;
}

