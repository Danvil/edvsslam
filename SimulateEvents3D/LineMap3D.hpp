#ifndef INCLUDED_EDVS_TOOLS_MAPS_LINEMAP3D_HPP
#define INCLUDED_EDVS_TOOLS_MAPS_LINEMAP3D_HPP

#include <Edvs/Tools/Maps/Map.hpp>
#include <Edvs/Tools/PixelViewCone.hpp>
#include <Edvs/Tools/SensorModel.hpp>
#include <Eigen/Dense>
#include <vector>
#include <iostream>

namespace Edvs {

class LineMap3D
: public Map
{
public:
	LineMap3D() {}
	virtual ~LineMap3D() {}

	/** Gets a short info string describing the map */
	std::string getInfoString() const  { return "LineMap3D"; }

	/** Gets event probability for a given pixel view cone */
	float evaluate(const PixelViewCone& pvc) {
		std::vector<float> dist(lines_.size());
		std::transform(lines_.begin(), lines_.end(), dist.begin(),
			[pvc,this](const LineSegment3D& s) {
				float d = isectRaySeg(pvc.ray_start, pvc.ray_dir, s.a, s.b);
				// FIXME include opening angle
				return d;
			});
		auto it_min = std::min_element(dist.begin(), dist.end());
		if(it_min != dist.end() && *it_min < 0.05f) {
			return 1.0f;
		}
		else {
			return 0.0f;
		}
	}

	/** Add a set of weighted pixel view cone events to the map */
	void store(const std::vector<PixelViewCone>& v_pxc, unsigned int num_event) {
		throw 0;
	}

	/** Add new Visibility Counts to the Visibility Map*/	
	void resample(const ZAlignedPose<double>& state) {
		throw 0;
	}

	/** Deletes map outside of sphere */
	void reduce(const Eigen::Vector3f& pos, float radius) {
		throw 0;
	}

public:
	typedef Eigen::Vector3f Vec3f;
	typedef Eigen::Vector2f Vec2f;

	struct LineSegment3D {
		Vec3f a, b;
	};

	static float cpaLinePoint(const Vec3f& a, const Vec3f& u, const Vec3f& b) {
		return (a-b).dot(u) / u.dot(u);
	}

	static Vec2f cpaLineLine(const Vec3f& a, const Vec3f& u, const Vec3f& b, const Vec3f& v) {
		float uu = u.dot(u);
		float vv = v.dot(v);
		float uv = u.dot(v);
		Vec3f ab = a - b;
		float abu = ab.dot(u);
		float abv = ab.dot(v);
		float D = uv*uv - uu*vv;
		if(D == 0.0f) {
			return {-abu/uu, 0.0f};
		}
		else {
			abu /= D;
			abv /= D;
			return {-abv*uv+abu*vv, -abv*uu+abu*uv};
		}
	}

	static Vec2f cpaLineSeg(const Vec3f& a, const Vec3f& u, const Vec3f& s1, const Vec3f& s2) {
		Vec2f q = cpaLineLine(a, u, s1, s2-s1);
		if(q[1] < 0.0f) {
			return {cpaLinePoint(a, u, s1), 0.0f};
		}
		if(q[1] > 1.0f) {
			return {cpaLinePoint(a, u, s2), 1.0f};
		}
		return q;
	}

	static float isectRaySeg(const Vec3f& a, const Vec3f& u, const Vec3f& s1, const Vec3f& s2) {
		Vec2f cpa = cpaLineSeg(a, u, s1, s2);
		if(cpa[0] <= 0.0f) {
			return 1000000000.0f;
		}
		return (a + cpa[0]*u - (s1 + cpa[1]*(s2-s1))).squaredNorm();
	}

public:
	std::vector<LineSegment3D> lines_;

};

}

#endif
