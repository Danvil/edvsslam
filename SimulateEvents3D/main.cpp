/*
 * main.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: david
 */

#include <Edvs/Tools/Path.hpp>
#include "LineMap3D.hpp"
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tools/EventSimulation.hpp>
#include <Edvs/EventIO.hpp>
#include <boost/program_options.hpp>
#include <boost/timer/timer.hpp>
#include <string>

int main(int argc, char** argv)
{
	bool p_verbose = true;
	std::string p_fn_path = "";
	std::string p_map = "";
	double p_sensor_opening_angle = 53.0;
	int64_t p_sim_dt = 100.0; // 0.1 ms
	std::string p_fn_events = "";
	unsigned int p_thread_count = 1;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("fn_path", po::value<std::string>(&p_fn_path), "filename for input path file")
		("map", po::value<std::string>(&p_map), "map type")
		("fov", po::value<double>(&p_sensor_opening_angle)->default_value(p_sensor_opening_angle), "retina sensor opening angle")
		("dt", po::value<int64_t>(&p_sim_dt)->default_value(p_sim_dt), "simulation delta time in micro seconds")
		("fn_events", po::value<std::string>(&p_fn_events), "filename for output events file")
		("thread_count", po::value(&p_thread_count)->default_value(p_thread_count), "number of threads")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help") || !vm.count("fn_path") || !vm.count("map") || !vm.count("fn_events")) {
		std::cout << desc << std::endl;
		return 1;
	}

	Edvs::EventSimulation sim;
	sim.thread_count = p_thread_count;
	sim.objective.sensor_model_.setOpeningAngleDeg(p_sensor_opening_angle);

	if(p_verbose) {
		std::cout << "Loading path from '" << p_fn_path << "'..." << std::endl;
	}
	sim.path = Edvs::LoadPath<double,2,false>(p_fn_path);

	if(p_verbose) {
		std::cout << "Creating map" << std::endl;
	}
	boost::shared_ptr<Edvs::LineMap3D> lm3(new Edvs::LineMap3D());
	sim.objective.map_ = lm3;

	// create map
	
	{
		typedef Edvs::LineMap3D::LineSegment3D Seg3;
		typedef Edvs::LineMap3D::Vec3f Vec3;
		// ceiling cross
		const float R = 2.0f;
		const float Z = 3.0f;
		for(float q=-R; q<=R; q+=0.5f) {
			lm3->lines_.push_back(Seg3{ Vec3{q,-R,Z}, Vec3{q,+R,Z} });
			lm3->lines_.push_back(Seg3{ Vec3{-R,q,Z}, Vec3{+R,q,Z} });
		}
	}

	// create map

	if(p_verbose) {
		std::cout << "Generating events..." << std::endl;
	}
	std::vector<Edvs::Event> events;
	{
		boost::timer::auto_cpu_timer timer;
		events = sim.SimulateAll(p_sim_dt);
		std::cout << "Generated " << events.size() << " events" << std::endl;
	}
	if(p_verbose) {
		std::cout << "Saving events to file '" << p_fn_events << "'..." << std::endl;
	}
	Edvs::SaveEvents(p_fn_events, events);

	if(p_verbose) {
		std::cout << "Finished." << std::endl;
	}

	return 1;

}
