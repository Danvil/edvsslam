Edvs Tracking Tools

If not mentioned otherwise all code Copyright 2012 by David Weikersdorfer, Technische Universitaet Muenchen

Author: David Weikersdorfer
Email: weikersd@in.tum.de

last edited: 06/03/2012


~~~~~ Description ~~~~~

This is a compilation of tools for event-based tracking with the EDVS retina sensor.


~~~~~ Required dependencies ~~~~~

Boost C++ Libraries
Required version: 1.46.1 or newer
http://www.boost.org/
sudo apt-get install libboost-all-dev

Eigen C++ template library for linear algebra
Required version: 3.0 or newer
http://eigen.tuxfamily.org
sudo apt-get install libeigen3-dev

QT UI framework
Requied version: 4.x
sudo apt-get install libqt4-dev

QWT Qt widgets library for technical applications
sudo apt-get install libqwt-dev

Q0 template global optimization and tracking toolbox.
http://www.github.com/Danvil/q0


~~~~~ Supported compilers ~~~~

GCC 4.6.3


~~~~~ Instructions ~~~~

mkdir build
cd build
ccmake ..
make
