/*
 * main.cpp
 *
 *  Created on: Jul 09, 2012
 *      Author: david
 */

#include <Edvs/Tools/Path.hpp>
#include <Edvs/Tools/Maps/GridMap.hpp>
#include <Edvs/Tools/ObjectiveFunction.hpp>
#include <Edvs/Tools/EventSimulation.hpp>
#include <Edvs/EventIO.hpp>
#include <boost/program_options.hpp>
#include <boost/timer.hpp>
#include <string>

int main(int argc, char** argv)
{
	bool p_verbose = true;
	std::string p_fn_path = "";
	std::string p_fn_map = "";
	double p_sensor_opening_angle = 53.0;
	int64_t p_sim_dt = 100.0; // 0.1 ms
	std::string p_fn_events = "";
	unsigned int p_thread_count = 1;

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("fn_path", po::value<std::string>(&p_fn_path), "filename for input path file")
		("fn_map", po::value<std::string>(&p_fn_map), "filename for map config file")
		("fov", po::value<double>(&p_sensor_opening_angle)->default_value(p_sensor_opening_angle), "retina sensor opening angle")
		("dt", po::value<int64_t>(&p_sim_dt)->default_value(p_sim_dt), "simulation delta time in micro seconds")
		("fn_events", po::value<std::string>(&p_fn_events), "filename for output events file")
		("thread_count", po::value(&p_thread_count)->default_value(p_thread_count), "number of threads")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help") || !vm.count("fn_path") || !vm.count("fn_map") || !vm.count("fn_events")) {
		std::cout << desc << std::endl;
		return 1;
	}

	Edvs::EventSimulation sim;
	sim.thread_count = p_thread_count;
	sim.objective.sensor_model_.setOpeningAngleDeg(p_sensor_opening_angle);

	if(p_verbose) {
		std::cout << "Loading path from '" << p_fn_path << "'..." << std::endl;
	}
	sim.path = Edvs::LoadPath<double,2,false>(p_fn_path);

	if(p_verbose) {
		std::cout << "Loading map config from '" << p_fn_map << "'..." << std::endl;
	}
	sim.objective.map_ = Edvs::GridMap::LoadConfig(p_fn_map);
	//sim.objective.map_ = boost::dynamic_pointer_cast<Edvs::GridMap>(sim.objective.map_);
	boost::shared_ptr<Edvs::GridMap> grid_map = boost::dynamic_pointer_cast<Edvs::GridMap>(sim.objective.map_);
	grid_map->setRawMap();
	//sim.objective.map_ = grid_map;
	//sim.objective.map_->setRawMap();

	if(p_verbose) {
		std::cout << "Generating events..." << std::endl;
	}
	boost::timer timer;
	std::vector<Edvs::Event> events = sim.SimulateAll(p_sim_dt);
	std::cout << "Generated " << events.size() << " in " << timer.elapsed() << "s" << std::endl;

	if(p_verbose) {
		std::cout << "Saving events to file '" << p_fn_events << "'..." << std::endl;
	}
	Edvs::SaveEvents(p_fn_events, events);

	if(p_verbose) {
		std::cout << "Finished." << std::endl;
	}

	return 1;

}
